# Bitazza Web Exchange

## Requirements
- git
- node.js 8 or higher

## Clone 
```
git clone https://gitlab.com/bitazza/bitazzacode.git
```

## Install dependencies
```
npm install
```

## Deployment Checklist

- ✅ Make sure `.env` file have AP Gateway, default url and correct other variables.
- ✅ Configuration located in `public/local/config.js`
- ✅ Running `npm run build` to make production build to `build` folder.
- ✅ Deploy `build` folder as static
- ✅ Need to make sure web server have fall back to `index.html` for SPA routing and avoid to see 404 page not found.

## Example NGINX

```
server {
  listen 80;

  root /home/bitazza/sites/bitazzacode/build;
  index index.html;

  # fallback all endpoint to index.html
  location / {
    try_files $uri $uri/ /index.html;
  }

  # direct render out .html file
  location ~ .+(?=\.html)$ {
    try_files $uri $uri/ /$1;
  }
}
```
## Run server
- development
```
npm start
```
- production
```
1. npm run build
2. ...
```

## Translations
```
1. npm run extract
2. update translation on locales/**.po file
3. npm run import-po
```
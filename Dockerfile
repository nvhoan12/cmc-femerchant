FROM node:8-alpine3.10

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY .npmrc ./
COPY package*.json ./
RUN npm install


# From current dir to workdir in container.
COPY . .
RUN npm run build

# 
RUN chown -R node:node /usr/src/app
USER node

ENV PORT=8000

# Open port 8000 in container
EXPOSE 8000
# Run server
CMD [ "npm", "run", "start:prod"]

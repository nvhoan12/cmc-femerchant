import { UPDATE_BTZ_LEVEL } from '../actions/btzLevelActions';

const initialState = {
  btzID: '',
  rank: '-',
  btzToClaim: '-',
  lastUpdatedClaim: '-',
  referral: '-',
  accumulation: '-',
  btzDiscountBack: '-',
  rewards: '-',
  rewardKYC: '-',
  showEA: false,
  ambassador: 'grey',
  whale: 'grey'
};

export default function btzLevel(state = initialState, action) {
  switch (action.type) {
    case UPDATE_BTZ_LEVEL:
      return action.payload;
    default:
      return state;
  }
}

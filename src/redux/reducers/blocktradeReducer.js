import {
  REQUEST_BLOCKTRADE_ORDERS,
  RECEIVED_BLOCKTRADE_ORDERS
} from '../actions/rfqActions';

const initialState = {
  orders: [],
  isFetching: false,
  error: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case REQUEST_BLOCKTRADE_ORDERS:
      return { ...state, isFetching: true };
    case RECEIVED_BLOCKTRADE_ORDERS:
      return { ...state, orders: action.payload, isFetching: false };
    default: {
      return state;
    }
  }
}

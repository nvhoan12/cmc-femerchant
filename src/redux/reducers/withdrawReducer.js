import {
  FETCH_PRODUCT_WITHDRAW_TEMPLATES,
  FETCH_PRODUCT_WITHDRAW_TEMPLATE,
  RECEIVE_WITHDRAW_TEMPLATES,
  RECEIVE_WITHDRAW_TEMPLATES_ERROR,
  RECEIVE_WITHDRAW_TEMPLATE,
  RECEIVE_WITHDRAW_TEMPLATE_ERROR,
  RECEIVE_WITHDRAW_TICKET,
  RECEIVE_WITHDRAW_TICKETS,
  RECEIVE_WITHDRAW_FEE,
  RECEIVE_WITHDRAW_WORKFLOW_INFO,
  ADD_WITHDRAWAL_TICKET
} from 'apex-web/lib/redux/actions/withdrawActions';
import { CLOSE_SIDE_PANE } from 'apex-web/lib/redux/actions/sidePaneActions';
import { distinct } from 'apex-web/lib/helpers/arrayHelper';

const initialState = {
  templateInfo: {},
  template: {},
  templateTypes: [],
  error: '',
  isLoading: false,
  withdrawStatus: {},
  withdrawTickets: [],
  withdrawWorkflowInfo: [],
  withdrawFee: 0
};

export default function withdraw(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_PRODUCT_WITHDRAW_TEMPLATES:
      return {
        ...state,
        isLoading: true
      };
    case FETCH_PRODUCT_WITHDRAW_TEMPLATE:
      return {
        ...state,
        isLoading: true,
        product: payload.product,
        templateType: payload.templateType
      };
    case RECEIVE_WITHDRAW_TEMPLATES: {
      return {
        ...state,
        isLoading: false,
        templateTypes: payload,
        error: ''
      };
    }
    case RECEIVE_WITHDRAW_TEMPLATE: {
      return {
        ...state,
        isLoading: false,
        templateInfo: payload,
        template: JSON.parse(payload.Template),
        error: ''
      };
    }
    case RECEIVE_WITHDRAW_TEMPLATES_ERROR:
    case RECEIVE_WITHDRAW_TEMPLATE_ERROR: {
      return {
        ...state,
        error: payload,
        isLoading: false
      };
    }
    case RECEIVE_WITHDRAW_TICKET: {
      return {
        ...state,
        withdrawStatus: payload
      };
    }
    case RECEIVE_WITHDRAW_FEE: {
      return {
        ...state,
        TicketAmount: payload.TicketAmount,
        withdrawFee: payload.FeeAmount
      };
    }
    case RECEIVE_WITHDRAW_TICKETS: {
      // temporary while waiting ap fix at back-end
      const payloadTmpFix = payload.map(item => {
        if (item.Status === 'Failed') {
          return { ...item, Status: 'AdminProcessing' };
        }
        return item;
      });
      return {
        ...state,
        withdrawTickets: payloadTmpFix
      };
    }
    case RECEIVE_WITHDRAW_WORKFLOW_INFO: {
      return {
        ...state,
        withdrawWorkflowInfo: payload
      };
    }
    case ADD_WITHDRAWAL_TICKET: {
      return {
        ...state,
        withdrawTickets: distinct(
          [payload, ...state.withdrawTickets],
          ticket => ticket.TicketNumber
        )
      };
    }
    case CLOSE_SIDE_PANE: {
      return {
        ...initialState,
        withdrawTickets: state.withdrawTickets
      };
    }
    default:
      return state;
  }
}

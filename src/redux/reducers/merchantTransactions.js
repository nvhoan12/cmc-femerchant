import { UPDATE_MERCHANT_TRANSACTIONS } from '../actions/merchantTransactionsActions';

const initialState = [];

export default function merchantTransactions(state = initialState, action) {
  switch (action.type) {
    case UPDATE_MERCHANT_TRANSACTIONS:
      return action.payload;
    default:
      return state;
  }
}

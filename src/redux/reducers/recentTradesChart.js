import { RECENT_TRADES_CHART_FETCH } from '../actions/recentTradesChartActions';

const initialState = [];

export default function recentTradesChart(state = initialState, action) {
  switch (action.type) {
    case RECENT_TRADES_CHART_FETCH:
      return [...state, action.payload];
    default:
      return state;
  }
}

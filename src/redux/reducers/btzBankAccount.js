import { UPDATE_BTZ_BANK_ACCOUNT } from '../actions/btzBankAccountActions';

const initialState = {
  bankAccounts: [],
  kycName: {}
};

export default function btzLevel(state = initialState, action) {
  switch (action.type) {
    case UPDATE_BTZ_BANK_ACCOUNT:
      return action.payload;
    default:
      return state;
  }
}

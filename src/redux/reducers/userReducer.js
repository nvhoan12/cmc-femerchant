import uniqBy from 'lodash/uniqBy';
import { updateObjectInArray } from 'apex-web/lib/helpers/arrayHelper';
import {
  USER_INFO_FETCHED,
  USER_CONFIG_FETCHED,
  RECEIVE_USER_PERMISSIONS,
  RECEIVE_ACCOUNT,
  SET_ACCOUNT,
  SET_GOOGLE_QRCODE,
  CLEAR_GOOGLE_QRCODE
} from 'apex-web/lib/redux/actions/userActions';
import { RECEIVE_VERIFICATION_LEVEL_CONFIGS } from 'apex-web/lib/redux/actions/withdrawActions';
import {
  USER_CONFIG_UPDATED,
  UPDATE_ACCOUNT
} from 'apex-web/lib/redux/actions/kycActions';
import { RECEIVE_KYC } from 'apex-web/lib/redux/actions/kycIdentityMindActions';

const initialState = {
  userInfo: {},
  permissions: [],
  accounts: [],
  userConfig: [],
  selectedAccountId: undefined,
  kyc: { urlParams: null },
  qrCode: '',
  verificationLevelConfigs: {}
};

export default function user(state = initialState, { type, payload }) {
  switch (type) {
    case USER_INFO_FETCHED:
      return {
        ...state,
        userInfo: payload
      };
    case USER_CONFIG_FETCHED:
      return {
        ...state,
        userConfig: payload
      };
    case USER_CONFIG_UPDATED:
      return {
        ...state,
        userConfig: updateObjectInArray(state.userConfig, payload)
      };
    case RECEIVE_USER_PERMISSIONS:
      return {
        ...state,
        permissions: payload
      };
    case RECEIVE_ACCOUNT:
      // hide gl for now
      if (/gl_/g.test(payload[0].AccountName)) {
        return state;
      }
      return {
        ...state,
        accounts: uniqBy([...state.accounts, ...payload], 'AccountId')
      };
    case UPDATE_ACCOUNT:
      console.log(payload);
      return {
        ...state,
        accounts: updateObjectInArray(state.accounts, payload)
      };
    case RECEIVE_KYC:
      return {
        ...state,
        kyc: payload
      };
    case SET_ACCOUNT:
      return {
        ...state,
        selectedAccountId: payload
      };
    case SET_GOOGLE_QRCODE:
      return {
        ...state,
        qrCode: payload
      };
    case CLEAR_GOOGLE_QRCODE:
      return {
        ...state,
        qrCode: ''
      };
    case RECEIVE_VERIFICATION_LEVEL_CONFIGS:
      return {
        ...state,
        verificationLevelConfigs: payload
      };
    default: {
      return state;
    }
  }
}

import {
  UPDATE_PRICE,
  SELECT_QUOTE_PRODUCT_RFQ,
  SELECT_BASE_PRODUCT_RFQ,
  FETCH_EXCHANGE_RATES_PENDING,
  FETCH_EXCHANGE_RATES_SUCCESS,
  FETCH_EXCHANGE_RATES_ERROR,
  REQUEST_QUOTE_SUCCESS,
  REQUEST_QUOTE_PENDING,
  REQUEST_QUOTE_FAILED,
  EXECUTE_QUOTE_SUCCESS,
  EXECUTE_QUOTE_PENDING,
  EXECUTE_QUOTE_FAILED,
  REQUEST_QUOTE_TIMEOUT,
  RESET_RFQ_STATE
} from '../actions/rfqActions';

const initialState = {
  selectedBaseId: undefined,
  selectedQuoteId: undefined,
  prices: [],
  rates: {},
  quote: undefined,
  isRequestQuote: false,
  isRFQAvailable: true, // default is true before fetching to get from server.
  isFetching: false,
  error: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case UPDATE_PRICE:
      return {
        ...state,
        prices: action.payload
      };
    case RESET_RFQ_STATE:
      return {
        ...state,
        quote: undefined,
        isFetching: false,
        error: null
      };
    case SELECT_QUOTE_PRODUCT_RFQ:
      return {
        ...state,
        selectedQuoteId: action.payload
      };
    case SELECT_BASE_PRODUCT_RFQ:
      return {
        ...state,
        selectedBaseId: action.payload
      };
    case FETCH_EXCHANGE_RATES_PENDING:
      return {
        ...state,
        isFetching: true
      };
    case FETCH_EXCHANGE_RATES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        rates: action.payload.rates,
        isRFQAvailable: action.payload.isAvailable
      };

    case REQUEST_QUOTE_PENDING:
    case EXECUTE_QUOTE_PENDING:
      return {
        ...state,
        isFetching: true,
        quote: undefined
      };
    case REQUEST_QUOTE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: null,
        quote: action.payload,
        isRequestQuote: true
      };
    case EXECUTE_QUOTE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: null,
        quote: undefined // reset quote when finish execute
      };
    case REQUEST_QUOTE_FAILED:
    case EXECUTE_QUOTE_FAILED:
      return {
        ...state,
        isFetching: false,
        quote: undefined,
        error: action.error
      };
    case REQUEST_QUOTE_TIMEOUT:
      return {
        ...state,
        isRequestQuote: false
      };
    case FETCH_EXCHANGE_RATES_ERROR:
      return {
        ...state,
        isFetching: false,
        quote: undefined,
        error: action.error,
        isRFQAvailable: false,
        isRequestQuote: false
      };
    default: {
      return state;
    }
  }
}

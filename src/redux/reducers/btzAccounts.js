import {
  PUT_BTZ_ACCOUNTS,
  UPDATE_BTZ_ACCOUNTS
} from '../actions/btzAccountsActions';

const initialState = [];

export default function btzAccounts(state = initialState, action) {
  switch (action.type) {
    case PUT_BTZ_ACCOUNTS:
      return [...state, action.payload];
    case UPDATE_BTZ_ACCOUNTS:
      return state.map(account => {
        if (account.email === action.payload.email) {
          return action.payload;
        }
        return account;
      });
    default:
      return state;
  }
}

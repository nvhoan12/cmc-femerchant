import config from '../../config';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';

export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';
export const SIGNUP_REQUESTED = 'SIGNUP_REQUESTED';
export const CLEAR_SIGNUP_ERROR = 'CLEAR_SIGNUP_ERROR';

export const signup = payload => async dispatch => {
  const { email, password, language } = payload;
  let { affiliateTag } = payload;

  dispatch(signupRequested);

  const fetchPayload = {
    username: email,
    password,
    language
  };

  if (affiliateTag) {
    fetchPayload.affiliate_tag = affiliateTag;
  }

  const signupUrl = config.services.signup;

  const response = await fetch(signupUrl, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(fetchPayload)
  });

  const responseBody = await response.json();

  if (response.status === 200) {
    if (responseBody.result) {
      dispatch(signupSuccess(responseBody));
    } else {
      return responseBody.error;
    }
  } else {
    dispatch(
      showSnack({
        id: 'signupError',
        text: responseBody.error.errormsg,
        type: 'warning'
      })
    );
    return responseBody.error;
  }
};

export const signupRequested = {
  type: SIGNUP_REQUESTED,
  payload: {
    requesting: true
  }
};

export const signupSuccess = userId => {
  return {
    type: SIGNUP_SUCCESS,
    payload: {
      userId,
      requesting: false
    }
  };
};

export const signupError = payload => ({
  type: SIGNUP_ERROR,
  payload: {
    errorMsg: payload.errormsg,
    requesting: false
  }
});

export const clearSignupError = () => ({
  type: CLEAR_SIGNUP_ERROR
});

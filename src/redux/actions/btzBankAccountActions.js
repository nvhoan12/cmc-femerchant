export const FETCH_BTZ_BANK_ACCOUNT_REQ = 'FETCH_BTZ_BANK_ACCOUNT_REQ';
export const UPDATE_BTZ_BANK_ACCOUNT = 'UPDATE_BTZ_BANK_ACCOUNT';

export const fetchBTZBankAccount = () => ({
  type: FETCH_BTZ_BANK_ACCOUNT_REQ
});

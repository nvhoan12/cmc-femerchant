import { change, untouch } from 'redux-form';
import qcpService from '../../services/subscriptions/QCPIndicativePrice';
import { selectOptionByCondition } from '../selectors/rfqSelectors';

import {
  getBlocktradeHistory,
  requestQuote,
  executeQuote
} from '../../services/api/QCP';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';

export const subscribeToQCPIndicativePrice = () => (dispatch) => {
  qcpService.subscribe((updates) => {
    dispatch(updatePrice(updates));
  });
};

export const unsubscribeToQCPIndicativePrice = () => (dispatch) => {
  qcpService.stopAll();
  dispatch(stopUpdatePrice());
};

export const FETCH_EXCHANGE_RATES_PENDING = 'FETCH_EXCHANGE_RATES_PENDING';
export const FETCH_EXCHANGE_RATES_SUCCESS = 'FETCH_EXCHANGE_RATES_SUCCESS';
export const FETCH_EXCHANGE_RATES_ERROR = 'FETCH_EXCHANGE_RATES_ERROR';

export const UPDATE_PRICE = 'UPDATE_PRICE';
export const updatePrice = (payload) => ({
  type: UPDATE_PRICE,
  payload
});

export const STOP_UPDATE_PRICE = 'STOP_UPDATE_PRICE';
export const stopUpdatePrice = (payload) => ({
  type: STOP_UPDATE_PRICE,
  payload
});

export const SELECT_BASE_PRODUCT_RFQ = 'SELECT_BASE_PRODUCT_RFQ';
export const selectBaseRFQProductId = (productId) => async (
  dispatch,
  getState
) => {
  await dispatch({
    type: SELECT_BASE_PRODUCT_RFQ,
    payload: productId
  });

  const options = selectOptionByCondition(getState());

  localStorage.setItem('selectedBaseRFQProductId', productId);

  // console.log('ooptions', options);

  if (options.length) {
    dispatch(selectQuoteRFQProductId(options[0].Quote));
  }
};

export const SELECT_QUOTE_PRODUCT_RFQ = 'SELECT_QUOTE_PRODUCT_RFQ';
export const selectQuoteRFQProductId = (id) => async (dispatch, getState) => {
  localStorage.setItem('selectedQuoteRFQProductId', id);

  dispatch({
    type: SELECT_QUOTE_PRODUCT_RFQ,
    payload: id
  });
};

export const REQUEST_QUOTE_TIMEOUT = 'REQUEST_QUOTE_TIMEOUT';
export const requestQuoteTimeout = () => (dispatch) => {
  dispatch({
    type: REQUEST_QUOTE_TIMEOUT
  });
};

const clearField = (form, field, initialValue) => (dispatch) => {
  dispatch(change(form, field, initialValue));
  dispatch(untouch(form, field));
};

export const clearRFQForm = () => (dispatch) => {
  const form = 'rfq';
  dispatch(clearField(form, 'quote', 0));
  dispatch(clearField(form, 'base', 0));
};

export const onBaseProductSelect = (productId) => (dispatch) => {
  dispatch(selectBaseRFQProductId(productId));
  dispatch(clearRFQForm());
};

export const onQuoteProductSelect = (productId) => (dispatch) => {
  dispatch(selectQuoteRFQProductId(productId));
  dispatch(clearRFQForm());
};

export const REQUEST_QUOTE_PENDING = 'REQUEST_QUOTE_PENDING';
export const REQUEST_QUOTE_SUCCESS = 'REQUEST_QUOTE_SUCCESS';
export const REQUEST_QUOTE_FAILED = 'REQUEST_QUOTE_FAILED';
export const handleRequestQuote = (data) => async (dispatch) => {
  dispatch({
    type: REQUEST_QUOTE_PENDING
  });

  try {
    const response = await requestQuote(data);

    if (response.status === 200) {
      const data = await response.json();

      dispatch({
        type: REQUEST_QUOTE_SUCCESS,
        payload: data
      });
      dispatch(change('rfq', 'quoteState', 1)); // change quoteState = 1 to Buy
    } else {
      const error = await response.json();
      dispatch({
        type: REQUEST_QUOTE_FAILED,
        error
      });

      dispatch(
        showSnack({
          id: 'requestQuote',
          timeout: 10000,
          text: error.message || 'Something went wrong',
          type: 'warning'
        })
      );
    }
  } catch (error) {
    dispatch({
      type: REQUEST_QUOTE_FAILED,
      error
    });

    dispatch(
      showSnack({
        id: 'requestQuote',
        timeout: 10000,
        text: error.message || 'Something went wrong',
        type: 'warning'
      })
    );
  }
};

export const RESET_RFQ_STATE = 'RESET_RFQ_STATE';
export const resetRFQFormAndState = () => async (dispatch) => {
  dispatch({
    type: RESET_RFQ_STATE
  });
};

export const EXECUTE_QUOTE_PENDING = 'EXECUTE_QUOTE_PENDING';
export const EXECUTE_QUOTE_SUCCESS = 'EXECUTE_QUOTE_SUCCESS';
export const EXECUTE_QUOTE_FAILED = 'EXECUTE_QUOTE_FAILED';
export const handleExecuteQuote = (data) => async (dispatch) => {
  dispatch({
    type: EXECUTE_QUOTE_PENDING
  });

  const response = await executeQuote(data);

  if (response.status === 200) {
    const data = await response.json();

    dispatch({
      type: EXECUTE_QUOTE_SUCCESS,
      payload: data
    });

    clearRFQForm();

    return Promise.resolve(data);
  } else {
    dispatch({
      type: EXECUTE_QUOTE_FAILED,
      payload: { message: `Enable to execute quote` }
    });
    const data = await response.json();
    return Promise.reject({ message: data.message });
  }
};

export const REQUEST_BLOCKTRADE_ORDERS = 'REQUEST_BLOCKTRADE_ORDERS';
export const RECEIVED_BLOCKTRADE_ORDERS = 'RECEIVED_BLOCKTRADE_ORDERS';

export const fetchBlocktradeHistory = () => async (dispatch) => {
  dispatch({
    type: REQUEST_BLOCKTRADE_ORDERS
  });

  try {
    const response = await getBlocktradeHistory();

    if (response.status === 200) {
      const data = await response.json();
      dispatch({ type: RECEIVED_BLOCKTRADE_ORDERS, payload: data.items });
    }
  } catch (error) {
    console.log('error', error);
  }
};

export const START_INTERVAL_FETCH_RFQ_TRANSACTIONS =
  'START_INTERVAL_FETCH_RFQ_TRANSACTIONS';
export const STOP_INTERVAL_FETCH_RFQ_TRANSACTIONS =
  'STOP_INTERVAL_FETCH_RFQ_TRANSACTIONS';
export const startIntervalFetchRFQ = () => (dispatch) => {
  dispatch({ type: START_INTERVAL_FETCH_RFQ_TRANSACTIONS });
};

export const stopIntervalFetchRFQ = () => (dispatch) => {
  dispatch({ type: STOP_INTERVAL_FETCH_RFQ_TRANSACTIONS });
};

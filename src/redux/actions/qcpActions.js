import qcpService from '../../services/subscriptions/QCPIndicativePrice';

export const UPDATE_PRICE = 'UPDATE_PRICE';
export const STOP_UPDATE_PRICE = 'STOP_UPDATE_PRICE';

export const subscribeToQCPIndicativePrice = () => dispatch => {
  qcpService.subscribe(updates => {
    dispatch(updatePrice(updates));
  });
};

export const unsubscribeToQCPIndicativePrice = () => dispatch => {
  qcpService.stopAll();
  dispatch(stopUpdatePrice());
};

export const updatePrice = payload => ({
  type: UPDATE_PRICE,
  payload
});

export const stopUpdatePrice = payload => ({
  type: STOP_UPDATE_PRICE,
  payload
});

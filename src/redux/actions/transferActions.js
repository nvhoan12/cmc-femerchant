import apex from 'apex-web/lib/apex';
import { callAPI } from 'apex-web/lib/redux/actions/apiActions';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import { formatErrorText } from 'apex-web/lib/helpers/errorTextFormatter';
import {
  MODAL_TYPES,
  closeModal
} from 'apex-web/lib/redux/actions/modalActions';
import { closeSidePane } from 'apex-web/lib/redux/actions/sidePaneActions';

export const GET_TRANSFERS_REQUESTED = 'GET_TRANSFERS_REQUESTED';
export const GET_TRANSFERS_RECEIVED = 'GET_TRANSFERS_RECEIVED';
export const GET_INCOMING_TRANSFERS = 'GET_INCOMING_TRANSFERS';
export const TRANSFER_REQUEST_SUCCEEDED = 'TRANSFER_REQUEST_SUCCEEDED';

export const getTransfers = AccountId => async (dispatch, getState) => {
  dispatch(requestTransfers());
  const { OMSId } = getState().user.userInfo;
  await dispatch(callAPI('GetTransfers', { OMSId, AccountId })).then(res => {
    dispatch(receiveTransfers(res));
  });
};

export const getIncomingTransfers = (OMSId, AccountId) => async dispatch =>
  dispatch(
    callAPI('GetTransfersReceived', {
      OMSId,
      AccountId
    })
  ).then(res => dispatch(receiveIncomingTransfers(res)));

export const requestTransferFunds = payload => async (dispatch, getState) => {
  const { OMSId } = getState().user.userInfo;
  const response = await dispatch(
    callAPI('RequestTransferFunds', {
      OMSId,
      ...payload
    })
  );

  const snackbarPayload = response.result
    ? {
        id: 'requestTransferSuccess',
        // context.t('Request transfer succeeded');
        text: 'Request transfer succeeded',
        type: 'success'
      }
    : {
        id: 'requestTransferError',
        text: formatErrorText(response),
        type: 'warning'
      };

  dispatch(showSnack(snackbarPayload));
};

export const transferFunds = (payload, pane) => async (dispatch, getState) => {
  const {
    selectedAccountId: SenderAccountId,
    userInfo: { OMSId }
  } = getState().user;
  const response = await dispatch(
    callAPI('TransferFunds', {
      OMSId,
      SenderAccountId,
      ...payload
    })
  );

  const snackbarPayload = response.result
    ? {
        id: 'transferSuccess',
        // context.t('Transfer succeeded');
        text: 'Transfer succeeded',
        type: 'success'
      }
    : {
        id: 'transferError',
        text: formatErrorText(response),
        type: 'warning'
      };

  dispatch(closeSidePane(pane));
  dispatch(showSnack(snackbarPayload));
  return snackbarPayload;
};

export const receiveIncomingTransfers = payload => ({
  type: GET_INCOMING_TRANSFERS,
  payload
});

export const receiveTransfers = payload => ({
  type: GET_TRANSFERS_RECEIVED,
  payload
});

export const requestTransfers = () => ({
  type: GET_TRANSFERS_REQUESTED
});

export const requestTransferSucceeded = payload => ({
  type: TRANSFER_REQUEST_SUCCEEDED,
  payload
});

export const sendTwoFactorAuthCodeForTransfers = code => async (
  dispatch,
  getState
) => {
  const auth = await apex.connection.Authenticate2FA({
    Code: code
  });

  if (!auth.Authenticated) {
    return auth;
  }

  dispatch(closeModal(MODAL_TYPES.SEND_REQUEST_2FA));
  dispatch(closeSidePane());
  dispatch(
    showSnack({
      id: 'transferSuccess',
      // context.t('Transfer succeeded');
      text: 'Transfer succeeded',
      type: 'success'
    })
  );
};

import { callAPI } from 'apex-web/lib/redux/actions/apiActions';
import apex, { getApex } from '../../apex';
import {
  getAccountTrades,
  getOrders,
  getOpenOrders,
  getOpenTradeReports,
  initOrderHistorySubscription,
  subscribeToOrderHistory
} from 'apex-web/lib/redux/actions/orderHistoryActions';
import { marginAccountSelector } from 'apex-web/lib/redux/selectors/marginSelectors';
import { getDepositTickets } from 'apex-web/lib/redux/actions/depositActions';
import {
  getAccountVerificationLevelConfig,
  getWithdrawTickets,
  subscribeToWithdrawalTickets
} from 'apex-web/lib/redux/actions/withdrawActions';
import { changeLanguage } from 'apex-web/lib/redux/actions/languageActions';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import {
  MODAL_TYPES,
  closeModal
} from 'apex-web/lib/redux/actions/modalActions';
import { formatErrorText } from '../../helpers/errorTextFormatter';
import { subscribeToVerificationLevelUpdateEvents } from 'apex-web/lib/redux/actions/kycActions';
import {
  getTransfers,
  getIncomingTransfers
} from 'apex-web/lib/redux/actions/transferActions';
import {
  getTransferRequestsReceived,
  getTransferRequestsRequested
} from 'apex-web/lib/redux/actions/transferRequestActions';
import { getAffiliate } from 'apex-web/lib/redux/actions/affiliateActions';
import {
  getAccountPositions,
  subscribeToAccountPositionEvents
} from 'apex-web/lib/redux/actions/positionActions';
import { subscribeToDeposits } from 'apex-web/lib/redux/actions/depositActions';
// import {
//   initMarketTradeSubscription,
//   subscribeToMarketTradeUpdated
// } from 'apex-web/lib/redux/actions/marketStateUpdateActions';
import { subscribeToAccountSettlements } from 'apex-web/lib/redux/actions/eotcSettlementActions';
import {
  receiveMarginAccountPositionEvent,
  subscribeToMarginAccountPositionEvents,
  getMarginAccountState
} from 'apex-web/lib/redux/actions/marginActions';
import { logout, authSuccess } from 'apex-web/lib/redux/actions/authActions';
import { getOperatorLoyaltyFeeConfigs } from 'apex-web/lib/redux/actions/loyaltyFeeActions';

import { change } from 'redux-form';
import { updateAccount as UpdateAccount } from 'apex-web/lib/redux/actions/kycActions';
import AccountEventsSubscriptionService from 'apex-web/lib/services/subscriptions/AccountEventsSubscriptionService';
import config from '../../config';

export const USER_INFO_FETCHED = 'USER_INFO_FETCHED';
export const USER_CONFIG_FETCHED = 'USER_CONFIG_FETCHED';
export const RECEIVE_USER_PERMISSIONS = 'RECEIVE_USER_PERMISSIONS';
export const RECEIVE_ACCOUNT = 'RECEIVE_ACCOUNT';
export const SET_ACCOUNT = 'SET_ACCOUNT';
export const RECEIVE_KYC = 'RECEIVE_KYC';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';
export const SET_GOOGLE_QRCODE = 'SET_GOOGLE_QRCODE';
export const CLEAR_GOOGLE_QRCODE = 'CLEAR_GOOGLE_QRCODE';

export const userInfoFetched = payload => ({
  type: USER_INFO_FETCHED,
  payload
});

export const setUserInfo = payload => async dispatch => {
  const userInfo = await dispatch(
    callAPI('SetUserInfo', {
      UserName: payload.username,
      Email: payload.email
    })
  );
  if (userInfo.result !== false) {
    dispatch(userInfoFetched(userInfo));
    dispatch(changeLanguage(payload.language));
  } else {
    dispatch(
      showSnack({
        id: 'setUserInfoError',
        text: formatErrorText(userInfo),
        type: 'warning'
      })
    );
  }
};

export const getUserInfo = UserId => async dispatch => {
  const userInfo = await dispatch(callAPI('GetUserInfo', { UserId })).catch(
    e => {
      console.error('getUserInfo action error', e);
      return;
    }
  );

  dispatch(userInfoFetched(userInfo));
  dispatch(getUserPermissions(userInfo.UserId));
  dispatch(getUserAccounts(userInfo.OMSId));
  dispatch(getUserConfig(userInfo.UserId, userInfo.UserName));
  dispatch(selectAccount(userInfo.AccountId, userInfo.OMSId));
  dispatch(getAffiliate(userInfo.OMSId, userInfo.UserId));
  dispatch(getOperatorLoyaltyFeeConfigs(userInfo.OMSId));

  return userInfo;
};

export const userConfigFetched = payload => ({
  type: USER_CONFIG_FETCHED,
  payload
});

export const getUserConfig = (UserId, UserName) => async dispatch => {
  let userConfig;
  userConfig = await dispatch(
    callAPI('GetUserConfig', { UserId, UserName })
  ).catch(e => {
    console.error('getUserConfig action error', e);
    return;
  });

  dispatch(userConfigFetched(userConfig));

  return userConfig;
};

/**
 * Merge `config` arg with existing user config
 */
export const updateUserConfig = config => (dispatch, getState) => {
  const mergedConfig = [...getState().user.userConfig];

  config.forEach(current => {
    const index = mergedConfig.findIndex(i => i.Key === current.Key);

    if (index !== -1) {
      mergedConfig[index] = current;
    } else {
      mergedConfig.push(current);
    }
  });

  dispatch(userConfigFetched(mergedConfig));
};

export const setUserConfig = (
  UserId,
  UserName,
  Config,
  getUserConfigAfterSetUserConfig = true
) => async (dispatch, getState) => {
  const { user: { userInfo } = {} } = getState();
  const userConfig = await dispatch(
    callAPI('SetUserConfig', {
      UserId: UserId || userInfo.UserId,
      UserName: UserName || userInfo.UserName,
      Config
    })
  );

  if (userConfig.result !== false) {
    if (getUserConfigAfterSetUserConfig)
      dispatch(getUserConfig(UserId, UserName));
  } else {
    dispatch(
      showSnack({
        id: 'setUserConfigError',
        text: formatErrorText(userConfig),
        type: 'warning'
      })
    );
  }

  return userConfig;
};

export const getUserPermissions = UserId => async dispatch => {
  const permissions = await dispatch(callAPI('GetUserPermissions', { UserId }));
  dispatch(receiveUserPermissions(permissions));
};

export const receiveUserPermissions = payload => ({
  type: RECEIVE_USER_PERMISSIONS,
  payload
});

export const getUserAccounts = OMSId => async dispatch => {
  const userAccountIds = await dispatch(callAPI('GetUserAccounts', { OMSId }));
  const accountInfoArray = [];

  for (let accountId of userAccountIds) {
    const accountInfo = await dispatch(
      callAPI('GetAccountInfo', {
        AccountId: accountId,
        OMSId
      })
    );
    dispatch(receiveAccount([accountInfo]));
    accountInfoArray.push(accountInfo);
  }
  dispatch(subscribeToVerificationLevelUpdateEvents(accountInfoArray[0]));
};

export const updateAccount = loyaltyProduct => async (dispatch, getState) => {
  const {
    selectedAccountId,
    accounts,
    userInfo: { OMSId }
  } = getState().user;
  const currentAccount = accounts.find(
    account => selectedAccountId === account.AccountId
  );

  const payload = {
    ...currentAccount,
    LoyaltyProductId: 0,
    LoyaltyEnabled: false
  };

  if (loyaltyProduct) {
    const { Amount, ProductId } = loyaltyProduct;
    payload.LoyaltyProductId = ProductId;

    // Never enable the loyalty token functionality if the balance is 0.
    if (Amount > 0) {
      payload.LoyaltyEnabled = true;
    }
  }

  const response = await dispatch(callAPI('UpdateAccount', payload));

  // context.t('Turn On: Using BTZ for fees. You will not be able to accumulate BTZ in this mode.')
  // context.t('Turn Off: Stop using BTZ for fees. You will now accumulate BTZ for every trade you make.')
  if (response.result) {
    const text = loyaltyProduct
      ? 'Turn On: Using BTZ for fees. You will not be able to accumulate BTZ in this mode.'
      : 'Turn Off: Stop using BTZ for fees. You will now accumulate BTZ for every trade you make.';
    dispatch(
      showSnack({
        id: 'UpdateAccountSuccess',
        timeout: 6000,
        text,
        type: 'success'
      })
    );
    dispatch(getUserAccounts(OMSId));
  } else {
    dispatch(
      change(
        'loyaltyTokenForm',
        'loyaltyToken',
        currentAccount.LoyaltyProductId
      )
    );
    dispatch(
      showSnack({
        id: 'UpdateAccountError',
        text: formatErrorText(response),
        type: 'warning'
      })
    );
  }
};

export const selectAccount = (accountId, OMSId) => async (
  dispatch,
  getState
) => {
  dispatch(setAccount(accountId));

  if (config.global.privateMarketData) {
    dispatch(getOrders(accountId, OMSId));
  } else {
    dispatch(getOpenOrders(accountId, OMSId));
  }

  dispatch(getAccountTrades(accountId, OMSId));
  dispatch(getOpenTradeReports(accountId, OMSId));
  dispatch(getDepositTickets(accountId, OMSId));
  dispatch(getWithdrawTickets(accountId, OMSId));
  dispatch(subscribeToWithdrawalTickets());
  dispatch(initOrderHistorySubscription(accountId, OMSId));
  dispatch(subscribeToOrderHistory());
  dispatch(getTransfers(accountId));
  dispatch(getIncomingTransfers(OMSId, accountId));
  dispatch(getTransferRequestsReceived(accountId, OMSId));
  dispatch(getTransferRequestsRequested(accountId, OMSId));
  // dispatch(initMarketTradeSubscription());
  // dispatch(subscribeToMarketTradeUpdated());
  dispatch(subscribeToAccountSettlements());
  AccountEventsSubscriptionService.subscribe(update =>
    dispatch(
      UpdateAccount({
        item: update,
        index: getState().user.accounts.findIndex(
          i => i.AccountId === accountId
        )
      })
    )
  );

  const marginAccount = marginAccountSelector(getState());
  const marginAccountId = marginAccount ? marginAccount.AccountId : null;

  if (accountId !== marginAccountId) {
    dispatch(subscribeToAccountPositionEvents(accountId));
    dispatch(getAccountPositions({ accountId, OMSId }));
  } else {
    dispatch(getMarginAccountState());
    dispatch(
      subscribeToMarginAccountPositionEvents(
        accountId,
        receiveMarginAccountPositionEvent
      )
    );
  }

  dispatch(getAccountVerificationLevelConfig());
  dispatch(subscribeToDeposits());
};

export const receiveAccount = payload => ({
  type: RECEIVE_ACCOUNT,
  payload
});

export const setAccount = payload => ({
  type: SET_ACCOUNT,
  payload
});

export const goToResetPasswordFormStep = step => dispatch =>
  dispatch(change('resetPasswordRequest', 'step', step));

export const goToBaseCurrencyFormStep = step => dispatch =>
  dispatch(change('baseCurrencies', 'step', step));

export const sendResetPasswordEmail = UserName => async dispatch => {
  let Apex; // eslint-disable-line
  try {
    Apex = getApex();
  } catch (e) {
    return dispatch(
      showSnack({
        id: 'apexError',
        text: e.message,
        type: 'warning'
      })
    );
  }

  const response = await dispatch(callAPI('ResetPassword', { UserName }));
  const snackbarPayload = response.result
    ? {
        id: 'sendResetPasswordEmailSuccess',
        // context.t('Check Your Email For Reset Password Link');
        text: 'Check Your Email For Reset Password Link',
        type: 'success'
      }
    : {
        id: 'sendResetPasswordEmailError',
        text: formatErrorText(response),
        type: 'warning'
      };

  dispatch(showSnack(snackbarPayload));
  setTimeout(() => dispatch(logout()), config.Snackbar.timeout);
};

export const submitResetPasswordForm = () => (dispatch, getState) => {
  const { user } = getState();
  dispatch(sendResetPasswordEmail(user.userInfo.UserName));
};

export const setGoogleQrCode = payload => ({
  type: SET_GOOGLE_QRCODE,
  payload
});

export const clearGoogleQrCode = () => ({
  type: CLEAR_GOOGLE_QRCODE
});

export const getGoogleQrCode = () => async dispatch => {
  const qrCode = await apex.connection.EnableGoogle2FA();
  dispatch(setGoogleQrCode(qrCode));
};

export const disable2FA = code => async (dispatch, getState) => {
  const { userInfo } = getState().user;

  await apex.connection.Disable2FA();
  const auth = await apex.connection.Authenticate2FA({
    Code: code
  });

  if (auth.Authenticated) {
    dispatch(userInfoFetched({ ...userInfo, Use2FA: false }));
    dispatch(closeModal(MODAL_TYPES.DISABLE_2FA));
    dispatch(
      showSnack({
        id: 'DISABLE_2FA_SUCCESS',
        // context.t('Two Factor Authenticated is disabled')
        text: 'Two-Factor Authenticated is disabled',
        type: 'success'
      })
    );
  } else {
    dispatch(
      showSnack({
        id: 'DISABLE_2FA_ERROR',
        // context.t('Code Rejected')
        text: 'Code Rejected',
        type: 'warning'
      })
    );
  }
};

export const enable2FA = code => async (dispatch, getState) => {
  const { userInfo } = getState().user;
  const auth = await apex.connection.Authenticate2FA({
    Code: code
  });

  if (auth.Authenticated) {
    dispatch(authSuccess(auth));
    dispatch(userInfoFetched({ ...userInfo, Use2FA: true }));
    dispatch(closeModal(MODAL_TYPES.ENABLE_2FA));
    dispatch(clearGoogleQrCode());
    dispatch(
      showSnack({
        id: 'ENABLE_2FA_SUCCESS',
        // context.t('Two Factor Authenticated is enabled')
        text: 'Two-Factor Authenticated is enabled',
        type: 'success'
      })
    );
  } else {
    dispatch(
      showSnack({
        id: 'ENABLE_2FA_ERROR',
        // context.t('Code Rejected')
        text: 'Code Rejected',
        type: 'warning'
      })
    );
  }
};

export const getGoogle2FARecoveryCode = code => async dispatch => {
  const response = await apex.connection.RPCPromise('GetGoogle2FARecoveryKey', {
    Code: code
  });

  const recoveryCode = JSON.parse(response.o);

  if (recoveryCode.Authenticated) {
    dispatch(
      change(
        'google2FARecoveryKeyModal',
        'recoveryKey',
        recoveryCode.RecoveryKey
      )
    );
  } else {
    dispatch(
      showSnack({
        id: 'RECOVERY_ERROR',
        text: recoveryCode.errormsg,
        type: 'warning'
      })
    );
  }
};

export const resendVerificationEmail = payload => async dispatch => {
  let Apex;
  try {
    Apex = getApex();
  } catch (e) {
    return dispatch(
      showSnack({
        id: 'apexError',
        text: e.message,
        type: 'warning'
      })
    );
  }

  if (!Apex.connection.ResendVerificationEmail)
    return dispatch(
      showSnack({
        id: 'resendVerificationEmailError',
        text: 'Feature Not Available.',
        type: 'warning'
      })
    );

  const response = await Apex.connection.ResendVerificationEmail(payload);
  const snackbarPayload = response.result
    ? {
        id: 'resendVerificationEmailSuccess',
        // context.t('Check Your Email For An Account Verification Email');
        text: 'Check Your Email For An Account Verification Email',
        type: 'success'
      }
    : {
        id: 'resendVerificationEmailError',
        text: formatErrorText(response),
        type: 'warning'
      };

  dispatch(showSnack(snackbarPayload));
};

import config from '../../config';

export const PROCESSING_RESET_PASSWORD = 'PROCESSING_NEW_PASSWORD';
export const FINISHED_RESET_PASSWORD = 'FINISHED_VERIFY_NEW_PASSWORD';
export const SET_RESET_PASSWORD_ERROR = 'SET_PASSWORD_ERROR';
export const CLEAR_RESET_PASSWORD_ERROR = 'CLEAR_RESET_PASSWORD_ERROR';

export const processingResetPassword = {
  type: PROCESSING_RESET_PASSWORD
};

export const finishedResetPassword = payload => ({
  type: FINISHED_RESET_PASSWORD,
  payload
});

export const setResetPasswordError = payload => ({
  type: SET_RESET_PASSWORD_ERROR,
  payload
});

export const clearResetPasswordError = () => ({
  type: CLEAR_RESET_PASSWORD_ERROR
});

export const fetchResetPassword = payload => dispatch => {
  const {
    verifycode,
    userid,
    formValues: { password }
  } = payload;

  dispatch(processingResetPassword);
  const fetchUrl = config.services.resetPassword;
  const fetchPayload = {
    verifyCode: verifycode,
    password: password,
    userId: userid
  };

  fetch(fetchUrl, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(fetchPayload)
  }).then(async res => {
    const jsonRes = await res.json();
    if (res.status === 200) {
      return dispatch(
        finishedResetPassword({
          error: false,
          message:
            // context.t('You successfully reset your password. You can now login with your new password.')
            'You successfully reset your password. You can now login with your new password.'
        })
      );
    }
    dispatch(
      finishedResetPassword({
        error: true,
        message:
          // context.t('Your attempt to reset your password has failed. Contact the exchange admin if the problem persists. (New password cannot be same as old password)')
          `Your attempt to reset your password has failed. Contact the exchange admin if the problem persists. (${
            jsonRes.errormsg
          })`
      })
    );
  });
};

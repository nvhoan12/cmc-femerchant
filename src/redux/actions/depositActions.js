import { callAPI } from 'apex-web/lib/redux/actions/apiActions';
import depositSubscriptionService from 'apex-web/lib/services/subscriptions/depositSubscriptionService';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
// import { closeDepositSidePane } from 'apex-web/lib/redux/actions/sidePaneActions';
import { formatErrorText } from 'apex-web/lib/helpers/errorTextFormatter';
import config from 'apex-web/lib/config';

export const FETCH_PRODUCT_DEPOSIT_TEMPLATES =
  'FETCH_PRODUCT_DEPOSIT_TEMPLATES';
export const FETCH_PRODUCT_DEPOSIT_TEMPLATE = 'FETCH_PRODUCT_DEPOSIT_TEMPLATE';
export const RECEIVE_DEPOSIT_TEMPLATES = 'RECEIVE_DEPOSIT_TEMPLATES';
export const RECEIVE_DEPOSIT_TEMPLATES_ERROR =
  'RECEIVE_DEPOSIT_TEMPLATES_ERROR';
export const RECEIVE_DEPOSIT_TEMPLATE = 'RECEIVE_DEPOSIT_TEMPLATE';
export const RECEIVE_DEPOSIT_TEMPLATE_ERROR = 'RECEIVE_DEPOSIT_TEMPLATE_ERROR';
export const RECEIVE_DEPOSIT_INFO_ERROR = 'RECEIVE_DEPOSIT_INFO_ERROR';
export const SUBMITTING_DEPOSIT = 'SUBMITTING_DEPOSIT';
export const FETCH_DEPOSIT_INFO = 'FETCH_DEPOSIT_INFO';
export const RECEIVE_DEPOSIT_INFO = 'RECEIVE_DEPOSIT_INFO';
export const RECEIVE_DEPOSIT_TICKET = 'RECEIVE_DEPOSIT_TICKET';
export const RECEIVE_DEPOSIT_TICKETS = 'RECEIVE_DEPOSIT_TICKETS';
export const RESET_DEPOSIT_INFO = 'RESET_DEPOSIT_INFO';
export const RETAIL_FIAT_CONFIRM_DEPOSIT = 'RETAIL_FIAT_CONFIRM_DEPOSIT';
export const ADD_DEPOSIT = 'ADD_DEPOSIT';

export const fetchAllProductDepositTemplates = ProductId => async (
  dispatch,
  getState
) => {
  if (isNaN(ProductId)) {
    return dispatch({ type: RESET_DEPOSIT_INFO });
  }

  {
    // Follow previous deposit workflow for cryptos
    let product = getState().apexCore.product.products.find(
      prod => prod.ProductId === ProductId
    );
    if (product.ProductType === 'CryptoCurrency') {
      return dispatch(fetchProductDepositTemplate(ProductId));
    }
  }

  dispatch(fetchingDepositTemplates(ProductId));
  const payload = {
    ProductId,
    OMSId: getState().user.userInfo.OMSId,
    AccountId: getState().user.selectedAccountId
  };

  const data = await dispatch(
    callAPI('GetDepositTemplateTypes', payload)
  ).catch(() => dispatch(fetchProductDepositTemplate(ProductId)));

  try {
    if (data.result) {
      if (data.TemplateTypes && !data.TemplateTypes.length) {
        // this.context.t('There are no providers configured for this product');
        return dispatch(
          depositTemplatesError(
            'There are no providers configured for this product'
          )
        );
      }
      dispatch(receiveDepositTemplates(data.TemplateTypes));
      if (data.TemplateTypes && data.TemplateTypes.length === 1) {
        dispatch(
          fetchProductDepositTemplate(
            ProductId,
            data.TemplateTypes[0].TemplateName
          )
        );
      }
      return true;
    }
    return dispatch(depositTemplatesError(data.errormsg));
  } catch (e) {
    dispatch(fetchProductDepositTemplate(ProductId));
  }
};

export const fetchProductDepositTemplate = (ProductId, TemplateType) => async (
  dispatch,
  getState
) => {
  if (isNaN(ProductId)) {
    return dispatch({ type: RESET_DEPOSIT_INFO });
  }

  dispatch(fetchingDepositTemplate(ProductId, TemplateType));
  const templateTypeObj = getState().deposit.templateTypes.find(
    template => template.TemplateName === TemplateType
  );
  const payload = {
    OMSId: getState().user.userInfo.OMSId,
    AccountId: getState().user.selectedAccountId,
    ProductId,
    TemplateType
  };
  if (templateTypeObj) {
    payload.AccountProviderId = templateTypeObj.AccountProviderId;
  }
  const data = await dispatch(
    callAPI('GetDepositRequestInfoTemplate', payload)
  );

  if (data.result) {
    const { UseGetDepositWorkflow, Template, DepositWorkflow } = data.Template;
    if (
      Template === '{}' &&
      (UseGetDepositWorkflow || DepositWorkflow === 'CryptoWallet')
    ) {
      dispatch(getDepositInfo(ProductId));
    } else {
      dispatch(depositInfoError('Failed to load'));
    }

    if (!Template) {
      // this.context.t('Server provided an unexpected response');
      return dispatch(
        depositTemplateError('Server provided an unexpected response')
      );
    }

    return dispatch(receiveDepositTemplate(data));
  }

  return dispatch(depositTemplateError(data.errormsg));
};

export const getDepositInfo = (ProductId, template) => async (
  dispatch,
  getState
) => {
  const { OMSId } = getState().user.userInfo;
  const AccountId = getState().user.selectedAccountId;
  const templateType = getState().deposit.templateType;
  const templateTypeObj = getState().deposit.templateTypes.find(
    template => template.TemplateName === templateType
  );
  const payload = {
    OMSId,
    AccountId,
    ProductId: +ProductId,
    GenerateNewKey: config.Deposit.generateNewKey
  };

  if (templateTypeObj) {
    payload.AccountProviderId = templateTypeObj.AccountProviderId;
  }
  if (template) {
    payload.DepositInfo = template;
  }
  dispatch(fetchingDepositInfo(ProductId));

  const depositInfo = await dispatch(callAPI('GetDepositInfo', payload));

  if (depositInfo.result) {
    return dispatch(receiveDepositInfo(JSON.parse(depositInfo.DepositInfo)));
  }
  return dispatch(depositInfoError(depositInfo.errormsg));
};

export const getDepositTickets = () => async (dispatch, getState) => {
  const { OMSId } = getState().user.userInfo;
  const AccountId = getState().user.selectedAccountId;
  const depositTickets = await dispatch(
    callAPI('GetDepositTickets', {
      OMSId,
      AccountId,
      OperatorId: 1
    })
  );

  return dispatch(receiveDepositTickets(depositTickets));
};

export const createDepositTicket = ({
  ProductId,
  amount,
  TemplateType,
  ...info
}) => async (dispatch, getState) => {
  const { OMSId } = getState().user.userInfo;
  const AccountId = getState().user.selectedAccountId;
  const { UseGetDepositWorkflow } = getState().deposit.templateInfo;
  const DepositInfo = JSON.stringify(info);
  const payload = {
    OMSId,
    OperatorId: 1,
    AccountId,
    RequestUser: AccountId,
    AssetId: +ProductId,
    Amount: amount ? amount : +info.Amount,
    DepositInfo,
    Status: 'New'
  };

  dispatch(submittingDeposit);

  if (UseGetDepositWorkflow) {
    return dispatch(getDepositInfo(ProductId, DepositInfo));
  }

  const depositStatus = await dispatch(callAPI('CreateDepositTicket', payload));

  if (depositStatus.result === false) {
    return dispatch(
      showSnack({
        id: 'depositRejectSnack',
        text: formatErrorText(depositStatus),
        type: 'warning'
      })
    );
  }
  // dispatch(closeDepositSidePane());
  // context.t('Your deposit has been successfully added')
  dispatch(getDepositTickets());
  // if (!config.Deposit.showDepositIdModal) {
  //   dispatch(
  //     showSnack({
  //       id: 'depositSuccessSnack',
  //       text: 'Your deposit has been successfully added',
  //       type: 'success'
  //     })
  //   );
  // }
  return dispatch(receiveDepositTicket(depositStatus));
};

export const subscribeToDeposits = () => dispatch => {
  depositSubscriptionService.subscribe(updates => {
    dispatch(addDeposit(updates));
  });
};

export const addDeposit = payload => ({
  type: ADD_DEPOSIT,
  payload
});

export const fetchingDepositTemplates = product => ({
  type: FETCH_PRODUCT_DEPOSIT_TEMPLATES,
  payload: { product }
});

export const fetchingDepositTemplate = (product, templateType) => ({
  type: FETCH_PRODUCT_DEPOSIT_TEMPLATE,
  payload: { product, templateType }
});

export const fetchingDepositInfo = product => ({
  type: FETCH_DEPOSIT_INFO,
  payload: { product }
});

export const receiveDepositTemplates = payload => ({
  type: RECEIVE_DEPOSIT_TEMPLATES,
  payload
});

export const receiveDepositTemplate = payload => ({
  type: RECEIVE_DEPOSIT_TEMPLATE,
  payload
});

export const depositTemplatesError = payload => ({
  type: RECEIVE_DEPOSIT_TEMPLATES_ERROR,
  payload
});

export const depositTemplateError = payload => ({
  type: RECEIVE_DEPOSIT_TEMPLATE_ERROR,
  payload
});

export const depositInfoError = payload => ({
  type: RECEIVE_DEPOSIT_INFO_ERROR,
  payload
});

export const submittingDeposit = { type: SUBMITTING_DEPOSIT };

export const receiveDepositInfo = payload => ({
  type: RECEIVE_DEPOSIT_INFO,
  payload
});

export const receiveDepositTicket = payload => ({
  type: RECEIVE_DEPOSIT_TICKET,
  payload
});

export const receiveDepositTickets = payload => ({
  type: RECEIVE_DEPOSIT_TICKETS,
  payload
});

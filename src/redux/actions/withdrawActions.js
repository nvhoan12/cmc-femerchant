import { callAPI } from 'apex-web/lib/redux/actions/apiActions';
import withdrawSubscriptionService from 'apex-web/lib/services/subscriptions/withdrawSubscriptionService';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import {
  closeWithdrawSidePane,
  closeSidePane
} from 'apex-web/lib/redux/actions/sidePaneActions';
import { formatErrorText } from 'apex-web/lib/helpers/errorTextFormatter';
import { openModal, MODAL_TYPES } from './modalActions';

export const FETCH_PRODUCT_WITHDRAW_TEMPLATES =
  'FETCH_PRODUCT_WITHDRAW_TEMPLATES';
export const FETCH_PRODUCT_WITHDRAW_TEMPLATE =
  'FETCH_PRODUCT_WITHDRAW_TEMPLATE';
export const RECEIVE_WITHDRAW_TEMPLATES = 'RECEIVE_WITHDRAW_TEMPLATES';
export const RECEIVE_WITHDRAW_TEMPLATE = 'RECEIVE_WITHDRAW_TEMPLATE';
export const RECEIVE_WITHDRAW_TEMPLATES_ERROR =
  'RECEIVE_WITHDRAW_TEMPLATE_ERROR';
export const RECEIVE_WITHDRAW_TEMPLATE_ERROR =
  'RECEIVE_WITHDRAW_TEMPLATE_ERROR';
export const SUBMITTING_WITHDRAW = 'SUBMITTING_WITHDRAW';
export const RECEIVE_WITHDRAW_TICKET = 'RECEIVE_WITHDRAW_TICKET';
export const RECEIVE_WITHDRAW_TICKETS = 'RECEIVE_WITHDRAW_TICKETS';
export const RECEIVE_WITHDRAW_WORKFLOW_INFO = 'RECEIVE_WITHDRAW_WORKFLOW_INFO';
export const RECEIVE_WITHDRAW_FEE = 'RECEIVE_WITHDRAW_FEE';
export const RETAIL_FIAT_CONFIRM_WITHDRAW = 'RETAIL_FIAT_CONFIRM_DEPOSIT';
export const ADD_WITHDRAWAL_TICKET = 'ADD_WITHDRAWAL_TICKET';

export const fetchProductWithdrawTemplates = productId => async (
  dispatch,
  getState
) => {
  dispatch(fetchingWithdrawTemplates);
  const payload = {
    ProductId: productId,
    OMSId: getState().user.userInfo.OMSId,
    AccountId: getState().user.selectedAccountId
  };

  try {
    // Adds support for 3.2 updated withdrawal workflows
    const data = await dispatch(
      callAPI('GetWithdrawFormTemplateTypes', payload)
    );

    if (data.result) {
      if (!data.TemplateTypes.length) {
        // this.context.t('There are no providers configured for this product');
        return dispatch(
          withdrawTemplatesError(
            'There are no providers configured for this product'
          )
        );
      }
      dispatch(receiveWithdrawTemplates(data.TemplateTypes));
      if (data.TemplateTypes.length === 1) {
        dispatch(
          fetchProductWithdrawTemplate(
            productId,
            data.TemplateTypes[0].TemplateName
          )
        );
      }
      return true;
    }
    return dispatch(withdrawTemplatesError(data.errormsg));
  } catch (e) {
    // Fallback for older withdrawal workflow
    const data = await dispatch(callAPI('GetWithdrawTemplateTypes', payload));
    if (data.result) {
      dispatch(fetchProductWithdrawTemplate(productId, data.TemplateTypes[0]));
    }
    dispatch(withdrawTemplatesError(data.errormsg));
  }
};

export const fetchProductWithdrawTemplate = (
  productId,
  templateType,
  options = {}
) => async (dispatch, getState) => {
  dispatch(fetchingWithdrawTemplate(productId, templateType));
  const templateTypeObj = getState().withdraw.templateTypes.find(
    type => type.TemplateName === templateType
  );
  const payload = {
    OMSId: getState().user.userInfo.OMSId,
    AccountId: getState().user.selectedAccountId,
    ProductId: productId,
    TemplateType: templateType,
    ...options
  };
  if (templateTypeObj) {
    payload.AccountProviderId = templateTypeObj.AccountProviderId;
  }
  const data = await dispatch(callAPI('GetWithdrawTemplate', payload));
  if (data.result && data.Template) {
    return dispatch(receiveWithdrawTemplate(data));
  }
  return dispatch(withdrawTemplateError(data.errormsg));
};

export const createWithdrawTicket = ({
  ProductId,
  amount,
  iconFileName,
  ProductSymbol,
  ProductFullName,
  TemplateType,
  ...info
}) => async (dispatch, getState) => {
  dispatch(submittingWithdraw);
  const { OMSId } = getState().user.userInfo;
  const AccountId = getState().user.selectedAccountId;
  const templateForm = info.TemplateForm || info;
  const templateType = TemplateType || getState().withdraw.templateType;
  const templateTypeObj = getState().withdraw.templateTypes.find(
    template => template.TemplateName === templateType
  );
  // Since undefined is not serializable we default every prop to an empty string to avoid missing fields
  Object.keys(templateForm).forEach(
    prop => (templateForm[prop] = templateForm[prop] || '')
  );

  const payload = {
    OMSId,
    AccountId: AccountId,
    ProductId: +ProductId,
    Amount: +info.Amount,
    TemplateForm: JSON.stringify(templateForm),
    TemplateType: templateType
  };

  if (templateTypeObj) {
    payload.AccountProviderId = templateTypeObj.AccountProviderId;
  }

  const withdrawStatus = await dispatch(
    callAPI('CreateWithdrawTicket', payload)
  );

  if (withdrawStatus.result === false) {
    return dispatch(
      showSnack({
        id: 'withdrawRejectSnack',
        text: formatErrorText(withdrawStatus),
        type: 'warning'
      })
    );
  }

  try {
    let workflowDetail = JSON.parse(withdrawStatus.detail);

    if (
      typeof workflowDetail === 'object' &&
      workflowDetail.ReplyObject &&
      workflowDetail.ReplyObject !== '0'
    ) {
      return dispatch(receiveWithdrawWorkflowInfo(workflowDetail));
    }
  } catch (e) {
    // do nothing
  }

  // dispatch(closeWithdrawSidePane());
  // dispatch(
  //   showSnack({
  //     id: 'withdrawSuccessSnack',
  //     // context.t("Your withdraw has been successfully added, please check email for instructions.")
  //     text:
  //       'Your withdraw has been successfully added, please check email for instructions.',
  //     type: 'success'
  //   })
  // );

  dispatch(receiveWithdrawTicket(withdrawStatus));
  return dispatch(getWithdrawTickets(AccountId, OMSId));
};

export const closeSidepaneAfterCryptoWithdraw = () => async dispatch => {
  dispatch(closeWithdrawSidePane());
  dispatch(openModal(MODAL_TYPES.SEND_CRYPTO_SUCCESS));
};

export const getWithdrawFee = (ProductId, Amount) => async (
  dispatch,
  getState
) => {
  const { OMSId } = getState().user.userInfo;
  const AccountId = getState().user.selectedAccountId;

  const payload = {
    OMSId,
    ProductId,
    AccountId,
    Amount
  };

  const withdrawFee = await dispatch(callAPI('GetWithdrawFee', payload));

  if (withdrawFee.result === false) {
    return dispatch(
      showSnack({
        id: 'withdrawRejectSnack',
        text: formatErrorText(withdrawFee),
        type: 'warning'
      })
    );
  }
  dispatch(receiveWithdrawFee(withdrawFee));
};

export const cancelWithdrawTicket = ({ RequestCode }) => async (
  dispatch,
  getState
) => {
  const { OMSId, UserId } = getState().user.userInfo;
  const AccountId = getState().user.selectedAccountId;

  const payload = {
    OMSId,
    UserId,
    AccountId,
    RequestCode
  };

  const withdrawStatus = await dispatch(callAPI('CancelWithdraw', payload));

  if (withdrawStatus.result === false) {
    return dispatch(
      showSnack({
        id: 'cancelWithdrawSnack',
        text: formatErrorText(withdrawStatus),
        type: 'warning'
      })
    );
  }
  // context.t('Your withdraw has been successfully canceled')
  dispatch(
    showSnack({
      id: 'cancelWithdrawSuccessSnack',
      text: 'Your withdraw has been successfully canceled',
      type: 'success'
    })
  );
  dispatch(getWithdrawTickets(AccountId, OMSId));
};

export const subscribeToWithdrawalTickets = () => dispatch => {
  withdrawSubscriptionService.subscribe(updates => {
    dispatch(addWithdrawalTicket(updates));
  });
};

export const addWithdrawalTicket = payload => ({
  type: ADD_WITHDRAWAL_TICKET,
  payload
});

export const fetchingWithdrawTemplates = {
  type: FETCH_PRODUCT_WITHDRAW_TEMPLATES
};
export const fetchingWithdrawTemplate = (product, templateType) => ({
  type: FETCH_PRODUCT_WITHDRAW_TEMPLATE,
  payload: { product, templateType }
});
export const receiveWithdrawTemplates = payload => ({
  type: RECEIVE_WITHDRAW_TEMPLATES,
  payload
});
export const receiveWithdrawTemplate = payload => ({
  type: RECEIVE_WITHDRAW_TEMPLATE,
  payload
});
export const withdrawTemplatesError = payload => ({
  type: RECEIVE_WITHDRAW_TEMPLATES_ERROR,
  payload
});
export const withdrawTemplateError = payload => ({
  type: RECEIVE_WITHDRAW_TEMPLATE_ERROR,
  payload
});
export const submittingWithdraw = { type: SUBMITTING_WITHDRAW };

export const receiveWithdrawTicket = payload => ({
  type: RECEIVE_WITHDRAW_TICKET,
  payload
});

export const receiveWithdrawFee = payload => ({
  type: RECEIVE_WITHDRAW_FEE,
  payload
});

export const receiveWithdrawTickets = payload => ({
  type: RECEIVE_WITHDRAW_TICKETS,
  payload
});

export const receiveWithdrawWorkflowInfo = payload => ({
  type: RECEIVE_WITHDRAW_WORKFLOW_INFO,
  payload
});

export const getWithdrawTickets = (AccountId, OMSId) => async dispatch => {
  const withdrawTickets = await dispatch(
    callAPI('GetWithdrawTickets', {
      AccountId,
      OMSId
    })
  );
  dispatch(receiveWithdrawTickets(withdrawTickets));
};

export const RECEIVE_VERIFICATION_LEVEL_CONFIGS =
  'RECEIVE_VERIFICATION_LEVEL_CONFIGS';
const receiveVerificationLevelConfigs = payload => ({
  type: RECEIVE_VERIFICATION_LEVEL_CONFIGS,
  payload
});

export const getAccountVerificationLevelConfig = () => async (
  dispatch,
  getState
) => {
  const state = getState();
  await dispatch(
    callAPI('GetVerificationLevelConfig', {
      OMSId: state.user.userInfo.OMSId,
      AccountId: state.user.selectedAccountId
    })
  ).then(res => dispatch(receiveVerificationLevelConfigs(res)));
};

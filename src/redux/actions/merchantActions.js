import { change, untouch } from 'redux-form';

import { setInstrument } from 'apex-web/lib/redux/actions/instrumentActions';
import { getDepositInfo } from 'apex-web/lib/redux/actions/depositActions';

import { CRYPTO_PRODUCTS } from '../../pages/Merchant/components/MerchantTransferBlock';

const clearField = (form, field, initialValue) => (dispatch) => {
  dispatch(change(form, field, initialValue));
  dispatch(untouch(form, field));
};

export const clearMerchantForm = () => (dispatch) => {
  const form = 'merchant';
  dispatch(clearField(form, 'to', 0));
  dispatch(clearField(form, 'from', 0));
};

export const onProductSelect = (instrumentId) => async (dispatch, getState) => {
  const { products } = getState().apexCore.product;

  const product = CRYPTO_PRODUCTS.find((data) => data.id === +instrumentId);
  const selectedProduct = products.find((p) => p.Product === product.symbol);

  // SET INSTRUMENT AND UNSUBSCRIBE PREVIOUS CRYPTO
  const id = +instrumentId;

  // Fetch product template / info for get address
  dispatch({
    type: 'FETCH_PRODUCT_DEPOSIT_TEMPLATE',
    payload: { product: selectedProduct.ProductId }
  });

  dispatch(getDepositInfo(selectedProduct.ProductId));

  dispatch(setInstrument(id));

  // No nned to get recentTrade , use Level1 BestBid
  // dispatch(subscribeToLevel1(id));
  // dispatch(unsubscribeLevel2(selectedInstrumentId));
  // dispatch(getLevel2Data(id));

  // dispatch(unsubscribeRecentTrades(selectedInstrumentId));
  // dispatch(getRecentTrades(id));

  dispatch(clearMerchantForm());
};

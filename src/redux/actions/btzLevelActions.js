export const FETCH_BTZ_LEVEL_REQ = 'FETCH_BTZ_LEVEL_REQ';
export const UPDATE_BTZ_LEVEL = 'UPDATE_BTZ_LEVEL';
export const CLAIMED_BTZ = 'CLAIMED_BTZ';

export const claimedBTZ = () => ({
  type: CLAIMED_BTZ
});

import { SIDE_PANES } from '../../constants/sidePanesConstants';

export const CLOSE_SIDE_PANE = 'CLOSE_SIDE_PANE';
export const OPEN_SIDE_PANE = 'OPEN_SIDE_PANE';

export const openSidePane = (pane, props) => ({
  type: OPEN_SIDE_PANE,
  payload: { pane, sidePaneProps: props }
});

export const closeSidePane = pane => ({
  type: CLOSE_SIDE_PANE,
  payload: pane
});

export const openAdvancedOrderSidePane = () =>
  openSidePane(SIDE_PANES.ADVANCED_ORDER);
export const closeAdvancedOrderSidePane = () =>
  closeSidePane(SIDE_PANES.ADVANCED_ORDER);
export const openReportBlockTradeSidePane = () =>
  openSidePane(SIDE_PANES.ADVANCED_ORDER);
export const closeReportBlockTradeSidePane = () =>
  closeSidePane(SIDE_PANES.REPORT_BLOCK_TRADE);

export const openReportsSidePane = index =>
  openSidePane(SIDE_PANES.REPORTS, index);
export const closeReportsSidePane = () => closeSidePane(SIDE_PANES.REPORTS);

export const openDepositSidePane = product =>
  openSidePane(SIDE_PANES.DEPOSITS, { product });
export const closeDepositSidePane = () => closeSidePane(SIDE_PANES.DEPOSITS);

export const openWithdrawSidePane = product =>
  openSidePane(SIDE_PANES.WITHDRAWS, { product });
export const closeWithdrawSidePane = () => closeSidePane(SIDE_PANES.WITHDRAWS);

export const openApiKeysSidePane = () => openSidePane(SIDE_PANES.API_KEYS);
export const closeApiKeysSidePane = () => closeSidePane(SIDE_PANES.API_KEYS);

export const openKYCSidePane = () => openSidePane(SIDE_PANES.KYC);
export const openKYC_IMSidePane = () => openSidePane(SIDE_PANES.KYC_IM);
export const openKYC_IMLegacySidePane = () =>
  openSidePane(SIDE_PANES.KYC_IMLegacy);
export const openKYC_JumioSidePane = () => openSidePane(SIDE_PANES.KYC_Jumio);
export const openKYC_ManualSidePane = () => openSidePane(SIDE_PANES.KYC_Manual);
export const closeKYCSidePane = () => closeSidePane(SIDE_PANES.KYC);
export const closeKYC_JumioSidePane = () => closeSidePane(SIDE_PANES.KYC_Jumio);

export const openRetailSendReceiveSidePane = (isSend, product) =>
  openSidePane(SIDE_PANES.RETAIL_SEND_RECEIVE, { isSend, product });

export const closeRetailSendReceiveSidePane = (isSend, product) =>
  closeSidePane(SIDE_PANES.RETAIL_SEND_RECEIVE);

export const openRetailFiatSidePane = (
  isSend,
  product,
  isDepositDisabled,
  isWithdrawDisabled
) =>
  openSidePane(SIDE_PANES.RETAIL_FIAT, {
    isSend,
    product,
    isDepositDisabled,
    isWithdrawDisabled
  });

export const openKYCViewDetailSidePane = () =>
  openSidePane(SIDE_PANES.KYC_VIEW_DETAIL);
export const closeKYCViewDetailSidePane = () =>
  closeSidePane(SIDE_PANES.KYC_VIEW_DETAIL);

export const CLOSE_MODAL = 'CLOSE_MODAL';
export const OPEN_MODAL = 'OPEN_MODAL';

export const openModal = (name, modalProps) => ({
  type: OPEN_MODAL,
  payload: { name, modalProps }
});

export const closeModal = name => ({
  type: CLOSE_MODAL,
  payload: { name }
});

export const MODAL_TYPES = {
  NEW_API_KEY: 'NEW_API_KEY',
  REMOVE_API_KEY: 'REMOVE_API_KEY',
  CONFIRM_SINGLE_REPORT: 'CONFIRM_SINGLE_REPORT',
  CONFIRM_CYCLICAL_REPORT: 'CONFIRM_CYCLICAL_REPORT',
  CONFIRM_FIAT_DEPOSIT: 'CONFIRM_FIAT_DEPOSIT',
  CONFIRM_FIAT_WITHDRAW: 'CONFIRM_FIAT_WITHDRAW',
  CONFIRM_BLOCK_REPORT: 'CONFIRM_BLOCK_REPORT',
  CONFIRM_ORDER_REPORT: 'CONFIRM_ORDER_REPORT',
  RETAIL_SUBMIT_ORDER: 'RETAIL_SUBMIT_ORDER',
  EOTC_SUBMIT_ORDER: 'EOTC_SUBMIT_ORDER',
  NEW_AFFILIATE_KEY: 'NEW_AFFILIATE_KEY',
  ENABLE_2FA: 'ENABLE_2FA',
  DISABLE_2FA: 'DISABLE_2FA',
  DEPOSIT_STATUS: 'DEPOSIT_STATUS',
  DEPOSIT_REFERENCE_ID: 'DEPOSIT_REFERENCE_ID',
  SEND_REQUEST_2FA: 'SEND_REQUEST_2FA',
  SUBMIT_LIMIT_ORDER: 'SUBMIT_LIMIT_ORDER',
  MARGIN_DEPOSIT: 'MARGIN_DEPOSIT',
  MARGIN_WITHDRAW: 'MARGIN_WITHDRAW',
  MARGIN_TRADING: 'MARGIN_TRADING',
  MARGIN_CONFIRM: 'MARGIN_CONFIRM',
  MARGIN_DISABLE: 'MARGIN_DISABLE',
  MARGIN_CLOSE_POSITION: 'MARGIN_CLOSE_POSITION',
  MARGIN_TRANSFER_ASSETS: 'MARGIN_TRANSFER_ASSETS',
  GET_GOOGLE_2FA_RECOVERY_CODE: 'GET_GOOGLE_2FA_RECOVERY_CODE',
  REQUIRE_2FA: 'REQUIRE_2FA',
  PREVIEW_CLOSE_BY_SETTLEMENT: 'PREVIEW_CLOSE_BY_SETTLEMENT',
  INTEREST_ACCOUNTS_CONFIRMATION: 'INTEREST_ACCOUNTS_CONFIRMATION',
  INTEREST_LENDING_ENABLE: 'INTEREST_LENDING_ENABLE',
  INTEREST_DISABLE: 'INTEREST_DISABLE',
  SEND_CRYPTO_SUCCESS: 'SEND_CRYPTO_SUCCESS'
};

import { change, untouch, reset } from 'redux-form';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { callAPI } from 'apex-web/lib/redux/actions/apiActions';
import BigNumber from 'bignumber.js';
import { adTrack } from '../../helpers/adjust';
import { getFormValuesForSubmittingOrder } from '../../helpers/placeOrderHelper';
import {
  buyValue,
  sellValue,
  orderTypes
} from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import {
  MODAL_TYPES,
  openModal
} from 'apex-web/lib/redux/actions/modalActions';
import { orderFormTypes } from 'apex-web/lib/constants/sendOrder/orderFormTypes';
import { marketPriceOfSelectedInstrumentSelector } from 'apex-web/lib/redux/selectors/buySellSelectors';
import {
  syncQuantityWithPrice,
  syncPriceWithQuantity
} from '../../helpers/orderHelper';
import { selectedInstrumentSelector } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import { accountIdSelector } from 'apex-web/lib/redux/selectors/marginSelectors';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import { truncateToDecimals } from 'apex-web/lib/helpers/numberFormatter';
import {
  level2SellSelector,
  level2BuySelector
} from 'apex-web/lib/redux/selectors/level2Selectors';
import { orderTotalSelector } from 'apex-web/lib/redux/selectors/orderTotalSelector';
import config from '../../config';

export const placeOrderWithChecks = (orderFormType, form) => async (
  dispatch,
  getState
) => {
  const { apexCore } = getState();
  const { level1, instrument } = apexCore;
  if (
    form.orderType === orderTypes.limit.displayName &&
    form.side === sellValue
  ) {
    const limitPrice = new BigNumber(form.limitPrice);
    const marketPrice = new BigNumber(
      level1[instrument.selectedInstrumentId.toString()].BestOffer
    );
    if (limitPrice.lt(marketPrice)) {
      return dispatch(
        openModal(MODAL_TYPES.SUBMIT_LIMIT_ORDER, { orderFormType })
      );
    }
  }

  return dispatch(placeOrder(orderFormType, form));
};

const checksMinimumTransection = (selectedInstrument, form, state) => {
  let price = form.price;
  if (form.orderType === 'Limit Order') {
    const isBuySide = form.side === buyValue;
    const marketPrice = marketPriceOfSelectedInstrumentSelector(state, {
      side: form.side
    });
    const floatMarketPrice = parseFloat(marketPrice.replace(',', ''));
    if (isBuySide && parseFloat(form.price) > floatMarketPrice) {
      price = parseFloat(form.quantity) * floatMarketPrice;
    }
    if (!isBuySide && parseFloat(form.price) < floatMarketPrice) {
      price = parseFloat(form.quantity) * floatMarketPrice;
    }
  }

  if (selectedInstrument.Product2Symbol === 'THB') {
    if (
      (price && price < config.global.minimumTransection.thb) ||
      orderTotalSelector(state, { form }).isLessThan(
        config.global.minimumTransection.thb
      )
    ) {
      return {
        isLess: true,
        minimum: formatNumberToLocale(config.global.minimumTransection.thb),
        symbol: selectedInstrument.Product2Symbol
      };
    }
  }
  if (selectedInstrument.Product2Symbol === 'USDT') {
    if (
      (price && price < config.global.minimumTransection.usdt) ||
      orderTotalSelector(state, { form }).isLessThan(
        config.global.minimumTransection.usdt
      )
    )
      return {
        isLess: true,
        minimum: config.global.minimumTransection.usdt,
        symbol: selectedInstrument.Product2Symbol
      };
  }
  return { isLess: false };
};

// context.t('Minimum transaction value of {minimum} {symbol}')
// context.t('Not_Enough_Funds')
export const placeOrder = (orderFormType, form, { useNewForm } = {}) => async (
  dispatch,
  getState
) => {
  const state = getState();
  const selectedInstrument = selectedInstrumentSelector(getState());
  const checksMinimumResult = checksMinimumTransection(
    selectedInstrument,
    form,
    state
  );

  if (checksMinimumResult.isLess) {
    return {
      result: false,
      errormsg: 'Minimum transaction value of {minimum} {symbol}',
      textVars: {
        minimum: checksMinimumResult.minimum,
        symbol: checksMinimumResult.symbol
      }
    };
  }

  const selectedAccountId = accountIdSelector(state);
  const {
    user: { userInfo },
    apexCore: { product, level1, level2 }
  } = getState();
  const priceDecimalPlaces = convertIncrementToIntDecimalPlaces(
    selectedInstrument.PriceIncrement
  );
  const quantityDecimalPlaces = convertIncrementToIntDecimalPlaces(
    selectedInstrument.QuantityIncrement
  );
  const options = getFormValuesForSubmittingOrder(orderFormType, {
    state: getState(),
    decimalPlaces: product.decimalPlaces,
    instrument: { selectedInstrumentId: selectedInstrument.InstrumentId },
    form,
    userInfo,
    selectedAccountId,
    priceDecimalPlaces,
    quantityDecimalPlaces,
    level1,
    level2,
    useNewForm
  });
  let response;
  if (orderFormType === orderFormTypes.block) {
    response = await dispatch(callAPI('SubmitBlockTrade', options));
  } else {
    response = await dispatch(callAPI('SendOrder', options));
  }
  adTrack('TradeAttempt');
  return response;
};

export const buyPriceClicked = e => (dispatch, getState) => {
  e.persist();
  const { price } = e.currentTarget.dataset;
  const state = getState();
  const selectedOrderType = state.form.orderEntry.values.orderType;
  const isStopOrder = selectedOrderType === orderTypes.stopMarket.displayName;

  const { PriceIncrement } = selectedInstrumentSelector(state);
  const convertedPriceIncrement = convertIncrementToIntDecimalPlaces(
    PriceIncrement
  );

  if (!isStopOrder) {
    dispatch(checkAndSwitchToLimitOrderType());
  }
  dispatch(change('orderEntry', 'side', sellValue));
  dispatch(clearFeeAndTotalAmountValues('orderEntry'));

  dispatch(
    change(
      'orderEntry',
      isStopOrder ? 'stopPrice' : 'limitPrice',
      truncateToDecimals(parseFloat(price), convertedPriceIncrement)
    )
  );
};

export const buyQuantityClicked = e => (dispatch, getState) => {
  e.persist();

  const { quantity, index } = e.currentTarget.dataset;
  dispatch(clearFeeAndTotalAmountValues('orderEntry'));
  const state = getState();
  const buy = level2BuySelector(state, { filterMode: 'selectedInstrumentId' });
  const { QuantityIncrement } = selectedInstrumentSelector(state);
  const convertedQuantityIncrement = convertIncrementToIntDecimalPlaces(
    QuantityIncrement
  );
  const overallQuantity = calculateQuantity(
    truncateToDecimals(quantity, convertedQuantityIncrement),
    buy,
    index
  );
  dispatch(change('orderEntry', 'quantity', overallQuantity));
  dispatch(change('orderEntry', 'side', sellValue));
  dispatch(change('orderEntry', 'orderType', orderTypes.market.displayName));
};

export const sellPriceClicked = e => (dispatch, getState) => {
  e.persist();
  const { price } = e.currentTarget.dataset;
  const state = getState();
  const selectedOrderType = state.form.orderEntry.values.orderType;
  const isStopOrder = selectedOrderType === orderTypes.stopMarket.displayName;

  dispatch(clearFeeAndTotalAmountValues('orderEntry'));

  if (!isStopOrder) {
    dispatch(checkAndSwitchToLimitOrderType());
  }

  const { PriceIncrement } = selectedInstrumentSelector(state);
  const convertedPriceIncrement = convertIncrementToIntDecimalPlaces(
    PriceIncrement
  );

  dispatch(
    change(
      'orderEntry',
      isStopOrder ? 'stopPrice' : 'limitPrice',
      truncateToDecimals(parseFloat(price), convertedPriceIncrement)
    )
  );
  dispatch(change('orderEntry', 'side', buyValue));
};

export const sellQuantityClicked = e => (dispatch, getState) => {
  e.persist();

  const { quantity, index } = e.currentTarget.dataset;
  dispatch(clearFeeAndTotalAmountValues('orderEntry'));

  const state = getState();
  const sell = level2SellSelector(state, {
    filterMode: 'selectedInstrumentId'
  });
  const { QuantityIncrement } = selectedInstrumentSelector(state);
  const convertedQuantityIncrement = convertIncrementToIntDecimalPlaces(
    QuantityIncrement
  );
  const overallQuantity = calculateQuantity(
    truncateToDecimals(quantity, convertedQuantityIncrement),
    sell,
    index,
    false
  );
  dispatch(change('orderEntry', 'quantity', overallQuantity));

  dispatch(change('orderEntry', 'side', buyValue));
  dispatch(change('orderEntry', 'orderType', orderTypes.market.displayName));
};

export const clearOrderForm = formName => dispatch => {
  dispatch(clearFeeAndTotalAmountValues(formName));
  dispatch(reset(formName));
};

export const clearBuySellForm = () => dispatch => {
  const form = 'buySell';
  dispatch(clearField(form, 'quantity', ''));
  dispatch(clearField(form, 'price', ''));
  dispatch(clearField(form, 'amountRadio', 'custom'));
  dispatch(clearField(form, 'limitPrice', ''));
  dispatch(clearField(form, 'fee', '0'));
  dispatch(clearField(form, 'responseMessage', ''));
};

export const clearRFQForm = () => dispatch => {
  const form = 'rfq';
  dispatch(clearField(form, 'buy', 0));
  dispatch(clearField(form, 'with', 0));
};

export const showBuySellSuccessMessage = () => dispatch => {
  // context.t('Your order was filled successfully!')
  dispatch(
    change('buySell', 'responseMessage', 'Your order was filled successfully!')
  );
};

export const showBuySellFailMessage = () => dispatch => {
  // context.t('Your order failed, please try again')
  dispatch(
    change('buySell', 'responseMessage', 'Your order failed, please try again')
  );
};

export const synchronizeQuantityWithPrice = (price, form, field, formObj) => (
  dispatch,
  getState
) => {
  const {
    apexCore: {
      level2,
      product: { decimalPlaces }
    }
  } = getState();

  const selectedInstrument = selectedInstrumentSelector(getState());
  const quantity = syncQuantityWithPrice(price, {
    level2,
    selectedInstrument,
    side: formObj.values.side,
    decimalPlaces
  });
  dispatch(change(form, field, quantity));
};

export const synchronizePriceWithQuantity = (
  quantity,
  form,
  field,
  formObj
) => (dispatch, getState) => {
  const {
    apexCore: {
      level2,
      product: { decimalPlaces }
    }
  } = getState();

  const selectedInstrument = selectedInstrumentSelector(getState());
  const price = syncPriceWithQuantity(quantity, {
    level2,
    selectedInstrument,
    side: formObj.values.side,
    decimalPlaces
  });
  dispatch(change(form, field, price));
};

const calculateQuantity = (initValue, orders, quantityIndex, buy = true) => {
  const init = new BigNumber(initValue);

  const quantity = orders
    .filter(
      (data, index) => (buy ? index < quantityIndex : index > quantityIndex)
    )
    .reduce((prev, current) => prev.plus(current.Quantity), init);

  return quantity.toString();
};

/**
 * Switch order type to 'Limit' if 'Market' is selected before
 */
const checkAndSwitchToLimitOrderType = () => (dispatch, getState) => {
  const selectedOrderType = getState().form.orderEntry.values.orderType;

  if (selectedOrderType === orderTypes.market.displayName) {
    dispatch(change('orderEntry', 'orderType', orderTypes.limit.displayName));
  }
};

const clearFeeAndTotalAmountValues = form => dispatch => {
  dispatch(clearField(form, 'fee', '0'));
  dispatch(clearField(form, 'totalAmount', '0'));
};

const clearField = (form, field, initialValue) => dispatch => {
  dispatch(change(form, field, initialValue));
  dispatch(untouch(form, field));
};

import { all, take, select, put } from 'redux-saga/effects';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import { USER_INFO_FETCHED } from 'apex-web/lib/redux/actions/userActions';
import {
  FETCH_BTZ_LEVEL_REQ,
  UPDATE_BTZ_LEVEL,
  CLAIMED_BTZ
} from '../actions/btzLevelActions';
import config from '../../config';

function* onUserInfoFetched() {
  while (true) {
    yield take(USER_INFO_FETCHED);
    yield put({ type: FETCH_BTZ_LEVEL_REQ });
  }
}

function* freshBTZLevel() {
  while (true) {
    yield take(FETCH_BTZ_LEVEL_REQ);
    const state = yield select();
    const res = yield fetch(config.services.getLevel, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-BTZ-TOKEN': state.auth.token
      }
    });

    if (res.status === 200) {
      const jsonRes = yield res.json();
      const ea = jsonRes.badges.reduce((acc, item) => {
        if (item.id === 1) {
          return true;
        }
        return acc;
      }, false);
      const ambassador = jsonRes.badges.reduce((acc, item) => {
        if (item.id === 2) {
          return 'normal';
        }
        if (item.id === 3) {
          return 'rare';
        }
        if (item.id === 4) {
          return 'epic';
        }
        if (item.id === 5) {
          return 'legendary';
        }
        return acc;
      }, 'grey');
      const whale = jsonRes.badges.reduce((acc, item) => {
        if (item.id === 6) {
          return 'dolphin';
        }
        if (item.id === 7) {
          return 'baby';
        }
        if (item.id === 8) {
          return 'normal';
        }
        if (item.id === 9) {
          return 'killer';
        }
        if (item.id === 10) {
          return 'legendary';
        }
        return acc;
      }, 'grey');
      yield put({
        type: UPDATE_BTZ_LEVEL,
        payload: {
          btzID: `@${jsonRes.username}`,
          displayName: jsonRes.display_name,
          rank: jsonRes.rank,
          btzToClaim: jsonRes.pending_btz.total,
          lastUpdatedClaim: jsonRes.last_calculated_at,
          referral: jsonRes.pending_btz.referral,
          accumulation: jsonRes.pending_btz.accumulation,
          btzDiscountBack: jsonRes.pending_btz.cashback,
          rewards: jsonRes.pending_btz.sharing_btz,
          rewardKYC: jsonRes.pending_btz.reward_kyc,
          showEA: ea,
          ambassador: ambassador,
          whale: whale
        }
      });
    }
  }
}

function* claimedBTZ() {
  while (true) {
    yield take(CLAIMED_BTZ);
    const state = yield select();
    const currentAccount = state.user.accounts.find(
      account => account.AccountId === state.user.selectedAccountId
    );

    if (!currentAccount || currentAccount.VerificationLevel === 0) {
      // context.t('Please verify your account before claiming BTZ'),
      yield put(
        showSnack({
          id: 'claimedBTZ',
          timeout: 6000,
          text: 'Please verify your account before claiming BTZ',
          type: 'warning'
        })
      );
    } else {
      const res = yield fetch(config.services.claimedBTZ, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-BTZ-TOKEN': state.auth.token
        }
      });
      if (res.status === 200) {
        // context.t('Claimed')
        yield put(
          showSnack({
            id: 'claimedBTZ',
            timeout: 6000,
            text: 'Claimed',
            type: 'success'
          })
        );
        yield put({ type: FETCH_BTZ_LEVEL_REQ });
      } else {
        const jsonRes = yield res.json();
        // context.t('Not found a record or already claimed')
        yield put(
          showSnack({
            id: 'claimedBTZ',
            timeout: 6000,
            text: jsonRes.message,
            type: 'warning'
          })
        );
      }
    }
  }
}

export default function*() {
  yield all([onUserInfoFetched(), freshBTZLevel(), claimedBTZ()]);
}

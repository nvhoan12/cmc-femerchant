import { all, take, call, select } from 'redux-saga/effects';
import { getQueryParam } from 'apex-web/lib/helpers/queryParamsHelper';
import { FINISHED_VERIFY_EMAIL } from 'apex-web/lib/redux/actions/verifyEmailActions';
import { adTrack } from '../../helpers/adjust';
import config from '../../config';

function* confirmEmail() {
  while (true) {
    const {
      payload: { error }
    } = yield take(FINISHED_VERIFY_EMAIL);
    if (!error) {
      const userid = getQueryParam('UserId');
      const state = yield select();
      yield fetch(config.services.emailVerified, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-BTZ-TOKEN': state.auth.token
        },
        body: JSON.stringify({ user_id: userid })
      });
      yield call(adTrack, 'EmailVerified');
    }
  }
}

export default function*() {
  yield all([confirmEmail()]);
}

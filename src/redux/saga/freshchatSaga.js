import { all, take } from 'redux-saga/effects';
import { AUTH_NONE } from 'apex-web/lib/redux/actions/authActions';

function* freshchatRemoveUser() {
  while (true) {
    yield take(AUTH_NONE);
    if (window.fcWidget) {
      window.fcWidget.user.clear();
      localStorage.removeItem('fc-user-email');
    }
  }
}

export default function*() {
  yield all([freshchatRemoveUser()]);
}

import { all, take, select, put } from 'redux-saga/effects';
import { USER_CONFIG_FETCHED } from 'apex-web/lib/redux/actions/userActions';
import {
  FETCH_BTZ_BANK_ACCOUNT_REQ,
  UPDATE_BTZ_BANK_ACCOUNT
} from '../actions/btzBankAccountActions';
import config from '../../config';
import { banks } from '../../components/Retail/RetailSidePanesCustom/THB/helper/banks-data';

function* onUserConfigFetched() {
  while (true) {
    yield take(USER_CONFIG_FETCHED);
    yield put({ type: FETCH_BTZ_BANK_ACCOUNT_REQ });
  }
}

function* fetchBankAccount() {
  while (true) {
    yield take(FETCH_BTZ_BANK_ACCOUNT_REQ);
    const state = yield select();
    const res = yield fetch(config.services.getBankAcc, {
      method: 'GET',
      headers: { 'X-BTZ-TOKEN': state.auth.token }
    });
    if (res.status === 200) {
      const jsonRes = yield res.json();
      yield put({
        type: UPDATE_BTZ_BANK_ACCOUNT,
        payload: {
          bankAccounts: jsonRes['bankacc'].map(item => {
            const bank =
              banks.find(
                bank => parseInt(bank.id) === parseInt(item['bank_id'])
              ) || {};
            return {
              id: item['id'],
              bankID: item['bank_id'],
              accountName: item['acc_name'],
              bankNumber: item['acc_no'],
              verify: item['verify'] === 1 ? true : false,
              bankIcon: bank.icon,
              bankName: bank.name,
              bankNameTH: bank.nameTH
            };
          }),
          kycName: {
            EN: jsonRes['kycname'],
            TH: jsonRes['kycname_th']
          }
        }
      });
    }
  }
}

export default function*() {
  yield all([onUserConfigFetched(), fetchBankAccount()]);
}

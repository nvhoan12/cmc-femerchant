import { all } from 'redux-saga/effects';
import btzLevelSaga from './btzLevelSaga';
import btzAccountsSaga from './btzAccountsSaga';
import btzBankAccountSaga from './btzBankAccountSaga';
import freshchatSage from './freshchatSaga';
import recentTradesSaga from './recentTradesSaga';
import userSaga from './userSaga';
import merchantTransactionsSaga from './merchantTransactionsSaga';
import rfqSaga from './rfqSaga';
import verifyEmailSaga from './verifyEmailSaga';
import withdrawSaga from './withdrawSaga';
import signupSaga from './signupSaga';

export default function* rootSage() {
  yield all([
    btzLevelSaga(),
    btzAccountsSaga(),
    btzBankAccountSaga(),
    freshchatSage(),
    recentTradesSaga(),
    userSaga(),
    merchantTransactionsSaga(),
    rfqSaga(),
    verifyEmailSaga(),
    withdrawSaga(),
    signupSaga()
  ]);
}

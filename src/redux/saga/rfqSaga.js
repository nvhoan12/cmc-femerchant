import { all, take, call, cancel, put, delay, fork } from 'redux-saga/effects';
import {
  START_INTERVAL_FETCH_RFQ_TRANSACTIONS,
  STOP_INTERVAL_FETCH_RFQ_TRANSACTIONS,
  RECEIVED_BLOCKTRADE_ORDERS,
  FETCH_EXCHANGE_RATES_SUCCESS
} from '../actions/rfqActions';

import { getExchangeRates, getBlocktradeHistory } from '../../services/api/QCP';

const intervalFetch = 5000;

function* fetchExchangeRates() {
  while (true) {
    try {
      const res = yield call(getExchangeRates);
      const jsonRes = yield res.json();
      yield put({
        type: FETCH_EXCHANGE_RATES_SUCCESS,
        payload: jsonRes
      });
    } catch (error) {}
    yield delay(intervalFetch);
  }
}

function* fetchRFQTransactions() {
  while (true) {
    try {
      const res = yield call(getBlocktradeHistory);

      if (res.status === 200) {
        const jsonRes = yield res.json();
        yield put({
          type: RECEIVED_BLOCKTRADE_ORDERS,
          payload: jsonRes.items
        });
      }
    } catch (error) {}
    yield delay(intervalFetch);
  }
}

function* mainFetchRFQ() {
  while (yield take(START_INTERVAL_FETCH_RFQ_TRANSACTIONS)) {
    const fetchExchangeRatesTask = yield fork(fetchExchangeRates);
    const fetchRFQTask = yield fork(fetchRFQTransactions);

    yield take(STOP_INTERVAL_FETCH_RFQ_TRANSACTIONS);
    yield cancel(fetchExchangeRatesTask);
    yield cancel(fetchRFQTask);
  }
}

export default function*() {
  yield all([mainFetchRFQ()]);
}

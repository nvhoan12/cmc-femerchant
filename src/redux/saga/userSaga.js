import { all, take, put } from 'redux-saga/effects';
import { USER_CONFIG_FETCHED } from 'apex-web/lib/redux/actions/userActions';
import { userConfigUpdated } from 'apex-web/lib/redux/actions/kycActions';
import config from '../../config';

const SET_BASE_CURRENCY = 'SET_BASE_CURRENCY';

function* onUserConfigFetched() {
  while (true) {
    const action = yield take(USER_CONFIG_FETCHED);
    yield put({ type: SET_BASE_CURRENCY });
    try {
      const config = action.payload.find(
        data => data.Key === 'favoriteInstrumentsID'
      );
      localStorage.setItem(
        'favoriteInstrumentsID',
        config ? config.Value : '[]'
      );
    } catch (error) {}
  }
}

function* setBaseCurrency() {
  while (true) {
    yield take(SET_BASE_CURRENCY);
    yield put(
      userConfigUpdated({
        item: { Key: 'BaseCurrency', Value: config.global.baseCurrency }
      })
    );
  }
}

export default function*() {
  yield all([onUserConfigFetched(), setBaseCurrency()]);
}

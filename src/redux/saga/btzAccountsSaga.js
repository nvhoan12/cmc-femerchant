import { all, take, select, put, call, fork } from 'redux-saga/effects';
import config from '../../config';
import {
  GET_TRANSFERS_RECEIVED,
  GET_INCOMING_TRANSFERS
} from 'apex-web/lib/redux/actions/transferActions';
import {
  TRANSFER_REQUESTS_RECEIVED_RECEIVED,
  TRANSFER_REQUESTS_REQUESTED_RECEIVED
} from 'apex-web/lib/redux/actions/transferRequestActions';
import {
  FETCH_BTZ_ACCOUNT,
  PUT_BTZ_ACCOUNTS,
  UPDATE_BTZ_ACCOUNTS
} from '../actions/btzAccountsActions';

function* btzAccountFromTransfers() {
  while (true) {
    const actions = yield take([
      GET_TRANSFERS_RECEIVED,
      GET_INCOMING_TRANSFERS,
      TRANSFER_REQUESTS_RECEIVED_RECEIVED,
      TRANSFER_REQUESTS_REQUESTED_RECEIVED
    ]);
    const customersEmail = actions.payload.reduce((sum, item) => {
      const userReceiveEmail = item.ReceiverUserName || item.RequestorUsername;
      const oldUserReceiveEmail = sum.find(
        userEmail => userEmail === userReceiveEmail
      );
      if (!oldUserReceiveEmail) {
        sum.push(userReceiveEmail);
      }
      const userSenderEmail = item.SenderUserName || item.PayerUsername;
      const oldUserSenderEmail = sum.find(
        userEmail => userEmail === userSenderEmail
      );
      if (!oldUserSenderEmail) {
        sum.push(userSenderEmail);
      }
      return sum;
    }, []);
    yield all(
      customersEmail.map(email =>
        put({ type: FETCH_BTZ_ACCOUNT, payload: { email } })
      )
    );
  }
}

function* fetchBTZAccountInfo(email) {
  const store = yield select();
  const url = config.services.getInfoWithEmail(email);
  const opt = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': store.auth.token
    }
  };
  try {
    const res = yield call(fetch, url, opt);
    const jsonRes = yield res.json();
    yield put({
      type: UPDATE_BTZ_ACCOUNTS,
      payload: {
        email: email,
        btzID: jsonRes.btz_id ? `@${jsonRes.btz_id}` : '',
        displayName: jsonRes.display_name
      }
    });
  } catch (error) {}
}

function* putBTZAccount() {
  while (true) {
    const {
      payload: { email }
    } = yield take(FETCH_BTZ_ACCOUNT);
    const store = yield select();
    const oldCustomer = store.btzAccounts.find(
      customer => customer.email === email
    );
    if (!oldCustomer) {
      yield put({ type: PUT_BTZ_ACCOUNTS, payload: { email } });
      yield fork(fetchBTZAccountInfo, email);
    }
  }
}

export default function*() {
  yield all([btzAccountFromTransfers(), putBTZAccount()]);
}

import { all, take, call } from 'redux-saga/effects';
import {
  SIGNUP_REQUESTED,
  SIGNUP_SUCCESS
} from 'apex-web/lib/redux/actions/signupActions';
import { adTrack } from '../../helpers/adjust';

function* onSignupRequested() {
  while (true) {
    yield take(SIGNUP_REQUESTED);
    yield call(adTrack, 'SignupAttempt');
  }
}

function* onSignupSuccess() {
  while (true) {
    yield take(SIGNUP_SUCCESS);
    yield call(adTrack, 'SignupCompleted');
  }
}

export default function*() {
  yield all([onSignupRequested(), onSignupSuccess()]);
}

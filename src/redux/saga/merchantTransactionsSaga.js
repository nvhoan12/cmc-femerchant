import { isArray } from 'lodash';
import {
  all,
  take,
  call,
  select,
  cancel,
  put,
  delay,
  fork
} from 'redux-saga/effects';
import {
  UPDATE_MERCHANT_TRANSACTIONS,
  START_INTERVAL_FETCH_MERCHANT_TRANSACTIONS,
  STOP_INTERVAL_FETCH_MERCHANT_TRANSACTIONS
} from '../actions/merchantTransactionsActions';
import config from '../../config';

import {
  isMerchantRole,
  getMerchantIdFromUserConfig
} from '../../helpers/userConfigHelper';

const intervalFetch = 5000;

function* fetchMerchantTransactions() {
  while (true) {
    const state = yield select();

    let userId = state.user.userInfo.UserId;
    if (isMerchantRole(state.user.userConfig)) {
      userId = getMerchantIdFromUserConfig(state.user.userConfig);
    }

    const url = config.services.getMerchantTransactions(
      state.user.userInfo.AccountId,
      userId
    );

    const opt = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-BTZ-TOKEN': state.auth.token
      }
    };

    try {
      const res = yield call(fetch, url, opt);
      const jsonRes = yield res.json();
      yield put({
        type: UPDATE_MERCHANT_TRANSACTIONS,
        payload: isArray(jsonRes) ? jsonRes : []
      });
    } catch (error) {}
    yield delay(intervalFetch);
  }
}

function* mainFetchMerchantTransactions() {
  while (yield take(START_INTERVAL_FETCH_MERCHANT_TRANSACTIONS)) {
    const fetchMerchantTask = yield fork(fetchMerchantTransactions);

    yield take(STOP_INTERVAL_FETCH_MERCHANT_TRANSACTIONS);
    yield cancel(fetchMerchantTask);
  }
}

export default function*() {
  yield all([mainFetchMerchantTransactions()]);
}

import { all, take, call } from 'redux-saga/effects';
import { FINISHED_CONFIRM_WITHDRAW } from 'apex-web/lib/redux/actions/confirmWithdrawActions';
import { adTrack } from '../../helpers/adjust';

function* onConfirmWithdraw() {
  while (true) {
    yield take(FINISHED_CONFIRM_WITHDRAW);
    yield call(adTrack, 'WithdrawAttempt');
  }
}

export default function*() {
  yield all([onConfirmWithdraw()]);
}

import { all, take, call, put, select } from 'redux-saga/effects';
import { chunk, sumBy } from 'lodash';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import {
  RECEIVE_RECENT_TRADES,
  ADD_RECENT_TRADE
} from 'apex-web/lib/redux/actions/recentTradesActions';
import { STARTUP_DATA_LOADED } from 'apex-web/lib/redux/actions/appActions';
import { RECENT_TRADES_CHART_FETCH } from '../actions/recentTradesChartActions';
import tradesSubscriptionService from '../../services/subscriptions/tradesSubscriptionService';

const tradesCount = 500;
const normalizeRecentTrades = 100;
const instruments = [
  {
    InstrumentId: 1,
    symbol: 'BTCTHB'
  },
  {
    InstrumentId: 2,
    symbol: 'ETHTHB'
  },
  {
    InstrumentId: 3,
    symbol: 'XLMTHB'
  },
  {
    InstrumentId: 4,
    symbol: 'XRPTHB'
  },
  {
    InstrumentId: 7,
    symbol: 'BTCUSDT'
  },
  {
    InstrumentId: 8,
    symbol: 'ETHUSDT'
  },
  {
    InstrumentId: 17,
    symbol: 'COMPTHB'
  },
  {
    InstrumentId: 18,
    symbol: 'DAITHB'
  }
];

function* freshRecentTrades() {
  while (true) {
    yield take(STARTUP_DATA_LOADED);
    const state = yield select();

    yield call(
      tradesSubscriptionService.stopOne,
      state.apexCore.instrument.selectedInstrumentId
    );

    for (let i = 0; i < instruments.length; i++) {
      const instrument = instruments[i];
      const res = yield call(
        tradesSubscriptionService.initSubscribtion,
        instrument.InstrumentId,
        tradesCount
      );
      yield call(tradesSubscriptionService.stopOne, instrument.InstrumentId);
      const recentTrades = chunk(res, res.length / normalizeRecentTrades).map(
        item => ({
          Price: sumBy(item, 'Price') / item.length
        })
      );
      yield put({
        type: RECENT_TRADES_CHART_FETCH,
        payload: {
          ...instrument,
          recentTrades
        }
      });
    }

    yield call(
      tradesSubscriptionService.initSubscribtion,
      state.apexCore.instrument.selectedInstrumentId
    );
  }
}

function* receiveRecentTrades() {
  while (true) {
    yield take([RECEIVE_RECENT_TRADES, ADD_RECENT_TRADE]);
    const state = yield select();
    if (state.auth.isAuthenticated) {
      const selectedInstrument = state.apexCore.instrument.instruments.find(
        instrument =>
          instrument.InstrumentId ===
          state.apexCore.instrument.selectedInstrumentId
      );
      const recentTrade =
        state.recentTrades[selectedInstrument.InstrumentId][0];
      const price = formatNumberToLocale(
        recentTrade.Price,
        state.apexCore.product.decimalPlaces[selectedInstrument.Product2Symbol]
      );

      document.title = `${price} | ${selectedInstrument.Product1Symbol}/${
        selectedInstrument.Product2Symbol
      } | Bitazza`;
    }
  }
}

export default function*() {
  yield all([freshRecentTrades(), receiveRecentTrades()]);
}

import { createSelector } from 'reselect';

export const getMerchantName = createSelector(
  [(state) => state.user.userConfig],
  (userConfig) => {
    const config = userConfig.find((config) => config.Key === 'Full Name');

    if (config) {
      return config.Value;
    }

    return (config && config.value) || 'N/A';
  }
);
export const getRecentTrade = createSelector(
  [
    (state) => state.auth.isAuthenticated,
    (state) => state.instrument.instruments,
    (state) => state.instrument.selectedInstrumentId,
    (state) => state.level1,
    (state) => state.product.decimalPlaces
  ],
  (
    isAuthenticated,
    instruments,
    selectedInstruemntId,
    recentTrades,
    decimalPlaces
  ) => {
    if (!isAuthenticated) return 0;

    const selectedInstrument = instruments.find(
      (instrument) => instrument.InstrumentId === selectedInstruemntId
    );

    const recentTrade =
      recentTrades[selectedInstrument && selectedInstrument.InstrumentId];

    if (!recentTrade) return 0;

    return {
      id: recentTrade && recentTrade.InstrumentId,
      price: recentTrade.BestBid
    };
  }
);

import { createSelector } from 'reselect';
import get from 'lodash/get';
import { formValueSelector } from 'redux-form';
import config from 'apex-web/lib/config';
import getTime from 'date-fns/get_time';
import { instrumentsWithProductFullNamesAndProductSymbols } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import { typeFilter, dateFilter } from 'apex-web/lib/helpers/activityFilters';
import { getRecentActivity } from '../../helpers/recentActivityHelper';

const formattedTradesSelector = createSelector(
  [
    (state) => state.apexCore.orderHistory.accountTrades,
    instrumentsWithProductFullNamesAndProductSymbols,
    ({ productManifest: { manifest } }) => manifest
  ],
  (accountTrades, instruments, manifest) => {
    const formattedTrades = accountTrades.reduce(
      (
        acc,
        { Quantity, TradeTimeMS, Side, InstrumentId, ExecutionId, Value }
      ) => {
        let instrument = instruments.find(
          (instrument) => instrument.InstrumentId === InstrumentId
        );
        let Product1Symbol = get(instrument, 'Product1Symbol');
        let Product2Symbol = get(instrument, 'Product2Symbol');

        if (!Product1Symbol || !Product2Symbol) return acc;
        return acc.concat([
          {
            Type: 'trades',
            Action: {
              typeIcon: Side === 'Buy' ? 'deposit' : 'withdraw',
              productIcon: {
                productManifest: {
                  fetching: false
                },
                iconFileName: get(
                  manifest,
                  [Product1Symbol, 'iconFileName'],
                  `${Product1Symbol}DEFAULT_ICON`
                ),
                size: 24
              },
              params: { Product1Symbol, Product2Symbol, Side }
            },
            Amount: { Quantity, Value, Product1Symbol, Product2Symbol },
            Status: { id: ExecutionId },
            TimeStamp: {
              raw: TradeTimeMS
            }
          }
        ]);
      },
      []
    );

    return formattedTrades;
  }
);

const formattedDepositTicketsSelector = createSelector(
  [
    ({ deposit: { depositsStatus } }) => depositsStatus,
    ({ apexCore: { product } }) => product,
    ({ productManifest: { manifest } }) => manifest
  ],
  (depositTickets, product, manifest) => {
    const { products } = product;
    const formattedDepositTickets = depositTickets.reduce(
      (
        acc,
        {
          Amount,
          AssetId,
          RequestCode,
          LastUpdateTimeStamp,
          Status,
          DepositInfo
        }
      ) => {
        let { Product, ProductType } =
          products.find((product) => product.ProductId === AssetId) || {};
        let Value = Amount;
        if (!Product) return acc;
        return acc.concat([
          {
            Type: 'deposit',
            Action: {
              typeIcon: 'deposit',
              productIcon: {
                productManifest: {
                  fetching: false
                },
                iconFileName: get(
                  manifest,
                  [Product, 'iconFileName'],
                  `${Product}DEFAULT_ICON`
                ),
                size: 24
              },
              params: { Product, ProductType, Status }
            },
            Amount: {
              Quantity: Amount,
              Value,
              Product1Symbol: Product,
              Product2Symbol: Product
            },
            Status: {
              id:
                ProductType === 'CryptoCurrency'
                  ? JSON.parse(DepositInfo).TXId
                  : RequestCode
            },
            TimeStamp: {
              raw: getTime(LastUpdateTimeStamp)
            },
            AssetId: AssetId
          }
        ]);
      },
      []
    );

    return formattedDepositTickets;
  }
);

const formattedWithdrawalsSelector = createSelector(
  [
    ({ withdraw: { withdrawTickets } }) => withdrawTickets,
    ({ apexCore: { product } }) => product,
    ({ productManifest: { manifest } }) => manifest
  ],
  (withdrawTickets, product, manifest) => {
    const { products } = product;
    const formattedWithdrawals = withdrawTickets.reduce(
      (
        acc,
        {
          Amount,
          AssetId,
          RequestCode,
          LastUpdateTimestamp,
          Status,
          WithdrawTransactionDetails
        }
      ) => {
        let { Product, ProductType } =
          products.find((product) => product.ProductId === AssetId) || {};
        let Value = Amount;
        if (!Product) return acc;
        return acc.concat([
          {
            Type: 'withdraw',
            Action: {
              typeIcon: 'withdraw',
              productIcon: {
                productManifest: {
                  fetching: false
                },
                iconFileName: get(
                  manifest,
                  [Product, 'iconFileName'],
                  `${Product}DEFAULT_ICON`
                ),
                size: 24
              },
              params: { Product, ProductType, Status }
            },
            Amount: {
              Quantity: Amount,
              Value,
              Product1Symbol: Product,
              Product2Symbol: Product
            },
            Status: {
              id:
                ProductType === 'CryptoCurrency'
                  ? JSON.parse(WithdrawTransactionDetails).TxId
                  : RequestCode
            },
            TimeStamp: {
              raw: getTime(LastUpdateTimestamp)
            },
            AssetId: AssetId
          }
        ]);
      },
      []
    );
    return formattedWithdrawals;
  }
);

const formattedTransfersSelector = createSelector(
  [
    ({ transfer: { transfers, transfersReceived } }) => ({
      transfersSent: transfers,
      transfersReceived
    }),
    ({
      apexCore: {
        product: { products }
      }
    }) => products,
    ({ productManifest: { manifest } }) => manifest
  ],
  (transfers, products, manifest) => {
    const { transfersSent, transfersReceived } = transfers;

    const formattedTransfers = transfersSent.reduce((acc, el) => {
      const { ReceiverUserName, ProductId, Amount } = el;
      const { Product } =
        products.find((product) => ProductId === product.ProductId) || {};
      if (!Product) return acc;
      return acc.concat([
        {
          Type: 'send',
          Action: {
            typeIcon: 'withdraw',
            productIcon: {
              productManifest: {
                fetching: false
              },
              iconFileName: get(
                manifest,
                [Product, 'iconFileName'],
                `${Product}DEFAULT_ICON`
              ),
              size: 24
            },
            params: { Product, ReceiverUserName }
          },
          Amount: {
            Quantity: Amount,
            Product1Symbol: Product,
            Value: Amount,
            Product2Symbol: Product
          },
          Status: { id: 0 },
          TimeStamp: {
            raw: 0
          }
        }
      ]);
    }, []);

    const formattedIncomingTransfers = transfersReceived.map(
      ({ SenderUserName, ProductId, Amount }) => {
        const { Product } =
          products.find((product) => ProductId === product.ProductId) || {};
        return {
          Type: 'receive',
          Action: {
            typeIcon: 'withdraw',
            productIcon: {
              productManifest: {
                fetching: false
              },
              iconFileName: get(
                manifest,
                [Product, 'iconFileName'],
                `${Product}DEFAULT_ICON`
              ),
              size: 24
            },
            params: { Product, SenderUserName }
          },
          Amount: {
            Quantity: Amount,
            Product1Symbol: Product,
            Value: Amount,
            Product2Symbol: Product
          },
          Status: { id: 0 },
          TimeStamp: {
            raw: 0
          }
        };
      }
    );
    return [...formattedTransfers, ...formattedIncomingTransfers];
  }
);

export const recentActivitySelector = createSelector(
  [
    // This is where we will gather the information
    formattedDepositTicketsSelector,
    formattedWithdrawalsSelector,
    formattedTradesSelector,
    formattedTransfersSelector
  ],
  (
    formattedDepositTickets,
    formattedWithdrawals,
    formattedTrades,
    formattedTransfers
  ) => {
    let results = [
      ...formattedDepositTickets,
      ...formattedWithdrawals,
      ...formattedTrades
    ];
    if (config.RecentActivity.showTransfers && config.global.allowTransfer) {
      results = [...results, ...formattedTransfers];
    }
    return results.sort((a, b) => {
      const firstTimestamp = get(a, 'TimeStamp.raw');
      const secondTimestamp = get(b, 'TimeStamp.raw');
      if (firstTimestamp > secondTimestamp) {
        return -1;
      } else if (firstTimestamp < secondTimestamp) {
        return 1;
      } else {
        return 0;
      }
    });
  }
);

export const selectedRecentActivitySelector = createSelector(
  [
    recentActivitySelector,
    (state) => state.apexCore.position.selectedProductId,
    (state) => state.apexCore.product.products
  ],
  (recentActivity, selectedProductId, products) => {
    const selectedProduct =
      products.find((item) => item.ProductId === selectedProductId) || {};

    return recentActivity.filter((item) => {
      return (
        item.Amount.Product1Symbol === selectedProduct.Product ||
        item.Amount.Product2Symbol === selectedProduct.Product
      );
    });
  }
);

export const getFilteredActivity = createSelector(
  [(state) => state, (state, filter) => filter],
  (state, filter) => {
    const formValue = formValueSelector('recent-activity');
    const recentActivity = getRecentActivity(state, filter);
    const type = formValue(state, 'type');
    const startDate = formValue(state, 'startDate');
    const endDate = formValue(state, 'endDate');

    return recentActivity
      .filter((item) => typeFilter(item, type))
      .filter((item) => dateFilter(item, startDate, endDate));
  }
);

export const getDepositWithdrawActivity = createSelector(
  [
    formattedDepositTicketsSelector,
    formattedWithdrawalsSelector,
    (state) => state.apexCore.position.selectedProductId,
    (state, props) => props.filterMode
  ],
  (
    formattedDepositTickets,
    formattedWithdrawals,
    selectedProductId,
    filterMode
  ) => {
    const results = [...formattedDepositTickets, ...formattedWithdrawals];
    const filteredResults =
      filterMode === 'selectedProduct'
        ? results.filter((item) => item.AssetId === selectedProductId)
        : results;

    return filteredResults.sort((a, b) => {
      const firstTimestamp = get(a, 'TimeStamp.raw');
      const secondTimestamp = get(b, 'TimeStamp.raw');
      if (firstTimestamp > secondTimestamp) {
        return -1;
      } else if (firstTimestamp < secondTimestamp) {
        return 1;
      } else {
        return 0;
      }
    });
  }
);

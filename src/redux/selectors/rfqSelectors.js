import { createSelector } from 'reselect';
import BigNumber from 'bignumber.js';

import { buyValue } from '../../constants/sendOrder/orderEntryFormConstants';
import { positionSelector } from 'apex-web/lib/redux/selectors/positionSelectors';

import config from '../../config';

const instruments = config.global.qcp.instruments;

const THB = 'THB';
const BTZ = 'BTZ';
const USD = 'USD';

export const isBuySideRFQFormSelector = createSelector(
  [(state) => state.form.rfq],
  (formObj) => {
    return formObj ? formObj.values.side === buyValue : true;
  }
);

export const getProductListForQuote = createSelector([], () => {
  const result = instruments.map((data) => {
    return {
      id: data.id,
      Base: data.product1,
      Quote: data.product1,
      Buy: 0,
      Sell: 0
    };
  });

  // Move USDT to the top of option select.
  // const result = [...items];
  // let index = 0;
  // for (let i = 0; i < result.length; i++) {
  //   if (result[i].Base === 'USDT') {
  //     index = i;
  //   }
  // }
  // result.unshift(result.splice(index, 1)[0]);

  return result;
});

export const selectProducts = createSelector(
  [(state) => state.product.products],
  (products) => {
    const newProducts = Array.from({
      ...products,
      length: Object.keys(products).length
    });
    return newProducts;
  }
);

export const getBalanceFromSelected = createSelector(
  [
    (state) => state.rfq.selectedBaseId,
    (state) => state.rfq.selectedQuoteId,
    (state) => positionSelector(state)
  ],
  (baseId, quoteId, positions) => {
    const keys = Object.keys(positions);
    const length = keys[keys.length - 1] || 100;
    const newPositions = Array.from({
      ...positions,
      length: parseInt(length) + 1
    }).filter((v) => v);

    if (!newPositions) return {};

    return {
      basePosition: newPositions.find(
        ({ ProductSymbol }) => ProductSymbol === baseId
      ),
      quotePosition: newPositions.find(
        ({ ProductSymbol }) => ProductSymbol === quoteId
      )
    };
  }
);

export const getIndicativePriceSelected = createSelector(
  [
    (state) => state.rfq.selectedBaseId,
    (state) => state.rfq.selectedQuoteId,
    (state) => state.rfq.prices,
    (state) => state.rfq.rates
  ],
  (baseSymbol, quoteSymbol, prices, rates) => {
    let quote = quoteSymbol;
    if (quoteSymbol === BTZ || quoteSymbol === THB) {
      quote = USD;
    }

    const indicativePrice =
      prices.find(
        (price) => price.Base === baseSymbol && price.Quote === quote
      ) || {};

    if (quoteSymbol === THB) {
      return {
        ...indicativePrice,
        Buy: BigNumber(indicativePrice.Buy)
          .div(rates.THB)
          .decimalPlaces(2),
        Sell: BigNumber(indicativePrice.Sell)
          .div(rates.THB)
          .decimalPlaces(2)
      };
    }

    if (quoteSymbol === BTZ) {
      const BTZ_TO_USD = 0.11;

      return {
        ...indicativePrice,
        Buy: BigNumber(indicativePrice.Buy)
          .div(BTZ_TO_USD)
          .decimalPlaces(2),
        Sell: BigNumber(indicativePrice.Sell)
          .div(BTZ_TO_USD)
          .decimalPlaces(2)
      };
    }

    return indicativePrice;
  }
);

export const selectOptionByBaseId = createSelector(
  [(state) => state.rfq.prices, (state) => state.rfq.selectedBaseId],
  (prices, baseId) => prices.filter(({ Base }) => Base === baseId)
);

export const selectProductSymbols = createSelector(
  [(state) => state.product.products],
  (products) => [...products.map(({ Product }) => Product)]
);

export const selectOptionByCondition = createSelector(
  [selectOptionByBaseId, selectProductSymbols],
  (selectOptions, symbols) => {
    const extendTHB = [
      {
        Base: 'THB',
        Quote: 'THB'
      }
    ];
    return extendTHB;

    // List only THB for now
    // return [
    //   ...selectOptions.filter(({ Quote }) => symbols.indexOf(Quote) > -1),
    //   ...extendTHB
    // ];
  }
);

export const blocktradeHistorySelector = createSelector(
  [(state) => state.blocktradeHistory.orders],
  (blocktrades) => {
    const trades =
      blocktrades &&
      blocktrades.filter((order) => order.OrderType === 'BlockTrade');

    return trades;
  }
);

// export const blocktradeHistorySelector = createSelector(
//   [
//     state => state.apexCore.orderHistory.tradeReports,
//     state => state.apexCore.instrument.instruments,
//     state => `${state.rfq.selectedBaseId}${state.rfq.selectedQuoteId}`
//   ],
//   (tradeReports, instruments, selectedPair) => {
//     console.log('selectedPair', selectedPair);
//     const trades = tradeReports.filter(order => order.OrderState !== 'Working');
//     trades.map(order => {
//       const instrument = instruments.find(item => item.Symbol === selectedPair);
//       console.log('intrument ', instrument);

//       const { Product1Symbol, Product2Symbol } = instrument;
//       const quantity = currency(order.Quantity, Product1Symbol);
//       const price = currency(order.Price, Product2Symbol);
//       const symbol = order.Side === 'Buy' ? Product1Symbol : Product2Symbol;
//       const fee = currency(order.Fee, symbol);

//       return {
//         ...order,
//         Quantity: quantity,
//         Price: price,
//         Fee: fee
//       };
//     });

//     return trades;
//   }
// );

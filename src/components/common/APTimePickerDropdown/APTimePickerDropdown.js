import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import { generateOptionsFromNumber } from '../../../helpers/formGeneratorHelper';
import { parseTime } from '../../../helpers/dateHelper';

import './APTimePickerDropdown.css';
import { minTime, maxTime } from '../../../helpers/formValidations';

class APTimePickerDropdown extends React.Component {
  static propTypes = {
    input: PropTypes.object.isRequired,
    label: PropTypes.string,
    customClass: PropTypes.string,
    info: PropTypes.string,
    meta: PropTypes.shape({
      touched: PropTypes.bool,
      error: PropTypes.string
    }),
    disabled: PropTypes.bool,
    leadingZero: PropTypes.bool,
    minTime: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.instanceOf(Date)
    ]),
    maxTime: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)])
  };

  static contextTypes = {
    t: PropTypes.func
  };

  static defaultProps = {
    label: 'Time',
    info: '',
    disabled: false,
    leadingZero: true
  };

  constructor(props) {
    super(props);
    this.state = {
      date: new Date(0, 0, 0, 0, 0, 0),
      isAM: true
    };
  }

  componentDidMount() {
    const {
      input: { value }
    } = this.props;
    if (value) {
      const date = new Date(value);
      this.setState({ date });
      this.setIsAM(date);
    }
  }

  setIsAM = date => {
    this.setState({ isAM: date.getHours() < 12 });
  };

  handleChange = date => {
    this.props.input.onChange(date);
    if (this.props.customChange) {
      this.props.customChange(date);
    }
    this.setIsAM(date);
    this.setState({ date: new Date(date) });
  };

  getClasses = () => {
    const { customClasses } = this.props;
    return getBEMClasses([
      'ap-timepicker-dropdown',
      'ap-select',
      customClasses
    ]);
  };

  getHours = () => {
    const { isAMPM, leadingZero } = this.props;

    return generateOptionsFromNumber(
      isAMPM ? 1 : 0,
      isAMPM ? 13 : 24,
      leadingZero ? 2 : 0
    );
  };

  onHoursChange = e => {
    const { value } = e.target;
    const { date, isAM } = this.state;
    const { isAMPM } = this.props;

    let hours = +value;

    if (hours === 12) {
      hours = isAM ? 0 : 12;
    } else {
      hours = isAMPM && !isAM ? +value + 12 : +value;
    }

    date.setHours(hours);
    this.handleChange(date);
  };

  onMinutesChange = e => {
    const { value } = e.target;
    const { date } = this.state;

    date.setMinutes(value);
    this.handleChange(date);
  };

  getCurrentValue = () => {
    const { date, isAM } = this.state;
    const { isAMPM } = this.props;
    let hours = date.getHours();

    if (isAMPM) {
      if (isAM) {
        hours = hours === 0 ? 12 : hours;
      } else {
        hours = hours === 12 ? hours : hours - 12;
      }
    }

    return {
      hours,
      minutes: date.getMinutes()
    };
  };

  onAmPmChange = e => {
    const { value } = e.target;
    const { date } = this.state;
    let hours = date.getHours();

    hours = value === 'AM' ? hours - 12 : hours + 12;

    date.setHours(hours);
    this.handleChange(date);
  };

  render() {
    const {
      label,
      meta,
      info,
      customClass,
      leadingZero,
      isAMPM,
      ...otherProps
    } = this.props;
    const { isAM } = this.state;
    const baseClasses = this.getClasses();
    const value = this.getCurrentValue();

    return (
      <div className={`form-group ${baseClasses('select-wrapper')}`}>
        <label className={`ap--label ${baseClasses('label')}`}>{label}</label>
        <div className={baseClasses('input')}>
          <select
            value={value.hours}
            className={baseClasses('select')}
            onChange={this.onHoursChange}
            {...otherProps}>
            {this.getHours()}
          </select>
          <select
            value={value.minutes}
            className={baseClasses('select')}
            onChange={this.onMinutesChange}
            {...otherProps}>
            {generateOptionsFromNumber(0, 60, leadingZero ? 2 : 0)}
          </select>
          {isAMPM && (
            <select
              value={isAM ? 'AM' : 'PM'}
              className={baseClasses('select')}
              onChange={this.onAmPmChange}
              {...otherProps}>
              <option>PM</option>
              <option>AM</option>
            </select>
          )}
        </div>
        <div className={`form-text-muted ${baseClasses('info')}`}>{info}</div>
        {meta.dirty &&
          meta.error && (
            <div className={baseClasses('error', 'dpk-error')}>
              {meta.error}
            </div>
          )}
      </div>
    );
  }
}

let minTimeProp;
let maxTimeProp;

const minTimeValidation = value =>
  minTimeProp && minTime(value, parseTime(minTimeProp));
const maxTimeValidation = value =>
  maxTimeProp && maxTime(value, parseTime(maxTimeProp));

const maxMinValidations = [minTimeValidation, maxTimeValidation];

export default function APTimePicker(props) {
  const { validate, minTime, maxTime, ...otherProps } = props;

  minTimeProp = minTime;
  maxTimeProp = maxTime;

  const validators = validate
    ? [...validate, ...maxMinValidations]
    : maxMinValidations;

  return (
    <Field
      {...otherProps}
      name={props.name}
      component={APTimePickerDropdown}
      validate={validators}
    />
  );
}

export { APTimePickerDropdown };

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import config from '../../../config/';
import APIcon from '../APIcon';

import './Snackbar.css';

class Snackbar extends Component {
  static defaultProps = {
    timeout: config.Snackbar.timeout,
    text: '',
    textVars: {},
    type: config.Snackbar.type,
    customClass: 'custom-snackbar',
    onClose: () => {}
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  static propTypes = {
    timeout: PropTypes.number,
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    textVars: PropTypes.object,
    type: PropTypes.oneOf(['info', 'success', 'warning']),
    customClass: PropTypes.string,
    onClose: PropTypes.func
  };

  render() {
    const bemClasses = getBEMClasses(['snackbar', this.props.customClass]);
    const { type, text, textVars } = this.props;

    return (
      <div className={bemClasses('', type)}>
        <div className={bemClasses('text', type)}>
          {this.context.t(text, textVars)}
        </div>
        <div className={bemClasses('close', type)} onClick={this.props.onClose}>
          <APIcon
            name="close"
            className={bemClasses('close-icon')}
            customClass={bemClasses('icon')}
            classModifiers="big"
          />
        </div>
      </div>
    );
  }
}

export default Snackbar;

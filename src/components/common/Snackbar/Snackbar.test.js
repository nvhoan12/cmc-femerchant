import React from 'react';
import { shallow } from 'enzyme';
import Snackbar from './Snackbar';
import snackbarConfig from '../../../config/snackbarConfig';

jest.mock('../../../config/snackbarConfig', () => {
  return {};
});

describe('Snackbar component', () => {
  const otherProps = {
    timeout: snackbarConfig.timeout,
    text: 'snackbar text',
    type: 'success',
    customClass: 'custom-snackbar',
    id: 'id',
    onClose: jest.fn()
  };
  const buildSubject = (props = {}) => {
    const componentProps = {
      ...otherProps,
      ...props
    };
    return shallow(<Snackbar {...componentProps} />, {
      context: { t: v => v }
    });
  };

  it('should render text prop', () => {
    const subject = buildSubject();

    expect(subject.find('.snackbar__text').text()).toBe(otherProps.text);
  });

  it('should render the type class', () => {
    const subject = buildSubject();

    expect(subject.find('.snackbar--success').length).toBe(1);
  });

  it('should call onClose prop when the close button is clicked', () => {
    const subject = buildSubject();
    const closeIcon = subject.find('.snackbar__close');

    closeIcon.simulate('click');
    expect(otherProps.onClose).toBeCalled();
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import APIcon from '../APIcon';
import { InnerInput } from '../APInput';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import './APPasswordInput.css';

const baseClass = 'ap-password-input';

class PasswordInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      reveal: false
    };
  }

  handleClick = () => {
    this.setState({ reveal: !this.state.reveal });
  };

  renderIcon = bemClasses => (
    <span
      onClick={this.handleClick}
      className={bemClasses('icon', { active: this.state.reveal })}>
      <APIcon name={'eye'} />
    </span>
  );

  render() {
    const { customClass, ...passedProps } = this.props;
    const bemClasses = getBEMClasses(baseClass, customClass);
    const iconComponent = this.renderIcon(bemClasses);

    return (
      <div className={bemClasses()}>
        <InnerInput
          customClass={customClass}
          {...passedProps}
          type={this.state.reveal ? 'text' : 'password'}
          iconComponent={iconComponent}
        />
      </div>
    );
  }
}

const APPasswordInput = props => (
  <Field name={props.name} component={PasswordInput} {...props} />
);

APPasswordInput.propTypes = {
  customClass: PropTypes.string
};

export default APPasswordInput;

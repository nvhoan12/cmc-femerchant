import React from 'react';
import PropTypes from 'prop-types';
import Pikaday from 'pikaday-no-moment';
import { Field } from 'redux-form';
import BEMHelper from 'react-bem-helper';
import {
  formatDate,
  parseDate,
  createInputDateMask
} from '../../../helpers/dateHelper';
import APIcon from '../APIcon';
import isSameDay from 'date-fns/is_same_day';
import InputMask from 'react-input-mask';
import isDate from 'date-fns/is_date';

import 'pikaday-no-moment/css/pikaday.css';
import './APDatePicker.css';

const baseClasses = new BEMHelper({
  name: 'ap-datepicker',
  outputIsString: true
});

class DatePicker extends React.Component {
  static propTypes = {
    input: PropTypes.object.isRequired,
    label: PropTypes.string,
    customClass: PropTypes.string,
    // For custom format visit `toString` section of https://github.com/dbushell/Pikaday#formatting
    customFormat: PropTypes.func,
    customParse: PropTypes.func,
    info: PropTypes.string,
    meta: PropTypes.shape({
      touched: PropTypes.bool,
      error: PropTypes.string
    }),
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    initialOptions: PropTypes.object,
    showIcon: PropTypes.bool,
    iconClick: PropTypes.func
  };

  static defaultProps = {
    label: 'Date',
    customClass: 'custom-datepicker',
    info: '',
    disabled: false,
    required: false,
    initialOptions: {},
    customFormat: formatDate,
    customParse: parseDate,
    showIcon: false
  };

  constructor(props) {
    super(props);
    this.datepickerRef = null;
    this.inputMaskRef = null;
    this._picker = null;
    this.state = { inputMask: '', formatMask: '' };
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value === undefined) {
      this.inputMaskRef.value = '';
    }

    if (
      !isSameDay(prevProps.input.value, this.props.input.value) &&
      (isDate(this.props.input.value) || this.props.input.value === null)
    ) {
      this._picker.setDate(this.props.input.value);

      if (this.props.input.value === null) {
        this.inputMaskRef.value = '';
      } else {
        this.inputMaskRef.value = formatDate(
          this.props.input.value,
          this.state.formatMask
        );
      }
    }

    if (
      (isDate(this.props.minDate) || this.props.minDate === null) &&
      !isSameDay(prevProps.minDate, this.props.minDate)
    ) {
      this._picker.setMinDate(this.props.minDate);
    }

    if (
      (isDate(this.props.maxDate) || this.props.maxDate === null) &&
      !isSameDay(prevProps.maxDate, this.props.maxDate)
    ) {
      this._picker.setMaxDate(this.props.maxDate);
    }
  }

  componentDidMount() {
    const { inputMask, formatMask } = createInputDateMask();
    this.setState({ inputMask, formatMask });
    const el = this.datepickerRef;
    const { customFormat, customParse, minDate, maxDate } = this.props;

    this._picker = new Pikaday({
      field: el,
      yearRange: [1900, new Date().getFullYear()],
      format: formatMask,
      toString: customFormat,
      parse: customParse,
      minDate,
      maxDate,
      onSelect: this.handleChange,
      ...this.props.initialOptions
    });

    this.addClassNames();

    if (isDate(this.props.input.value)) {
      this._picker.setDate(this.props.input.value);
      this.inputMaskRef.value = formatDate(this.props.input.value, formatMask);
    }
  }

  componentWillUnmount() {
    this._picker.destroy();
    this._picker = null;
  }

  getCustomClasses() {
    return new BEMHelper({
      name: this.props.customClass,
      outputIsString: true
    });
  }

  addClassNames() {
    const customClasses = this.getCustomClasses();
    this._picker.el.classList.add(baseClasses('calendar'));
    customClasses('calendar')
      .split(' ')
      .forEach(customClass => this._picker.el.classList.add(customClass));
  }

  handleChange = value => {
    this.props.input.onChange(value);
    if (this.props.customChange) {
      this.props.customChange(value);
    }
  };

  render() {
    const {
      label,
      meta,
      info,
      input,
      disabled,
      required,
      showIcon,
      iconClick
    } = this.props;
    const { onChange, onBlur, value, ...otherInputProps } = input;
    const customClasses = this.getCustomClasses();

    return (
      <div
        className={`form-group ${baseClasses('input-wrapper')} ${customClasses(
          'dpk-wrapper'
        )}`}>
        <label
          className={`ap--label ${baseClasses('label')} ${customClasses(
            'dpk-label'
          )}`}>
          {label}
        </label>

        <InputMask
          ref={ref => (this.inputMaskRef = ref)}
          mask={this.state.inputMask}
          placeholder={this.state.formatMask}
          {...{ disabled, required }}
          {...otherInputProps}
          onChange={e => this.handleChange(e.target.value)}
          onBlur={e => onBlur(parseDate(e.target.value))}>
          {inputProps => (
            <input
              ref={ref => (this.datepickerRef = ref)}
              className={`form-control ${baseClasses('input')} ${customClasses(
                'dpk-input'
              )}`}
              {...inputProps}
              autoComplete="off"
            />
          )}
        </InputMask>
        {showIcon && (
          <div className={baseClasses('icon-container')} onClick={iconClick}>
            <APIcon name="calendar" customClass={baseClasses('icon')} />
          </div>
        )}
        <small
          className={`form-text-muted ${baseClasses('info')}  ${customClasses(
            'dpk-info'
          )}`}>
          {info}
        </small>
        {meta.dirty &&
          meta.error && (
            <span className={baseClasses('error', 'dpk-error')}>
              {meta.error}
            </span>
          )}
      </div>
    );
  }
}

export default function APDatePicker(props) {
  return <Field name={props.name} component={DatePicker} {...props} />;
}

export { DatePicker };

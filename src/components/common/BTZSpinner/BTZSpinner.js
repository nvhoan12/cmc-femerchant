import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import './BTZSpinner.css';

const classes = getBEMClasses('btz-spinner');

const BTZSpinner = ({ children, loading, className }) => {
  if (loading) {
    return <span className={clsx(classes('circle'), className)} />;
  }
  if (children) {
    return children;
  }
  return null;
};

BTZSpinner.propTypes = {
  loading: PropTypes.bool
};

BTZSpinner.defaultProps = {
  loading: true
};

const BTZLogoSpinner = ({ children, loading, className }) => {
  if (loading) {
    return <span className={clsx(classes('logo'), className)} />;
  }
  if (children) {
    return children;
  }
  return null;
};

BTZLogoSpinner.propTypes = {
  loading: PropTypes.bool
};

BTZLogoSpinner.defaultProps = {
  loading: true
};

export { BTZLogoSpinner };

export default BTZSpinner;

import React from 'react';
import PropTypes from 'prop-types';
import APUpload from '../APUpload/APUpload';

import '../APUpload/APUpload.css';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

const baseClass = 'ap-image-upload';

class APImageUpload extends React.Component {
  static defaultProps = {
    name: '',
    customClass: '',
    placeholder: '',
    label: '',
    value: ''
  };

  static propTypes = {
    name: PropTypes.string,
    customClass: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    required: PropTypes.bool
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      imgSrc: null
    };
  }

  onChange = (e, { file, imgSrc }) => {
    const { onChange, name } = this.props;
    const changeFn = onChange || (() => {});

    if (file) {
      changeFn(name, { file, imgSrc });
      setTimeout(() => this.setState({ imgSrc }));
    } else {
      this.setState({ imgSrc: null });
      changeFn(name, null);
    }
  };

  render() {
    const { customClass, ...otherProps } = this.props;
    const { imgSrc } = this.state;
    const bemClasses = getBEMClasses(baseClass, customClass);

    return (
      <APUpload
        {...otherProps}
        customChange={this.onChange}
        label={
          imgSrc
            ? this.context.t('Choose another image')
            : this.context.t('Place image')
        }
        customClass={bemClasses()}
        acceptedFiles={'.png, .jpg, .jpeg, .gif'}
      />
    );
  }
}

APImageUpload.defaultProps = {
  name: '',
  label: '',
  classModifiers: '',
  placeholder: '',
  info: '',
  autoFocus: '',
  disabled: false,
  required: false,
  maxSize: null
};

APImageUpload.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  customClass: PropTypes.string,
  classModifiers: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  placeholder: PropTypes.string,
  info: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  autoFocus: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool
};

export default APImageUpload;

import React from 'react';
import PropTypes from 'prop-types';
import detectEmojiSupport from 'detect-emoji-support';

const emojisSupported = detectEmojiSupport();

const APPhoneInputFlags = props => {
  const { country } = props;
  const flag = country
    .toUpperCase()
    .replace(/./g, char => String.fromCodePoint(char.charCodeAt(0) + 127397));
  return (
    <div className="react-phone-number-input__icon">
      {emojisSupported ? flag : country}
    </div>
  );
};
APPhoneInputFlags.defaultProps = {
  country: ''
};

APPhoneInputFlags.propTypes = {
  country: PropTypes.string
};

export default APPhoneInputFlags;

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Field } from 'redux-form';
import PhoneInput from 'react-phone-number-input/native';
import { getCountryCallingCode, parseNumber } from 'libphonenumber-js/custom';
import metadata from 'libphonenumber-js/metadata.min.json';
import FlagsComponent from './APPhoneInputFlags';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import config from '../../../config';

import 'react-phone-number-input/style.css';
import './APPhoneInput.css';

const baseClass = 'ap-phone-input';

class APPhoneInnerInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: props.value,
      focused: false,
      countryPrefix: null,
      countryCode: null
    };
  }

  static getDerivedStateFromProps(nextProps) {
    const phone = nextProps.value;

    if (phone) {
      const parsed = parseNumber(phone, metadata);
      if (parsed.country) {
        const countryCode = parsed.country;
        const countryPrefix =
          countryCode && getCountryCallingCode(countryCode, metadata);
        return { phone, countryCode, countryPrefix };
      }
    }

    return { phone };
  }

  onChange = (value, phone) => {
    const {
      input: { onChange },
      name
    } = this.props;
    const { countryPrefix, countryCode } = this.state;

    const meta = {
      countryPrefix,
      countryCode,
      noPrefixPhone: phone && phone.replace(`+${countryPrefix}`, '')
    };

    onChange(value, phone, meta, name);
  };

  onCountryChange = countryCode => {
    const countryPrefix = getCountryCallingCode(countryCode, metadata);
    this.setState({ countryPrefix, countryCode });
  };

  onFocus = () => {
    this.setState({ focused: true });
  };

  onBlur = () => {
    this.setState({ focused: false });
  };

  render() {
    const {
      customClass,
      classModifiers,
      info,
      label,
      rightLabelText,
      meta: { error, dirty }
    } = this.props;

    const bemClasses = getBEMClasses([baseClass, customClass]);
    return (
      <div
        className={`form-group ${bemClasses('input-wrapper', classModifiers)}`}
        onFocus={this.onFocus}
        onBlur={this.onBlur}>
        <label className={`ap--label ${bemClasses('label', classModifiers)}`}>
          {label}
        </label>
        <div
          className={classnames(
            rightLabelText && bemClasses('input-with-right-label-container'),
            bemClasses('input-box')
          )}>
          <PhoneInput
            name="telephone"
            value={this.state.phone}
            flagComponent={FlagsComponent}
            international={false}
            displayInitialValueAsLocalNumber
            country={this.props.country || config.global.phoneDefaultCountry}
            countries={config.global.phoneWhiteListCountries}
            placeholder={this.props.placeholder}
            inputClassName={bemClasses('input')}
            onChange={this.onChange}
            onCountryChange={this.onCountryChange}
            metadata={metadata}
          />
          {info && (
            <div className={`form-text-muted ${bemClasses('info')}`}>
              {info}
            </div>
          )}

          {dirty && error && <div className={bemClasses('error')}>{error}</div>}
        </div>
      </div>
    );
  }
}

APPhoneInnerInput.defaultProps = {
  name: '',
  customClass: '',
  placeholder: '',
  label: '',
  value: '',
  country: '',
  onChange: () => {}
};

APPhoneInnerInput.propTypes = {
  name: PropTypes.string,
  customClass: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  country: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool
};

const APPhoneInput = props => (
  <Field name={props.name} component={APPhoneInnerInput} {...props} />
);

APPhoneInput.defaultProps = {
  name: '',
  label: '',
  labelInInput: '',
  type: '',
  classModifiers: '',
  placeholder: '',
  info: '',
  autoFocus: '',
  disabled: false,
  required: false,
  rows: 1
};

APPhoneInput.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  labelInInput: PropTypes.string,
  type: PropTypes.string,
  customClass: PropTypes.string,
  classModifiers: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  placeholder: PropTypes.string,
  info: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  autoFocus: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  rightLabelText: PropTypes.string,
  rows: PropTypes.number
};

export default APPhoneInput;

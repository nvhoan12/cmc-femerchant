import React from 'react';
import PropTypes from 'prop-types';
import { styleNames, styleNamePropType } from '../../propTypes/commonComponent';
import { getBEMClasses } from '../../helpers/cssClassesHelper';

import '../../styles/components/common/Buttons.css';

const APButton = props => {
  const {
    children,
    styleName,
    baseClass,
    customClass,
    classModifiers,
    disabled,
    dataTest,
    ...otherProps
  } = props;

  const bemClasses = getBEMClasses(baseClass, customClass, classModifiers);

  return (
    <button
      disabled={disabled}
      data-test={dataTest}
      className={bemClasses('btn', [styleName, classModifiers])}
      {...otherProps}>
      {children}
    </button>
  );
};

APButton.defaultProps = {
  name: '',
  value: '',
  type: '',
  autoFocus: false,
  disabled: false,
  baseClass: 'ap-button',
  styleName: styleNames.general
};

APButton.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.string,
  autoFocus: PropTypes.bool,
  disabled: PropTypes.bool,
  baseClass: PropTypes.string,
  customClass: PropTypes.string,
  styleName: styleNamePropType
};

APButton.contextTypes = {
  t: PropTypes.func.isRequired
};

export default APButton;

import React from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import APButton from '../APButton';
import APIcon from '../APIcon';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import history from '../../../helpers/history';

import './Modal.css';

const baseClass = 'ap-modal';

export default class Modal extends React.Component {
  static propTypes = {
    isOpen: PropTypes.bool,
    title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
      PropTypes.func
    ]),
    onCancel: PropTypes.func,
    close: PropTypes.func.isRequired,
    overrideClose: PropTypes.bool,
    customClass: PropTypes.string
  };

  static defaultProps = {
    overrideClose: false
  };

  constructor(props) {
    super(props);
    this.modalRef = null;
    this.unlisten = null;
  }

  componentDidMount() {
    ReactModal.setAppElement(this.modalRef);
    this.unlisten = history.listen((location, action) => {
      const close = this.props.onCancel || this.props.close;
      if (action === 'POP' && this.props.isOpen) {
        close();
      }
    });
  }

  componentWillUnmount() {
    this.unlisten();
    this.unlisten = null;
  }

  getBEMClasses = () => {
    return getBEMClasses(baseClass, this.props.customClass);
  };

  renderCloseIcon = () => {
    const bemClasses = this.getBEMClasses();
    const onClick = this.props.onCancel || this.props.close;

    return (
      <div onClick={onClick} className={bemClasses('close-icon')}>
        <APIcon name="close" classModifiers="big" />
      </div>
    );
  };

  handleFooterButtonClick = e => {
    this.props.footer.onClick();
    !this.props.footer.overrideClose && this.props.close();
  };

  render() {
    if (!this.props.isOpen) return null;

    const bemClasses = this.getBEMClasses();

    return (
      <div ref={ref => (this.modalRef = ref)}>
        <ReactModal
          className={bemClasses()}
          ariaHideApp={false}
          overlayClassName={bemClasses('overlay')}
          onRequestClose={this.props.onCancel || this.props.close}
          isOpen={this.props.isOpen}>
          <div className={bemClasses('header-container')}>
            <div className={bemClasses('header')}>{this.props.title}</div>
            {this.renderCloseIcon()}
          </div>
          <div className={bemClasses('content')}>{this.props.children}</div>
          {this.props.footer && (
            <div className={bemClasses('footer')}>
              <APButton
                customClass={bemClasses()}
                onClick={this.handleFooterButtonClick}
                styleName={this.props.footer.buttonStyle}
                disabled={this.props.footer.disabled}>
                {this.props.footer.buttonText}
              </APButton>
            </div>
          )}
        </ReactModal>
      </div>
    );
  }
}

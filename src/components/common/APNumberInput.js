import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { parseNumberToLocale } from '../../helpers/numberFormatter';
import { InnerInput } from './APInput';
import BigNumber from 'bignumber.js';

const separators = ['.', ','];

class InnerNumberInput extends React.Component {
  // state.displayValue and props.input.value aren't in sync. Why:
  // props.input.value - redux-form value from reducer and it is source of truth.
  // state.displayValue - value that shown in input
  // Conditions when input.value and state.displayValue aren't in sync:
  // 1) user prints fractional number:
  // - Imagine user wants to enter 5.9
  //    User enters `5`: reducer (props.input.value) stores `5`, state.displayValue stores `5`
  //    User enters `.`: reducer stores `5` (!!!), state.displayValue stores `5.`
  //    User enters `9`: reducer stores `5.9`, state.displayValue stores `5.9`
  // 2) input change forced from outside by dispatching redux-form's change directly (examples: clearing order form, sync price and quantity)
  state = {
    displayValue: ''
  };

  componentDidUpdate(prevProps) {
    if (prevProps.input.value !== this.props.input.value) {
      // handle forced change based on difference between input.value and displayValue
      // exclude case with entering the separator symbol first
      // keep input.value and displayValue in sync otherwise

      if (
        this.props.input.value === '' ||
        (Number(this.state.displayValue) !== 0 &&
          (Number(this.props.input.value) === 0 ||
            Number.isNaN(Number(this.props.input.value))) &&
          !this.isSeparator(this.state.displayValue))
      ) {
        this.setState({
          displayValue: ''
        });
      } else if (!this.isSeparator(this.state.displayValue)) {
        this.setState({
          displayValue: new BigNumber(this.props.input.value).toFixed()
        });
      }
    }
  }

  handleChange = e => {
    // custom onChange to parse and validate input value
    // before calling standard onChange from redux-form and customChange (if exists)

    const { allowNegative, customChange } = this.props;
    const inputValue = e.target.value;

    const value = parseNumberToLocale(inputValue, null, !allowNegative);
    // prevent adding two dots or two commas
    const validSeparator =
      !inputValue.includes('..') && !inputValue.includes(',,');

    if (
      !Number.isNaN(value) &&
      this.validateDecimalPlaces(inputValue) &&
      validSeparator
    ) {
      this.setState({ displayValue: inputValue });
      this.props.input.onChange(value);

      if (customChange) {
        customChange(value);
      }
    }
  };

  handleFocus = e => {
    if (Number(this.state.displayValue) === 0) {
      this.setState({ displayValue: '' });
    }
  };

  validateDecimalPlaces = valueString => {
    const { decimalPlaces } = this.props;
    if (decimalPlaces >= 0) {
      // Handle multiple zeroes after the comma, valueString contains these
      const decimals = valueString.split('.')[1];

      return !decimals || decimals.length <= decimalPlaces;
    }

    return true;
  };

  isSeparator = value => separators.includes(value);

  render() {
    const {
      input,
      allowNegative,
      customChange,
      decimalPlaces,
      ...passedProps
    } = this.props;

    return (
      <InnerInput
        {...passedProps}
        autoComplete="off"
        type="number"
        step="any"
        input={{
          ...input,
          value: this.state.displayValue,
          onChange: this.handleChange,
          //call onBlur without args so the stored value is not changed
          onBlur: () => input.onBlur()
        }}
        onClick={() => {
          input.onFocus();
          this.handleFocus('onClick');
        }}
        onFocus={() => {
          input.onFocus();
          this.handleFocus('onFocus');
        }}
      />
    );
  }
}

const APNumberInput = props => (
  <Field name={props.name} component={InnerNumberInput} {...props} />
);

APNumberInput.defaultProps = {
  allowNegative: false
};

APNumberInput.propTypes = {
  allowNegative: PropTypes.bool,
  decimalPlaces: PropTypes.number
};

export default APNumberInput;

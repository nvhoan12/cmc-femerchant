import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import './APSwitch.css';

const Switch = (props, context) => {
  const { input, onClick, customClass } = props;
  const value = input.value;
  const modifier = { on: value };
  const text = value ? context.t('On') : context.t('Off');
  const bemClasses = getBEMClasses('ap-switch', customClass);

  const handleClick = () => {
    onClick(value);
    input.onChange(!value);
  };

  return (
    <div className={bemClasses('switch', modifier)} onClick={handleClick}>
      <div className={bemClasses('toggle', modifier)} />
      <div className={bemClasses('text', modifier)}>{text}</div>
    </div>
  );
};

Switch.contextTypes = {
  t: PropTypes.func.isRequired
};

const APSwitch = props => (
  <Field name={props.name} component={Switch} {...props} />
);

APSwitch.propTypes = {
  onClick: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  customClass: PropTypes.string
};

export default APSwitch;

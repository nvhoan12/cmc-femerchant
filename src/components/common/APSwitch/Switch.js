import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import './APSwitch.css';

const Switch = (props, context) => {
  const { value, onClick, customClass, disabled } = props;
  const modifier = { on: value };
  const text = value ? context.t('On') : context.t('Off');
  const bemClasses = getBEMClasses('ap-switch', customClass);

  return (
    <div
      className={clsx(
        bemClasses('switch', modifier),
        disabled && bemClasses('disabled')
      )}
      onClick={disabled ? () => {} : onClick}>
      <div className={bemClasses('toggle', modifier)} />
      <div className={bemClasses('text', modifier)}>{text}</div>
    </div>
  );
};

Switch.contextTypes = {
  t: PropTypes.func.isRequired
};

Switch.propTypes = {
  value: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  customClass: PropTypes.string
};

export default Switch;

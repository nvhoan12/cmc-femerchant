import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const BTZIcon = ({ iconName, size, className }) => {
  const iconSize = size ? `fa-${size}` : '';
  return (
    <i
      className={clsx('fa', iconName, iconSize, className)}
      aria-hidden="true"
    />
  );
};

BTZIcon.propTypes = {
  iconName: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['lg', '2x', '3x', '4x', '5x'])
};

export default BTZIcon;

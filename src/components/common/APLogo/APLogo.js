import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import logo from '../../../images/site-logo.png';
import './APLogo.css';

const APLogo = props => {
  const { customClass, linkTo } = props;
  const bemClasses = getBEMClasses('ap-logo', customClass);

  return (
    <Link to={linkTo} className={bemClasses()}>
      <img src={logo} className={bemClasses('img')} alt="Logo" />
    </Link>
  );
};

APLogo.defaultProps = {
  customClass: ''
};

APLogo.propTypes = {
  customClass: PropTypes.string
};

export default APLogo;

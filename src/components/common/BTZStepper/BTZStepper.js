import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import './BTZStepper.css';

const classes = getBEMClasses('btz-stepper');

const BTZStepper = props => {
  const { className, items, step } = props;

  return (
    <div className={clsx(classes(), className)}>
      {items.map(item => (
        <div key={item.step} className={classes('step-item')}>
          <div className="line" />
          <div
            className={clsx(
              classes('circle'),
              step === item.step ? 'active' : ''
            )}>
            {item.step}
          </div>
          <p
            className={clsx(
              classes('text'),
              step === item.step ? 'active' : ''
            )}>
            {item.title}
          </p>
        </div>
      ))}
    </div>
  );
};

BTZStepper.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      step: PropTypes.number,
      title: PropTypes.string
    })
  ),
  step: PropTypes.number
};

export default BTZStepper;

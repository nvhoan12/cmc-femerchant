import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import APIcon from '../APIcon';

const baseClass = 'ap-checkbox';

export const InnerCheckboxInput = props => {
  const {
    input,
    customClass,
    meta,
    inline,
    label,
    info,
    id,
    isChecked,
    ...passedProps
  } = props;

  const bemClasses = getBEMClasses([baseClass, customClass]);

  delete passedProps.meta;

  return (
    <div className={bemClasses('checkbox', inline && 'inline')}>
      <input
        className={bemClasses('checkbox')}
        type="checkbox"
        {...input}
        {...passedProps}
        id={id}
      />
      <label htmlFor={id}>
        {label}
        {input &&
          (input.value || isChecked) && (
            <APIcon name="checkbox" customClass={bemClasses('icon')} />
          )}
      </label>

      {info && (
        <small className={`form-text-muted ${bemClasses('checkbox-info')}`}>
          {info}
        </small>
      )}

      {meta.submitFailed &&
        meta.error && (
          <span className={`${bemClasses('checkbox-error')}`}>
            {meta.error}
          </span>
        )}
    </div>
  );
};

const APCheckbox = props => {
  return (
    <Field
      name={props.name}
      component={InnerCheckboxInput}
      {...props}
      type="checkbox"
    />
  );
};

APCheckbox.defaultProps = {
  name: '',
  label: '',
  autoFocus: false,
  required: false,
  readOnly: false,
  inline: false,
  info: '',
  isChecked: false
};

APCheckbox.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  customClass: PropTypes.string,
  autoFocus: PropTypes.bool,
  required: PropTypes.bool,
  readOnly: PropTypes.bool,
  inline: PropTypes.bool,
  info: PropTypes.string,
  id: PropTypes.string,
  isChecked: PropTypes.bool
};

export default APCheckbox;

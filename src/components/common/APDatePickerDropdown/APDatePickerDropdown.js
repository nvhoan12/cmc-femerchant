import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import { generateOptionsFromNumber } from '../../../helpers/formGeneratorHelper';

import './APDatePickerDropdown.css';
import { maxDate, minDate } from '../../../helpers/formValidations';

class APDatePickerDropdown extends React.Component {
  static propTypes = {
    input: PropTypes.object.isRequired,
    label: PropTypes.string,
    customClass: PropTypes.string,
    info: PropTypes.string,
    meta: PropTypes.shape({
      touched: PropTypes.bool,
      error: PropTypes.string
    }),
    disabled: PropTypes.bool,
    leadingZero: PropTypes.bool,
    minYear: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    maxYear: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    minDate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.instanceOf(Date)
    ]),
    maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)])
  };

  static contextTypes = {
    t: PropTypes.func
  };

  static defaultProps = {
    label: 'Date',
    customClass: 'custom-datepicker',
    info: '',
    disabled: false,
    leadingZero: true
  };

  constructor(props) {
    super(props);
    this.state = {
      date: new Date()
    };
  }

  componentDidMount() {
    const {
      input: { value }
    } = this.props;
    if (value) {
      const date = new Date(value);
      this.setState({ date });
    }
  }

  handleChange = date => {
    this.props.input.onChange(date);

    if (this.props.customChange) {
      this.props.customChange(date);
    }
    this.setState({ date: new Date(date) });
  };

  getClasses = () => {
    const { customClasses } = this.props;
    return getBEMClasses([
      'ap-datepicker-dropdown',
      'ap-select',
      customClasses
    ]);
  };

  getDaysInMonth(date) {
    return new Date(date.getYear(), date.getMonth() + 1, 0).getDate();
  }

  getDays = () => {
    const { date } = this.state;
    const { leadingZero } = this.props;
    const daysCount = this.getDaysInMonth(date);

    return generateOptionsFromNumber(1, daysCount + 1, leadingZero ? 2 : 0);
  };

  getMonths = () => {
    const months = [
      { value: 0, text: this.context.t('January') },
      { value: 1, text: this.context.t('February') },
      { value: 2, text: this.context.t('March') },
      { value: 3, text: this.context.t('April') },
      { value: 4, text: this.context.t('May') },
      { value: 5, text: this.context.t('June') },
      { value: 6, text: this.context.t('July') },
      { value: 7, text: this.context.t('August') },
      { value: 8, text: this.context.t('September') },
      { value: 9, text: this.context.t('October') },
      { value: 10, text: this.context.t('November') },
      { value: 11, text: this.context.t('December') }
    ];

    return months.map(month => (
      <option key={month.value} value={month.value}>
        {month.text}
      </option>
    ));
  };

  getYears = () => {
    let { minYear, maxYear, leadingZero } = this.props;
    const today = new Date();

    minYear = minYear || 1900;
    maxYear = maxYear || today.getFullYear();

    return generateOptionsFromNumber(
      minYear,
      maxYear + 1,
      leadingZero ? 2 : 0
    ).reverse();
  };

  onDayChange = e => {
    const { value } = e.target;
    const { date } = this.state;

    date.setDate(+value);
    this.handleChange(date);
  };
  onMonthChange = e => {
    const { value } = e.target;
    const { date } = this.state;
    const currentDays = date.getDate();

    // If user's selected month has less days than the actual month we set the day to the latest in that given month
    // else month will change to next because of the days difference
    date.setDate(1);
    date.setMonth(value);

    const monthDays = this.getDaysInMonth(date);

    if (currentDays > monthDays) date.setDate(monthDays);

    this.handleChange(date);
  };
  onYearChange = e => {
    const { value } = e.target;
    const { date } = this.state;

    date.setFullYear(value);
    this.handleChange(date);
  };

  getCurrentValue = () => {
    const { date } = this.state;

    return {
      day: date.getDate(),
      month: date.getMonth(),
      year: date.getFullYear()
    };
  };

  render() {
    const {
      label,
      meta,
      info,
      customClass,
      leadingZero,
      minYear,
      maxYear,
      minDate,
      maxDate,
      ...otherProps
    } = this.props;
    const baseClasses = this.getClasses();
    const value = this.getCurrentValue();

    return (
      <div className={`form-group ${baseClasses('select-wrapper')}`}>
        <label className={`ap--label ${baseClasses('label')}`}>{label}</label>
        <div className={baseClasses('input')}>
          <select
            value={value.day}
            className={baseClasses('select')}
            onChange={this.onDayChange}
            {...otherProps}>
            {this.getDays()}
          </select>
          <select
            value={value.month}
            className={baseClasses('select')}
            onChange={this.onMonthChange}
            {...otherProps}>
            {this.getMonths()}
          </select>
          <select
            value={value.year}
            className={baseClasses('select')}
            onChange={this.onYearChange}
            {...otherProps}>
            {this.getYears()}
          </select>
        </div>
        <div className={`form-text-muted ${baseClasses('info')}`}>{info}</div>
        {meta.dirty &&
          meta.error && (
            <div className={baseClasses('error', 'dpk-error')}>
              {meta.error}
            </div>
          )}
      </div>
    );
  }
}

let minDateProp;
let maxDateProp;

const maxDateValidation = value =>
  maxDateProp && maxDate(value, new Date(maxDateProp));
const minDateValidation = value =>
  minDateProp && minDate(value, new Date(minDateProp));

const maxMinValidations = [maxDateValidation, minDateValidation];

export default function APDatePicker(props) {
  minDateProp = props.minDate;
  maxDateProp = props.maxDate;
  const { validate, ...otherProps } = props;
  const validators = validate
    ? [...validate, ...maxMinValidations]
    : maxMinValidations;

  return (
    <Field
      {...otherProps}
      component={APDatePickerDropdown}
      validate={validators}
      name={props.name}
    />
  );
}

export { APDatePickerDropdown };

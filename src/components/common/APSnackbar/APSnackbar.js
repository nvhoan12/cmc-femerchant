import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import Snackbar from '../Snackbar/Snackbar';
import config from '../../../config/';
import './APSnackbar.css';

class APSnackbar extends Component {
  static defaultProps = {
    timeout: config.Snackbar.timeout,
    snacks: []
  };

  static propTypes = {
    timeout: PropTypes.number,
    snacks: PropTypes.arrayOf(
      PropTypes.shape({
        timeout: PropTypes.number,
        text: PropTypes.string.isRequired,
        textVars: PropTypes.object,
        id: PropTypes.string.isRequired,
        type: PropTypes.oneOf(['info', 'success', 'warning']),
        customClass: PropTypes.string,
        onClose: PropTypes.func
      })
    ).isRequired
  };

  bemClasses = getBEMClasses(['ap-snackbar']);

  closeSnack(snack) {
    this.props.dismissSnackByUniqueId(snack.uniqueId);
    snack.onClose && snack.onClose();
  }

  // context.t('100: Invalid Request: Creating new withdraw ticket requires 2FA to be turned on and login using it!')
  // context.t('101: Operation Failed: Exceeds_Daily_Deposit_Limit')

  getSnackElement(snack, idx = 0) {
    let timeout = config.Snackbar.timeout;
    if (snack.timeout) {
      timeout = snack.timeout;
    }
    if (/(deposit|withdraw)/i.test(snack.id)) {
      timeout = config.Snackbar.depositWithdrawTimeout;
    }
    const animationTimeout = Math.round(timeout / 1000);

    setTimeout(() => this.closeSnack(snack), timeout);

    return (
      <div key={idx} className={this.bemClasses('snack')}>
        <Snackbar
          {...snack}
          onClose={() => this.closeSnack(this.props.snacks[0])}
        />
        {config.Snackbar.showProgressBar && (
          <div
            className={this.bemClasses('snack-progress')}
            style={{ animationDuration: `${animationTimeout}s` }}
          />
        )}
      </div>
    );
  }

  render() {
    return ReactDOM.createPortal(
      <div className={this.bemClasses()}>
        {config.Snackbar.showMultiple &&
          this.props.snacks.map((snack, idx) =>
            this.getSnackElement(snack, idx)
          )}
        {!config.Snackbar.showMultiple &&
          this.props.snacks[0] &&
          this.getSnackElement(this.props.snacks[0])}
      </div>,
      document.body
    );
  }
}

export default APSnackbar;

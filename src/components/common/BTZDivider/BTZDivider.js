import React from 'react';
import clsx from 'clsx';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import './BTZDivider.css';

const classes = getBEMClasses('btz-divider');

const BTZDivider = ({ className, dark }) => {
  return (
    <div className={clsx(classes(), dark && classes('dark'), className)} />
  );
};

export default BTZDivider;

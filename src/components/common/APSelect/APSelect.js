import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import './APSelect.css';

export const InnerSelect = props => {
  const {
    input,
    meta,
    classModifiers,
    customClass,
    info,
    label,
    onSelect,
    showTriangles,
    options,
    showLabel = true,
    ...passedProps
  } = props;

  const baseClasses = getBEMClasses(['ap-select', customClass]);

  return (
    <div
      className={`form-group ${baseClasses('select-wrapper', classModifiers)}`}>
      {showLabel && (
        <label className={`ap--label ${baseClasses('select-label')}`}>
          {label}
        </label>
      )}

      {showTriangles ? (
        <div className={baseClasses('triangles-container')}>
          <span className={'triangle-up'} />
          <span className={'triangle-down'} />
        </div>
      ) : (
        ''
      )}

      <select
        className={`form-control ${baseClasses('select')}`}
        {...input}
        {...passedProps}
        onChange={e => {
          if (onSelect) onSelect(e.target.value);
          input && input.onChange(e);
        }}>
        {options.map(i => {
          return (
            <option key={i.value} value={i.value} disabled={i.disabled}>
              {i.label}
            </option>
          );
        })}
      </select>

      <small className={`form-text-muted ${baseClasses('select-info')}`}>
        {info}
      </small>
      {meta &&
        meta.dirty &&
        meta.error && (
          <span className={baseClasses('select-error')}>{meta.error}</span>
        )}
    </div>
  );
};

const APSelect = props => (
  <Field name={props.name} component={InnerSelect} {...props} />
);

APSelect.defaultProps = {
  name: '',
  label: '',
  classes: 'input',
  customClass: 'custom-select',
  classModifiers: '',
  info: '',
  autoFocus: '',
  disabled: false,
  multiple: false,
  required: false,
  showLabel: true,
  options: []
};

APSelect.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  classes: PropTypes.string,
  customClass: PropTypes.string,
  classModifiers: PropTypes.string,
  info: PropTypes.string,
  autoFocus: PropTypes.string,
  onSelect: PropTypes.func,
  disabled: PropTypes.bool,
  multiple: PropTypes.bool,
  required: PropTypes.bool,
  showLabel: PropTypes.bool
};

export default APSelect;

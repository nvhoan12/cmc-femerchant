import React from 'react';
import PropTypes from 'prop-types';
import APUpload from '../APUpload/APUpload';

import '../APUpload/APUpload.css';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import { maxFileSize } from '../../../helpers/formValidations';

const baseClass = 'ap-image-upload';

class APDocumentUpload extends React.Component {
  static defaultProps = {
    name: '',
    customClass: '',
    placeholder: '',
    label: '',
    value: ''
  };

  static propTypes = {
    name: PropTypes.string,
    customClass: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    required: PropTypes.bool
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  maxSize = value => {
    const { maxSize } = this.props;
    if (maxSize) return maxFileSize(value, maxSize);
  };

  getValidators = () => {
    const { validate } = this.props;
    return validate ? [...validate, this.maxSize] : [this.maxSize];
  };

  render() {
    const { customClass, ...otherProps } = this.props;
    const bemClasses = getBEMClasses(baseClass, customClass);

    return (
      <APUpload
        {...otherProps}
        customClass={bemClasses()}
        acceptedFiles={'.pdf, .doc, .docx'}
        validate={this.getValidators()}
      />
    );
  }
}

APDocumentUpload.defaultProps = {
  name: '',
  label: '',
  classModifiers: '',
  placeholder: '',
  info: '',
  autoFocus: '',
  disabled: false,
  required: false,
  maxSize: null
};

APDocumentUpload.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  customClass: PropTypes.string,
  classModifiers: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  placeholder: PropTypes.string,
  info: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  autoFocus: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool
};

export default APDocumentUpload;

import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import SidePaneContainer from './SidePaneContainer';

jest.mock('../../../redux/actions/sidePaneActions', () => {
  return {
    closeSidePane: jest.fn(payload => ({ type: 'CLOSE_SIDE_PANE', payload }))
  };
});

jest.mock('./SidePaneComponent', () => {
  return () => {
    return <div className="component">Content</div>;
  };
});

describe('SidePaneContainer', () => {
  const mockStore = configureStore();

  const state = {
    sidePane: {
      isOpen: true,
      name: 'sidePane1'
    }
  };

  let store;

  const getSubject = options => {
    store = mockStore(state);
    return mount(<SidePaneContainer store={store} />);
  };

  beforeEach(() => {
    store = mockStore(state);
  });

  it('should pass close action to component that dispatches close action', () => {
    const sidePaneName = 'sidePane1';
    const subject = getSubject({ sidePaneName });

    subject.childAt(0).prop('close')(sidePaneName);
    expect(store.getActions()).toContainEqual({
      type: 'CLOSE_SIDE_PANE',
      payload: 'sidePane1'
    });
  });
});

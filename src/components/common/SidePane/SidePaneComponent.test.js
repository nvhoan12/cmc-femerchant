import React from 'react';
import { mount } from 'enzyme';
import SidePane from './SidePaneComponent';
import { SIDE_PANES } from '../../../constants/sidePanesConstants';

jest.mock(
  '../../ApiKeysSidePaneComponents/ApiKeysSidePane/ApiKeysSidePaneComponent',
  () => {
    return () => {
      return <div className="component" />;
    };
  }
);
jest.mock('../APIcon', () => {
  return () => {
    return <div className="ap-icon" />;
  };
});

describe('SidePane', () => {
  const buildSubject = (props = {}) => {
    const componentProps = {
      isOpen: true,
      close: () => {},
      name: SIDE_PANES.API_KEYS,
      ...props
    };
    return mount(<SidePane {...componentProps} />, {
      context: { t: () => {} }
    });
  };

  describe('close icon', () => {
    it('should render close icon', () => {
      const subject = buildSubject();
      expect(subject.find('.ap-icon').length).toBe(1);
    });
  });
});

import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import './BTZButton.css';

const classes = getBEMClasses('btz-button');

const BTZButton = props => {
  const {
    children,
    className,
    variant,
    size,
    fullWidth,
    disabled,
    ...otherProps
  } = props;
  const classesName = clsx(
    classes(),
    classes('contain'),
    classes('medium'),
    variant === 'outline' && classes('outline'),
    variant === 'link' && classes('link'),
    size === 'small' && classes('small'),
    size === 'large' && classes('large'),
    fullWidth && classes('full-width'),
    disabled && 'disabled',
    className
  );
  return (
    <button className={classesName} disabled={disabled} {...otherProps}>
      {children}
    </button>
  );
};

BTZButton.defaultProps = {
  variant: 'contain',
  size: 'medium',
  fullWidth: false,
  disabled: false
};

BTZButton.propTypes = {
  variant: PropTypes.oneOf('contained', 'outlined', 'link'),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool
};

export default BTZButton;

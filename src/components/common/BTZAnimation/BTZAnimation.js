import React from 'react';
import { useCountUp } from 'react-countup';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import './BTZAnimation.css';

const classes = getBEMClasses('btz-animation');

const BTZAnimationTextScale = ({ text }) => {
  const [play, setPlay] = React.useState(false);
  React.useEffect(() => setPlay(true), [text]);
  return (
    <span
      onAnimationEnd={() => setPlay(false)}
      className={play ? classes('scale') : ''}>
      {text}
    </span>
  );
};

const BTZAnimationNumberCount = ({ number, decimals }) => {
  const { countUp, update } = useCountUp({
    start: number || 0,
    end: number || 0,
    duration: 1,
    decimals: decimals
  });
  React.useEffect(
    () => {
      if (typeof number === 'number') {
        update(number);
      }
    },
    [number]
  );
  return <span>{formatNumberToLocale(countUp, decimals)}</span>;
};

export { BTZAnimationTextScale, BTZAnimationNumberCount };

import React from 'react';
import clsx from 'clsx';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import './BTZPanel.css';

const classes = getBEMClasses('btz-panel');

const BTZPanel = ({ children, bg, className, ...props }) => {
  return (
    <div
      className={clsx(
        classes(''),
        bg === 'no' && classes('no-background'),
        bg === 'secondary' && classes('secondary-background'),
        className
      )}
      {...props}>
      {children}
    </div>
  );
};

export default BTZPanel;

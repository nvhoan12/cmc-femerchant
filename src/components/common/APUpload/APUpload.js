import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import APButton from '../APButton';
import APIcon from '../APIcon';

import { readFile } from '../../../helpers/fileHelpers';
import { maxFileSize } from '../../../helpers/formValidations';

import './APUpload.css';
const baseClass = 'ap-upload';

class APUploadInner extends React.Component {
  static defaultProps = {
    name: '',
    customClass: '',
    placeholder: '',
    label: '',
    value: ''
  };

  static propTypes = {
    name: PropTypes.string,
    customClass: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    required: PropTypes.bool
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      file: null,
      hasFocused: false,
      focused: false,
      hasFile: false
    };
  }

  onChange = e => {
    const {
      input: { name },
      customChange
    } = this.props;

    const file = e.target.files[0];

    if (file) {
      readFile(file, imgSrc => {
        customChange(name, { file, imgSrc });
        setTimeout(() => this.setState({ file, imgSrc, hasFile: true }));
      });
    } else {
      this.setState({ file: null, imgSrc: null, hasFile: false });
      customChange(name, null);
    }
  };

  clearField = () => {
    const {
      input: { onChange }
    } = this.props;

    this.setState({ file: null, imgSrc: null });
    onChange(null);
  };

  onFocus = () => {
    this.setState({ focused: true, hasFocused: true });
  };

  onBlur = () => {
    this.setState({ focused: false });
  };

  isImage = fileName => {
    const parts = fileName.split('.');
    const ext = parts[parts.length - 1];

    return ['jpg', 'jpeg', 'png', 'gif'].includes(ext);
  };

  render() {
    const {
      classModifiers,
      customClass,
      info,
      placeholder,
      label,
      acceptedFiles,
      meta: { error },
      alt
    } = this.props;
    const { file, hasFocused, focused, hasFile, imgSrc } = this.state;
    const bemClasses = getBEMClasses(baseClass, customClass);

    return (
      <div className={bemClasses('', { hasFile })}>
        <div
          className={`form-group ${bemClasses(
            'wrapper',
            `${classModifiers} ${hasFile ? 'hasFile' : ''}`
          )}`}>
          <div className={bemClasses('children', classModifiers)}>
            {file &&
              this.isImage(file.name) && (
                <div className={bemClasses('img-wrapper')}>
                  <img
                    src={imgSrc}
                    className={bemClasses('preview')}
                    alt={alt}
                  />
                </div>
              )}
          </div>
          <div
            className={`${bemClasses('label', classModifiers)} ${
              hasFile ? bemClasses('label', ['hasFile']) : ''
            }`}>
            {file ? file.name : placeholder}
            {file && (
              <span
                className={bemClasses('cancel-btn', classModifiers)}
                onClick={this.clearField}>
                <APIcon
                  name="cancel"
                  classModifiers="danger"
                  customClass={bemClasses('image')}
                />
              </span>
            )}
          </div>
          <label className={bemClasses('input-box')}>
            <APButton customClass={bemClasses('btn')} type="button">
              {label}
            </APButton>
            <input
              type="file"
              onChange={this.onChange}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              className={bemClasses('input')}
              value={''}
              accept={acceptedFiles}
            />
          </label>
        </div>
        {info && (
          <div className={`form-text-muted ${bemClasses('info')}`}>{info}</div>
        )}

        {(hasFocused || focused) &&
          hasFile &&
          !file &&
          error && (
            <div className={bemClasses('error')}>{this.context.t(error)}</div>
          )}
      </div>
    );
  }
}

const APUpload = props => {
  const maxSize = value => {
    const { maxSize } = props;
    if (maxSize) return maxFileSize(value, maxSize);
  };

  let validators = props.validate ? [...props.validate, maxSize] : [maxSize];

  return (
    <Field
      component={APUploadInner}
      {...props}
      validate={validators}
      name={props.name}
    />
  );
};

APUpload.defaultProps = {
  name: '',
  label: '',
  classModifiers: '',
  placeholder: '',
  info: '',
  autoFocus: '',
  disabled: false,
  required: false,
  maxSize: null
};

APUpload.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  customClass: PropTypes.string,
  classModifiers: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  placeholder: PropTypes.string,
  info: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  autoFocus: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  customChange: PropTypes.func
};

export default APUpload;

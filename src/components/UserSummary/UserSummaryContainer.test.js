import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { connect } from 'react-redux';
import UserSummaryContainer from './UserSummaryContainer';
import UserSummary from './UserSummaryComponent';
import { logout } from '../../redux/actions/authActions';
import { selectAccount } from '../../redux/actions/userActions';

jest.mock('./UserSummaryComponent', () => jest.fn().mockReturnValue(<div />));

jest.mock('../../redux/actions/authActions', () => {
  return {
    logout: jest.fn().mockReturnValue({ type: 'LOGOUT' })
  };
});

jest.mock('../../redux/actions/userActions', () => {
  return {
    selectAccount: jest.fn().mockReturnValue({ type: 'SELECT_ACCOUNT' })
  };
});

describe('UserSummaryContainer', () => {
  const initialState = {
    user: {
      accounts: [{ AccountId: 1 }, { AccountId: 2 }],
      selectedAccountId: 1
    }
  };
  const mockStore = configureStore();
  let store, container;

  beforeEach(() => {
    jest.clearAllMocks();
    store = mockStore(initialState);
    container = mount(<UserSummaryContainer store={store} />);
  });

  it('should render UserSummary component', () => {
    expect(UserSummary).toHaveBeenCalled();
  });

  it('should pass accounts prop', () => {
    expect(container.childAt(0).prop('accounts')).toEqual(
      initialState.user.accounts
    );
  });

  it('should pass selectedAccount prop', () => {
    expect(container.childAt(0).prop('selectedAccount')).toEqual(
      initialState.user.accounts[0]
    );
  });

  it('should pass user logout', () => {
    container.childAt(0).prop('logout')();

    expect(logout).toHaveBeenCalled();
  });

  it('should pass selectAccount', () => {
    container.childAt(0).prop('selectAccount')();

    expect(selectAccount).toHaveBeenCalled();
  });
});

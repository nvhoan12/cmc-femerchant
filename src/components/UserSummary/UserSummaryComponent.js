import React from 'react';
import PropTypes from 'prop-types';
import PopoverMenu from 'apex-web/lib/components/common/PopoverMenu/PopoverMenu';
import APIcon from 'apex-web/lib/components/common/APIcon';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

import './UserSummaryComponent.css';

class UserSummary extends React.Component {
  static propTypes = {
    selectedAccount: PropTypes.shape({
      AccountName: PropTypes.string.isRequired
    }),
    accounts: PropTypes.array.isRequired,
    logout: PropTypes.func.isRequired,
    history: PropTypes.object,
    settingsRoute: PropTypes.string,
    activityRoute: PropTypes.string
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  static defaultProps = {
    settingsRoute: ''
  };

  renderMenuItems = () => {
    const {
      accounts,
      selectedAccount,
      history,
      settingsRoute
      // activityRoute
    } = this.props;

    const menuItems = accounts
      .filter(() => {
        if (this.props.role.name === 'broker') {
          return true; // if user is broker then include other account
        }
        return false; // not include other account
      })
      .filter(account => account.AccountId !== selectedAccount.AccountId)
      .map(account => {
        return {
          label: this.context.t('Switch to {accountName}', {
            accountName: account.AccountName
          }),
          onClick: () =>
            this.props.selectAccount(account.AccountId, account.OMSID)
        };
      });

    menuItems.push(
      { divider: true },
      { label: this.context.t('Sign Out'), onClick: this.props.logout }
    );

    if (settingsRoute) {
      menuItems.unshift({
        label: this.context.t('Settings'),
        onClick: () => history.push(settingsRoute)
      });
    }
    // if (activityRoute) {
    //   menuItems.unshift({
    //     label: this.context.t('Activity'),
    //     onClick: () => history.push(activityRoute)
    //   });
    // }

    return menuItems;
  };

  render() {
    const { selectedAccount, customClass } = this.props;
    const bemClasses = getBEMClasses(['user-summary', customClass]);

    return (
      <div className={bemClasses('container')}>
        {selectedAccount ? (
          <PopoverMenu
            customClass={bemClasses()}
            popoverProps={{
              trigger: props => (
                <button
                  className={bemClasses('popover-menu-trigger')}
                  {...props}>
                  <span className={bemClasses('user-info-container')}>
                    <APIcon name="user" customClass={bemClasses('icon')} />
                    <span className={bemClasses('username-in-display')}>
                      {/* {selectedAccount.AccountName} */}
                      {/* remove th_ for now */}
                      {this.props.role.name === 'broker'
                        ? selectedAccount.AccountName
                        : selectedAccount.AccountName.replace('th_', '')}
                    </span>
                    <span
                      className={bemClasses('popover-menu-trigger-triangle')}
                    />
                  </span>
                </button>
              ),
              customClass: 'user-summary'
            }}
            items={this.renderMenuItems()}
          />
        ) : (
          <Spinner isInline customClass={bemClasses} />
        )}
      </div>
    );
  }
}

export default UserSummary;

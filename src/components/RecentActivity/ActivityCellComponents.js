import React from 'react';
import PropTypes from 'prop-types';

import APIcon from 'apex-web/lib/components/common/APIcon';
import ProductIconContainer from 'apex-web/lib/components/common/ProductIcon/ProductIconContainer';
import CancelAPButton from 'apex-web/lib/components/common/CancelAPButton';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { formatDate, formatTime } from 'apex-web/lib/helpers/dateHelper';
import BTZIcon from '../common/BTZIcon/BTZIcon';

const baseClasses = getBEMClasses('activity');

export const Action = (props, context) => {
  const { typeIcon, primaryText, secondaryText, params } = props.value;
  const { showProductIcon, themeModifier } = props;

  return (
    <div className={baseClasses('action')}>
      <APIcon name={typeIcon} customClass={baseClasses('type-icon')} />
      {showProductIcon && (
        <ProductIconContainer
          customClass={baseClasses('product-icon', themeModifier)}
          iconFileName={props.value.productIcon.iconFileName}
          size={36}
        />
      )}
      <div className={baseClasses('action-column')}>
        <div className={baseClasses('action-text', 'primary')}>
          {context.t(primaryText, params)}
        </div>
        <div className={baseClasses('action-text', 'secondary')}>
          {context.t(secondaryText, params)}
        </div>
      </div>
    </div>
  );
};

export const Amount = props => {
  const { Quantity, Value, Product1Symbol, Product2Symbol } = props.value;
  return (
    <div className={baseClasses('amount')}>
      <div className={baseClasses('amount-quantity')}>{`${formatNumberToLocale(
        Quantity,
        Product1Symbol
      )} ${Product1Symbol}`}</div>
      <div className={baseClasses('amount-value')}>{`${formatNumberToLocale(
        Value,
        Product2Symbol
      )} ${Product2Symbol}`}</div>
    </div>
  );
};

export const AmountValue = props => {
  const { Value, ProductSymbol } = props.value;
  return (
    <div className={baseClasses('amount')}>
      <div className={baseClasses('amount-quantity')}>
        {Value === 0
          ? '-'
          : `${formatNumberToLocale(Value, ProductSymbol)} ${ProductSymbol}`}
      </div>
    </div>
  );
};

export const Status = (props, context) => {
  const { status, id } = props.value;
  const { customClass } = props;
  return (
    <div className={`${customClass} ${baseClasses('status')}`}>
      <div className={baseClasses('status-id')}>{id}</div>
      {/* TO-DO : we should use something else for context --> t(status). auto-translation will not pick this.*/}
      <div className={baseClasses('status-text')}>{context.t(status)}</div>
    </div>
  );
};

export const DateTime = props => {
  const { raw } = props.value;
  return (
    <div className={baseClasses('date')}>
      <div className={baseClasses('date-date')}>{formatDate(raw)}</div>
      <div className={baseClasses('date-time')}>{formatTime(raw)}</div>
    </div>
  );
};

export const OrderType = (props, context) => {
  const orderType = props.value;
  return (
    <div className={baseClasses('order-type')}>{context.t(orderType)}</div>
  );
};

export const Quantity = props => {
  const { OrigQuantity, Product1Symbol } = props.value;
  return (
    <div className={baseClasses('quantity')}>
      {formatNumberToLocale(OrigQuantity, OrigQuantity.symbol)}
      <div className={baseClasses('quantity-text')}>{Product1Symbol}</div>
    </div>
  );
};

export const DepositWithdrawQuantity = props =>
  Quantity({
    value: {
      OrigQuantity: props.value.Quantity,
      Product1Symbol: props.value.Product1Symbol
    }
  });

export const Price = props => {
  const { Price, Product2Symbol } = props.value;
  return (
    <div className={baseClasses('price')}>
      {formatNumberToLocale(Price, Price.symbol)} {Product2Symbol}
    </div>
  );
};

export const CancelAction = (props, context) => {
  const orderId = props.value;
  const cancelOrders = props.action;
  return (
    <CancelAPButton
      text={context.t('Cancel Order')}
      customClass={baseClasses()}
      onClick={() => {
        cancelOrders([orderId]);
      }}
    />
  );
};

export const Text = (props, context) => {
  const { primaryText, secondaryText } = props.value;
  return (
    <div className={baseClasses('text')}>
      <div className={baseClasses('text', 'primary')}>
        {context.t(primaryText)}
      </div>
      <div className={baseClasses('text', 'secondary')}>
        {context.t(secondaryText)}
      </div>
    </div>
  );
};

export const Product = (props, context) => {
  const { primaryText, iconFileName } = props.value;
  return (
    <div className={baseClasses('text')}>
      <div className={baseClasses('text', 'primary')}>
        <span className="mr-2">
          <ProductIconContainer iconFileName={iconFileName} size={21} />
        </span>
        &nbsp;{primaryText}
      </div>
    </div>
  );
};

export const invoiceStatusParser = status => {
  if (status === 'new') {
    return { text: 'New', iconName: 'fa-circle' };
  }
  if (status === 'pending') {
    return { text: 'Pending', iconName: 'fa-spinner fa-spin' };
  }
  if (status === 'confirm') {
    return {
      text: 'Paid',
      iconName: 'fa-check-circle',
      className: baseClasses('text', 'success')
    };
  }
  if (status === 'expired') {
    return {
      text: 'Expired',
      iconName: 'fa-times-circle',
      className: baseClasses('text', 'danger')
    };
  }
  if (status === 'incorrect_amount') {
    return {
      text: 'Incorrect Amount',
      iconName: 'fa-exclamation-circle',
      className: baseClasses('text', 'warning')
    };
  }
  return { text: 'Unknown', iconName: 'fa-question-circle-o' };
};

export const InvoiceStatus = (props, context) => {
  const { Value } = props.value;
  const { text, iconName, className } = invoiceStatusParser(Value);
  return (
    <div className={baseClasses('text')}>
      <div className={baseClasses('text', 'primary')}>
        <span className="mr-2">
          <BTZIcon iconName={iconName} className={className} />
        </span>
        &nbsp;{context.t(text)}
      </div>
    </div>
  );
};

Action.contextTypes = {
  t: PropTypes.func.isRequired
};

Status.contextTypes = {
  t: PropTypes.func.isRequired
};

Status.defaultProps = {
  customClass: ''
};

OrderType.contextTypes = {
  t: PropTypes.func.isRequired
};

CancelAction.contextTypes = {
  t: PropTypes.func.isRequired
};

Text.contextTypes = {
  t: PropTypes.func.isRequired
};

Product.contextTypes = {
  t: PropTypes.func.isRequired
};

InvoiceStatus.contextTypes = {
  t: PropTypes.func.isRequired
};

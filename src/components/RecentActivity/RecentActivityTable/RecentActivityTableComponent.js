import React from 'react';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { formatDate, formatTime } from 'apex-web/lib/helpers/dateHelper';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APTable from 'apex-web/lib/components/common/APTable';

const baseClasses = getBEMClasses('activity');

const getBlockchairLink = (infar, txid) => {
  if (infar === 'BTC') {
    return `https://blockchair.com/bitcoin/transaction/${txid}`;
  }
  if (['ETH', 'DAI', 'COM', 'USDT'].includes(infar)) {
    return `https://blockchair.com/ethereum/transaction/${txid}`;
  }
  if (infar === 'XRP') {
    return `https://blockchair.com/ripple/transaction/${txid}`;
  }
  if (infar === 'XLM') {
    return `https://blockchair.com/stellar/transaction/${txid}`;
  }
};

const RecentActivityTableComponent = props => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [row, setRow] = React.useState({});
  const { columns, content, context, usePagination, pageSize, minRow } = props;

  const handleClickRow = row => {
    if (row.Action.params.ProductType === 'CryptoCurrency') {
      setRow(row);
      setIsOpen(true);
    }
  };

  return (
    <React.Fragment>
      <APTable
        {...{
          componentName: 'RecentActivityComponent',
          columns,
          pageSize,
          minRow,
          rows: content,
          onRowClicked: handleClickRow,
          baseClass: baseClasses,
          headerOutside: true,
          usePagination: usePagination,
          empty: context.t('No data is available')
        }}
      />
      {isOpen && (
        <Modal
          isOpen={isOpen}
          title={row.Action.primaryText}
          close={() => setIsOpen(false)}>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Amount')}</label>
            <span>
              {`${formatNumberToLocale(
                row.Amount.Quantity,
                row.Amount.Product1Symbol
              )} ${row.Amount.Product1Symbol}`}
            </span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Status')}</label>
            <span>{context.t(row.Status.status)}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Date')}</label>
            <span>{formatDate(row.TimeStamp.raw)}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Time')}</label>
            <span>{formatTime(row.TimeStamp.raw)}</span>
          </div>
          {row.Status.id && (
            <div className={baseClasses('modal-txid')}>
              <label>{context.t('TxID')}</label>
              <span>
                <a
                  rel="noopener noreferrer"
                  target="_blank"
                  href={getBlockchairLink(
                    row.Action.params.Product,
                    row.Status.id
                  )}>
                  {row.Status.id}
                </a>
              </span>
            </div>
          )}
        </Modal>
      )}
    </React.Fragment>
  );
};

RecentActivityTableComponent.defaultProps = {
  usePagination: false,
  pageSize: 6,
  minRow: 6
};

export default RecentActivityTableComponent;

import { connect } from 'react-redux';
import RecentActivityTableComponent from './RecentActivityTableComponent';
import {
  formatActivity,
  getColumns
} from '../../../helpers/recentActivityHelper';
import { getFilteredActivity } from '../../../redux/selectors/recentActivitySelector';

const mapStateToProps = (state, ownProps) => {
  const filteredActivity = getFilteredActivity(
    state,
    ownProps.filterForSelected
  );
  const content = formatActivity(
    filteredActivity.slice(0, 6),
    ownProps.context
  );

  const columns = getColumns(
    ownProps.width,
    ownProps.themeModifier,
    ownProps.context
  );

  return {
    columns,
    content
  };
};

export default connect(mapStateToProps)(RecentActivityTableComponent);

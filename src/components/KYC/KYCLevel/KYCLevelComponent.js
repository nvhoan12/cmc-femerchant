import React from 'react';
import PropTypes from 'prop-types';
import APIcon from '../../common/APIcon/APIcon';
import APButton from '../../common/APButton';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import { getlocalLanguage } from '../../../helpers/localLanguage';
import { kycConfigLevelShape } from '../../../propTypes/kycConfigShapePropTypes';

import './KYCLevelComponent.css';

const kycClasses = getBEMClasses('kyc-level');

class KYCLevelComponent extends React.Component {
  render() {
    const {
      verified,
      underReview,
      canIncrease,
      currentLevel,
      openKYCViewDetailSidePane,
      highlightStyle,
      config: {
        level,
        label,
        button,
        verifiedMessage,
        underReviewMessage,
        description: { benefits, requirements }
      }
    } = this.props;

    const activeCard = currentLevel ? `selected` : '';
    const language = getlocalLanguage();

    return (
      <div className={kycClasses(null, activeCard)}>
        <div className={kycClasses('header-title', activeCard)}>
          {this.context.t(label)}
        </div>

        {highlightStyle === 'star' &&
          currentLevel && (
            <APIcon name="star" className={kycClasses('star-icon')} />
          )}

        <div className={kycClasses('body')}>
          {benefits &&
            benefits.length > 0 && (
              <div className={kycClasses('benefits')}>
                <div className={kycClasses('sub-title', activeCard)}>
                  {this.context.t('Benefits')}
                </div>
                <ul className={kycClasses('list')}>
                  {benefits.map((benefit, idx) => (
                    <li key={idx} className={kycClasses('list-item')}>
                      {this.context.t(benefit)}
                    </li>
                  ))}
                </ul>
              </div>
            )}

          <div className={kycClasses('requirements')}>
            <div className={kycClasses('sub-title', activeCard)}>
              {this.context.t('Requirements')}
            </div>
            {requirements.map((item, i) => (
              <div key={i} className={kycClasses('item-description')}>
                {this.context.t(item)}
              </div>
            ))}
          </div>

          {level === 1 && (
            <div className={kycClasses('kyc-video')}>
              <a
                className="font-weight-bold"
                href="https://www.youtube.com/watch?v=RTAVBGS5FkE&feature=youtu.be"
                target="_blank"
                rel="noopener noreferrer">
                {this.context.t('How to KYC video')}
              </a>
            </div>
          )}
        </div>

        {verified && (
          <div className={kycClasses('footer', 'verified')}>
            <div>
              <APIcon
                name="simple-check"
                customClass={kycClasses('verified-icon')}
              />
              {this.context.t(verifiedMessage)}
            </div>
            {level === 1 && (
              <div
                className={kycClasses('footer', 'verified-view')}
                onClick={openKYCViewDetailSidePane}>
                {this.context.t('KYC details')}
              </div>
            )}
          </div>
        )}

        {underReview && (
          <div className={kycClasses('footer', 'under-review')}>
            <APIcon
              name="pending"
              customClass={kycClasses('under-review-icon')}
            />
            {this.context.t(underReviewMessage)}
          </div>
        )}
        {canIncrease && (
          <div className={kycClasses('footer', 'can-increase')}>
            {level === 1 ? (
              <a
                href={`https://kyc.bitazza.com?l=${language.toLowerCase()}`}
                target="_blank"
                rel="noopener noreferrer"
                className="ap-button__btn ap-button__btn--general kyc-level__change__btn kyc-level__change__btn--general">
                {this.context.t(button) ||
                  this.context.t('Increase to {label}', { label })}
              </a>
            ) : (
              <a
                href="mailto:compliance@bitazza.com"
                target="_blank"
                rel="noopener noreferrer"
                className="ap-button__btn ap-button__btn--general kyc-level__change__btn kyc-level__change__btn--general">
                {this.context.t(button) ||
                  this.context.t('Increase to {label}', { label })}
              </a>
            )}
          </div>
        )}
        {!verified &&
          !underReview &&
          !canIncrease && (
            <div className={kycClasses('footer', 'cannot-increase')}>
              <APButton customClass={kycClasses()} disabled={true}>
                {this.context.t(button) ||
                  this.context.t('Increase to {label}', { label })}
              </APButton>
            </div>
          )}
      </div>
    );
  }
}

KYCLevelComponent.propTypes = {
  verified: PropTypes.bool,
  underReview: PropTypes.bool,
  currentLevel: PropTypes.bool,
  canIncrease: PropTypes.bool,
  config: PropTypes.shape(kycConfigLevelShape),
  openKYCViewDetailSidePane: PropTypes.func,
  openPane: PropTypes.func,
  highlightStyle: PropTypes.string
};

KYCLevelComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default KYCLevelComponent;

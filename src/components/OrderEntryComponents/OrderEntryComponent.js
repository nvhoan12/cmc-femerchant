import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import APSegmentedButton from 'apex-web/lib/components/common/APSegmentedButton';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import APButton from 'apex-web/lib/components/common/APButton';
import APSelect from 'apex-web/lib/components/common/APSelect';
import BuySellTabs from 'apex-web/lib/components/OrderEntryComponents/BuySellTabs';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { biggerThanZero } from 'apex-web/lib/helpers/formValidations';
import {
  getOrderLeverage,
  getOrderBorrowingPower,
  getSubmitButtonText
} from 'apex-web/lib/helpers/orderHelper.js';
import {
  formatNumberToLocale,
  truncateToDecimals
} from 'apex-web/lib/helpers/numberFormatter';
import { styleNames } from 'apex-web/lib/propTypes/commonComponent';
import {
  orderTypes,
  sellValue,
  buyValue
} from '../../constants/sendOrder/orderEntryFormConstants';
import { instrumentPropType } from 'apex-web/lib/propTypes/instrumentPropTypes';
import { userPermissions } from 'apex-web/lib/constants/userPermissionsContants';
import ConfirmOrderModalContainer from '../ConfirmOrderModal';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';

import 'apex-web/lib/styles/components/TradeComponent.css';
import './OrderEntryComponent.css';

const baseClass = 'order-entry';
const baseClasses = getBEMClasses(baseClass);
const tradeComponentClasses = getBEMClasses('trade-component');

const OrderEntryComponent = (props, context) => {
  const {
    authorizedUserPermissions,
    reduxFormChange,
    synchronizeQuantityWithPrice,
    synchronizePriceWithQuantity,
    openAdvancedOrderSidePane,
    openReportBlockTradeSidePane,
    orderEntryForm,
    marketPrice,
    selectedInstrument,
    handleSubmit,
    submitting,
    fetching,
    invalid,
    disableTrading,
    VerificationRequiredComponent,
    hideFees,
    orderTotal,
    orderNet,
    orderFee,
    isMarginActive,
    marginProducts,
    availableBalance,
    title
  } = props;

  const buy = orderEntryForm.values.side === buyValue;
  const sell = orderEntryForm.values.side === sellValue;

  const decimalPlaceQuantity = convertIncrementToIntDecimalPlaces(
    selectedInstrument.QuantityIncrement
  );
  const decimalPlacePrice = convertIncrementToIntDecimalPlaces(
    selectedInstrument.PriceIncrement
  );

  const selectPrice = () => {
    if (orderEntryForm.values.orderType === orderTypes.limit.displayName) {
      return parseFloat(orderEntryForm.values.limitPrice) || 0;
    }
    if (orderEntryForm.values.orderType === orderTypes.stopMarket.displayName) {
      return parseFloat(orderEntryForm.values.stopPrice) || 0;
    }
  };

  const reduxFormChangeQuantity = quantity => {
    reduxFormChange(
      'quantity',
      truncateToDecimals(quantity, decimalPlaceQuantity)
    );
  };

  const reduxFormChangePrice = price => {
    reduxFormChange('price', truncateToDecimals(price, decimalPlacePrice));
  };

  const handleChoosePercent = e => {
    if (sell) {
      const sellAmount = e.target.value * availableBalance.position1;
      reduxFormChangeQuantity(sellAmount);
      handleQuantityChange(sellAmount);
    } else {
      const buyAmount = e.target.value * availableBalance.position2;
      reduxFormChangePrice(buyAmount);
      handlePriceChange(buyAmount);
    }
  };

  const handleLimitPriceChange = price => {
    const quantity = parseFloat(orderEntryForm.values.quantity) || 0;
    reduxFormChangePrice(price * quantity);
  };

  const handleQuantityChange = value => {
    if (orderEntryForm.values.orderType === orderTypes.market.displayName) {
      synchronizePriceWithQuantity(
        value,
        'orderEntry',
        'price',
        orderEntryForm
      );
    } else {
      const price = selectPrice();
      reduxFormChangePrice(price * value);
    }
  };

  const handlePriceChange = value => {
    if (orderEntryForm.values.orderType === orderTypes.market.displayName) {
      synchronizeQuantityWithPrice(
        value,
        'orderEntry',
        'quantity',
        orderEntryForm
      );
    } else {
      const price = selectPrice();
      reduxFormChangeQuantity(value / price);
    }
  };

  return (
    <div className={classnames(baseClasses())}>
      {title || null}
      <form onSubmit={handleSubmit} className={baseClasses('form')}>
        <BuySellTabs
          baseClass={baseClass}
          styleName={sell ? styleNames.subtractive : styleNames.additive}
        />

        <div className={baseClasses('body')}>
          <div className={baseClasses('order-type-wrapper')}>
            <APSegmentedButton
              name="orderType"
              label={context.t('Order Type')}
              items={[
                {
                  value: orderTypes.market.displayName,
                  text: context.t('Market'),
                  dataTest: 'Market Order Type'
                },
                {
                  value: orderTypes.limit.displayName,
                  text: context.t('Limit'),
                  dataTest: 'Limit Order Type'
                },
                {
                  value: orderTypes.stopMarket.displayName,
                  text: context.t('Stop'),
                  dataTest: 'Stop Order Type'
                }
              ]}
              customClass={baseClass}
              styleName={buy ? styleNames.additive : styleNames.subtractive}
            />
          </div>

          {orderEntryForm.values.orderType === orderTypes.limit.displayName && (
            <React.Fragment>
              <APNumberInput
                type="text"
                name="limitPrice"
                placeholder="0"
                customClass={baseClass}
                label={context.t('Limit Price')}
                labelInInput={
                  selectedInstrument && selectedInstrument.Product2Symbol
                }
                validate={[biggerThanZero]}
                decimalPlaces={decimalPlacePrice}
                customChange={handleLimitPriceChange}
                step={selectedInstrument.PriceIncrement}
              />
              <APSelect
                name="timeInForce"
                label={context.t('Time in Force')}
                customClass={baseClass}
                options={[
                  { value: 1, label: context.t('Good Til Canceled') },
                  { value: 3, label: context.t('Immediate or Cancel') },
                  { value: 4, label: context.t('Fill or Kill') }
                ]}
                showTriangles={true}
              />
            </React.Fragment>
          )}

          {orderEntryForm.values.orderType ===
            orderTypes.stopMarket.displayName && (
            <APNumberInput
              type="text"
              name="stopPrice"
              placeholder="0"
              customClass={baseClass}
              label={context.t('Stop Price')}
              labelInInput={
                selectedInstrument
                  ? context.t(`${selectedInstrument.Product2Symbol}:`)
                  : ''
              }
              validate={[biggerThanZero]}
              decimalPlaces={decimalPlacePrice}
              customChange={handleLimitPriceChange}
            />
          )}

          <APNumberInput
            type="text"
            name="quantity"
            placeholder="0"
            customClass={`${baseClass}-product1`}
            label={buy ? context.t('Buy Amount') : context.t('Sell Amount')}
            labelInInput={
              selectedInstrument && selectedInstrument.Product1Symbol
            }
            validate={[biggerThanZero]}
            decimalPlaces={decimalPlaceQuantity}
            customChange={handleQuantityChange}
            step={selectedInstrument.QuantityIncrement}
          />
          <APSegmentedButton
            name="percent"
            items={[
              {
                value: 0.25,
                text: '25%',
                onClick: handleChoosePercent
              },
              {
                value: 0.5,
                text: '50%',
                onClick: handleChoosePercent
              },
              {
                value: 0.75,
                text: '75%',
                onClick: handleChoosePercent
              },
              {
                value: 1.0,
                text: '100%',
                onClick: handleChoosePercent
              }
            ]}
            customClass={`${baseClass}-percent`}
          />
          <APNumberInput
            name="price"
            placeholder="0"
            customClass={`${baseClass}-product2`}
            labelInInput={
              selectedInstrument && selectedInstrument.Product2Symbol
            }
            decimalPlaces={decimalPlacePrice}
            customChange={handlePriceChange}
          />

          <div className={tradeComponentClasses('section')}>
            {isMarginActive && (
              <React.Fragment>
                <APLabelWithText
                  name="borrowingPower"
                  label={context.t('Borrowing Power')}
                  text={`${
                    buy
                      ? selectedInstrument.Product2Symbol
                      : selectedInstrument.Product1Symbol
                  } ${formatNumberToLocale(
                    getOrderBorrowingPower(
                      orderEntryForm,
                      selectedInstrument,
                      marginProducts
                    ),
                    buy ? decimalPlacePrice : decimalPlaceQuantity
                  )}`}
                  customClass={baseClass}
                />

                <APLabelWithText
                  name="leverage"
                  label={context.t('Leverage')}
                  text={getOrderLeverage(orderEntryForm)}
                  customClass={baseClass}
                />
              </React.Fragment>
            )}

            <APLabelWithText
              name="marketPrice"
              label={context.t('Market Price')}
              text={`${selectedInstrument.Product2Symbol} ${marketPrice}`}
              customClass={baseClass}
            />

            {!hideFees && (
              <APLabelWithText
                label={context.t('Fees')}
                customClass={baseClass}
                text={orderFee}
              />
            )}

            <APLabelWithText
              label={context.t('Order Total')}
              text={orderTotal}
              customClass={baseClass}
            />

            {!hideFees && (
              <APLabelWithText
                label={context.t('Net')}
                text={orderNet}
                customClass={baseClass}
              />
            )}
          </div>

          <div className={tradeComponentClasses('section')}>
            <APButton
              type="submit"
              disabled={submitting || fetching || invalid || disableTrading}
              customClass={baseClass}
              styleName={buy ? styleNames.additive : styleNames.subtractive}>
              {submitting
                ? context.t('Processing...')
                : context.t(getSubmitButtonText(orderEntryForm))}
            </APButton>

            {VerificationRequiredComponent}
          </div>

          <div className={tradeComponentClasses('section')}>
            <div
              className={classnames(baseClasses('item-button'))}
              onClick={openAdvancedOrderSidePane}>
              {`« ${context.t('Advanced Orders')}`}
            </div>
          </div>
          {authorizedUserPermissions &&
            (authorizedUserPermissions.includes(
              userPermissions.submitBlockTrade
            ) ||
              authorizedUserPermissions.includes(
                userPermissions.superUser
              )) && (
              <div className={tradeComponentClasses('section')}>
                <div
                  className={classnames(baseClasses('item-button'))}
                  onClick={openReportBlockTradeSidePane}>
                  {`« ${context.t('Report Block Trade')}`}
                </div>
              </div>
            )}
        </div>
      </form>
      <ConfirmOrderModalContainer />
    </div>
  );
};

OrderEntryComponent.defaultProps = {
  submitting: false,
  isMarginActive: false
};

OrderEntryComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  authorizedUserPermissions: PropTypes.array.isRequired,
  openAdvancedOrderSidePane: PropTypes.func.isRequired,
  openReportBlockTradeSidePane: PropTypes.func.isRequired,
  selectedInstrument: instrumentPropType,
  orderEntryForm: PropTypes.object.isRequired,
  marketPrice: PropTypes.string.isRequired,
  submitting: PropTypes.bool,
  fetching: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  disableTrading: PropTypes.bool.isRequired,
  VerificationRequiredComponent: PropTypes.element,
  ConfirmLimitOrderModalContainer: PropTypes.element
};

OrderEntryComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default OrderEntryComponent;

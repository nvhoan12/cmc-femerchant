import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import AdvancedOrderFormComponent from './AdvancedOrderFormComponent';
import VerificationRequiredContainer from 'apex-web/lib/components/VerificationRequired/VerificationRequiredContainer';
import {
  placeOrderWithChecks,
  clearOrderForm,
  synchronizePriceWithQuantity,
  synchronizeQuantityWithPrice
} from '../../../redux/actions/orderEntryActions';
import { closeAdvancedOrderSidePane } from 'apex-web/lib/redux/actions/sidePaneActions';
import {
  advancedOrderFormTypes,
  buyValue
} from '../../../constants/sendOrder/orderEntryFormConstants';
import { orderFormTypes } from 'apex-web/lib/constants/sendOrder/orderFormTypes';
import { marketPriceOfSelectedInstrumentSelector } from 'apex-web/lib/redux/selectors/buySellSelectors';
import instrumentPositionSelector, {
  selectedInstrumentSelector
} from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import { kycVerificationLevelsSelector } from 'apex-web/lib/redux/selectors/kycLevelsSelectors';
import { instrumentSelectorInstruments } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import store from 'apex-web/lib/redux/store';
import config from 'apex-web/lib/config';
import { orderTotalSelector } from 'apex-web/lib/redux/selectors/orderTotalSelector';
import { isMarginActiveSelector } from 'apex-web/lib/redux/selectors/marginSelectors';
import { marginProductSelector } from 'apex-web/lib/redux/selectors/marginProductSelectors';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { getOrderNet } from 'apex-web/lib/helpers/orderHelper';
import { positionSelector } from 'apex-web/lib/redux/selectors/positionSelectors';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';

const closeSidePane = () => store.dispatch(closeAdvancedOrderSidePane());

const mapStateToProps = state => {
  const { form } = state;

  const isMarginActive = isMarginActiveSelector(state);
  const marginProducts = marginProductSelector(state);
  const selectedInstrument = selectedInstrumentSelector(state);
  const formObj = form.advancedOrderEntry || { values: {} };

  const { disableTrading } = kycVerificationLevelsSelector(state);

  const orderTotal = form.advancedOrderEntry
    ? orderTotalSelector(state, {
        form: form.advancedOrderEntry.values
      })
    : 0;

  return {
    formObj,
    selectedInstrument,
    instruments: instrumentSelectorInstruments(state),
    marketPrice: marketPriceOfSelectedInstrumentSelector(state, {
      side: formObj.values.side
    }),
    orderTotal: `${selectedInstrument.Product2Symbol} ${formatNumberToLocale(
      orderTotal,
      convertIncrementToIntDecimalPlaces(selectedInstrument.PriceIncrement)
    )}`,
    orderNet: getOrderNet(formObj, selectedInstrument, orderTotal),
    positions: positionSelector(state),
    initialValues: {
      side: formObj.values.side || buyValue,
      instrument: selectedInstrument.InstrumentId,
      quantity: '',
      limitPrice: '',
      stopPrice: '',
      trailingAmount: '',
      limitOffset: '',
      displayQuantity: '',
      fee: '0',
      totalAmount: '0',
      pegPriceType: '1',
      price: '',
      orderType:
        formObj.values.orderType || advancedOrderFormTypes.market.displayName,
      timeInForce: '1'
    },
    disableTrading,
    ...instrumentPositionSelector(state),
    VerificationRequiredComponent: (
      <VerificationRequiredContainer onVerificationLinkClick={closeSidePane} />
    ),
    hideFees: !!config.global.hideFees,
    isMarginActive,
    marginProducts
  };
};

const mapDispatchToProps = {
  clearAdvancedOrderEntryForm: () => clearOrderForm('advancedOrderEntry'),
  synchronizeQuantityWithPrice,
  synchronizePriceWithQuantity
};

const Form = reduxForm({
  form: 'advancedOrderEntry',
  enableReinitialize: true,
  onSubmit: (payload, dispatch) =>
    dispatch(placeOrderWithChecks(orderFormTypes.advanced, payload)).then(
      res => {
        if (res.result === false) {
          dispatch(
            showSnack({
              id: 'placeOrderError',
              text: res.errormsg || res.detail,
              textVars: res.textVars,
              type: 'warning'
            })
          );
        }
      }
    )
})(AdvancedOrderFormComponent);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);

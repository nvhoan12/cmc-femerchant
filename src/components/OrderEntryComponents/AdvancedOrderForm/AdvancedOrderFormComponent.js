import React from 'react';
import PropTypes from 'prop-types';
import APButton from 'apex-web/lib/components/common/APButton';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import APSelect from 'apex-web/lib/components/common/APSelect';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import UserBalanceComponent from 'apex-web/lib/components/UserBalance/UserBalanceComponent';
import MarketPriceComponent from 'apex-web/lib/components/OrderEntryComponents/AdvancedOrderMarketPriceComponent';
import { instrumentPropType } from 'apex-web/lib/propTypes/instrumentPropTypes';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import {
  getOrderFee,
  getOrderLeverage,
  getOrderBorrowingPower,
  getSubmitButtonText
} from 'apex-web/lib/helpers/orderHelper';
import { styleNames } from 'apex-web/lib/propTypes/commonComponent';
import {
  advancedOrderFormTypes,
  sellValue
} from '../../../constants/sendOrder/orderEntryFormConstants';
import { pegPriceTypesNames } from 'apex-web/lib/constants/sendOrder/pegPriceTypeConstants';
import {
  isTrailingAmountFieldInForm,
  isLimitOffsetFieldInForm,
  isPegPriceFieldInForm,
  isLimitPriceFieldInForm,
  isStopPriceFieldInForm,
  isDisplayQuantityFieldInForm,
  isTimeInForceFieldInForm
} from 'apex-web/lib/helpers/placeOrderHelper';
import {
  biggerThanZero,
  biggerThanOrEqualToZero,
  requiredNumeric
} from 'apex-web/lib/helpers/formValidations';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import './AdvancedOrderForm.css';

const bemClasses = getBEMClasses('advanced-order-form');
const dividerClasses = getBEMClasses('divider');

const AdvancedOrderForm = (props, context) => {
  const {
    marketPrice,
    handleSubmit,
    instruments,
    submitting,
    formObj,
    positions,
    position1,
    position2,
    clearAdvancedOrderEntryForm,
    selectedInstrument,
    selectedInstrument: {
      Product1Symbol,
      Product2Symbol,
      PriceIncrement,
      QuantityIncrement
    },
    disableTrading,
    invalid,
    VerificationRequiredComponent,
    hideFees,
    orderTotal,
    orderNet,
    isMarginActive,
    marginProducts
  } = props;

  return (
    <form className={bemClasses('body')} onSubmit={handleSubmit}>
      <div className={bemClasses('main-section')}>
        <div>
          <div className={bemClasses(null, ['with-padding'])}>
            <APSelect
              name="instrument"
              label={`${context.t('Instrument')}:`}
              customClass={bemClasses()}
              options={instruments.map(instrument => {
                const { InstrumentId } = instrument;
                return { value: InstrumentId, label: instrument.Symbol };
              })}
              showTriangles
            />
          </div>

          <div>
            <APSelect
              name="orderType"
              label={`${context.t('Order Type')}:`}
              customClass={bemClasses()}
              onChange={clearAdvancedOrderEntryForm}
              options={Object.keys(advancedOrderFormTypes).map(key => {
                const { displayName } = advancedOrderFormTypes[key];
                return { value: displayName, label: displayName };
              })}
              showTriangles
            />
          </div>
        </div>

        <div className={bemClasses('main-section-limit-price')}>
          <MarketPriceComponent
            baseClass={bemClasses()}
            symbol1={Product1Symbol || ''}
            symbol2={Product2Symbol || ''}
            value={`${marketPrice}`}
            formSide={formObj.values.side}
          />
        </div>
      </div>

      <div
        className={dividerClasses(null, [
          'dark',
          'advanced-order-side-pane-size'
        ])}
      />

      {renderQuantityField(props, context)}

      {isLimitPriceFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses(null, ['with-padding'])}>
          <APNumberInput
            name="limitPrice"
            placeholder="0"
            decimalPlaces={convertIncrementToIntDecimalPlaces(PriceIncrement)}
            label={`${context.t('Limit Price')}:`}
            customClass={bemClasses()}
            labelInInput={Product2Symbol}
            validate={[biggerThanZero]}
          />
        </div>
      )}

      {isStopPriceFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses(null, ['with-padding'])}>
          <APNumberInput
            name="stopPrice"
            placeholder="0"
            decimalPlaces={convertIncrementToIntDecimalPlaces(PriceIncrement)}
            label={`${context.t('Stop Price')}:`}
            customClass={bemClasses()}
            labelInInput={Product2Symbol}
            validate={[biggerThanZero]}
          />
        </div>
      )}

      {isTrailingAmountFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses(null, ['with-padding'])}>
          <APNumberInput
            name="trailingAmount"
            placeholder="0"
            decimalPlaces={convertIncrementToIntDecimalPlaces(PriceIncrement)}
            label={`${context.t('Trailing Amount')}:`}
            customClass={bemClasses()}
            labelInInput={Product2Symbol}
            validate={[biggerThanZero]}
          />
        </div>
      )}

      {isLimitOffsetFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses(null, ['with-padding'])}>
          <APNumberInput
            name="limitOffset"
            placeholder="0"
            decimalPlaces={convertIncrementToIntDecimalPlaces(PriceIncrement)}
            label={`${context.t('Limit Offset')}:`}
            customClass={bemClasses()}
            labelInInput={Product2Symbol}
            validate={[biggerThanZero]}
          />
        </div>
      )}

      {isPegPriceFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses('row')}>
          <APSelect
            name="pegPriceType"
            label={`${context.t('Peg Price')}:`}
            customClass={bemClasses()}
            classModifiers={'short'}
            options={Object.keys(pegPriceTypesNames).map(key => ({
              value: key,
              label: pegPriceTypesNames[key]
            }))}
          />
        </div>
      )}

      {isDisplayQuantityFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses(null, ['with-padding'])}>
          <APNumberInput
            name="displayQuantity"
            placeholder="0"
            decimalPlaces={convertIncrementToIntDecimalPlaces(
              QuantityIncrement
            )}
            label={`${context.t('Display Quantity')}:`}
            customClass={bemClasses()}
            labelInInput={Product1Symbol}
            validate={[requiredNumeric, biggerThanOrEqualToZero]}
          />
        </div>
      )}

      {isTimeInForceFieldInForm(formObj.values.orderType) && (
        <div className={bemClasses(null, ['with-padding'])}>
          <APSelect
            name="timeInForce"
            label={`${context.t('Time in Force')}:`}
            customClass={bemClasses()}
            options={[
              { value: 1, label: context.t('Good Til Canceled') },
              { value: 3, label: context.t('Immediate or Cancel') },
              { value: 4, label: context.t('Fill or Kill') }
            ]}
          />
        </div>
      )}

      <div
        className={dividerClasses(null, [
          'dark',
          'advanced-order-side-pane-size-small'
        ])}
      />

      {isMarginActive && (
        <React.Fragment>
          <APLabelWithText
            name="borrowingPower"
            label={context.t('Borrowing Power')}
            text={`${
              formObj.values.side === sellValue
                ? Product1Symbol
                : Product2Symbol
            } ${getOrderBorrowingPower(
              formObj,
              selectedInstrument,
              marginProducts
            )}`}
            customClass={bemClasses()}
          />

          <APLabelWithText
            name="leverage"
            label={context.t('Leverage')}
            text={getOrderLeverage(formObj)}
            customClass={bemClasses()}
          />
        </React.Fragment>
      )}

      {!hideFees && (
        <APLabelWithText
          label={`${context.t('Fees')}:`}
          text={getOrderFee(formObj, positions)}
          customClass={bemClasses()}
        />
      )}

      <APLabelWithText
        label={`${context.t('Order Total')}:`}
        text={orderTotal}
        customClass={bemClasses()}
      />

      <APLabelWithText
        label={`${context.t('Received')}:`}
        text={orderNet}
        customClass={bemClasses()}
      />

      <div className={bemClasses('user-balance-container')}>
        <UserBalanceComponent position1={position1} position2={position2} />
      </div>

      <div
        className={dividerClasses(null, [
          'dark',
          'advanced-order-side-pane-size-small'
        ])}
      />

      <APButton
        type="submit"
        disabled={submitting || disableTrading || invalid}
        customClass={bemClasses()}
        styleName={
          formObj.values.side === sellValue
            ? styleNames.subtractive
            : styleNames.additive
        }>
        {submitting
          ? context.t('Processing...')
          : context.t(getSubmitButtonText(formObj))}
      </APButton>

      <div className={bemClasses('verification-required')}>
        {VerificationRequiredComponent}
      </div>
    </form>
  );
};

const renderQuantityField = (props, context) => {
  const {
    selectedInstrument: { Product1Symbol, QuantityIncrement }
  } = props;

  return (
    <div className={bemClasses(null, ['with-padding'])}>
      <APNumberInput
        name="quantity"
        placeholder="0"
        label={context.t('Order Size:')}
        decimalPlaces={convertIncrementToIntDecimalPlaces(QuantityIncrement)}
        customClass={bemClasses()}
        labelInInput={Product1Symbol}
        validate={[biggerThanZero]}
      />
    </div>
  );
};

AdvancedOrderForm.propTypes = {
  positions: PropTypes.object.isRequired,
  position1: PropTypes.object.isRequired,
  position2: PropTypes.object.isRequired,
  decimalPlaces: PropTypes.object.isRequired,
  selectedInstrument: PropTypes.object.isRequired,
  marketPrice: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  instruments: PropTypes.arrayOf(instrumentPropType).isRequired,
  formObj: PropTypes.object,
  submitting: PropTypes.bool.isRequired,
  clearAdvancedOrderEntryForm: PropTypes.func.isRequired,
  synchronizeQuantityWithPrice: PropTypes.func.isRequired,
  synchronizePriceWithQuantity: PropTypes.func.isRequired,
  disableTrading: PropTypes.bool,
  VerificationRequiredComponent: PropTypes.element
};

AdvancedOrderForm.contextTypes = {
  t: PropTypes.func.isRequired
};

export default AdvancedOrderForm;

import { connect } from 'react-redux';
import { change } from 'redux-form';
import AdvancedOrderSidePaneComponent from './AdvancedOrderSidePaneComponent';
import { clearOrderForm } from '../../../redux/actions/orderEntryActions';
import {
  buyValue,
  sellValue
} from '../../../constants/sendOrder/orderEntryFormConstants';

const mapStateToProps = state => {
  const { form } = state;
  return {
    form: form.advancedOrderEntry
  };
};

const mapDispatchToProps = {
  setBuyOrder: () => change('advancedOrderEntry', 'side', buyValue),
  setSellOrder: () => change('advancedOrderEntry', 'side', sellValue),
  clearAdvancedOrderEntryForm: () => clearOrderForm('advancedOrderEntry')
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvancedOrderSidePaneComponent);

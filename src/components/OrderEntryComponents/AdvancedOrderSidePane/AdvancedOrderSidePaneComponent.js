import React from 'react';
import PropTypes from 'prop-types';
import AdvancedOrderFormContainer from '../AdvancedOrderForm/AdvancedOrderFormContainer';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import {
  buyValue,
  sellValue
} from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';

import './AdvancedOrderSidePaneComponent.css';

const baseClasses = getBEMClasses('advanced-order-sidepane');

class AdvancedOrderSidePane extends React.Component {
  componentDidMount() {
    this.props.onSidePaneOpen({
      title: this.context.t('Advanced Orders'),
      customClass: baseClasses(),
      classModifiers: 'with-tabs'
    });
  }

  render() {
    const {
      form,
      setSellOrder,
      setBuyOrder,
      clearAdvancedOrderEntryForm
    } = this.props;

    const buyTabModifier =
      ((form && form.values.side === buyValue) || !form) && 'buy-selected';
    const sellTabModifier =
      form && form.values.side === sellValue && 'sell-selected';

    const selectBuyOrder = () => {
      clearAdvancedOrderEntryForm();
      setBuyOrder();
    };

    const selectSellOrder = () => {
      clearAdvancedOrderEntryForm();
      setSellOrder();
    };

    return (
      <React.Fragment>
        <div className={baseClasses('tab-container')}>
          <div
            className={baseClasses('tab', buyTabModifier)}
            onClick={selectBuyOrder}>
            {this.context.t('Buy')}
          </div>
          <div
            className={baseClasses('tab', sellTabModifier)}
            onClick={selectSellOrder}>
            {this.context.t('Sell')}
          </div>
        </div>

        <AdvancedOrderFormContainer />
      </React.Fragment>
    );
  }
}

AdvancedOrderSidePane.defaultProps = {
  onSidePaneOpen: () => {}
};

AdvancedOrderSidePane.propTypes = {
  clearAdvancedOrderEntryForm: PropTypes.func.isRequired,
  setBuyOrder: PropTypes.func.isRequired,
  setSellOrder: PropTypes.func.isRequired,
  form: PropTypes.object
};

AdvancedOrderSidePane.contextTypes = {
  t: PropTypes.func.isRequired
};

export default AdvancedOrderSidePane;

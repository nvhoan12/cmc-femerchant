import React from 'react';
import PropTypes from 'prop-types';
import APQRCode from 'apex-web/lib/components/common/APQRCode/APQRCode';
import copyToClipboard from 'apex-web/lib/helpers/clipboardHelper';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APAutoTabInput from 'apex-web/lib/components/common/APAutoTabInput/APAutoTabInput';
import { required } from 'apex-web/lib/helpers/formValidations';
import config from 'apex-web/lib/config';
import APButton from '../../components/common/APButton';

import './Enable2FAModal.css';

const baseClasses = getBEMClasses('enable-2fa-modal');

const Enable2FAForm = (props, context) => {
  const { GoogleQRCode, handleSubmit, userName } = props;

  const {
    global: { siteName }
  } = config;

  const code = `otpauth://totp/${userName}?secret=${GoogleQRCode}&issuer=${encodeURI(
    siteName
  )}`;

  return (
    <form onSubmit={handleSubmit}>
      <div className={baseClasses()}>
        <div>
          <ol className={baseClasses('text-container')}>
            <li className={baseClasses('text-item')}>
              {context.t(
                'Download a Two-Factor Authentication app to your phone such as Authy or Google Authenticator'
              )}
            </li>
            <li className={baseClasses('text-item')}>
              {context.t('Use the app to scan QR code.')}
            </li>
            <li className={baseClasses('text-item')}>
              {context.t('Type in code from your phone.')}
            </li>
          </ol>
          <APAutoTabInput
            name="code"
            type="input"
            numberOfInputs={6}
            validate={[required]}
          />
        </div>
        <div className={baseClasses('qr-code')}>
          <APQRCode value={code} />
          <div className={baseClasses('google-code')}>
            <p>{GoogleQRCode}</p>
            <APButton
              type="button"
              onClick={() => copyToClipboard(GoogleQRCode)}>
              {context.t('Copy Key')}
            </APButton>
          </div>
        </div>
      </div>
    </form>
  );
};

Enable2FAForm.defaultProps = {
  handleSubmit: () => {},
  submitting: false
};

Enable2FAForm.propTypes = {
  handleSubmit: PropTypes.func
};

Enable2FAForm.contextTypes = {
  t: PropTypes.func.isRequired
};

export default Enable2FAForm;

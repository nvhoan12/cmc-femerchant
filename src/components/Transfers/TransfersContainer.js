import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import TransfersComponent from './TransfersComponent';
import {
  approveTransferRequest,
  rejectTransferRequest
} from 'apex-web/lib/redux/actions/transferRequestActions';
import { transfersByProductIdSelector } from 'apex-web/lib/redux/selectors/transfersTableSelectors';
import config from 'apex-web/lib/config';

const mapStateToProps = state => {
  const {
    receivedRequests,
    sentRequests,
    sentTransfers,
    receivedTransfers
  } = transfersByProductIdSelector(state);
  const formSelector = formValueSelector('transfers-filters-form');
  const selectedFilter = formSelector(state, 'transfersFilters');

  const { products } = state.apexCore.product;
  const { usePagination } = config.OrderHistoryData;

  const addProduct = item =>
    (item.product = products.find(p => item.ProductId === p.ProductId));

  const addBTZIDForTranfer = item => {
    const receiverAccount = state.btzAccounts.find(
      account => account.email === item.ReceiverUserName
    );
    if (receiverAccount) {
      item.ReceiverBTZID = receiverAccount.btzID;
      item.ReceiverDisplayName = receiverAccount.displayName;
    }
    const senderAccount = state.btzAccounts.find(
      account => account.email === item.SenderUserName
    );
    if (senderAccount) {
      item.SenderBTZID = senderAccount.btzID;
      item.SenderDisplayName = senderAccount.displayName;
    }
  };

  const addBTZIDForRequest = item => {
    const payerAccount = state.btzAccounts.find(
      account => account.email === item.PayerUsername
    );
    if (payerAccount) {
      item.PayerBTZID = payerAccount.btzID;
      item.PayerDisplayName = payerAccount.displayName;
    }
    const requestorAccount = state.btzAccounts.find(
      account => account.email === item.RequestorUsername
    );
    if (requestorAccount) {
      item.RequestorBTZID = requestorAccount.btzID;
      item.RequestorDisplayName = requestorAccount.displayName;
    }
  };

  receivedRequests.forEach(addProduct);
  receivedRequests.forEach(addBTZIDForRequest);
  sentRequests.forEach(addProduct);
  sentRequests.forEach(addBTZIDForRequest);
  sentTransfers.forEach(addProduct);
  sentTransfers.forEach(addBTZIDForTranfer);
  receivedTransfers.forEach(addProduct);
  receivedTransfers.forEach(addBTZIDForTranfer);

  return {
    receivedRequests,
    sentRequests,
    sentTransfers,
    receivedTransfers,
    usePagination,
    selectedFilter: selectedFilter ? selectedFilter : 'sentTransfers'
  };
};

const mapDispatchToProps = dispatch => ({
  approveTransferRequest: requestCode =>
    dispatch(approveTransferRequest(requestCode)),
  rejectTransferRequest: requestCode =>
    dispatch(rejectTransferRequest(requestCode))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransfersComponent);

import React from 'react';
import clsx from 'clsx';
import icLine from '../../images/social/ic-line.png';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { BtnLink } from '../PublicPage/Btn';
import config from '../../config';
import './LinkLINE.css';

const classes = getBEMClasses('link-line');

export const LineIcon = ({ className }) => {
  return (
    <img
      src={icLine}
      className={clsx(className, classes('line-icon'))}
      alt="line icon"
    />
  );
};

export const handleMobileClick = () => {
  window.open(config.global.lineLink);
};

const LinkLINE = ({ className, text }) => {
  return (
    <div className={className}>
      <BtnLink onClick={handleMobileClick} className={classes('btn')}>
        <LineIcon className="mr-3" />
        {text}
      </BtnLink>
    </div>
  );
};

export default LinkLINE;

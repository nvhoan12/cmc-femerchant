import React from 'react';
import { connect } from 'react-redux';
import PopoverMenu from 'apex-web/lib/components/common/PopoverMenu/PopoverMenu';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import config from '../../config';
import { saveDataExchange } from '../../helpers/dataExchange';
import { getlocalLanguage } from '../../helpers/localLanguage';
import { isGlobalUser } from '../../helpers/userConfigHelper';
import enIcon from '../../images/flags/en-icon.png';
import thIcon from '../../images/flags/th-icon.png';
import './SiteSelector.css';

const classes = getBEMClasses('site-selector');

const flags = {
  global: enIcon,
  thailand: thIcon
};

const SiteSelector = ({ token, userConfig }) => {
  const handleNavigate = async site => {
    const tmpToken = await saveDataExchange(token, {
      token: token,
      language: getlocalLanguage()
    });
    window.location.href = `${site}/exchange?dataExchange=${tmpToken}`;
  };

  const renderItems = () => {
    const items = [];
    items.push({
      label: 'Bitazza Global',
      onClick: () => handleNavigate(config.sites.global),
      beforeLabelElement: (
        <img className={classes('menu-item-flag')} src={enIcon} alt="flag" />
      )
    });

    if (!isGlobalUser(userConfig)) {
      items.push({
        label: 'Bitazza Thailand',
        onClick: () => handleNavigate(config.sites.thailand),
        beforeLabelElement: (
          <img className={classes('menu-item-flag')} src={thIcon} alt="flag" />
        )
      });
    }
    return items;
  };

  return (
    <PopoverMenu
      customClass={classes('')}
      popoverProps={{
        trigger: props => (
          <button {...props}>
            <img
              className={classes('flag')}
              src={flags[config.global.site]}
              alt="flag"
            />
          </button>
        )
      }}
      items={renderItems()}
    />
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    userConfig: state.user.userConfig
  };
};

export default connect(
  mapStateToProps,
  null
)(SiteSelector);

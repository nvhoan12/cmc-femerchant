import React from 'react';
import conf from '../../config';
import GolabelMessageComponent from './GolabelMessageComponent';

const fetchGolabelMessage = async url => {
  const res = await fetch(url);
  const resJson = await res.json();
  const now = new Date();
  if (res.status === 200) {
    const messagesFitler = resJson
      .filter(item => {
        const start = new Date(item.start_timestamp);
        const end = new Date(item.end_timestamp);
        return start && end && start < now && now < end;
      })
      .map(item => ({
        type: item.type,
        message: {
          EN: item.message['EN'],
          TH: item.message['TH']
        },
        link: item.url,
        delay: item.delay
      }));
    if (messagesFitler.length > 0) {
      return messagesFitler;
    }
  }
  return null;
};

const GolabelMessageInterior = ({ className }) => {
  const [messages, setMessages] = React.useState([]);

  React.useEffect(() => {
    fetchGolabelMessage(conf.services.fetchGolabelMessageInterior).then(res => {
      if (res) {
        setMessages(res);
      }
    });
  }, []);

  return <GolabelMessageComponent className={className} messages={messages} />;
};

const GolabelMessageLanding = ({ className }) => {
  const [messages, setMessages] = React.useState([]);

  React.useEffect(() => {
    fetchGolabelMessage(conf.services.fetchGolabelMessageLaning).then(res => {
      if (res) {
        setMessages(res);
      }
    });
  }, []);

  return <GolabelMessageComponent className={className} messages={messages} />;
};

export { GolabelMessageInterior, GolabelMessageLanding };

import React from 'react';
import Slider from 'react-slick';
import clsx from 'clsx';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { useDidUpdate } from '../../helpers/hook';
import { getlocalLanguage } from '../../helpers/localLanguage';
import './GolabelMessage.css';

const classes = getBEMClasses('golabel-message');

const GolabelMessageComponent = ({ className, messages }) => {
  const settings = {
    dots: false,
    fade: true,
    infinite: true,
    autoplay: false,
    speed: 500,
    className: clsx(classes('slick'), className),
    slidesToScroll: 1
  };

  const [currentIndex, setCurrentIndex] = React.useState(null);
  const slider = React.useRef(null);
  const language = getlocalLanguage();

  useDidUpdate(
    () => {
      slider.current.slickGoTo(currentIndex);
      const delay = messages[currentIndex].delay;
      const next =
        currentIndex + 1 > messages.length - 1 ? 0 : currentIndex + 1;
      setTimeout(() => setCurrentIndex(next), delay);
    },
    [currentIndex]
  );

  useDidUpdate(
    () => {
      if (messages.length > 0) {
        setCurrentIndex(0);
      }
    },
    [messages]
  );

  return (
    messages.length > 0 && (
      <Slider {...settings} ref={slider}>
        {messages.map((item, key) => (
          <div
            key={key}
            delay={item.delay}
            className={clsx(
              classes(),
              item.type === 'blue' && classes('blue'),
              item.type === 'red' && classes('red')
            )}>
            <a href={item.link} target="_blank" rel="noopener noreferrer">
              <p className={classes('text')}>
                {language ? item.message[language] : item.message['EN']}
              </p>
            </a>
          </div>
        ))}
      </Slider>
    )
  );
};

export default GolabelMessageComponent;

import React from 'react';
import { connect } from 'react-redux';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { fundedPositionsSelector } from 'apex-web/lib/redux/selectors/productPositionSelectors';
import {
  baseCurrencyPriceSelector,
  baseCurrencySelector
} from 'apex-web/lib/redux/selectors/baseCurrenciesSelectors';

const SumValue = ({ sumValueAmount }) => {
  return (
    <span className="d-inline-block">
      <span className="px-2">≋</span>
      {sumValueAmount}
    </span>
  );
};

const mapStateToProps = state => {
  const baseCurrencySymbol = baseCurrencySelector(state);
  const products = fundedPositionsSelector(state);
  const sumValue = products.reduce((sum, product) => {
    if (product.ProductSymbol === baseCurrencySymbol) {
      return sum + product.rawAmount;
    }
    const baseCurrency = baseCurrencyPriceSelector(state, product.ProductId);
    if (baseCurrency) {
      return sum + baseCurrency.value * product.rawAmount;
    }
    return sum;
  }, 0);
  return {
    sumValueAmount: isNaN(sumValue)
      ? ''
      : `${formatNumberToLocale(
          sumValue,
          baseCurrencySymbol
        )} ${baseCurrencySymbol}`
  };
};

export default connect(
  mapStateToProps,
  null
)(SumValue);

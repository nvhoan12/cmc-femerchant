import { connect } from 'react-redux';
import { change, formValueSelector } from 'redux-form';
import { isBuySideBuySellFormSelector } from 'apex-web/lib/redux/selectors/buySellSelectors';
import {
  synchronizeQuantityWithPrice,
  synchronizePriceWithQuantity
} from '../../../../../redux/actions/orderEntryActions';
import { selectedInstrumentSelector } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import get from 'lodash/get';
import BuySellCustomRowComponent from './BuySellCustomRowComponent';

const selector = formValueSelector('buySell');

const mapStateToProps = state => {
  return {
    isBuySide: isBuySideBuySellFormSelector(state),
    activeAmount: selector(state, 'amountRadio'),
    formObj: get(state, 'form.buySell', { values: {} }),
    selectedInstrument: selectedInstrumentSelector(state)
  };
};

const mapDispatchToProps = {
  setAmount: amount => change('buySell', 'amountRadio', amount),
  synchronizeQuantityWithPrice,
  synchronizePriceWithQuantity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuySellCustomRowComponent);

import React from 'react';
import PropTypes from 'prop-types';
import APRadio from 'apex-web/lib/components/common/APRadio/APRadio';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import './BuySellCustomRow.css';

class BuySellCustomRowComponent extends React.Component {
  render() {
    const {
      synchronizeQuantityWithPrice,
      synchronizePriceWithQuantity,
      setAmount,
      activeAmount,
      themeModifier,
      formObj,
      isBuySide,
      baseClasses,
      selectedInstrument
    } = this.props;

    const arrow = (
      <p className={baseClasses('arrow')}>{isBuySide ? '→' : '←'}</p>
    );

    return (
      <div
        className={baseClasses('amount-row', 'two-inputs-layout')}
        onClick={() => setAmount('custom')}>
        <div className={baseClasses('first-in-row')}>
          <APRadio name="amountRadio" id="custom" value="custom" />
        </div>
        <div>
          <APNumberInput
            name="price"
            customClass={baseClasses()}
            classModifiers={`${
              activeAmount === 'custom' ? 'active' : 'disable'
            } ${themeModifier || ''}`}
            step={selectedInstrument.PriceIncrement}
            decimalPlaces={convertIncrementToIntDecimalPlaces(
              selectedInstrument.PriceIncrement
            )}
            customChange={value => {
              synchronizeQuantityWithPrice(
                value,
                'buySell',
                'quantity',
                formObj
              );
            }}
          />
        </div>
        {arrow}
        <div className={baseClasses('last-in-row')}>
          <APNumberInput
            name="quantity"
            step={selectedInstrument.QuantityIncrement}
            customClass={baseClasses()}
            classModifiers={`${
              activeAmount === 'custom' ? 'active' : 'disable'
            } ${themeModifier || ''}`}
            decimalPlaces={convertIncrementToIntDecimalPlaces(
              selectedInstrument.QuantityIncrement
            )}
            customChange={value => {
              synchronizePriceWithQuantity(value, 'buySell', 'price', formObj);
            }}
          />
        </div>
      </div>
    );
  }
}

BuySellCustomRowComponent.propTypes = {
  isBuySide: PropTypes.bool.isRequired,
  formObj: PropTypes.object.isRequired,
  synchronizeQuantityWithPrice: PropTypes.func.isRequired,
  synchronizePriceWithQuantity: PropTypes.func.isRequired,
  selectedInstrument: PropTypes.object.isRequired,
  themeModifier: PropTypes.string
};

export default BuySellCustomRowComponent;

import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import BuySellComponent from './BuySellComponent';
import {
  buyValue,
  orderTypes
} from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import {
  MODAL_TYPES,
  openModal
} from 'apex-web/lib/redux/actions/modalActions';

const BuySellComponentForm = reduxForm({
  form: 'buySell',
  onSubmit: (payload, dispatch) => {
    dispatch(openModal(MODAL_TYPES.RETAIL_SUBMIT_ORDER));
  },
  destroyOnUnmount: false
})(BuySellComponent);

const mapStateToProps = state => {
  return {
    initialValues: {
      side: buyValue,
      amountRadio: 'custom',
      price: '',
      quantity: '',
      fee: '0',
      orderType: orderTypes.market.displayName,
      responseMessage: ''
    }
  };
};

export default connect(mapStateToProps)(BuySellComponentForm);

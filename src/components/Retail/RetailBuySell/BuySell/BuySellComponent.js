import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import './BuySellComponent.css';
import BuySellOrderConfirmationModal from '../BuySellOrderConfirmationModal';
import BuySellTabsContainer from 'apex-web/lib/components/Retail/RetailBuySell/BuySell/BuySellTabs';
import BuySellOrderButton from 'apex-web/lib/components/Retail/RetailBuySell/BuySell/BuySellOrderButton';
import BuySellPresetValuesRowsContainer from 'apex-web/lib/components/Retail/RetailBuySell/BuySell/BuySellPresetValuesRows';
import InstrumentSelectorContainer from '../../../InstrumentSelector/InstrumentSelectorContainer';
import BuySellCustomRowContainer from './BuySellCustomRow';

const baseClasses = getBEMClasses('buy-sell');

class BuySellComponent extends React.Component {
  render() {
    const {
      handleSubmit,
      submitting,
      extraInfoComponent,
      displayInstrumentSelector,
      themeModifier,
      amountHeader,
      formHeader,
      hideKYC,
      customKYCmessage
    } = this.props;

    return (
      <div className={baseClasses('container', themeModifier)}>
        {formHeader && (
          <div className={baseClasses('form-header', themeModifier)}>
            {formHeader}
          </div>
        )}

        <BuySellTabsContainer
          baseClasses={baseClasses}
          themeModifier={themeModifier}
        />

        {displayInstrumentSelector && (
          <div className={baseClasses('instrument-selector')}>
            <InstrumentSelectorContainer customClass="buy-sell-popup" />
          </div>
        )}

        {amountHeader && (
          <div className={baseClasses('amount-header', themeModifier)}>
            {this.context.t(amountHeader)}
          </div>
        )}

        <div className={baseClasses('amount-container', themeModifier)}>
          <BuySellPresetValuesRowsContainer baseClasses={baseClasses} />

          <BuySellCustomRowContainer
            baseClasses={baseClasses}
            themeModifier={themeModifier}
          />

          <BuySellOrderConfirmationModal />
        </div>

        {extraInfoComponent && (
          <div className={baseClasses('extra-info-container', themeModifier)}>
            {extraInfoComponent}
          </div>
        )}

        <BuySellOrderButton
          baseClasses={baseClasses}
          handleSubmit={handleSubmit}
          submitting={submitting}
          hideKYC={hideKYC}
          customKYCmessage={customKYCmessage}
        />
      </div>
    );
  }
}

BuySellComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  displayInstrumentSelector: PropTypes.bool,
  extraInfoComponent: PropTypes.element,
  themeModifier: PropTypes.string,
  submitting: PropTypes.bool.isRequired,
  hideKYC: PropTypes.bool,
  customKYCmessage: PropTypes.string
};

BuySellComponent.defaultProps = {
  displayInstrumentSelector: true,
  hideKYC: false,
  amountHeader: 'Amount'
};

BuySellComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default BuySellComponent;

import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { formatDateTime } from 'apex-web/lib/helpers/dateHelper';
import { buyValue } from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import { getOrderFee } from 'apex-web/lib/helpers/orderHelper.js';

import './BuySellConfirmationModalComponent.css';

const bemClasses = getBEMClasses('buy-sell-order-confirmation');

class BuySellConfirmationModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitting: false
    };
  }

  shouldComponentUpdate(nextProps) {
    const responseMessage = this.props.buySellForm.values.responseMessage;
    const nextResponseMessage = nextProps.buySellForm.values.responseMessage;

    if (responseMessage && nextResponseMessage) {
      return false;
    }
    return true;
  }

  placeOrder = () => {
    const { confirmReport, buySellForm } = this.props;
    this.setState({ submitting: true });
    confirmReport(buySellForm.values).then(
      response => {
        this.setState({ submitting: false });
      },
      err => {}
    );
  };

  render() {
    const {
      orderType,
      timeInForce,
      buySellForm,
      isOpen,
      close,
      selectedInstrument,
      positions,
      price,
      quantity,
      finalQuantity,
      onClose,
      hideFees,
      displayPricePerUnit,
      pricePerUnit
    } = this.props;
    const { context } = this;

    const isBuySide = buySellForm.values.side === buyValue;
    const product = selectedInstrument.Product1Symbol;
    const responseMessage = buySellForm.values.responseMessage;

    return (
      <Modal
        isOpen={isOpen}
        title={context.t(
          `${
            isBuySide
              ? 'Buy ' + selectedInstrument.Product1Symbol
              : 'Sell ' + selectedInstrument.Product1Symbol
          }`
        )}
        close={() => {
          close();
          onClose();
        }}
        footer={{
          buttonText: context.t(
            getConfirmButtonText(isBuySide, responseMessage)
          ),
          disabled: this.state.submitting,
          onClick: responseMessage
            ? () => {
                close();
                onClose();
              }
            : this.placeOrder,
          overrideClose: true
        }}
        customClass={bemClasses('modal')}>
        <div className={bemClasses()}>
          <h3 className={bemClasses('header')}>
            {responseMessage || context.t('Confirmation')}
          </h3>

          <APLabelWithText
            customClass={bemClasses('order-type')}
            label={context.t('Order Type')}
            text={context.t(`${orderType} (${timeInForce})`)}
          />

          <APLabelWithText
            customClass={bemClasses()}
            label={
              isBuySide
                ? context.t('{product} to Buy', { product })
                : context.t('{product} to Sell', { product })
            }
            text={context.t(`${quantity}`)}
          />

          <APLabelWithText
            customClass={bemClasses()}
            label={context.t(`Total Transaction Cost`)}
            text={context.t(`${selectedInstrument.Product2Symbol} ${price}`)}
          />

          {displayPricePerUnit && (
            <APLabelWithText
              customClass={bemClasses()}
              label={context.t(`Price Per Unit`)}
              text={pricePerUnit}
            />
          )}

          {!hideFees && (
            <React.Fragment>
              <APLabelWithText
                customClass={bemClasses()}
                label={context.t(`Fee`)}
                text={context.t(`${getOrderFee(buySellForm, positions)}`)}
              />

              <APLabelWithText
                customClass={bemClasses()}
                label={context.t(`Final Quantity`)}
                text={`${
                  isBuySide
                    ? selectedInstrument.Product1Symbol
                    : selectedInstrument.Product2Symbol
                } ${finalQuantity}`}
              />
            </React.Fragment>
          )}

          <APLabelWithText
            customClass={bemClasses()}
            label={context.t('Date')}
            text={formatDateTime(new Date())}
          />
        </div>
      </Modal>
    );
  }
}

BuySellConfirmationModalComponent.defaultProps = {
  displayPricePerUnit: false
};

BuySellConfirmationModalComponent.propTypes = {
  isBuySide: PropTypes.bool.isRequired,
  buySellForm: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  confirmReport: PropTypes.func.isRequired,
  selectedInstrument: PropTypes.object.isRequired,
  positions: PropTypes.object.isRequired,
  price: PropTypes.oneOfType([
    PropTypes.number.isRequired,
    PropTypes.string.isRequired
  ]),
  quantity: PropTypes.oneOfType([
    PropTypes.number.isRequired,
    PropTypes.string.isRequired
  ]),
  finalQuantity: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  onClose: PropTypes.func.isRequired,
  displayPricePerUnit: PropTypes.bool.isRequired,
  pricePerUnit: PropTypes.string
};

BuySellConfirmationModalComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default BuySellConfirmationModalComponent;

const getConfirmButtonText = (isBuySide, responseMessage) => {
  // context.t('OK')
  if (responseMessage) {
    return 'OK';
  }

  // context.t('Confirm Buy Order')
  // context.t('Confirm Sell Order')
  return isBuySide ? 'Confirm Buy Order' : 'Confirm Sell Order';
};

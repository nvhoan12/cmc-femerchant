import { connect } from 'react-redux';
import modalDecorator from 'apex-web/lib/hocs/modalDecorator';
import { MODAL_TYPES } from 'apex-web/lib/redux/actions/modalActions';
import BuySellConfirmationModalComponent from './BuySellConfirmationModalComponent';
import {
  getOrderFormInfoSelector,
  isBuySideBuySellFormSelector
} from 'apex-web/lib/redux/selectors/buySellSelectors';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import {
  placeOrder,
  clearBuySellForm
} from '../../../../redux/actions/orderEntryActions';
import { orderFormTypes } from 'apex-web/lib/constants/sendOrder/orderFormTypes';
import { orderTypes } from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import { timeInForceForOrderType } from 'apex-web/lib/constants/sendOrder/timeInForceConstants';
import { getOrderPricePerUnit } from 'apex-web/lib/helpers/orderHelper.js';
import { truncateToDecimals } from 'apex-web/lib/helpers/numberFormatter';
import { selectedInstrumentSelector } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import config from 'apex-web/lib/config';
import { positionSelector } from 'apex-web/lib/redux/selectors/positionSelectors';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';

const mapStateToProps = state => {
  const { form } = state;
  const buySellForm = form.buySell || { values: { fee: {} } };
  const selectedValue = getOrderFormInfoSelector(state);

  const price = selectedValue.price ? selectedValue.price : 0;
  const quantity = selectedValue.quantity ? selectedValue.quantity : '';
  const pricePerUnit = selectedValue.pricePerUnit
    ? selectedValue.pricePerUnit
    : 0;

  const selectedInstrument = selectedInstrumentSelector(state);
  const {
    Product2Symbol,
    Product1,
    Product2,
    PriceIncrement,
    QuantityIncrement
  } = selectedInstrument;
  const isBuySide = isBuySideBuySellFormSelector(state);
  const finalQuantityProduct = isBuySide ? Product1 : Product2;
  let finalQuantity = isBuySide ? quantity : price;

  if (finalQuantityProduct === buySellForm.values.fee.ProductId) {
    finalQuantity -= buySellForm.values.fee.OrderFee;
  }

  return {
    isBuySide,
    positions: positionSelector(state),
    buySellForm,
    selectedInstrument,
    price,
    quantity,
    pricePerUnit: getOrderPricePerUnit(
      pricePerUnit,
      Product2Symbol,
      convertIncrementToIntDecimalPlaces(PriceIncrement)
    ),
    finalQuantity: truncateToDecimals(
      finalQuantity,
      convertIncrementToIntDecimalPlaces(
        isBuySide ? QuantityIncrement : PriceIncrement
      )
    ),
    hideFees: !!config.global.hideFees,
    displayPricePerUnit: config.RetailBuySell.pricePerUnit,
    orderType: (
      Object.values(orderTypes).find(
        i => i.orderType === String(config.RetailBuySell.orderType)
      ) || {}
    ).displayName,
    timeInForce: Object.entries(timeInForceForOrderType).find(
      ([k, v]) => v === config.RetailBuySell.timeInForce
    )[0]
  };
};

const mapDispatchToProps = dispatch => {
  return {
    confirmReport: form =>
      dispatch(placeOrder(orderFormTypes.buySell, form)).then(res => {
        if (res.result === false) {
          dispatch(
            showSnack({
              id: 'confirmBuySellFail',
              text: res.errormsg || res.detail,
              textVars: res.textVars,
              type: 'warning'
            })
          );
        }
      }),
    onClose: () => dispatch(clearBuySellForm())
  };
};

const container = connect(
  mapStateToProps,
  mapDispatchToProps
)(BuySellConfirmationModalComponent);

export default modalDecorator({
  name: MODAL_TYPES.RETAIL_SUBMIT_ORDER
})(container);

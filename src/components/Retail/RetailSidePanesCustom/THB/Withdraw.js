import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { connect } from 'react-redux';
import { change, reduxForm } from 'redux-form';
import clsx from 'clsx';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { getAvailableBalance } from 'apex-web/lib/helpers/withdrawHelper';
import { getWithdrawFee } from 'apex-web/lib/redux/actions/withdrawActions';
import {
  required,
  biggerThanOrEqualToValue,
  lessThanOrEqualToValue
} from '../../../../helpers/formValidations';
import APButton from 'apex-web/lib/components/common/APButton';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import { closeSidePane } from 'apex-web/lib/redux/actions/sidePaneActions';
import VerificationRequiredContainer from 'apex-web/lib/components/VerificationRequired/VerificationRequiredContainer';
import { positionByProductIdSelector } from 'apex-web/lib/redux/selectors/positionSelectors';
import { kycVerificationLevelsSelector } from 'apex-web/lib/redux/selectors/kycLevelsSelectors';
import config from '../../../../config';
import { createWithdrawTicket } from '../../../../redux/actions/withdrawActions';
import { useDidUpdate } from '../../../../helpers/hook';
import successfulImg from '../../../../images/successful.png';
import WithdrawInfo from './components/WithdrawInfo';
import BankSelect from './components/BankSelect';
import AddBankAccount from './AddBankAccount';
import './Withdraw.css';

const minWithdraw = biggerThanOrEqualToValue(config.Withdraw.MinimumAmount);
const maxWithdraw = lessThanOrEqualToValue(config.Withdraw.MaximumAmount);
const classes = getBEMClasses('thb-withdraw');

const Withdraw = (
  {
    TicketAmount,
    product,
    handleSubmit,
    disableDeposit,
    position,
    fee,
    data,
    bankAccounts,
    handleCloseSidePane,
    getWithdrawFee,
    reduxFormChange,
    submitWithdraw
  },
  context
) => {
  const [isSuccess, setIsSuccess] = React.useState(false);
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [bankAccount, setBankAccount] = React.useState(null);
  const balance = getAvailableBalance(position.Amount, position.Hold);

  React.useEffect(() => {
    if (bankAccounts.length > 0) {
      setBankAccount(bankAccounts[0].id);
    }
  }, []);

  useDidUpdate(
    () => {
      reduxFormChange('bankAccount', bankAccount);
    },
    [bankAccount]
  );

  const onSubmit = async payload => {
    const bankAccount = bankAccounts.find(
      item => item.id.toString() === payload.bankAccount.toString()
    );
    setIsSubmitting(true);
    await submitWithdraw({
      ProductId: product.ProductId,
      Amount: data.Amount,
      TemplateType: 'Custom',
      TemplateForm: {
        Amount: data.Amount,
        BankID: bankAccount.bankID,
        BankName: bankAccount.bankName,
        BankAccountName: bankAccount.accountName,
        BankAccountNumber: bankAccount.bankNumber,
        BankAccountVerify: bankAccount.verify ? 1 : 0,
        BankAccountComment: ''
      }
    });
    setIsSubmitting(false);
    setIsSuccess(true);
  };

  useDidUpdate(
    () => {
      if (!isNaN(data.Amount)) {
        getWithdrawFee(product.ProductId, data.Amount);
      }
    },
    [data.Amount]
  );

  const isDisableWithdraw = config.global.disableWithdrawProducts.includes(
    'THB'
  );

  return (
    <div className="container-fluid h-100">
      <div className="row h-100">
        <div
          className={clsx(
            'col-lg-7',
            classes('divider-top'),
            classes('divider-side')
          )}>
          <div className={classes('form-container')}>
            {disableDeposit ? (
              <VerificationRequiredContainer
                disabled={disableDeposit}
                onClick={handleCloseSidePane}
              />
            ) : isSuccess ? (
              <div className={classes('successful-container')}>
                <img src={successfulImg} alt="successfulImg" />
                <h4>
                  {context.t('Your withdraw has been successfully added')}
                </h4>
                <APButton
                  type="button"
                  customClass={classes('successful')}
                  onClick={handleCloseSidePane}>
                  {context.t('Ok')}
                </APButton>
              </div>
            ) : (
              <React.Fragment>
                <div className="d-flex justify-content-end my-3">
                  <AddBankAccount />
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                  <BankSelect
                    label={context.t('Select Bank Account')}
                    value={bankAccount}
                    onChange={setBankAccount}
                    bankAccounts={bankAccounts}
                  />
                  <APNumberInput
                    type="text"
                    name="Amount"
                    labelInInput={product.ProductSymbol}
                    label={context.t(
                      'You may proceed with your withdrawal even though your bank account is Pending Verification'
                    )}
                    decimalPlaces={product.DecimalPlaces}
                    customClass={classes('')}
                    validate={[required, minWithdraw, maxWithdraw]}
                  />
                  <APButton
                    customClass={classes('')}
                    type="submit"
                    disabled={!bankAccount || isDisableWithdraw}
                    styleName="subtractive">
                    {isSubmitting
                      ? context.t('Processing...')
                      : context.t('Withdraw')}
                  </APButton>
                </form>
              </React.Fragment>
            )}
          </div>
        </div>
        <div className={clsx('col-lg-5', classes('divider-top'))}>
          <WithdrawInfo
            TicketAmount={TicketAmount}
            data={data}
            fee={fee}
            product={product}
            balance={balance}
          />
        </div>
      </div>
    </div>
  );
};

Withdraw.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { disableDeposit } = kycVerificationLevelsSelector(state);
  const productId = get(ownProps, 'product.ProductId', null);
  const position = positionByProductIdSelector(productId)(state);
  const fee = state.withdraw.withdrawFee;
  const TicketAmount = get(state, ['withdraw', 'TicketAmount'], null);
  const data = get(state, ['form', 'createFiatWithdraw', 'values'], {});
  const bankAccounts = state.btzBankAccount.bankAccounts;

  return {
    TicketAmount,
    disableDeposit,
    position,
    fee,
    data,
    bankAccounts
  };
};

const mapDispatchToProps = {
  handleCloseSidePane: closeSidePane,
  submitWithdraw: createWithdrawTicket,
  getWithdrawFee: (productId, amount) => getWithdrawFee(productId, amount),
  reduxFormChange: (field, value) => change('createFiatWithdraw', field, value)
};

const FiatWithdrawForm = reduxForm({
  form: 'createFiatWithdraw'
})(Withdraw);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FiatWithdrawForm);

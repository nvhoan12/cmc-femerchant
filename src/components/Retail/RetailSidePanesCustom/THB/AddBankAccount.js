import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { change, reduxForm } from 'redux-form';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import APInput from 'apex-web/lib/components/common/APInput';
import { required, numeric } from 'apex-web/lib/helpers/formValidations';
import APButton from 'apex-web/lib/components/common/APButton';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import successfulImg from '../../../../images/successful.png';
import config from '../../../../config';
import { fetchBTZBankAccount } from '../../../../redux/actions/btzBankAccountActions';
import BTZIcon from '../../../common/BTZIcon/BTZIcon';
import { banks } from './helper/banks-data';
import {
  isMerchantRole,
  getMerchantIdFromUserConfig
} from '../../../../helpers/userConfigHelper';
import './AddBankAccount.css';

const classes = getBEMClasses('add-bank-account');

const AddBankAccount = (
  { handleSubmit, userId, kycName, token, reduxFormChange, fetchBankAccount },
  context
) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [isAddSuccess, setIsAddSuccess] = React.useState(false);
  const [bankID, setBankID] = React.useState(banks[0].id);

  const handleClose = () => {
    setIsOpen(false);
    setIsAddSuccess(false);
  };

  React.useEffect(
    () => {
      reduxFormChange('bankID', bankID);
    },
    [bankID]
  );

  React.useEffect(
    () => {
      reduxFormChange('bankName', kycName.EN);
    },
    [kycName]
  );

  const onSubmit = async payload => {
    const res = await fetch(config.services.addBankAcc, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-BTZ-TOKEN': token
      },
      body: JSON.stringify({
        user_id: userId,
        bank_id: payload.bankID,
        acc_no: payload.bankNumber,
        acc_name: payload.bankName
      })
    });
    if (res.status === 200) {
      setIsAddSuccess(true);
      fetchBankAccount();
    }
  };

  return (
    <React.Fragment>
      <APButton
        type="button"
        customClass={classes()}
        onClick={() => setIsOpen(true)}>
        <BTZIcon iconName="fa-plus" className="mr-2" />
        {context.t('Add Bank Account')}
      </APButton>
      <Modal
        isOpen={isOpen}
        title={context.t('Add Bank Account')}
        close={handleClose}
        customClass={classes()}>
        {isAddSuccess ? (
          <div className={classes('successful-container')}>
            <img src={successfulImg} alt="successfulImg" />
            <h4>{context.t('Add Bank Account Successful')}</h4>
            <p>{context.t('Your bank account will be updated shortly.')}</p>
            <APButton
              type="button"
              customClass={classes('successful')}
              onClick={handleClose}>
              {context.t('Done')}
            </APButton>
          </div>
        ) : (
          <form onSubmit={handleSubmit(onSubmit)}>
            <p className={classes('text-body')}>
              {context.t(
                'Please verify your Bank Account before withdraw. The account must be under your name'
              )}
            </p>
            <div className={classes('select-bank-account-input')}>
              <label className="ap--label ap-input__label add-bank-account__label">
                {context.t('Select Bank Account')}
              </label>
              <div className="select-bank-account">
                {banks.map(bank => (
                  <label
                    key={bank.id}
                    className={`bank-account${
                      bank.id === bankID ? ' selected' : ''
                    }`}
                    onClick={() => setBankID(bank.id)}>
                    <img src={bank.icon} alt={bank.name} />
                  </label>
                ))}
              </div>
            </div>
            <APInput
              type="text"
              name="bankName"
              label={context.t('Bank Account Name')}
              disabled={true}
              customClass={classes()}
              validate={[required]}
            />
            <APInput
              type="text"
              name="bankNumber"
              label={context.t('Bank Account Number')}
              customClass={classes()}
              validate={[required, numeric]}
            />
            <APButton type="submit" styleName="additive">
              {context.t('Verify bank account')}
            </APButton>
          </form>
        )}
      </Modal>
    </React.Fragment>
  );
};

AddBankAccount.contextTypes = {
  t: PropTypes.func.isRequired
};

const AddBankAccountWithForm = reduxForm({
  form: 'addBankAccount'
})(AddBankAccount);

const mapStateToProps = state => {
  let userId = state.user.userInfo.UserId;
  if (isMerchantRole(state.user.userConfig)) {
    userId = getMerchantIdFromUserConfig(state.user.userConfig);
  }

  return {
    userId,
    kycName: state.btzBankAccount.kycName,
    token: state.auth.token
  };
};

const mapDispatchToProps = {
  reduxFormChange: (field, value) => change('addBankAccount', field, value),
  fetchBankAccount: fetchBTZBankAccount
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddBankAccountWithForm);

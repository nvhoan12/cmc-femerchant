import icBankSCB from '../../../../../images/banks/ic_bank_scb.png';
import icBankKBANK from '../../../../../images/banks/ic_bank_kbank.png';
import icBankKTB from '../../../../../images/banks/ic_bank_ktb.png';
import icBankBBL from '../../../../../images/banks/ic_bank_bbl.png';
import icBankKMA from '../../../../../images/banks/ic_bank_kma.png';
import icBankTTB from '../../../../../images/banks/ic_bank_ttb.png';
import icBankGSB from '../../../../../images/banks/ic_bank_gsb.png';
import icBankUOB from '../../../../../images/banks/ic_bank_uob.png';
import icBankCIMB from '../../../../../images/banks/ic_bank_cimb.png';
import icBankCITI from '../../../../../images/banks/ic_bank_citi.png';
import icBankSCBT from '../../../../../images/banks/ic_bank_scbt.png';
import icBankHSBC from '../../../../../images/banks/ic_bank_hsbc.png';
import icBankBAAC from '../../../../../images/banks/ic_bank_baac.png';
import icBankGHB from '../../../../../images/banks/ic_bank_ghb.png';
import icBankISBT from '../../../../../images/banks/ic_bank_isbt.png';
import icBankTISCO from '../../../../../images/banks/ic_bank_tisco.png';
import icBankKKP from '../../../../../images/banks/ic_bank_kkp.png';
import icBankICBC from '../../../../../images/banks/ic_bank_icbc.png';
import icBankLH from '../../../../../images/banks/ic_bank_lh.png';

const banks = [
  {
    id: '1',
    code: '14',
    icon: icBankSCB,
    name: 'Siam Commercial Bank',
    nameTH: 'ไทยพานิชย์'
  },
  {
    id: '2',
    code: '04',
    icon: icBankKBANK,
    name: 'Kbank',
    nameTH: 'กสิกร'
  },
  {
    id: '3',
    code: '06',
    icon: icBankKTB,
    name: 'KTB',
    nameTH: 'กรุงไทย'
  },
  {
    id: '4',
    code: '02',
    icon: icBankBBL,
    name: 'Bangkok Bank',
    nameTH: 'กรุงเทพ'
  },
  {
    id: '5',
    code: '25',
    icon: icBankKMA,
    name: 'KMA',
    nameTH: 'กรุงศรี'
  },
  {
    id: '6',
    code: '11',
    icon: icBankTTB,
    name: 'ttb',
    nameTH: 'ธนาคารทหารไทยธนชาต'
  },
  {
    id: '7',
    code: '30',
    icon: icBankGSB,
    name: 'GSB',
    nameTH: 'ออมสิน'
  },
  {
    id: '9',
    code: '24',
    icon: icBankUOB,
    name: 'UOB',
    nameTH: 'ยูโอบี'
  },
  {
    id: '10',
    code: '22',
    icon: icBankCIMB,
    name: 'CIMB',
    nameTH: 'ซีไอเอ็มบี'
  },
  {
    id: '11',
    code: '17',
    icon: icBankCITI,
    name: 'Citi',
    nameTH: 'Citi'
  },
  {
    id: '12',
    code: '020',
    icon: icBankSCBT,
    name: 'SCBT',
    nameTH: 'ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)'
  },
  {
    id: '13',
    code: '031',
    icon: icBankHSBC,
    name: 'HSBC',
    nameTH: 'ธนาคารฮ่องกงและเซี่ยงไฮ้แบงกิ้งคอร์ปอเรชั่น'
  },
  {
    id: '14',
    code: '033',
    icon: icBankBAAC,
    name: 'BAAC',
    nameTH: 'ธนาคารอาคารสงเคราะห์'
  },
  {
    id: '15',
    code: '034',
    icon: icBankGHB,
    name: 'GHB',
    nameTH: 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร'
  },
  {
    id: '16',
    code: 'ISBT',
    icon: icBankISBT,
    name: 'ISBT',
    nameTH: 'ธนาคารอิสลามแห่งประเทศไทย'
  },
  {
    id: '17',
    code: '067',
    icon: icBankTISCO,
    name: 'TISCO',
    nameTH: 'ธนาคารทิสโก้ จำกัด (มหาชน)'
  },
  {
    id: '18',
    code: '069',
    icon: icBankKKP,
    name: 'KKP',
    nameTH: 'ธนาคารเกียรตินาคิน จำกัด (มหาชน)'
  },
  {
    id: '19',
    code: '070',
    icon: icBankICBC,
    name: 'ICBC',
    nameTH: 'ธนาคารไอซีบีซี (ไทย) จำกัด (มหาชน)'
  },
  {
    id: '20',
    code: '073',
    icon: icBankLH,
    name: 'LH',
    nameTH: 'ธนาคารแลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)'
  }
];

export { banks };

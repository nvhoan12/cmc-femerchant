import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { change, reduxForm } from 'redux-form';
import clsx from 'clsx';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import APButton from 'apex-web/lib/components/common/APButton';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { required, biggerThanZero } from 'apex-web/lib/helpers/formValidations';
import { closeSidePane } from 'apex-web/lib/redux/actions/sidePaneActions';
import VerificationRequiredContainer from 'apex-web/lib/components/VerificationRequired/VerificationRequiredContainer';
import { kycVerificationLevelsSelector } from 'apex-web/lib/redux/selectors/kycLevelsSelectors';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import APCheckbox from '../../../common/APCheckbox';
import { adTrack } from '../../../../helpers/adjust';
import { useDidUpdate } from '../../../../helpers/hook';
import { createDepositTicket } from '../../../../redux/actions/depositActions';
import BTZStepper from '../../../common/BTZStepper/BTZStepper';
import BTZIcon from '../../../common/BTZIcon/BTZIcon';
import config from '../../../../config';
import successfulImg from '../../../../images/successful.png';
import DepositPaymentInfo from './components/DepositPaymentInfo';
import DepositInfo from './components/DepositInfo';
import './Deposit.css';

const classes = getBEMClasses('thb-deposit');
const saveKey = 'IsRememberCreateFiatDeposit';

const Deposit = (
  {
    product,
    handleSubmit,
    disableDeposit,
    lastDeposit,
    verificationOnClick,
    token,
    reduxFormChange,
    createFiatDepositValue
  },
  context
) => {
  const [step, setStep] = React.useState(1);
  const [qr, setRr] = React.useState(1);
  const [reference, setReference] = React.useState('');
  const [isOpenConfirm, setIsOpenConfirm] = React.useState(false);
  const [isOpenDownloaded, setIsOpenDownloaded] = React.useState(false);

  const handleOpenConfirm = async () => {
    setIsOpenConfirm(true);
  };

  const handleCancelDeposit = async () => {
    await fetch(config.services.cancelDeposit(lastDeposit.TicketNumber), {
      method: 'DELETE',
      headers: {
        'X-BTZ-TOKEN': token
      }
    });
    setIsOpenConfirm(false);
  };

  useDidUpdate(
    () => {
      if (lastDeposit.Status === 'Rejected') {
        setStep(1);
      }
      if (lastDeposit.Status === 'FullyProcessed') {
        setStep(3);
      }
    },
    [lastDeposit.TicketNumber, lastDeposit.Status]
  );

  React.useEffect(
    () => {
      if (lastDeposit.Status === 'New') {
        (async () => {
          setStep(2);
          const res = await fetch(config.services.getQrDeposit, {
            method: 'POST',
            headers: {
              'X-BTZ-TOKEN': token,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              accid: lastDeposit.AccountId,
              refid: lastDeposit.ReferenceId
            })
          });
          if (res.status === 200) {
            const jsonRes = await res.json();
            setRr(jsonRes.qrimg);
            setReference(jsonRes.ref);
          }
        })();
      }
    },
    [lastDeposit.TicketNumber, lastDeposit.Status, token]
  );

  React.useEffect(() => {
    if (localStorage.getItem(saveKey)) {
      reduxFormChange('IsOwnBankAccount', true);
      reduxFormChange('IsNotReuseQR', true);
      reduxFormChange('IsNotCancelAfterScanningQR', true);
      reduxFormChange('IsRemember', true);
    }
  }, []);

  React.useEffect(
    () => {
      if (createFiatDepositValue.IsRemember === true) {
        localStorage.setItem(saveKey, 'true');
      }
      if (createFiatDepositValue.IsRemember === false) {
        localStorage.removeItem(saveKey);
      }
    },
    [createFiatDepositValue.IsRemember]
  );

  const isDisableDeposit = config.global.disableDepositProducts.includes('THB');

  return (
    <div className="container-fluid h-100">
      <div className="row h-100">
        <div
          className={clsx(
            'col-lg-7',
            classes('divider-top'),
            classes('divider-side')
          )}>
          <div className={classes('form-container')}>
            {disableDeposit ? (
              <VerificationRequiredContainer
                disabled={disableDeposit}
                onClick={verificationOnClick}
              />
            ) : (
              <React.Fragment>
                <BTZStepper
                  items={[
                    { step: 1, title: context.t('Specify Amount') },
                    { step: 2, title: context.t('QR Scan') },
                    { step: 3, title: context.t('Payment Status') }
                  ]}
                  step={step}
                />
                <div className="pt-5">
                  {step === 1 && (
                    <form onSubmit={handleSubmit}>
                      <APNumberInput
                        type="text"
                        name="Amount"
                        labelInInput={product.Product || product.ProductSymbol}
                        label={context.t('Amount of {symbol}', {
                          symbol: product.Product || product.ProductSymbol
                        })}
                        decimalPlaces={product.DecimalPlaces}
                        customClass={classes()}
                        validate={[required, biggerThanZero]}
                      />
                      <div className="py-3">
                        <h4>{context.t('Conditions')}</h4>
                        <p>
                          {context.t(
                            'Please accept depodit conditions below to proceed.'
                          )}
                        </p>
                        <APCheckbox
                          id="IsOwnBankAccount"
                          name="IsOwnBankAccount"
                          label={context.t(
                            'Deposit from own bank account only'
                          )}
                          inline={false}
                          validate={[required]}
                        />
                        <APCheckbox
                          id="IsNotReuseQR"
                          name="IsNotReuseQR"
                          label={context.t('DO NOT reuse QR codes')}
                          inline={false}
                          validate={[required]}
                        />
                        <APCheckbox
                          id="IsNotCancelAfterScanningQR"
                          name="IsNotCancelAfterScanningQR"
                          label={context.t(
                            'DO NOT cancel deposit tickets after scanning QR code'
                          )}
                          inline={false}
                          validate={[required]}
                        />
                      </div>
                      {createFiatDepositValue.IsOwnBankAccount &&
                        createFiatDepositValue.IsNotReuseQR &&
                        createFiatDepositValue.IsNotCancelAfterScanningQR && (
                          <div className="py-3">
                            <APCheckbox
                              id="IsRemember"
                              name="IsRemember"
                              label={context.t(
                                'Remember my acceptance for the next time'
                              )}
                              inline={false}
                            />
                          </div>
                        )}
                      <APButton
                        type="submit"
                        customClass={classes()}
                        disabled={isDisableDeposit}
                        styleName="additive">
                        {context.t('Place Deposit Ticket')}
                      </APButton>
                    </form>
                  )}
                  {step === 2 && (
                    <React.Fragment>
                      <div className={classes('qr-container')}>
                        <p>
                          {context.t(
                            'Use the mobile banking app to scan QR code or Save the QR code to your device then browse this QR code to scan manually.'
                          )}
                        </p>
                        <img src={qr} alt="qr" />
                        <p className="warning">
                          {context.t(
                            'Please ensure the bank account name you are sending from is the same as your Bitazza account name. Deposits will NOT be accepted from other names and will incur a 100 THB processing fee.'
                          )}
                        </p>
                        <a
                          className={classes('download-qr-link')}
                          download={`Bitazza_${reference}.png`}
                          onClick={() => setIsOpenDownloaded(true)}
                          href={qr}>
                          <BTZIcon iconName="fa-download" className="mr-2" />
                          {context.t('Download QR Code')}
                        </a>
                        <Modal
                          isOpen={isOpenDownloaded}
                          title={context.t('QR Code Saved')}
                          close={() => setIsOpenDownloaded(false)}>
                          <h4>
                            {context.t('You can use this QR code ONCE only.')}
                          </h4>
                        </Modal>
                      </div>
                      <DepositPaymentInfo
                        amount={lastDeposit.Amount}
                        reference={reference}
                        step={step}
                      />
                      <div className="d-flex justify-content-center my-4">
                        <APButton
                          styleName="subtractive"
                          onClick={handleOpenConfirm}>
                          <BTZIcon iconName="fa-ban" className="mr-2" />
                          {context.t('Cancel')}
                        </APButton>
                      </div>
                      <Modal
                        isOpen={isOpenConfirm}
                        title={context.t('Cancel Deposit')}
                        close={() => setIsOpenConfirm(false)}
                        footer={{
                          buttonText: context.t('Confirm'),
                          buttonStyle: 'subtractive',
                          onClick: handleCancelDeposit
                        }}>
                        <h4 className={classes('cancel-deposit-modal')}>
                          {context.t(
                            'Please DO NOT cancel your deposit ticket if you have completed your bank transfer. Are you sure you want to cancel your deposit ticket?'
                          )}
                        </h4>
                        <br />
                        <h5>{`${context.t('Reference No.')} ${reference}`}</h5>
                        <h5>{`${context.t('Amount:')} ${formatNumberToLocale(
                          lastDeposit.Amount,
                          'THB'
                        )} THB`}</h5>
                      </Modal>
                    </React.Fragment>
                  )}
                  {step === 3 && (
                    <React.Fragment>
                      <div className={classes('successful-container')}>
                        <img src={successfulImg} alt="successfulImg" />
                        <h4>{context.t('Deposit Successful')}</h4>
                        <p>
                          {context.t(
                            'Your account balance will be updated shortly.'
                          )}
                        </p>
                      </div>
                      <DepositPaymentInfo
                        amount={lastDeposit.Amount}
                        reference={reference}
                        step={step}
                      />
                    </React.Fragment>
                  )}
                </div>
              </React.Fragment>
            )}
          </div>
        </div>
        <div className={clsx('col-lg-5', classes('divider-top'))}>
          <DepositInfo />
        </div>
      </div>
    </div>
  );
};

Deposit.contextTypes = {
  t: PropTypes.func.isRequired
};

const FiatDepositForm = reduxForm({
  form: 'createFiatDeposit',
  onSubmit: (payload, dispatch, { product }) => {
    adTrack('DepositAttempt');
    dispatch(
      createDepositTicket({
        Amount: payload.Amount,
        ProductId: product.ProductId,
        ProductSymbol: product.ProductSymbol
      })
    );
  }
})(Deposit);

const mapStateToProps = (state, ownProps) => {
  const { disableDeposit } = kycVerificationLevelsSelector(state);
  const fiatDeposit = state.deposit.depositsStatus.filter(
    item => item.AssetId === ownProps.product.ProductId
  );
  const lastDeposit = fiatDeposit.length !== 0 ? fiatDeposit[0] : {};
  return {
    disableDeposit,
    lastDeposit,
    token: state.auth.token,
    createFiatDepositValue: state.form.createFiatDeposit
      ? state.form.createFiatDeposit.values || {}
      : {}
  };
};

const mapDispatchToProps = dispatch => ({
  verificationOnClick: () => dispatch(closeSidePane()),
  reduxFormChange: (field, value) =>
    dispatch(change('createFiatDeposit', field, value))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FiatDepositForm);

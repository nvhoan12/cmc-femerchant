import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import '../Deposit.css';

const classes = getBEMClasses('thb-deposit');

const DepositInfo = (props, context) => {
  return (
    <div className={classes('info-container')}>
      <p className={classes('text-header')}>{context.t('Instruction')}</p>
      <p className={classes('text-body')}>
        <p className="font-weight-bolder m-0">{context.t('Step 1:')}</p>
        {context.t(
          'Enter how much THB you wish to deposit then place the deposit ticket.'
        )}
      </p>
      <p className={classes('text-body')}>
        <p className="font-weight-bolder m-0">{context.t('Step 2:')}</p>
        {context.t(
          'A QR code will be displayed. Scan or download the QR code and open it with your mobile banking app.'
        )}
      </p>
      <p className={classes('text-body')}>
        <p className="font-weight-bolder m-0">{context.t('Note:')}</p>
        {context.t(
          'Sometimes bank name and KYC name may be spelled differently which will then require manual verification for newly added bank accounts. If your THB deposit does not appear in your account within 15 minutes, please kindly contact support for deposit status or refund.'
        )}
      </p>
    </div>
  );
};

DepositInfo.contextTypes = {
  t: PropTypes.func.isRequired
};

export default DepositInfo;

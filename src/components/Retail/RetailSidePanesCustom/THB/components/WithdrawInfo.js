import React from 'react';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { formatNumberToLocale } from 'apex-web/lib//helpers/numberFormatter';
import '../Withdraw.css';

const classes = getBEMClasses('thb-withdraw');

const WithdrawInfo = (
  {
    data: { Amount },
    product: { ProductSymbol },
    balance,
    fee,
    TicketAmount,
    setAmount
  },
  context
) => {
  const currentBalance = balance
    ? `${formatNumberToLocale(balance, ProductSymbol)} ${ProductSymbol}`
    : '-';
  const amount = TicketAmount
    ? `${formatNumberToLocale(TicketAmount, ProductSymbol)} ${ProductSymbol}`
    : '-';
  const remainingBalance =
    balance && Amount
      ? `${formatNumberToLocale(
          balance - (TicketAmount + fee),
          ProductSymbol
        )} ${ProductSymbol}`
      : '-';

  return (
    <div className={classes('info-container')}>
      <p className={classes('text-header')}>{context.t('Balances')}</p>
      <p className={classes('text-body')}>
        {context.t('Your current {ProductSymbol} Balance', {
          ProductSymbol
        })}
        <span
          className="float-right cursor-pointer"
          onClick={() =>
            setAmount(
              formatNumberToLocale(balance, ProductSymbol).replace(',', '')
            )
          }>
          {currentBalance}
        </span>
      </p>
      <div className={classes('divider')} />
      <p className={classes('text-body')}>
        {context.t('Amount to Withdraw')}
        <span className="float-right">{amount}</span>
      </p>
      <div className={classes('divider')} />
      <p className={classes('text-body')}>
        {context.t('Fee')}
        <span className="float-right">
          {fee
            ? `${formatNumberToLocale(fee, ProductSymbol)} ${ProductSymbol}`
            : '-'}
        </span>
      </p>
      <div className={classes('divider')} />
      <p className={classes('text-body')}>
        {context.t('Remaining Balance')}
        <span className="float-right">{remainingBalance}</span>
      </p>
      <div className={classes('divider')} />
      <p className={classes('text-body')}>
        {context.t(
          'THB withdrawals during 11:30pm to 12:30am will be delayed due to bank maintenance during these hours.'
        )}
      </p>
    </div>
  );
};

WithdrawInfo.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapDispatchToProps = {
  setAmount: balance => change('createFiatWithdraw', 'Amount', balance)
};

export default connect(
  null,
  mapDispatchToProps
)(WithdrawInfo);

import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import BTZIcon from '../../../../common/BTZIcon/BTZIcon';
import '../Deposit.css';

const classes = getBEMClasses('thb-deposit');

const DepositPaymentInfo = ({ amount, reference, step }, context) => {
  return (
    <div className={classes('divider-top')}>
      <div className="row">
        <div className="col-lg-6">
          <p className={classes('info-title')}>{context.t('Amount:')}</p>
          <p className={classes('info')}>
            {`THB ${formatNumberToLocale(amount, 'THB')}`}
          </p>
        </div>
        <div className="col-lg-6">
          <p className={classes('info-title')}>{context.t('Deposit to:')}</p>
          <p className={classes('info')}>Bitazza Co., Ltd.</p>
        </div>
        <div className="col-lg-6">
          <p className={classes('info-title')}>{context.t('Reference No.')}</p>
          <p className={classes('info')}>{reference}</p>
        </div>
        <div className="col-lg-6">
          <p className={classes('info-title')}>{context.t('Status')}</p>
          <p className={classes('info')}>
            {step === 2 && (
              <React.Fragment>
                <BTZIcon iconName="fa-spinner fa-spin" className="mr-2" />
                {context.t('Waiting for payment')}
              </React.Fragment>
            )}
            {step === 3 && (
              <React.Fragment>
                {context.t('Transaction Completed')}
              </React.Fragment>
            )}
          </p>
        </div>
      </div>
    </div>
  );
};

DepositPaymentInfo.contextTypes = {
  t: PropTypes.func.isRequired
};

export default DepositPaymentInfo;

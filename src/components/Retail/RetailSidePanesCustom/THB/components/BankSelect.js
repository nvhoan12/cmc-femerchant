import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APIcon from '../../../../common/APIcon';
import { getlocalLanguage } from '../../../../../helpers/localLanguage';
import { useDidUpdate } from '../../../../../helpers/hook';
import './BankSelect.css';

const classes = getBEMClasses('bank-select');

const Option = ({ data, handleChange = () => {} }, context) => {
  const language = getlocalLanguage();
  return (
    <div className={classes('option')} onClick={() => handleChange(data.id)}>
      <img
        className={classes('bank-logo')}
        src={data.bankIcon}
        alt={data.bankName}
      />
      <div className={classes('info-block')}>
        <p className={classes('bank-name')}>
          {language === 'EN' ? data.bankName : data.bankNameTH}
        </p>
        <p className={classes('info')}>
          {`${data.accountName}, ${data.bankNumber} ${
            data.verify ? context.t('(Verified)') : ''
          }`}
        </p>
      </div>
    </div>
  );
};

Option.contextTypes = {
  t: PropTypes.func.isRequired
};

const BankSelect = ({ label, value, bankAccounts, onChange = () => {} }) => {
  const [selected, setSelected] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const container = React.useRef();

  const handleChange = bankID => {
    const select = bankAccounts.find(
      item => parseInt(item.id) === parseInt(bankID)
    );
    setSelected(select);
    onChange(select.id);
  };

  useDidUpdate(
    () => {
      const select = bankAccounts.find(
        item => parseInt(item.id) === parseInt(value)
      );
      setSelected(select);
    },
    [value]
  );

  const handleClickOutside = event => {
    if (container.current && !container.current.contains(event.target)) {
      setOpen(false);
    }
  };

  React.useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, []);

  return (
    <div className={classes('')}>
      {label && <label className="ap--label ap-input__label">{label}</label>}
      <div
        ref={container}
        onClick={() => setOpen(!open)}
        className={classes('input')}>
        <APIcon name="dropdown" customClass={classes()} />
        {selected && <Option data={selected} />}
        {open &&
          bankAccounts.length > 0 && (
            <div className={classes('option-container')}>
              {bankAccounts.map(item => (
                <Option key={item.id} data={item} handleChange={handleChange} />
              ))}
            </div>
          )}
      </div>
    </div>
  );
};

BankSelect.contextTypes = {
  t: PropTypes.func.isRequired
};

export default BankSelect;

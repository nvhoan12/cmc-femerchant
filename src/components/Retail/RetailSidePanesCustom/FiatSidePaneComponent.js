import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import ProductIconContainer from 'apex-web/lib/components/common/ProductIcon/ProductIconContainer';
import THB from './THB';
import './FiatSidePaneComponent.css';

const classes = getBEMClasses('fiat-side-pane-component');

const FiatSidePaneComponent = (
  { onSidePaneOpen, product, isSend },
  context
) => {
  React.useEffect(() => {
    onSidePaneOpen({
      customClass: classes(),
      hideHeader: true
    });
  }, []);

  return (
    <React.Fragment>
      <div className={classes('header')}>
        <p className={classes('header-text-top')}>
          {isSend ? context.t('Deposit') : context.t('Withdraw')}
        </p>
        <div className="d-flex align-items-center">
          <div className={classes('icon-container')}>
            <ProductIconContainer iconFileName={product.iconFileName} />
          </div>
          <p className={classes('header-product-name')}>
            {product.ProductSymbol} {product.ProductFullName}
          </p>
        </div>
      </div>
      {isSend ? (
        <THB.Deposit product={product} />
      ) : (
        <THB.Withdraw product={product} />
      )}
    </React.Fragment>
  );
};

FiatSidePaneComponent.defaultProps = {
  showDetails: true,
  onSidePaneOpen: () => {},
  isSend: true
};

FiatSidePaneComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default FiatSidePaneComponent;

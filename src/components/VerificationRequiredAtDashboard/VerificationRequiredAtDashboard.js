import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import APIcon from 'apex-web/lib/components/common/APIcon';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { kycVerificationLevelsSelector } from 'apex-web/lib/redux/selectors/kycLevelsSelectors';
import path from 'apex-web/lib/helpers/path';
import config from '../../config';

import './VerificationRequiredAtDashboard.css';

const classes = getBEMClasses('verification-required-at-dashboard');

const getVerificationRequiredMessage = (context, isUnderReview, noLink) => {
  if (noLink && isUnderReview)
    return context.t('You are not yet verified. Verification under review.');
  else if (noLink)
    return context.t(
      'You are not yet verified. Verify your identity to start trading.'
    );
  else if (isUnderReview)
    return context.t(
      'Verification under review. Click here to check your verification status.'
    );
  else
    return context.t(
      'Verify your identity to start trading. Click here to verify.'
    );
};


const VerificationRequiredAtDashboard = (props, context) => {
  const {
    className,
    disabled,
    isUnderReview,
    verificationLevelPageUrl,
    enableOnClick,
    onVerificationLinkClick,
    onClick,
    customKYCmessage,
    history,
    disableKYCLinkUnderReview,
    TransferRequestNotificationList
  } = props;

  const noLink = disableKYCLinkUnderReview
    ? isUnderReview
    : verificationLevelPageUrl === '';
  const handleOnVerificationLinkClick = () => {
    if (verificationLevelPageUrl.match(/^http/)) {
      window.location.href = verificationLevelPageUrl;
    } else {
      history.push(path(verificationLevelPageUrl));
      if (enableOnClick) onVerificationLinkClick();
    }
  };

  if (!disabled) {
    return <TransferRequestNotificationList />;
  }
  return (
    <div className={classes('container')}>
      <div
        className={classes(noLink ? 'no-link' : '')}
        onClick={() => {
          if (noLink) return;
          handleOnVerificationLinkClick();
          onClick && onClick();
        }}>
        <APIcon
          name="alert"
          customClass={classes('warning')}
          classModifiers="small"
        />
        <span
          className={`ap--label ${classes('not-verified')} ${className}`}>
          {customKYCmessage
            ? customKYCmessage
            : getVerificationRequiredMessage(context, isUnderReview, noLink)}
        </span>
      </div>
    </div>
  );
};

VerificationRequiredAtDashboard.defaultProps = {
  className: '',
  onVerificationLinkClick: () => {},
  isUnderReview: false,
  disabled: false,
  enableOnClick: true,
  disableKYCLinkUnderReview: false
};

VerificationRequiredAtDashboard.propTypes = {
  className: PropTypes.string,
  verificationLevelPageUrl: PropTypes.string.isRequired,
  onVerificationLinkClick: PropTypes.func.isRequired,
  isUnderReview: PropTypes.bool.isRequired,
  disabled: PropTypes.bool,
  enableOnClick: PropTypes.bool,
  customKYCmessage: PropTypes.string
};

VerificationRequiredAtDashboard.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { disableTrading, isUnderReview } = kycVerificationLevelsSelector(
    state
  );

  return {
    verificationRequired: true,
    verificationLevelPageUrl: config.KYC.verificationRequiredUrl,
    enableVerificationLinkOnClick: false,
    disabled: disableTrading,
    isUnderReview,
    ...ownProps
  };
};

export default connect(
  mapStateToProps,
  null
)(withRouter(VerificationRequiredAtDashboard));

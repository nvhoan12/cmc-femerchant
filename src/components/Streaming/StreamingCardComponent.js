import React, { useState } from 'react';
import PropTypes from 'prop-types';
import BigNumber from 'bignumber.js';
import get from 'lodash/get';
import ConfirmOrderModalContainer from 'apex-web/lib/components/Streaming/ConfirmOrderModal';
import ProductIconContainer from 'apex-web/lib/components/common/ProductIcon/ProductIconContainer';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import resize from 'apex-web/lib/hocs/resize';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { parseNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import { getOrderFee } from 'apex-web/lib/helpers/recalculateOrderFeeHelper';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import './StreamingCard.css';

const streamingCardClasses = getBEMClasses('streaming-card');
const TIF_GTC = 'TIF: GTC';
const TIF_FOK = 'TIF: FOK';
const TIF_IOC = 'TIF: IOC';
const TIMEINFORCE_GTC = '1';
const TIMEINFORCE_FOK = '4';
const TIMEINFORCE_IOC = '3';
const MARKET_ORDER = 'Market Order';
const LIMIT_ORDER = 'Limit Order';

const StreamingCard = (props, context) => {
  const {
    defaultSelection,
    position,
    instrument,
    products,
    level1,
    selectOrderType,
    productManifest,
    timezone,
    AccountId,
    showNotification,
    clearForm,
    selectedInstrumentId,
    additionalSlipage
  } = props;

  const [isConfirmModalOpen, setConfirmModal] = useState(false);
  const [form, setForm] = useState(false);
  const [metaData, setMetaData] = useState(false);
  const [orderFee, setOrderFee] = useState({ OrderFee: 0, productId: 0 });

  const dismissConfirmModal = () => {
    setConfirmModal(false);
  };

  const fetchOrderFee = orderFeeData => {
    getOrderFee(orderFeeData).then(result => {
      const data = {
        ...result,
        product: products.find(prod => prod.ProductId === result.ProductId)
          .Product
      };
      setOrderFee(data);
    });
  };

  const handleSubmit = (form, orderFeeData, confirmationModalProps) => {
    fetchOrderFee(orderFeeData);
    setConfirmModal(true);
    setForm(form);
    setMetaData(confirmationModalProps);
  };

  const currentLevel = get(level1, [instrument.InstrumentId], {});

  const getProductName = product => {
    const search = products.find(prod => prod.Product === product);
    if (search) {
      return search.ProductFullName;
    } else {
      return product;
    }
  };

  const calculateSpread = (bestBid, bestOffer) => {
    return formatNumberToLocale(
      Math.abs(bestBid - bestOffer),
      convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
    );
  };

  const fetchProductFileName = symbol => {
    return Object.keys(productManifest.manifest).indexOf(symbol) === -1
      ? symbol + 'DEFAULT_ICON'
      : productManifest.manifest[symbol].iconFileName;
  };

  const fetchStartChars = input => {
    let startString = '';
    let middleString = '';
    let endString = '';

    let middleIndex = input.length / 2;
    let leftIndex = input.length % 2 == 0 ? middleIndex - 2 : middleIndex - 1;
    let rightIndex = input.length % 2 == 0 ? middleIndex + 1 : middleIndex + 2;

    startString = input.substring(0, leftIndex);
    middleString = input.substring(leftIndex, rightIndex);
    endString = input.substring(rightIndex, input.length);

    if (middleString.includes('.') || middleString.includes(',')) {
      leftIndex = leftIndex - 1;
      startString = input.substring(0, leftIndex);
    }

    return startString;
  };

  const fetchMiddleChars = input => {
    let startString = '';
    let middleString = '';
    let endString = '';

    let middleIndex = input.length / 2;
    let leftIndex = input.length % 2 == 0 ? middleIndex - 2 : middleIndex - 1;
    let rightIndex = input.length % 2 == 0 ? middleIndex + 1 : middleIndex + 2;

    startString = input.substring(0, leftIndex);
    middleString = input.substring(leftIndex, rightIndex);
    endString = input.substring(rightIndex, input.length);

    if (middleString.includes('.') || middleString.includes(',')) {
      leftIndex = leftIndex - 1;
    }

    middleString = input.substring(leftIndex, rightIndex);
    return middleString;
  };

  const fetchLastChars = input => {
    let endString = '';
    const middleIndex = input.length / 2;
    const leftIndex = input.length % 2 == 0 ? middleIndex - 2 : middleIndex - 1;
    const rightIndex =
      input.length % 2 == 0 ? middleIndex + 1 : middleIndex + 2;
    endString = input.substring(rightIndex, input.length);
    return endString;
  };

  const buyPrice =
    defaultSelection[position].newBuyPrice == ''
      ? formatNumberToLocale(
          currentLevel.BestOffer,
          convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
        )
      : defaultSelection[position].newBuyPrice;

  const sellPrice =
    defaultSelection[position].newSellPrice == ''
      ? formatNumberToLocale(
          currentLevel.BestBid,
          convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
        )
      : defaultSelection[position].newSellPrice;

  const limitBuyPrice =
    defaultSelection[position].limitBuyPrice == ''
      ? formatNumberToLocale(
          currentLevel.BestOffer,
          convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
        )
      : defaultSelection[position].limitBuyPrice;

  const limitSellPrice =
    defaultSelection[position].limitSellPrice == ''
      ? formatNumberToLocale(
          currentLevel.BestBid,
          convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
        )
      : defaultSelection[position].limitSellPrice;

  const maxValue = formatNumberToLocale(
    parseFloat(parseNumberToLocale(buyPrice)) +
      parseFloat(parseNumberToLocale(buyPrice)) *
        (parseFloat(defaultSelection[position].slipage) + additionalSlipage) *
        0.0001,
    convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
  );

  const minValue = formatNumberToLocale(
    parseFloat(parseNumberToLocale(sellPrice)) -
      parseFloat(parseNumberToLocale(sellPrice)) *
        (parseFloat(defaultSelection[position].slipage) + additionalSlipage) *
        0.0001,
    convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
  );

  const maxLimitValue = formatNumberToLocale(
    parseFloat(parseNumberToLocale(limitBuyPrice)) +
      parseFloat(parseNumberToLocale(limitBuyPrice)) *
        (parseFloat(defaultSelection[position].slipage) + additionalSlipage) *
        0.0001,
    convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
  );

  const minLimitValue = formatNumberToLocale(
    parseFloat(parseNumberToLocale(limitSellPrice)) -
      parseFloat(parseNumberToLocale(limitSellPrice)) *
        (parseFloat(defaultSelection[position].slipage) + additionalSlipage) *
        0.0001,
    convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
  );

  const buildOrderForm = (
    orderType,
    currentLevel,
    selectedInstrument,
    inputPrice,
    selectedSide,
    timeInForce,
    selectedPosition,
    limitPrice,
    minmaxLimitValue
  ) => {
    const price = inputPrice.replace(',', '');
    const orderFree = 0;
    const productId = selectedInstrument.Product1;
    const quantity = defaultSelection[position].qty;
    const slippage = defaultSelection[position].slipage;
    const selectedInstrumentId = selectedInstrument.InstrumentId;
    const side = selectedSide;
    const stopPrice = '';
    const totalAmount = '0';

    const form = {
      fee: orderFee,
      limitPrice: limitPrice,
      orderType: orderType,
      quantity: quantity,
      selectedInstrumentId: selectedInstrumentId,
      side: side,
      stopPrice: stopPrice,
      timeInForce: timeInForce,
      totalAmount: totalAmount
    };

    const orderFeeData = {
      OMSId: currentLevel.OMSId,
      AccountId: AccountId,
      InstrumentId: selectedInstrumentId,
      Amount: defaultSelection[position].qty,
      Quantity: defaultSelection[position].qty,
      Side: side,
      Price: price,
      OrderType: orderType,
      BestBid: currentLevel.BestBid,
      BestOffer: currentLevel.BestOffer
    };

    const orderTotal = new BigNumber(price).times(new BigNumber(quantity));

    const confirmationModalProps = {
      marketPrice: price,
      orderTotal: formatNumberToLocale(
        orderTotal,
        convertIncrementToIntDecimalPlaces(selectedInstrument.PriceIncrement)
      ),
      Product1Symbol: selectedInstrument.Product1Symbol,
      Product2Symbol: selectedInstrument.Product2Symbol,
      isBuySide: selectedSide == '0' ? true : false,
      minmaxLimitValue: minmaxLimitValue
    };
    handleSubmit(form, orderFeeData, confirmationModalProps);
  };

  const placeBuyOrder = (
    instrument,
    price,
    selectedPosition,
    minmaxValue,
    minmaxLimitValue
  ) => {
    const timeInForce =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? TIMEINFORCE_GTC
        : defaultSelection[position].orderTypeValue == TIF_IOC
          ? TIMEINFORCE_IOC
          : TIMEINFORCE_FOK;

    const orderType =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? MARKET_ORDER
        : LIMIT_ORDER;

    const selectedPrice =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? price
        : minmaxLimitValue;

    const limitPrice =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? price
        : minmaxLimitValue;

    if (defaultSelection[position].qty <= 0) {
      showNotification('Please enter valid quantity');
      return;
    }

    if (
      defaultSelection[position].orderTypeValue != TIF_GTC &&
      defaultSelection[position].slipage <= 0
    ) {
      showNotification('Please enter valid slippage');
      return;
    }

    buildOrderForm(
      orderType,
      currentLevel,
      instrument,
      selectedPrice,
      0,
      timeInForce,
      selectedPosition,
      limitPrice,
      minmaxLimitValue
    );
  };

  const placeSellOrder = (
    instrument,
    price,
    selectedPosition,
    minmaxValue,
    minmaxLimitValue
  ) => {
    const timeInForce =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? TIMEINFORCE_GTC
        : defaultSelection[position].orderTypeValue == TIF_IOC
          ? TIMEINFORCE_IOC
          : TIMEINFORCE_FOK;

    const orderType =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? MARKET_ORDER
        : LIMIT_ORDER;

    const selectedPrice =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? price
        : minmaxLimitValue;

    const limitPrice =
      defaultSelection[position].orderTypeValue == TIF_GTC
        ? ''
        : minmaxLimitValue;

    if (defaultSelection[position].qty <= 0) {
      showNotification('Please enter valid quantity');
      return;
    }

    if (
      defaultSelection[position].orderTypeValue != TIF_GTC &&
      defaultSelection[position].slipage <= 0
    ) {
      showNotification('Please enter valid slippage');
      return;
    }

    buildOrderForm(
      orderType,
      currentLevel,
      instrument,
      selectedPrice,
      1,
      timeInForce,
      selectedPosition,
      limitPrice,
      minmaxLimitValue
    );
  };

  return (
    <div
      className={`${
        defaultSelection[position].orderTypeValue != TIF_GTC
          ? streamingCardClasses('expanded')
          : streamingCardClasses()
      } container-with-shadow`}>
      <div className={streamingCardClasses('row')}>
        <APLabelWithText
          label={`${getProductName(
            instrument.Product1Symbol
          )} / ${getProductName(instrument.Product2Symbol)}`}
          text={instrument.Symbol}
          customClass={streamingCardClasses()}
          classModifiers={'header'}
        />
        <div className={streamingCardClasses('product-icon')}>
          <ProductIconContainer
            iconFileName={fetchProductFileName(instrument.Product1Symbol)}
            size={32}
          />
        </div>
      </div>

      <div className={streamingCardClasses('row')}>
        <APLabelWithText
          label={context.t('24-Hour Low')}
          text={formatNumberToLocale(
            currentLevel.SessionLow,
            convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
          )}
          customClass={streamingCardClasses()}
        />
        <APLabelWithText
          label={context.t('Spread')}
          text={calculateSpread(currentLevel.BestBid, currentLevel.BestOffer)}
          customClass={streamingCardClasses()}
          classModifiers={'spread-text'}
        />
        <APLabelWithText
          label={context.t('24-Hour High')}
          text={formatNumberToLocale(
            currentLevel.SessionHigh,
            convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
          )}
          customClass={streamingCardClasses()}
          z
          classModifiers={'high-hour-text'}
        />
      </div>

      <div className={streamingCardClasses('qty-container')}>
        <APNumberInput
          type="text"
          placeholder="0"
          name={`quantity-${position}`}
          classModifiers={'quantity'}
          customClass={streamingCardClasses()}
          decimalPlaces={convertIncrementToIntDecimalPlaces(
            instrument.QuantityIncrement
          )}
          step={instrument.QuantityIncrement}
        />
      </div>

      <div className={streamingCardClasses('sell-buy-container-for-graph')}>
        <div className={streamingCardClasses('sell-buy-container1')}>
          <div className={streamingCardClasses('sell-buy-container-graph')}>
            <div
              className={streamingCardClasses(
                'sell-buy-container-graph-mirror'
              )}>
              <div
                onClick={() =>
                  placeSellOrder(
                    instrument,
                    sellPrice,
                    position,
                    minValue,
                    minLimitValue
                  )
                }
                className={streamingCardClasses('lshapevertical-sell')}
              />
              <div
                onClick={() =>
                  placeSellOrder(
                    instrument,
                    sellPrice,
                    position,
                    minValue,
                    minLimitValue
                  )
                }
                className={streamingCardClasses('lshapehorizontal-sell')}
              />
              <div className={streamingCardClasses('lshapeinside-sell')} />
              <div
                onClick={() =>
                  placeSellOrder(
                    instrument,
                    sellPrice,
                    position,
                    minValue,
                    minLimitValue
                  )
                }
                className={streamingCardClasses('lshapeoutside-sell')}
              />
            </div>
          </div>
        </div>
        <div className={streamingCardClasses('sell-buy-container2')}>
          <div className={streamingCardClasses('sell-buy-container-graph')}>
            <div
              onClick={() =>
                placeBuyOrder(
                  instrument,
                  buyPrice,
                  position,
                  maxValue,
                  maxLimitValue
                )
              }
              className={streamingCardClasses('lshapevertical-buy')}
            />
            <div
              onClick={() =>
                placeBuyOrder(
                  instrument,
                  buyPrice,
                  position,
                  maxValue,
                  maxLimitValue
                )
              }
              className={streamingCardClasses('lshapehorizontal-buy')}
            />
            <div
              onClick={() =>
                placeBuyOrder(
                  instrument,
                  buyPrice,
                  position,
                  maxValue,
                  maxLimitValue
                )
              }
              className={streamingCardClasses('lshapeinside-buy')}
            />
            <div
              onClick={() =>
                placeBuyOrder(
                  instrument,
                  buyPrice,
                  position,
                  maxValue,
                  maxLimitValue
                )
              }
              className={streamingCardClasses('lshapeoutside-buy')}
            />
          </div>
        </div>
      </div>

      <div className={streamingCardClasses('buy-sell-text-container')}>
        <div className={streamingCardClasses('buy-sell-text-container-top')}>
          <div
            onClick={() =>
              placeSellOrder(
                instrument,
                sellPrice,
                position,
                minValue,
                minLimitValue
              )
            }
            className={streamingCardClasses('buy-text-container')}>
            <div className={streamingCardClasses('width-match-parent')}>
              <span
                className={streamingCardClasses(
                  'sell-text-container-span-left'
                )}>
                Sell
              </span>
            </div>
          </div>

          <div
            className={streamingCardClasses('sell-text-container')}
            onClick={() =>
              placeBuyOrder(
                instrument,
                buyPrice,
                position,
                maxValue,
                maxLimitValue
              )
            }>
            <div className={streamingCardClasses('width-match-parent')}>
              <span
                className={streamingCardClasses(
                  'sell-text-container-span-right'
                )}>
                Buy
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className={streamingCardClasses('buy-sell-price-container')}>
        <div className={streamingCardClasses('sell-price')}>
          <div
            onClick={() =>
              placeSellOrder(
                instrument,
                sellPrice,
                position,
                minValue,
                minLimitValue
              )
            }
            className={streamingCardClasses('sell-text-span')}>
            <span>{fetchStartChars(sellPrice)}</span>
            <span className={streamingCardClasses('magnifyNumber')}>
              {fetchMiddleChars(sellPrice)}
            </span>
            <span>{fetchLastChars(sellPrice)}</span>
          </div>
        </div>
        <div className={streamingCardClasses('buy-price')}>
          <div
            onClick={() =>
              placeBuyOrder(
                instrument,
                buyPrice,
                position,
                maxValue,
                maxLimitValue
              )
            }
            className={streamingCardClasses('buy-text-span')}>
            <span>{fetchStartChars(buyPrice)}</span>
            <span className={streamingCardClasses('magnifyNumber')}>
              {fetchMiddleChars(buyPrice)}
            </span>
            <span>{fetchLastChars(buyPrice)}</span>
          </div>
        </div>
      </div>

      <div className={streamingCardClasses('timezone')}>
        <div>
          <span className={streamingCardClasses('small-text-span')}>
            {timezone}
          </span>
        </div>
        <div>
          <select
            value={defaultSelection[position].orderTypeValue}
            onChange={value => selectOrderType(position, value)}
            className={streamingCardClasses('ap-instrument-type-select')}>
            <option value={TIF_GTC}>{TIF_GTC}</option>
            <option value={TIF_FOK}>{TIF_FOK}</option>
            <option value={TIF_IOC}>{TIF_IOC}</option>
          </select>
        </div>
      </div>

      {defaultSelection[position].orderTypeValue != TIF_GTC && (
        <div>
          <div className={streamingCardClasses('row')}>
            <div className={streamingCardClasses('slip-page-text')}>
              <span className={streamingCardClasses('small-text-span')}>
                Allowed slippage (bps)
              </span>
            </div>
            <APNumberInput
              type="text"
              placeholder="10"
              name={`slipage-${position}`}
              classModifiers={'slippage'}
              customClass={streamingCardClasses()}
              decimalPlaces={convertIncrementToIntDecimalPlaces(
                instrument.PriceIncrement
              )}
              step={instrument.PriceIncrement}
            />
          </div>

          <div className={streamingCardClasses('min-max')}>
            <div>
              <span className={streamingCardClasses('small-text-span')}>
                Min:{' '}
                <font className={streamingCardClasses('footer-text-span-min')}>
                  {minLimitValue}
                </font>
              </span>
            </div>
            <div>
              <span className={streamingCardClasses('small-text-span')}>
                Max:{' '}
                <font className={streamingCardClasses('footer-text-span-max')}>
                  {maxLimitValue}
                </font>
              </span>
            </div>
          </div>
        </div>
      )}

      <ConfirmOrderModalContainer
        isConfirmModalOpen={isConfirmModalOpen}
        formData={form}
        dismissConfirmModal={dismissConfirmModal}
        orderFee={orderFee}
        metaData={metaData}
        priceIncrement={instrument.PriceIncrement}
        quantityIncrement={instrument.QuantityIncrement}
        clearForm={clearForm}
        selectedInstrumentId={selectedInstrumentId}
      />
    </div>
  );
};

StreamingCard.contextTypes = {
  t: PropTypes.func.isRequired
};

export default resize(StreamingCard);

import React, { useState, useEffect } from 'react';
import StreamingCard from './StreamingCardComponent';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { getPriceDataForQuantitiesSelector } from 'apex-web/lib/redux/selectors/buySellSelectors';
import './StreamingCardGrid.css';

const streamingCardGridClasses = getBEMClasses('streaming-card-grid');

const TIF_GTC = 'TIF: GTC';
const TIF_FOK = 'TIF: FOK';
const TIF_IOC = 'TIF: IOC';

const StreamingCardGrid = props => {
  const defaultCardData = props.instruments.map(() => {
    return {
      qty: 0,
      orderTypeValue: TIF_GTC,
      slipage: 10,
      min: 0,
      max: 0,
      newBuyPrice: '',
      newSellPrice: '',
      limitBuyPrice: '',
      limitSellPrice: ''
    };
  });

  const [currentPos, setCurrentPos] = useState(0);
  const [currentVal, setCurrentVal] = useState(0);
  const [defaultSelection, setDefaultSelection] = useState(defaultCardData);
  const [timezone, setTimezone] = useState('');
  const [orderTypeCounter, setOrderTypeCounter] = useState(TIF_GTC);
  const [orderType, setOrderType] = useState([
    {
      value: TIF_GTC,
      label: TIF_GTC
    },
    {
      value: TIF_FOK,
      label: TIF_FOK
    },
    {
      value: TIF_IOC,
      label: TIF_IOC
    }
  ]);

  const selectOrderType = (position, e) => {
    let newSelection = {
      ...defaultSelection
    };
    newSelection[position].orderTypeValue = e.target.value;
    newSelection[position].slipage = 10;

    setDefaultSelection(newSelection);
    setOrderTypeCounter(e.target.value);

    if (e.target.value == TIF_GTC) {
      setTimeout(function() {
        setDefaultSlippage(0);
      }, 100);
    } else {
      setTimeout(function() {
        setDefaultSlippage(10);
      }, 100);
    }
  };

  const clearForm = () => {
    let result = Object.assign({}, defaultSelection);
    for (let [key, value] of Object.entries(result)) {
      result[key].qty = 0;
    }
    setCurrentPos(-1);
    setCurrentVal(-1);
    setDefaultSelection(result);
    props.reset();
  };

  const getCurrentTime = () => {
    const today = new Date().toGMTString();
    const currentTimezoneSplit = today.split(' ');
    const extractTime = `${currentTimezoneSplit[4]} UTC`;
    return extractTime;
  };

  const setQuantityDataForSelection = (selectedPosition, selectedValue) => {
    if (currentPos != -1) {
      const instrumentId = props.instruments[selectedPosition].InstrumentId;
      let newSelection = {
        ...defaultSelection
      };

      if (selectedPosition != currentPos) {
        props.changeReduxForm({ field: 'quantity-' + currentPos, value: 0 });
        newSelection[currentPos].qty = 0;
        newSelection[currentPos].newBuyPrice = '';
        newSelection[currentPos].newSellPrice = '';
        newSelection[currentPos].limitBuyPrice = '';
        newSelection[currentPos].limitSellPrice = '';
      }

      if (instrumentId != props.selectedInstrumentId) {
        props.setInstrument({
          selectedInstrumentId: props.selectedInstrumentId,
          instrumentId: instrumentId
        });
      }

      const newBuyPrice = getPriceDataForQuantitiesSelector(props.state, '0', [
        parseFloat(selectedValue)
      ]);
      const newSellPrice = getPriceDataForQuantitiesSelector(props.state, '1', [
        parseFloat(selectedValue)
      ]);

      newSelection[selectedPosition].qty = parseFloat(selectedValue);
      newSelection[selectedPosition].newBuyPrice =
        newBuyPrice[0].pricePerUnit != null ? newBuyPrice[0].pricePerUnit : '';
      newSelection[selectedPosition].newSellPrice =
        newSellPrice[0].pricePerUnit != null
          ? newSellPrice[0].pricePerUnit
          : '';
      newSelection[selectedPosition].limitBuyPrice =
        newBuyPrice[0].limitPrice != null ? newBuyPrice[0].limitPrice : '';
      newSelection[selectedPosition].limitSellPrice =
        newSellPrice[0].limitPrice != null ? newSellPrice[0].limitPrice : '';
      setDefaultSelection(newSelection);
    }
  };

  useEffect(
    () => {
      if (
        props.formData &&
        props.formData.active &&
        props.formData.active.includes('slipage')
      ) {
        const selectedPosition = props.formData.active.split('-')[1];
        const selectedValue = props.formData.values
          ? props.formData.values[props.formData.active]
          : 0;

        if (selectedValue > 0) {
          const newSelection = {
            ...defaultSelection
          };
          newSelection[selectedPosition].slipage = parseFloat(selectedValue);
          setDefaultSelection(newSelection);
        }
      } else if (
        props.formData &&
        props.formData.active &&
        props.formData.active.includes('quantity')
      ) {
        const selectedPosition = props.formData.active.split('-')[1];
        const selectedValue = props.formData.values
          ? props.formData.values[props.formData.active]
          : 0;

        setCurrentPos(selectedPosition);
        setCurrentVal(selectedValue);

        setQuantityDataForSelection(selectedPosition, selectedValue);
      }
    },
    [props.formData]
  );

  useEffect(() => {
    const interval = setInterval(() => {
      setTimezone(getCurrentTime());
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(
    () => {
      setQuantityDataForSelection(currentPos, currentVal);
    },
    [props.level1]
  );

  useEffect(() => {
    props.instruments.forEach(inst => {
      props.getLevel1Data(inst.InstrumentId);
    });

    return () => {
      props.instruments
        .filter(i => i.InstrumentId !== props.selectedInstrumentId)
        .forEach(inst => {
          props.unsubscribeLevel1(inst.InstrumentId);
        });
    };
  }, []);

  const setDefaultSlippage = value => {
    props.instruments.map((item, position) => {
      props.changeReduxForm({ field: `slipage-${position}`, value: value });
    });
  };

  const items = props.instruments.map((item, position) => {
    return (
      <StreamingCard
        key={position}
        defaultSelection={defaultSelection}
        position={position}
        instrument={item}
        formData={props.formData}
        products={props.products}
        level1={props.level1}
        productManifest={props.productManifest}
        orderType={orderType}
        selectOrderType={selectOrderType}
        timezone={timezone}
        AccountId={props.AccountId}
        showNotification={props.showNotification}
        clearForm={clearForm}
        selectedInstrumentId={props.selectedInstrumentId}
        additionalSlipage={props.additionalSlipage}
      />
    );
  });

  return (
    <React.Fragment>
      {props.fetching || !!items.length ? (
        <div className={streamingCardGridClasses()}>
          {props.fetching ? <Spinner /> : items}
        </div>
      ) : (
        ''
      )}
    </React.Fragment>
  );
};

export default StreamingCardGrid;

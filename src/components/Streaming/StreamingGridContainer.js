import { connect } from 'react-redux';
import { reduxForm, change } from 'redux-form';
import {
  fetchingSelector,
  fundedPositionsSelector
} from 'apex-web/lib/redux/selectors/productPositionSelectors';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import { standardInstrumentsWithProductFullNamesAndProductSymbols } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import {
  getLevel1Data,
  subscribeToLevel1,
  unsubscribeLevel1
} from 'apex-web/lib/redux/actions/level1Actions';
import {
  getLevel2Data,
  subscribeToLevel2,
  unsubscribeLevel2
} from 'apex-web/lib/redux/actions/level2Actions';
import { setInstrument } from 'apex-web/lib/redux/actions/instrumentActions';
import StreamingCardGridComponent from './StreamingCardGridComponent';
import filterSortSupportedInstruments from 'apex-web/lib/helpers/filterSortSupportedInstruments';
import config from '../../config';

const streamingForm = 'StreamingForm';
const { StreamingInstruments } = config;

const favoriteInstrumentsIDString = localStorage.getItem(
  'favoriteInstrumentsID'
);
const favoriteInstruments = favoriteInstrumentsIDString
  ? JSON.parse(favoriteInstrumentsIDString)
  : [];

const filterInstruments = allInstruments => {
  const standardInstruments = filterSortSupportedInstruments(allInstruments);

  if (favoriteInstruments.length !== 0) {
    return standardInstruments.filter(i =>
      favoriteInstruments.includes(i.InstrumentId)
    );
  }
  return standardInstruments.filter(i => i.Product2Symbol === 'THB');
};

const mapStateToProps = (state, ownProps) => {
  return {
    fetching: fetchingSelector(state),
    state: state,
    products: fundedPositionsSelector(state),
    instruments: filterInstruments(
      standardInstrumentsWithProductFullNamesAndProductSymbols(state)
    ),
    level1: state.apexCore.level1,
    productManifest: state.productManifest,
    formData: state.form[streamingForm],
    AccountId: state.user.selectedAccountId,
    selectedInstrumentId: state.apexCore.instrument.selectedInstrumentId,
    additionalSlipage: StreamingInstruments.additionalSlippage
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setInstrument: payload => {
      dispatch(setInstrument(payload.instrumentId));
      // resubscribe to level2
      dispatch(subscribeToLevel1(payload.instrumentId));
      dispatch(unsubscribeLevel2(payload.selectedInstrumentId));
      dispatch(getLevel2Data(payload.instrumentId));
      dispatch(subscribeToLevel2(payload.instrumentId));
      localStorage.setItem('selectedInstrumentId', payload.instrumentId);
    },
    getLevel1Data: InstrumentId => dispatch(getLevel1Data(InstrumentId)),
    unsubscribeLevel1: InstrumentId =>
      dispatch(unsubscribeLevel1(InstrumentId)),
    subscribeToLevel1: InstrumentId =>
      dispatch(subscribeToLevel1(InstrumentId)),
    changeReduxForm: payload =>
      dispatch(change(streamingForm, payload.field, payload.value)),
    showNotification: message => {
      dispatch(
        showSnack({
          id: 'validationNotification',
          text: message,
          type: 'warning'
        })
      );
    }
  };
};

const StreamingCardGridContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StreamingCardGridComponent);

export default reduxForm({
  form: streamingForm,
  enableReinitialize: true
})(StreamingCardGridContainer);

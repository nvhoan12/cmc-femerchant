import React from 'react';
import KYCLevelComponent from '../KYC/KYCLevel';

import {
  getIMLevel1VerificationProps,
  getIMLevel2VerificationProps,
  getIMLevelVerificationProps
} from 'apex-web/lib/components/KYC/kycLevelsVerificationPropsHelper';

const KYC_IMLevelsComponent = props => {
  const {
    highlightStyle,
    levels,
    levelIncreaseStatus,
    openKYCViewDetailSidePane,
    openKYCSidePane,
    verificationLevel
  } = props;

  return levels.map(level => {
    if (level.level === 0) {
      const levelProps = getIMLevel1VerificationProps(
        verificationLevel,
        levelIncreaseStatus
      );

      return (
        <KYCLevelComponent
          key={level.level}
          verified={true}
          config={level}
          {...levelProps}
          highlightStyle={highlightStyle}
        />
      );
    }

    const levelProps =
      level.level === 1
        ? getIMLevel2VerificationProps(verificationLevel, levelIncreaseStatus)
        : getIMLevelVerificationProps(
            level.level,
            verificationLevel - 1,
            levelIncreaseStatus
          );

    return (
      <KYCLevelComponent
        key={level.level}
        config={level}
        {...levelProps}
        openKYCViewDetailSidePane={openKYCViewDetailSidePane}
        openPane={openKYCSidePane}
        highlightStyle={highlightStyle}
      />
    );
  });
};

export default KYC_IMLevelsComponent;

import React from 'react';
import KYCBaseContainer from 'apex-web/lib/components/KYC';
import KYC_IMLevelsContainer from './KYC_IMLevelsContainer';

const KYC_IMComponent = () => {
  return <KYCBaseContainer LevelsComponent={KYC_IMLevelsContainer} />;
};

export default KYC_IMComponent;

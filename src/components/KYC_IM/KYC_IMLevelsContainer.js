import { connect } from 'react-redux';
import { openKYCViewDetailSidePane } from '../../redux/actions/sidePaneActions';
import KYC_IMLevelsComponent from './KYC_IMLevelsComponent';

const mapDispatchToProps = {
  openKYCViewDetailSidePane,
  openKYCSidePane: () => {
    // TODO: for level 2 and level 3 handle here.
  }
};

const KYC_IMLevelsContainer = connect(
  null,
  mapDispatchToProps
)(KYC_IMLevelsComponent);

export default KYC_IMLevelsContainer;

import React from 'react';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MobileDetect from 'mobile-detect';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import APLogo from 'apex-web/lib/components/common/APLogo/APLogo';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import path from 'apex-web/lib/helpers/path';
import config from '../../config';
import doneIcon from '../../images/icons/done.png';
import bannersIosAndroid from '../../images/news-banners-ios-android.jpg';
import BTZIcon from '../../components/common/BTZIcon/BTZIcon';

import 'apex-web/lib/styles/components/common/StandaloneModal.css';
import './VerifyEmailComponent.css';

const baseClasses = getBEMClasses('standalone-modal');
const classes = getBEMClasses('verify-email');

// context.t('Invalid Verification Code or Time Expired!')
// context.t('Your email verification has failed. Follow the instructions after clicking the button below. Contact the exchange admin if the problem persists.')
export default class VerifyEmailComponent extends React.Component {
  static defaultProps = {
    fetchConfirmEmail: () => {},
    processingVerifyEmail: true,
    message: '',
    error: false
  };

  static propTypes = {
    fetchConfirmEmail: PropTypes.func.isRequired,
    processingVerifyEmail: PropTypes.bool.isRequired,
    message: PropTypes.string,
    error: PropTypes.bool
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchConfirmEmail();
  }

  render() {
    const {
      context,
      props: { processingVerifyEmail, message, error }
    } = this;

    const md = new MobileDetect(window.navigator.userAgent);
    const os = md.os(); // AndroidOS, iOS
    const isMobile = !!os;

    return (
      <div className={`${baseClasses('wrapper')}`}>
        <APLogo customClass={baseClasses('logo')} linkTo="/" />
        <div className={`${baseClasses('container')}`}>
          {processingVerifyEmail ? (
            <Spinner
              isVisible={processingVerifyEmail}
              text="Verifying your email address"
            />
          ) : error ? (
            <React.Fragment>
              <div className={`${baseClasses('body')}`}>
                <div className={`${baseClasses('body-text')}`}>
                  {context.t(message)}
                </div>
              </div>
              <div className={`${baseClasses('footer')}`}>
                <Link
                  to={path('/problem-logging-in')}
                  className={baseClasses('btn', 'subtractive')}>
                  {context.t('Problem with verification?')}
                </Link>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="m-5">
                <div className="d-flex flex-column align-items-center justify-content-center">
                  <img className="mb-3" src={doneIcon} alt="done-icon" />
                  <p className="text-body-1 font-weight-bold text-primary mb-5">
                    {context.t('Verified Successfully')}
                  </p>
                  <p className="text-white-50">
                    {context.t('Your email has been verified successfully.')}
                  </p>
                  <p className="text-white-50">
                    {context.t('You can now login to the Platform.')}
                  </p>
                </div>
              </div>
              <img
                src={bannersIosAndroid}
                className="w-100"
                alt="bannersIosAndroid"
              />
              <div className={classes('footer')}>
                {isMobile ? (
                  <React.Fragment>
                    <a
                      href={
                        os === 'iOS'
                          ? config.global.appsStore
                          : config.global.googlePlay
                      }
                      className={clsx(baseClasses('btn', 'additive'))}>
                      <BTZIcon iconName="fa-download" className="mr-2" />
                      {context.t('Download App')}
                    </a>
                    <a
                      href="bitazzaapp://login"
                      className={clsx(
                        baseClasses('btn'),
                        classes('login-btn')
                      )}>
                      {context.t('Login')}
                    </a>
                  </React.Fragment>
                ) : (
                  <Link
                    to={path('/login')}
                    className={clsx(baseClasses('btn', 'additive'))}>
                    {context.t('Login')}
                  </Link>
                )}
              </div>
            </React.Fragment>
          )}
        </div>
      </div>
    );
  }
}

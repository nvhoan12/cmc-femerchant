import React from 'react';
import PropTypes from 'prop-types';
import {
  getTotalRender,
  getAvailableBalance
} from '../../helpers/withdrawHelper';
import { product } from 'apex-web/lib/propTypes/sendReceiveComponent';
import SendReceiveHeaderComponent from 'apex-web/lib/components/SendReceiveSidePane/SendReceiveHeader/SendReceiveHeaderComponent';
import SendReceiveFormTabsComponent from './SendReceiveFormTabs/SendReceiveFormTabsComponent';
import SendFormContainer from './SendForm/SendFormContainer';
import ReceiveContainer from './Receive/ReceiveContainer';
import SendReceiveDetailsComponent from './SendReceiveDetails/SendReceiveDetailsComponent';
import SendReceiveConfirmModal from './SendReceiveConfirmModal/SendReceiveConfirmModalComponent';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import config from '../../config';

import './SendReceiveSidePane.css';

const sendReceiveClasses = getBEMClasses('send-receive');

class SendReceiveSidePane extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isSend: props.isSend,
      useExternalAddress: true,
      data: {},
      openConfirmModal: false,
      fee: null
    };
  }

  componentDidMount() {
    this.props.onSidePaneOpen({
      customClass: this.props.showDetails
        ? 'retail-sidepane-with-details'
        : 'retail-sidepane',
      hideHeader: true
    });

    if (this.props.verificationLevelConfig.RequireWhitelistedAddress) {
      this.props.fetchAccountWithdrawInfos();
    }
    this.props.selectWithdrawProduct(this.props.product.ProductId);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.fee !== prevState.fee) {
      return { fee: nextProps.fee };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.fee !== this.state.fee) {
      this.setTotal();
    }
  }

  toggleUseExternalAddress = () => {
    this.setState({ useExternalAddress: !this.state.useExternalAddress });
  };

  onUpdate = () => {
    this.getWithdrawFee();
    this.setTotal();
  };

  setTotal = () => {
    const { position } = this.props;
    const {
      data: { Amount },
      fee
    } = this.state;
    const balance = getAvailableBalance(position.Amount, position.Hold);
    const amount = parseFloat(Amount);
    let total = 0;

    if (amount) {
      total = getTotalRender(balance, amount, fee);
    }

    this.setState({ total });
  };

  getWithdrawFee = () => {
    const {
      product: { ProductId },
      getWithdrawFee
    } = this.props;
    const {
      data: { Amount }
    } = this.state;

    if (!isNaN(Amount)) {
      getWithdrawFee(ProductId, Amount);
    }
  };

  onDataUpdate = data => {
    const {
      data: { Amount },
      isSend
    } = this.state;
    this.setState({ data }, () => {
      if (isSend && data.Amount !== Amount) {
        this.onUpdate();
      }
    });
  };

  openConfirmModal = data => this.setState({ openConfirmModal: true, data });

  closeConfirmModal = () => this.setState({ openConfirmModal: false });

  getUserDetail = async (type, search) => {
    const searchParam = new URLSearchParams({ type, search }).toString();
    const res = await fetch(config.services.getUserDetail(searchParam), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-BTZ-TOKEN': this.props.token
      }
    });
    const jsonRes = await res.json();
    return jsonRes;
  };

  validateBTZID = async data => {
    if (data.ReceiverUsername) {
      if (/^@/g.test(data.ReceiverUsername)) {
        // input BTZ id
        const btzID = data.ReceiverUsername.slice(1);
        const res = await this.getUserDetail('userTag', btzID);
        if (!res.email) {
          this.props.showSnack({
            id: 'validateReceiverUsername',
            text: `Not found Bitazza ID {btzID}`,
            textVars: { btzID: data.ReceiverUsername },
            type: 'warning'
          });
          return null;
        }
        return this.openConfirmModal({
          ...data,
          ReceiverAccountId: res.default_account_id,
          displayName: res.display_name
        });
      } else {
        // input email
        const res = await this.getUserDetail('email', data.ReceiverUsername);
        if (!res.email) {
          this.props.showSnack({
            id: 'validateReceiverUsername',
            text: `Not found email {email}`,
            textVars: { email: data.ReceiverUsername },
            type: 'warning'
          });
          return null;
        }
        return this.openConfirmModal({
          ...data,
          ReceiverAccountId: res.default_account_id,
          displayName: res.display_name
        });
      }
    }
    this.openConfirmModal(data);
  };

  onTabChange = () => {
    this.setState({
      isSend: !this.state.isSend,
      data: {},
      useExternalAddress: true
    });
  };

  submitData = () => {
    const {
      receiveFunds,
      sendFunds,
      sendToAddress,
      product: { ProductId },
      template,
      template: { TemplateType },
      selectedAccountId
    } = this.props;
    const {
      isSend,
      useExternalAddress,
      data,
      data: { Notes, Amount, ReceiverAccountId }
    } = this.state;

    const payload = {
      Notes: Notes || '',
      ReceiverAccountId: ReceiverAccountId,
      ReceiverUsername: '',
      Amount,
      ProductId,
      selectedAccountId
    };

    const addressPayload = {
      ProductId,
      Amount,
      TemplateType,
      TemplateForm: {
        ...template, // Get all fields
        ...data // Overwrite those with values
      }
    };

    if (isSend) {
      if (useExternalAddress) {
        sendToAddress(addressPayload);
      } else {
        sendFunds(payload);
      }
    } else {
      receiveFunds(payload);
    }
    this.closeConfirmModal();
  };

  render() {
    const {
      product,
      position: { Amount, Hold },
      fee,
      showDetails,
      allowTransfer,
      hideFees,
      TicketAmount,
      verificationLevelConfig,
      networkName
    } = this.props;
    const {
      isSend,
      useExternalAddress,
      data,
      openConfirmModal,
      total
    } = this.state;
    const balance = getAvailableBalance(Amount, Hold);
    return (
      <div className={sendReceiveClasses()}>
        <section className={sendReceiveClasses('main')}>
          <SendReceiveHeaderComponent product={product} />
          {product.ProductSymbol === 'BTZ' && (
            <p>
              {this.context.t(
                'BTZ Wallets are temporarily disabled. Please contact our support at support@bitazza.com if you have any queries.'
              )}
            </p>
          )}
          {product.ProductSymbol !== 'BTZ' && (
            <section className={sendReceiveClasses('main-form')}>
              <SendReceiveFormTabsComponent
                isSend={isSend}
                toggleTab={this.onTabChange}
              />
              {isSend ? (
                <SendFormContainer
                  product={product}
                  networkName={networkName}
                  useExternalAddress={useExternalAddress}
                  balance={balance}
                  fee={fee}
                  total={total}
                  showDetails={showDetails}
                  toggleUseExternalAddress={this.toggleUseExternalAddress}
                  onChange={this.onDataUpdate}
                  onSubmit={this.validateBTZID}
                  allowTransfer={allowTransfer}
                  verificationLevelConfig={verificationLevelConfig}
                />
              ) : (
                <ReceiveContainer
                  product={product}
                  useExternalAddress={useExternalAddress}
                  toggleUseExternalAddress={this.toggleUseExternalAddress}
                  onChange={this.onDataUpdate}
                  onSubmit={this.validateBTZID}
                  allowTransfer={allowTransfer}
                />
              )}
            </section>
          )}
        </section>

        {showDetails && (
          <section className={sendReceiveClasses('side')}>
            <section
              className={sendReceiveClasses(
                isSend ? 'send-side-details' : 'side-details'
              )}>
              <SendReceiveDetailsComponent
                isSend={isSend}
                useExternalAddress={useExternalAddress}
                details={data}
                balance={balance}
                total={total}
                product={product}
                networkName={networkName}
                TicketAmount={TicketAmount}
                fee={fee}
              />
            </section>
          </section>
        )}
        <SendReceiveConfirmModal
          isOpen={openConfirmModal}
          useExternalAddress={useExternalAddress}
          close={this.closeConfirmModal}
          product={product}
          networkName={networkName}
          isSend={isSend}
          values={data}
          total={total}
          fee={fee}
          onConfirm={this.submitData}
          hideFees={hideFees}
        />
      </div>
    );
  }
}

SendReceiveSidePane.defaultProps = {
  showDetails: true,
  onSidePaneOpen: () => {},
  isSend: true
};

SendReceiveSidePane.propTypes = {
  allowTransfer: PropTypes.bool.isRequired,
  openRetailSendSidePane: PropTypes.func,
  openRetailReceiveSidePane: PropTypes.func,
  product,
  position: PropTypes.shape({
    AccountId: PropTypes.number,
    Amount: PropTypes.number,
    Hold: PropTypes.number,
    OMSId: PropTypes.number,
    PendingDeposits: PropTypes.number,
    PendingWithdraws: PropTypes.number,
    ProductId: PropTypes.number,
    ProductSymbol: PropTypes.string,
    TotalDayDeposits: PropTypes.number,
    TotalDayWithdraws: PropTypes.number,
    TotalMonthWithdraws: PropTypes.number,
    TotalYearWithdraws: PropTypes.number
  })
};

SendReceiveSidePane.contextTypes = {
  t: PropTypes.func.isRequired
};

export default SendReceiveSidePane;

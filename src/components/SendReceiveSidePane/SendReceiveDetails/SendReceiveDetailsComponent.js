import React from 'react';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import PropTypes from 'prop-types';
import SidePaneDetailsComponent from 'apex-web/lib/components/common/SidePaneDetailsComponent/SidePaneDetailsComponent';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import config from '../../../config';

class SendReceiveDetailsComponent extends React.Component {
  render() {
    const {
      isSend,
      useExternalAddress,
      total,
      balance,
      details: { Amount },
      product: { ProductSymbol },
      setAmount,
      networkName
    } = this.props;
    const { getItem } = SidePaneDetailsComponent;
    const title = isSend
      ? this.context.t('Send Details')
      : this.context.t('Instructions');
    const amountText = useExternalAddress
      ? this.context.t('Amount to be Deducted')
      : this.context.t('Amount to Send');
    const amount = useExternalAddress ? total : Amount;
    const remainingBalance = formatNumberToLocale(
      balance - amount,
      ProductSymbol
    );
    const classModifiers = isSend ? 'top' : '';

    let items = [];
    let info = [];
    let infoHeader = '';

    if (isSend) {
      items.push(
        getItem(
          this.context.t('Your current {ProductSymbol} Balance', {
            ProductSymbol
          }),
          <span
            className="cursor-pointer"
            onClick={() =>
              setAmount(
                formatNumberToLocale(balance, ProductSymbol).replace(',', '')
              )
            }>
            {balance
              ? `${formatNumberToLocale(
                  balance,
                  ProductSymbol
                )} ${ProductSymbol}`
              : '-'}
          </span>
        )
      );
      items.push(
        getItem(
          amountText,
          amount
            ? `${formatNumberToLocale(amount, ProductSymbol)} ${ProductSymbol}`
            : '-'
        )
      );
      items.push(
        getItem(
          this.context.t(`Remaining Balance`),
          amount && balance ? `${remainingBalance} ${ProductSymbol}` : '-'
        )
      );
    }

    if (isSend && !useExternalAddress) {
      infoHeader = this.context.t('Note');
      info.push(
        this.context.t(
          'If the sender is not a registered user of Retail, they will receive an email invitation to create an account and claim their funds.'
        )
      );
    }

    if (!isSend && !useExternalAddress) {
      info.push(this.context.t('Use this form to request funds via email.'));
      info.push(
        this.context.t(
          'Note - if the requestee is not a registered user of Retail, they will receive an email invitation to create an account.'
        )
      );
    }
    if (!isSend && useExternalAddress) {
      info.push(
        this.context.t(
          'Depositing cryptocurrency into your wallet is safe and easy. The address below can always be used to deposit cryptocurrency into your account.'
        )
      );
      info.push(
        this.context.t(
          'Your account will automatically update after the cryptocurrency network confirms your transaction. The confirmation may take up to 1 hour'
        )
      );
      const isActiveWarning = config.global.activeWarningExternalTransferProducts.includes(
        ProductSymbol
      );
      if (isActiveWarning && networkName) {
        info.push(
          this.context.t(
            'The network you have selected is {networkName}. Please ensure that the receiver address supports the {networkName} network. You will lose your assets if the chosen platform does not support retrievals.',
            {
              networkName
            }
          )
        );
      }
    }

    return (
      <SidePaneDetailsComponent
        title={title}
        items={items}
        info={info}
        infoHeader={infoHeader}
        classModifiers={classModifiers}
      />
    );
  }
}

SendReceiveDetailsComponent.propTypes = {
  isSend: PropTypes.bool,
  useExternalAddress: PropTypes.bool,
  details: PropTypes.object,
  fee: PropTypes.number,
  total: PropTypes.number
};

SendReceiveDetailsComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapDispatchToProps = {
  setAmount: balance => change('SendForm', 'Amount', balance)
};

export default connect(
  null,
  mapDispatchToProps
)(SendReceiveDetailsComponent);

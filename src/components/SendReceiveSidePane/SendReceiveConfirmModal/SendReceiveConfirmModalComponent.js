import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../components/common/Modal/Modal';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { formatDateTime } from 'apex-web/lib/helpers/dateHelper';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import SendReceiveConfirmHeaderComponent from 'apex-web/lib/components/SendReceiveSidePane/SendReceiveConfirmModal/SendReceiveConfirmHeaderComponent';
import config from '../../../config';

import './SendReceiveConfirmModal.css';

const bemClasses = getBEMClasses('send-receive-confirm-modal');

const SendReceiveConfirmModal = (props, context) => {
  const {
    isOpen,
    close,
    product,
    isSend,
    values,
    onConfirm,
    useExternalAddress,
    fee,
    total,
    hideFees,
    networkName
  } = props;
  const addressLabel = context.t('External Address');
  const sendLabel = context.t('Recipient’s');
  const receiveLabel = context.t('Requestee’s');

  const isActiveWarning = config.global.activeWarningExternalTransferProducts.includes(
    product.ProductSymbol
  );

  return (
    <Modal
      isOpen={isOpen}
      title={
        <SendReceiveConfirmHeaderComponent isSend={isSend} product={product} />
      }
      close={close}
      footer={{
        buttonText: context.t('Confirm'),
        buttonStyle: 'additive',
        onClick: onConfirm
      }}
      customClass={bemClasses()}>
      <header className={bemClasses('title')}>
        {context.t('Confirmation')}
      </header>
      <section className={bemClasses('details')}>
        {!useExternalAddress && (
          <React.Fragment>
            <APLabelWithText
              label={isSend ? sendLabel : receiveLabel}
              text={values.ReceiverUsername}
              customClass={bemClasses()}
            />
            <APLabelWithText
              label={context.t('Name')}
              text={values.displayName}
              customClass={bemClasses()}
            />
          </React.Fragment>
        )}

        <APLabelWithText
          label={`${product.ProductSymbol} ${context.t('Amount')}`}
          text={`${formatNumberToLocale(
            values.Amount,
            product.ProductSymbol
          )} ${product.ProductSymbol}`}
          customClass={bemClasses()}
        />

        {useExternalAddress && (
          <React.Fragment>
            {!hideFees && (
              <React.Fragment>
                <APLabelWithText
                  label={context.t('Fees')}
                  text={`${formatNumberToLocale(fee, product.ProductSymbol)} ${
                    product.ProductSymbol
                  }`}
                  customClass={bemClasses()}
                />
                <APLabelWithText
                  label={context.t('Net Amount')}
                  text={`${formatNumberToLocale(
                    total,
                    product.ProductSymbol
                  )} ${product.ProductSymbol}`}
                  customClass={bemClasses()}
                />
              </React.Fragment>
            )}
            <APLabelWithText
              label={addressLabel}
              text={values.ExternalAddress}
              customClass={bemClasses()}
            />
          </React.Fragment>
        )}
        {!useExternalAddress && (
          <APLabelWithText
            label={context.t('Note')}
            text={values.Notes}
            customClass={bemClasses()}
          />
        )}

        <APLabelWithText
          label={context.t('Time')}
          text={formatDateTime(new Date())}
          customClass={bemClasses()}
        />
      </section>
      {isSend &&
        useExternalAddress &&
        isActiveWarning &&
        networkName && (
          <p className="mt-5">
            {context.t(
              'You have selected {networkName} network. Please ensure it is correct',
              {
                networkName
              }
            )}
          </p>
        )}
    </Modal>
  );
};

SendReceiveConfirmModal.propTypes = {
  close: PropTypes.func.isRequired,
  isSend: PropTypes.bool.isRequired,
  fee: PropTypes.number.isRequired,
  total: PropTypes.number,
  useExternalAddress: PropTypes.bool.isRequired,
  values: PropTypes.object.isRequired,
  onConfirm: PropTypes.func.isRequired
};

SendReceiveConfirmModal.contextTypes = {
  t: PropTypes.func.isRequired
};

export default SendReceiveConfirmModal;

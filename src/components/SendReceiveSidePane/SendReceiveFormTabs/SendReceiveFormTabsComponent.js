import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APIcon from '../../common/APIcon';
import './SendReceiveFormTabs.css';

const bemClasses = getBEMClasses('send-receive-tabs');

class SendReceiveTabs extends React.Component {
  render() {
    const { isSend, toggleTab } = this.props;
    return (
      <header className={bemClasses()}>
        <div
          className={bemClasses('tab', { selected: isSend })}
          onClick={() => !isSend && toggleTab()}>
          <APIcon name={'buy'} customClass={bemClasses('tab-icon')} />
          <span className={bemClasses('tab-text')}>
            {this.context.t('Send')}
          </span>
        </div>
        <div
          className={bemClasses('tab', { selected: !isSend })}
          onClick={() => isSend && toggleTab()}>
          <APIcon name={'sell'} customClass={bemClasses('tab-icon')} />
          <span className={bemClasses('tab-text')}>
            {this.context.t('Receive')}
          </span>
        </div>
      </header>
    );
  }
}

SendReceiveTabs.propTypes = {
  isSend: PropTypes.bool.isRequired,
  toggleTab: PropTypes.func.isRequired
};

SendReceiveTabs.contextTypes = {
  t: PropTypes.func.isRequired
};

export default SendReceiveTabs;

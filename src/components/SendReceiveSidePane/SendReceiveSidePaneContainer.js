import { connect } from 'react-redux';
import { get } from 'lodash';
import SendReceiveSidePaneComponent from './SendReceiveSidePaneComponent';
import {
  fetchProductWithdrawTemplates,
  createWithdrawTicket,
  getWithdrawFee,
  closeSidepaneAfterCryptoWithdraw
} from '../../redux/actions/withdrawActions';
import {
  requestTransferFunds,
  transferFunds,
  getTransfers
} from 'apex-web/lib/redux/actions/transferActions';
import { getTransferRequestsRequested } from 'apex-web/lib/redux/actions/transferRequestActions';
import { positionByProductIdSelector } from 'apex-web/lib/redux/selectors/positionSelectors';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import config from 'apex-web/lib/config';
import { fetchAccountWithdrawInfos } from 'apex-web/lib/redux/actions/eotcWithdrawActions';

const mapStateToProps = (state, ownProps) => {
  const productId = get(ownProps, 'product.ProductId', null);
  const productSymbol = get(ownProps, 'product.ProductSymbol', null);
  const position = positionByProductIdSelector(productId)(state);
  const decimalPlaces = state.apexCore.product.decimalPlaces[productSymbol];
  const product = { ...ownProps.product, decimalPlaces };
  const { allowTransfer, hideFees } = config.global;

  const { withdrawFee: fee, template } = state.withdraw;

  const { selectedAccountId } = state.user;
  const TicketAmount = get(state, ['withdraw', 'TicketAmount'], 0);
  // const withdrawFee = get(state, ['withdraw', 'withdrawFee'], 0);

  const verificationLevelConfig =
    get(state, ['user', 'verificationLevelConfigs', 'Products'], []).find(
      i => i.ProductId === productId
    ) || {};

  const currentTemplate = state.withdraw.templateTypes.find(
    item => item.TemplateName === state.withdraw.template.TemplateType
  );

  const { token } = state.auth;

  return {
    position,
    fee,
    template,
    product,
    allowTransfer,
    hideFees,
    selectedAccountId,
    TicketAmount,
    verificationLevelConfig,
    token,
    networkName: currentTemplate ? currentTemplate.AccountProviderName : null
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  receiveFunds: ({ selectedAccountId, ...rest }) =>
    dispatch(requestTransferFunds(rest, ownProps.name)).then(() =>
      dispatch(getTransferRequestsRequested(selectedAccountId))
    ),
  sendFunds: ({ selectedAccountId, ...rest }) =>
    dispatch(transferFunds(rest, ownProps.name)).then(() =>
      dispatch(getTransfers(selectedAccountId))
    ),
  sendToAddress: payload =>
    dispatch(createWithdrawTicket(payload, ownProps.name)).then(() =>
      dispatch(closeSidepaneAfterCryptoWithdraw())
    ),
  getWithdrawFee: (productId, amount) =>
    dispatch(getWithdrawFee(productId, amount)),
  fetchAccountWithdrawInfos: () => dispatch(fetchAccountWithdrawInfos()),
  showSnack: text => dispatch(showSnack(text)),
  selectWithdrawProduct: productId =>
    dispatch(fetchProductWithdrawTemplates(productId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SendReceiveSidePaneComponent);

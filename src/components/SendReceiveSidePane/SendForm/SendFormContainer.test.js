import React from 'react';
import { shallow } from 'enzyme';
import SendFormContainer from './SendFormContainer';
import SendFormComponent from './SendFormComponent';
import configureStore from 'redux-mock-store';

const testState = {
  withdraw: {
    error: ''
  },
  user: {
    userInfo: {
      AccountId: 1,
      UserId: 1,
      UserName: 'name',
      Email: 'test@alphapoint.com'
    },
    accounts: [
      {
        AccountId: 0,
        VerificationLevel: 1
      },
      {
        AccountId: 1,
        VerificationLevel: 2
      }
    ],
    userConfig: [
      {
        Key: 'levelIncreaseStatus',
        Value: 'pass'
      }
    ],
    kyc: 1234
  }
};

jest.mock('./SendFormComponent', () => {
  return () => <div />;
});

jest.mock('redux-form', () => ({
  reduxForm: jest.fn(() => Component => Component)
}));

describe('SendFormContainer', () => {
  let store, container, childProps;
  const mock = configureStore();
  beforeEach(() => {
    store = mock(testState);
    container = shallow(<SendFormContainer store={store} />);
    childProps = container.first().props();
  });

  it('renders a <SendFormComponent />', () => {
    expect(container.find(SendFormComponent).length).toBe(1);
  });

  it('passes the expected props', () => {
    expect(childProps).toEqual(
      expect.objectContaining({
        disableWithdraw: expect.any(Boolean),
        userEmail: expect.any(String)
      })
    );
  });

  it('passes the verificationOnClick action', () => {
    expect(childProps).toEqual(
      expect.objectContaining({
        verificationOnClick: expect.any(Function)
      })
    );
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import SendAddressContainer from './SendAddressContainer';
import SendAddressComponent from './SendAddressComponent';
import configureStore from 'redux-mock-store';

const testState = {
  withdraw: {}
};

jest.mock('./SendAddressComponent', () => {
  return () => <div />;
});

jest.mock('../../../redux/actions/withdrawActions', () => {
  return {
    fetchProductWithdrawTemplates: jest.fn(payload => ({
      type: 'FETCH_TEMPLATES',
      payload
    })),
    fetchProductWithdrawTemplate: jest.fn(payload => ({
      type: 'FETCH_TEMPLATE',
      payload
    }))
  };
});

describe('SendAddressContainer', () => {
  let store, container, childProps;
  const mock = configureStore();
  beforeEach(() => {
    store = mock(testState);
    container = shallow(<SendAddressContainer store={store} />);
    childProps = container.first().props();
  });

  it('renders a <SendAddressComponent />', () => {
    expect(container.find(SendAddressComponent).length).toBe(1);
  });

  it('passes the expected props', () => {
    expect(childProps).toEqual(
      expect.objectContaining({
        withdrawInfo: expect.any(Object)
      })
    );
  });

  it('passes the actions', () => {
    expect(childProps).toEqual(
      expect.objectContaining({
        selectWithdrawProduct: expect.any(Function),
        selectWithdrawTemplate: expect.any(Function)
      })
    );
  });

  it('selectWithdrawProduct should dispatch fetchProductWithdrawTemplates action', () => {
    container.instance().selector.props.selectWithdrawProduct(1);

    expect(store.getActions()).toEqual([
      { type: 'FETCH_TEMPLATES', payload: 1 }
    ]);
  });

  it('selectWithdrawTemplate should dispatch fetchProductWithdrawTemplate action', () => {
    container.instance().selector.props.selectWithdrawTemplate(1);

    expect(store.getActions()).toEqual([
      { type: 'FETCH_TEMPLATE', payload: 1 }
    ]);
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import APSelect from 'apex-web/lib/components/common/APSelect';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { templateFormRenderer } from 'apex-web/lib/helpers/formTemplateHelper';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';

import './SendAddress.css';

const baseClasses = getBEMClasses('send-address');

class SendAddressComponent extends React.Component {
  componentWillMount() {
    // fetching at SendReceiveSidePaneComponent instead
    // this.getProductData();
  }

  // getProductData = () => {
  //   const {
  //     selectWithdrawProduct,
  //     product: { ProductId }
  //   } = this.props;
  //   selectWithdrawProduct(ProductId);
  // };

  render() {
    const {
      withdrawInfo,
      withdrawInfo: { template },
      product,
      product: { ProductId },
      selectWithdrawTemplate,
      WalletsDisabledMessage,
      verificationLevelConfig
    } = this.props;
    const showTemplateFields = Object.keys(template || {}).length;
    const showTemplateTypeSelect =
      withdrawInfo.templateTypes.length > 1 &&
      !Object.keys(withdrawInfo.templateInfo).length &&
      !withdrawInfo.isLoading;
    const templateTypes = [
      { value: '', label: 'Select an option' },
      ...withdrawInfo.templateTypes.map(template => ({
        value: template.TemplateName,
        label: template.TemplateName
      }))
    ];

    const hasError = withdrawInfo.error || WalletsDisabledMessage;

    return (
      <div>
        {withdrawInfo.withdrawStatus.success ? (
          <div className={baseClasses('success')}>
            {this.context.t('Your withdraw ticket was created successfully.')}
          </div>
        ) : (
          <div className={baseClasses('form-body')}>
            {hasError ? (
              <div className={baseClasses('error')}>{hasError}</div>
            ) : (
              <React.Fragment>
                {withdrawInfo.isLoading && <Spinner />}

                {showTemplateTypeSelect && (
                  <React.Fragment>
                    <p>
                      {this.context.t(
                        'Select an option to continue the withdraw process'
                      )}
                    </p>
                    <APSelect
                      name="TemplateType"
                      customClass={baseClasses()}
                      label={this.context.t('Options for withdraw')}
                      onSelect={value =>
                        selectWithdrawTemplate(ProductId, value)
                      }
                      options={templateTypes}
                    />
                  </React.Fragment>
                )}

                {showTemplateFields > 0 && (
                  <div className={baseClasses('section')}>
                    {templateFormRenderer(
                      template,
                      baseClasses(),
                      this.context,
                      product,
                      verificationLevelConfig
                    )}
                  </div>
                )}
              </React.Fragment>
            )}
          </div>
        )}
      </div>
    );
  }
}

SendAddressComponent.defaultProps = {
  withdrawInfo: {
    templateInfo: {},
    template: {},
    isLoading: false,
    error: '',
    TemplateForm: {},
    withdrawStatus: {},
    getWithdrawFee: () => {}
  }
};

SendAddressComponent.propTypes = {
  getWithdrawFee: PropTypes.func,
  selectWithdrawProduct: PropTypes.func.isRequired,
  product: PropTypes.shape({
    ProductSymbol: PropTypes.string,
    ProductId: PropTypes.number
  }).isRequired,
  withdrawInfo: PropTypes.shape({
    templateInfo: PropTypes.object.isRequired,
    template: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    TemplateForm: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    withdrawStatus: PropTypes.shape({
      success: PropTypes.bool,
      RequestCode: PropTypes.string
    }).isRequired
  })
};

SendAddressComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default SendAddressComponent;

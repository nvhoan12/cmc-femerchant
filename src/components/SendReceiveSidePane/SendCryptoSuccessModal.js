import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../components/common/Modal/Modal';
import modalDecorator from 'apex-web/lib/hocs/modalDecorator';
import { MODAL_TYPES } from '../../redux/actions/modalActions';

const SendCryptoSuccessModal = ({ isOpen, close }, context) => {
  return (
    <Modal
      isOpen={isOpen}
      title={context.t('Success')}
      close={close}
      footer={{
        buttonText: context.t('OK'),
        onClick: close
      }}>
      <h3>{context.t('Your withdraw has been successfully added')}</h3>
    </Modal>
  );
};

SendCryptoSuccessModal.contextTypes = {
  t: PropTypes.func.isRequired
};

export default modalDecorator({
  name: MODAL_TYPES.SEND_CRYPTO_SUCCESS
})(SendCryptoSuccessModal);

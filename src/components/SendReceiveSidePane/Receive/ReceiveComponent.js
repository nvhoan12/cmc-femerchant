import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import VerificationRequiredContainer from 'apex-web/lib/components/VerificationRequired/VerificationRequiredContainer';
import { product } from 'apex-web/lib/propTypes/sendReceiveComponent';
import config from '../../../config';
import ReceiveFormContainer from '../ReceiveForm/ReceiveFormContainer';
import ReceiveAddressContainer from '../ReceiveAddress/ReceiveAddressContainer';
import './Receive.css';

const bemClasses = getBEMClasses('receive');

class ReceiveComponent extends React.Component {
  render() {
    const {
      product,
      useExternalAddress,
      toggleUseExternalAddress,
      onChange,
      onSubmit,
      disableDeposit,
      verificationOnClick,
      allowTransfer
    } = this.props;

    const isDisableDeposit = config.global.disableDepositProducts.includes(
      product.ProductSymbol
    );
    if (useExternalAddress && isDisableDeposit) {
      toggleUseExternalAddress();
    }

    return (
      <section className={bemClasses()}>
        <header className={bemClasses('source')}>
          <div
            className={bemClasses('source-item', {
              active: useExternalAddress,
              disable: isDisableDeposit
            })}
            onClick={() => {
              !useExternalAddress &&
                !isDisableDeposit &&
                toggleUseExternalAddress();
            }}>
            {this.context.t('External Wallet')}
          </div>
          <div className={bemClasses('source-item-with-border')}>
            {allowTransfer && (
              <div
                className={bemClasses('source-item', {
                  active: !useExternalAddress
                })}
                onClick={() => {
                  useExternalAddress && toggleUseExternalAddress();
                }}>
                {this.context.t('Bitazza ID')}
              </div>
            )}
          </div>
        </header>
        <section className={bemClasses('body')}>
          {disableDeposit ? (
            <VerificationRequiredContainer
              disabled={disableDeposit}
              onClick={verificationOnClick}
            />
          ) : useExternalAddress ? (
            <ReceiveAddressContainer product={product} />
          ) : (
            <ReceiveFormContainer
              product={product}
              onChange={onChange}
              onSubmit={onSubmit}
            />
          )}
        </section>
      </section>
    );
  }
}

ReceiveComponent.propTypes = {
  product,
  useExternalAddress: PropTypes.bool.isRequired,
  toggleUseExternalAddress: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  allowTransfer: PropTypes.bool.isRequired,
  VerificationRequiredComponent: PropTypes.element,
  disableDeposit: PropTypes.bool,
  verificationOnClick: PropTypes.func
};

ReceiveComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ReceiveComponent;

import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import {
  required,
  validateReceiverEmail,
  btzIDOrEmail
} from '../../../helpers/formValidations';
import APNumberInput from 'apex-web/lib/components/common/APNumberInput';
import APInput from 'apex-web/lib/components/common/APInput';
import APButton from 'apex-web/lib/components/common/APButton';
import { product } from 'apex-web/lib/propTypes/sendReceiveComponent';
import './ReceiveForm.css';

const bemClasses = getBEMClasses('receive-form');

class ReceiveForm extends React.Component {
  checkUserEmail = value => {
    const { userEmail } = this.props;
    return validateReceiverEmail(value, userEmail);
  };

  render() {
    const {
      product: { ProductFullName: name, ProductSymbol: symbol, decimalPlaces },
      handleSubmit,
      invalid,
      submitting,
      onChange,
      onSubmit
    } = this.props;
    return (
      <form onSubmit={handleSubmit(onSubmit)} className={bemClasses()}>
        <section className={bemClasses('receive-from')}>
          <p className={bemClasses('label-text')}>
            {this.context.t('Request From')}
          </p>

          <APInput
            name="ReceiverUsername"
            customClass={bemClasses()}
            onChange={handleSubmit(onChange)}
            validate={[required, this.checkUserEmail, btzIDOrEmail]}
          />
        </section>

        <p className={bemClasses('label-text')}>
          {this.context.t('Amount of {name} to Request', { name })}
        </p>

        <section className={bemClasses('amounts')}>
          <APNumberInput
            name="Amount"
            customClass={bemClasses()}
            labelInInput={symbol}
            decimalPlaces={decimalPlaces}
            onChange={handleSubmit(onChange)}
            validate={[required]}
          />
        </section>

        <section className={bemClasses('note')}>
          <p className={bemClasses('label-text')}>{this.context.t('Note')}</p>

          <APInput name="Notes" rows={3} customClass={bemClasses()} />
        </section>

        <APButton
          type="submit"
          customClass={bemClasses()}
          styleName="additive"
          disabled={invalid || submitting}>
          {this.context.t('Request')} {name}
        </APButton>
      </form>
    );
  }
}

ReceiveForm.propTypes = {
  product: product.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

ReceiveForm.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ReceiveForm;

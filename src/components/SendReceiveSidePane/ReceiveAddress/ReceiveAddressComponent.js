import React from 'react';
import PropTypes from 'prop-types';
import APQRCode from 'apex-web/lib/components/common/APQRCode/APQRCode';
import APIcon from 'apex-web/lib/components/common/APIcon';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import copyToClipboard from 'apex-web/lib/helpers/clipboardHelper';
import destinationTagHelper from 'apex-web/lib/helpers/destinationTagHelper';
import './ReceiveAddress.css';

const bemClasses = getBEMClasses('receive-address');

class ReceiveAddressComponent extends React.Component {
  componentWillMount() {
    const { selectDepositProduct, product } = this.props;
    selectDepositProduct(product.ProductId);
  }

  render() {
    const {
      deposit: { depositInfo: addressList },
      deposit: { isLoading, error },
      product,
      showSnack,
      WalletsDisabledMessage
    } = this.props;

    // Protocol prefix for the QR Code, lowercase name with no spaces.
    const depositType = product.ProductFullName.toLowerCase().replace(
      /\W/g,
      ''
    );
    let selectedAddress;

    if (!isLoading && addressList.length) {
      selectedAddress = addressList[addressList.length - 1];
    }

    const [address, destinationTag] = destinationTagHelper(selectedAddress);

    return (
      <section className={bemClasses()}>
        {selectedAddress && (
          <React.Fragment>
            <p className={bemClasses('description')}>
              {this.context.t('Scan this QR code to receive funds:')}
            </p>
            <div className={bemClasses('qr-code')}>
              <APQRCode value={`${selectedAddress}`} />
            </div>
            {!['BTC', 'XLM', 'XRP'].includes(product.ProductSymbol) && (
              <p className={bemClasses('notic-text')}>
                {['BNB'].includes(product.ProductSymbol)
                  ? this.context.t('BSC (BEP20) network ONLY')
                  : this.context.t('ERC20 protocol ONLY')}
              </p>
            )}
            <p className={bemClasses('copy-address-text')}>
              {this.context.t('Or copy this address:')}
            </p>
            <div
              className={bemClasses('copy-address-payload')}
              onClick={() => {
                showSnack({
                  id: 'RECEIVE_ADDRESS_COPY',
                  text: this.context.t(
                    'The address has been copied to the clipboard.'
                  )
                });
                copyToClipboard(address);
              }}>
              <span className={bemClasses('address')}>{address}</span>
              <APIcon name="copy" customClass={bemClasses('copy-icon')} />
            </div>
            {destinationTag && (
              <div
                onClick={() => {
                  showSnack({
                    id: 'RECEIVE_ADDRESS_COPY',
                    text: this.context.t(
                      'The destination tag has been copied to the clipboard.'
                    )
                  });
                  copyToClipboard(destinationTag);
                }}>
                <p className={bemClasses('destination-tag-text')}>
                  {this.context.t('IMPORTANT')}:{' '}
                  {this.context.t('Be sure to include this destination tag:')}
                </p>
                <span className={bemClasses('address')}>{destinationTag}</span>
                <APIcon name="copy" customClass={bemClasses('copy-icon')} />
              </div>
            )}
          </React.Fragment>
        )}
        {isLoading && (
          <Spinner
            text={this.context.t('Loading url...')}
            customClass={bemClasses}
          />
        )}
        {WalletsDisabledMessage ? (
          <span className={bemClasses('error')}>
            {this.context.t(WalletsDisabledMessage)}
          </span>
        ) : (
          error && (
            <span className={bemClasses('error')}>{this.context.t(error)}</span>
          )
        )}
      </section>
    );
  }
}

ReceiveAddressComponent.defaultProps = {
  deposit: {
    templateInfo: {},
    template: {},
    isLoading: false,
    error: '',
    depositInfo: {},
    depositStatus: {}
  },
  product: {}
};

ReceiveAddressComponent.propTypes = {
  product: PropTypes.object,
  deposit: PropTypes.shape({
    templateInfo: PropTypes.object.isRequired,
    template: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    product: PropTypes.number,
    depositInfo: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
      .isRequired,
    depositStatus: PropTypes.shape({
      success: PropTypes.bool,
      RequestCode: PropTypes.string
    }).isRequired
  })
};

ReceiveAddressComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ReceiveAddressComponent;

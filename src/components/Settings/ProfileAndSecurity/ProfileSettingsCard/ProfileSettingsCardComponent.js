import React from 'react';
import PropTypes from 'prop-types';
import ContentWithHeaderLayoutTemplate from 'apex-web/lib/components/common/LayoutTemplates/ContentWithHeaderLayoutTemplate/ContentWithHeaderLayoutTemplate';
import ProfileFormContainer from './ProfileForm/ProfileFormContainer';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

const profileAndSecurityWidgetClasses = getBEMClasses([
  'profile-and-security-widget'
]);

const ProfileSettingsCardComponent = (props, context) => {
  const getLayoutHeader = (header, classes) => {
    return (
      <div className={classes('header')}>
        <div className={classes('header-text')}>{header}</div>
      </div>
    );
  };

  return (
    <ContentWithHeaderLayoutTemplate
      header={getLayoutHeader(
        context.t('Profile'),
        profileAndSecurityWidgetClasses
      )}
      customClass="profile-and-security-widget">
      <ProfileFormContainer />
    </ContentWithHeaderLayoutTemplate>
  );
};

ProfileSettingsCardComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ProfileSettingsCardComponent;

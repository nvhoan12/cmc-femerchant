import React from 'react';
import UserProfileStaticComponent from './UserProfileStaticComponent';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

import './ProfileFormComponent.css';

const classes = getBEMClasses('retail-profile-form');

const ProfileFormComponent = props => {
  const showUsernameField = !props.useEmailAsUsername;

  return (
    <div className={classes()}>
      <div className={classes('container')}>
        <UserProfileStaticComponent
          showUsernameField={showUsernameField}
          userInfo={{
            email: props.formObj.email,
            username: props.formObj.username,
            bitazzaID: props.formObj.bitazzaID,
            displayName: props.formObj.displayName
          }}
        />
      </div>
    </div>
  );
};

export default ProfileFormComponent;

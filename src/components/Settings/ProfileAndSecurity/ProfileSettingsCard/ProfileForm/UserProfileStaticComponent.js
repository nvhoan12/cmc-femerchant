import React from 'react';
import PropTypes from 'prop-types';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

import './UserProfileStaticComponent.css';

const classes = getBEMClasses('user-profile-form-static');

const UserProfileStaticComponent = (props, context) => {
  const { showUsernameField, userInfo } = props;

  return (
    <div className={classes()}>
      {showUsernameField && (
        <div className={classes('row')}>
          <APLabelWithText
            label={context.t('Username')}
            text={userInfo.username}
            customClass={classes()}
          />
        </div>
      )}
      <div className={classes('row')}>
        <APLabelWithText
          label={context.t('Name')}
          text={userInfo.displayName}
          customClass={classes()}
        />
      </div>
      <div className={classes('row')}>
        <APLabelWithText
          label={context.t('Email')}
          text={userInfo.email}
          customClass={classes()}
        />
      </div>
      <div className={classes('row')}>
        <APLabelWithText
          label={context.t('Bitazza ID')}
          text={userInfo.bitazzaID}
          customClass={classes()}
        />
      </div>
    </div>
  );
};

UserProfileStaticComponent.propTypes = {
  userInfo: PropTypes.object,
  showUsernameField: PropTypes.bool.isRequired
};

UserProfileStaticComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default UserProfileStaticComponent;

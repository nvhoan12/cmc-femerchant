import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import ProfileFormComponent from './ProfileFormComponent';
import { isProfileSectionEditingDisabled } from 'apex-web/lib/redux/selectors/userSelectors';
import {
  submitUserProfileForm,
  goToProfileFormStep
} from 'apex-web/lib/redux/actions/userProfileFormActions';
import config from 'apex-web/lib/config';

const { useEmailAsUsername } = config.SignupForm;

const ProfileForm = reduxForm({
  form: 'userProfileForm',
  enableReinitialize: true,
  onSubmit: (payload, dispatch) => dispatch(submitUserProfileForm(payload))
})(ProfileFormComponent);

const mapStateToProps = state => {
  const {
    form,
    user: { userInfo }
  } = state;

  const editIsDisabled = isProfileSectionEditingDisabled(state);
  const bitazzaID = state.btzLevel.btzID;
  const displayName = state.btzLevel.displayName;

  return {
    initialValues: {
      bitazzaID,
      displayName,
      username: userInfo.UserName,
      email: userInfo.Email
    },
    formObj: form.userProfileForm ? form.userProfileForm.values : {},
    editIsDisabled,
    useEmailAsUsername
  };
};

const mapDispatchToProps = {
  goToStep: goToProfileFormStep
};

const ProfileFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileForm);

export default ProfileFormContainer;

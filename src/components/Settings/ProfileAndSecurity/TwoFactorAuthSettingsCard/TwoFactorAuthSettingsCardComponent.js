import React from 'react';
import PropTypes from 'prop-types';
import ContentWithHeaderLayoutTemplate from 'apex-web/lib/components/common/LayoutTemplates/ContentWithHeaderLayoutTemplate/ContentWithHeaderLayoutTemplate';
import EnableTwoFactorAuthContainerFactory from 'apex-web/lib/components/EnableTwoFactorAuth/EnableTwoFactorAuthContainerFactory';
import EnableTwoFactorAuthComponent from './EnableTwoFactorAuth/EnableTwoFactorAuthComponent';

const EnableTwoFactorAuthContainer = EnableTwoFactorAuthContainerFactory(
  EnableTwoFactorAuthComponent
);

const TwoFactorAuthSettingsCardComponent = (props, context) => {
  return (
    <ContentWithHeaderLayoutTemplate
      header={context.t('Two-Factor Authentication')}
      customClass="profile-and-security-widget"
      classModifiers={['sm']}>
      <EnableTwoFactorAuthContainer />
    </ContentWithHeaderLayoutTemplate>
  );
};

TwoFactorAuthSettingsCardComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default TwoFactorAuthSettingsCardComponent;

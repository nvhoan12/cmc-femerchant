import React from 'react';
import PropTypes from 'prop-types';
import APSwitch from 'apex-web/lib/components/common/APSwitch/APSwitch';
import APButton from 'apex-web/lib/components/common/APButton';
import Enable2FAModalContainer from '../../../../EnableTwoFactorAuth/Enable2FAModalContainer';
import Disable2FAModalContainer from 'apex-web/lib/components/EnableTwoFactorAuth/Disable2FAModalContainer';
import GetGoogle2FARecoveryKeyModalContainer from 'apex-web/lib/components/EnableTwoFactorAuth/GetGoogle2FARecoveryKeyModalContainer';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

import './EnableTwoFactorAuthComponent.css';

const bemClasses = getBEMClasses('settings-enable-two-factor-auth');

const EnableTwoFactorAuthComponent = (props, context) => {
  const handleClick = newValue => {
    if (newValue) {
      props.disable2FA();
    } else {
      props.enable2FA();
    }
  };

  return (
    <div className={bemClasses()}>
      <div className={bemClasses('on-off-2fa-container')}>
        <div className={bemClasses('switch')}>
          <APSwitch name="use2FA" onClick={handleClick} />
        </div>
        <div className={bemClasses('info')}>
          <p>
            {context.t(
              'Two-Factor Authentication (2FA) adds another layer of protection to your account by requiring an additional passcode during the logging in process.'
            )}
          </p>
          <p>
            {context.t(
              'You will need to download Google Authenticator on your smartphone as part of the process to enable 2FA.'
            )}
          </p>
        </div>
      </div>
      {props.use2FAOriginal && (
        <div>
          <APButton
            name="recovery2FA"
            customClass={bemClasses()}
            onClick={props.getGoogle2FARecoveryKey}>
            {context.t('Recovery')}
          </APButton>
        </div>
      )}
      <Enable2FAModalContainer />
      <Disable2FAModalContainer />
      <GetGoogle2FARecoveryKeyModalContainer />
    </div>
  );
};

EnableTwoFactorAuthComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

EnableTwoFactorAuthComponent.propTypes = {
  getGoogle2FARecoveryKey: PropTypes.func.isRequired,
  disable2FA: PropTypes.func.isRequired,
  enable2FA: PropTypes.func.isRequired,
  use2FAOriginal: PropTypes.bool.isRequired
};

export default EnableTwoFactorAuthComponent;

import React from 'react';
import { connect } from 'react-redux';
import Modal from '../../components/common/Modal/Modal';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { getlocalLanguage } from '../../helpers/localLanguage';
import config from '../../config';
import './PDPA.css';

const classes = getBEMClasses('pdpa');

const PdpaNotification = ({ token }) => {
  const [open, setOpen] = React.useState(false);
  const [pdpaUrl, setPdpaUrl] = React.useState('');

  React.useEffect(
    () => {
      const pdpaAccepted = localStorage.getItem('pdpaAccepted');
      if (!pdpaAccepted && token) {
        (async () => {
          const res = await fetch(config.services.pdpa, {
            method: 'GET',
            headers: { 'X-BTZ-TOKEN': token }
          });
          const resJson = await res.json();
          if (resJson['pdpa_accepted'] === 0) {
            const language = getlocalLanguage();
            setPdpaUrl(
              language === 'EN'
                ? 'https://api-doc.bitazza.com/pdpa/en.html'
                : 'https://api-doc.bitazza.com/pdpa/th.html'
            );
            return setOpen(true);
          }
          return localStorage.setItem('pdpaAccepted', 'true');
        })();
      }
    },
    [token]
  );

  const handleAccept = async () => {
    await fetch(config.services.pdpa, {
      method: 'PUT',
      headers: { 'X-BTZ-TOKEN': token }
    });
    return localStorage.setItem('pdpaAccepted', 'true');
  };

  return (
    <React.Fragment>
      <Modal
        isOpen={open}
        title="Personal Data Privacy Act."
        close={() => setOpen(false)}
        customClass={classes('modal')}
        footer={{
          buttonText: 'accept',
          buttonStyle: 'additive',
          onClick: handleAccept
        }}>
        <iframe className={classes('iframe')} src={pdpaUrl} />
      </Modal>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return { token: state.auth.token };
};

export default connect(
  mapStateToProps,
  null
)(PdpaNotification);

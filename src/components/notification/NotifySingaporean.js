import React from 'react';
import { useLocation } from 'react-router-dom';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { getCountry } from '../../helpers/cacheLocaltion';

import './NotifySingaporean.css';

const classes = getBEMClasses('notify-singaporean');

const NotifySingaporean = () => {
  const { pathname } = useLocation();
  const [open, setOpen] = React.useState(false);

  React.useEffect(
    () => {
      if (!open && ['/', '/signup'].includes(pathname)) {
        const country = getCountry();
        if (country && country === 'Singapore') {
          setOpen(true);
        }
      }
    },
    [pathname]
  );

  return (
    <React.Fragment>
      <Modal
        isOpen={open}
        title="Singapore"
        close={() => setOpen(false)}
        footer={{
          buttonText: 'OK, I CONFIRM I HAVE READ THIS',
          buttonStyle: 'additive',
          onClick: () => {}
        }}>
        <p className={classes('text')}>
          This website and documents thereon are not for display, circulation,
          reception, distribution or use in Singapore (including to the public
          or any section of the public in Singapore) or any other jurisdiction
          where the products or services offered by Bitazza would be prohibited
          without a requisite license or exemption. In particular, Bitazza is
          not licensed as a payment services provider in Singapore under the
          Payment Services Act 2019 (the Act) and would not be able to provide
          payment services (as defined in the Act) to any person in Singapore.
        </p>
      </Modal>
    </React.Fragment>
  );
};

export default NotifySingaporean;

import { connect } from 'react-redux';
import ResetPasswordFormComponent from './ResetPasswordFormComponent';
import {
  fetchResetPassword,
  setResetPasswordError,
  clearResetPasswordError
} from '../../redux/actions/resetPasswordActions';
import { getQueryParam } from 'apex-web/lib/helpers/queryParamsHelper';
import { reduxForm, formValueSelector, clearFields } from 'redux-form';
// import { validatePassword } from 'apex-web/lib/helpers/formValidations';
import { validatePasswordStricted } from '../../helpers/formValidations';

const d1 = getQueryParam('d1');
const userid = getQueryParam('UserId');
const verifycode = getQueryParam('verifycode');

const ResetPasswordForm = reduxForm({
  form: 'resetPassword',
  onSubmit: (payload, dispatch) => {
    const errorMessage = validatePasswordStricted(payload.password);
    if (errorMessage) {
      dispatch(
        clearFields(
          'resetPassword',
          false,
          true,
          'password',
          'matchingPassword'
        )
      );
      dispatch(setResetPasswordError({ errorMessage }));
    } else {
      dispatch(
        fetchResetPassword({
          verifycode,
          userid,
          d1,
          formValues: payload
        })
      );
    }
  }
})(ResetPasswordFormComponent);

const selector = formValueSelector('resetPassword');

const mapStateToProps = state => {
  const {
    apiProcessing: { processingResetPassword },
    resetPassword: { message, error: fetchError, validateError }
  } = state;
  return {
    passwordValue: selector(state, 'password'),
    processingResetPassword,
    message,
    fetchError,
    validateError
  };
};

const mapDispatchToProps = {
  clearResetPasswordError
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPasswordForm);

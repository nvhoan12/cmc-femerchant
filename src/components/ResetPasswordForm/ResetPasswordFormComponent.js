import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { styleNames } from 'apex-web/lib/propTypes/commonComponent';
import APIcon from 'apex-web/lib/components/common/APIcon';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APInput from 'apex-web/lib/components/common/APInput';
import APButton from 'apex-web/lib/components/common/APButton';
import {
  required,
  matchingPassword
} from 'apex-web/lib/helpers/formValidations';
import passwordScore from 'apex-web/lib/helpers/passwordScore';
import path from 'apex-web/lib/helpers/path';
import StandAloneLayout from 'apex-web/lib/layouts/StandAloneLayout/StandAloneLayout';

import 'apex-web/lib/styles/components/common/StandaloneForm.css';
import './ResetPasswordFormComponent.css';

const baseClasses = getBEMClasses('standalone-form');
const resetPasswordClasses = getBEMClasses('reset-password');

export default class ResetPasswordFormComponent extends React.Component {
  static defaultProps = {
    handleSubmit: () => {},
    processingResetPassword: false,
    message: '',
    fetchError: false
  };

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    processingResetPassword: PropTypes.bool.isRequired,
    fetchError: PropTypes.bool
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  handleChange = () => {
    if (this.props.validateError) {
      this.props.clearResetPasswordError();
    }
  };

  getPasswordStrength = () => {
    const { context } = this;
    const score = passwordScore(this.props.passwordValue);
    const passwordStrengthValues = {
      0: context.t('Very Weak'),
      1: context.t('Weak'),
      2: context.t('Medium'),
      3: context.t('Strong'),
      4: context.t('Very Strong')
    };
    return passwordStrengthValues[score];
  };

  render() {
    const {
      context,
      props: {
        processingResetPassword,
        message,
        handleSubmit,
        fetchError,
        pristine,
        passwordValue,
        invalid,
        validateError
      }
    } = this;
    console.log('fetchError', fetchError);
    console.log('message', message);
    return (
      <StandAloneLayout>
        <div
          className={`${baseClasses('container')} ${resetPasswordClasses(
            'container'
          )}`}>
          {!message && (
            <div
              className={`${baseClasses('header')} ${resetPasswordClasses(
                'header'
              )}`}>
              <div className={`${baseClasses('header-text')}`}>
                {context.t('Reset Your Password')}
              </div>
              <Link to={path('/')} className={baseClasses('close-icon')}>
                <APIcon name="close" classModifiers="big" />
              </Link>
            </div>
          )}

          {!message && (
            <form onSubmit={handleSubmit} className={`${baseClasses('form')}`}>
              <APInput
                type="password"
                name="password"
                customClass={resetPasswordClasses()}
                label={context.t('New Password')}
                validate={[required]}
                readOnly={processingResetPassword}
                info={
                  passwordValue && (
                    <span className={baseClasses('password-strength')}>
                      {this.getPasswordStrength()}
                    </span>
                  )
                }
                onChange={this.handleChange}
              />
              <APInput
                type="password"
                name="matchingPassword"
                customClass={resetPasswordClasses()}
                label={context.t('Matching Password')}
                readOnly={processingResetPassword}
                validate={[required, matchingPassword]}
                onChange={this.handleChange}
              />

              {validateError && (
                <div className={baseClasses('password-error')}>
                  {context.t(validateError)}
                </div>
              )}

              <hr className={`${resetPasswordClasses('separator')}`} />
              <APButton
                type="submit"
                disabled={pristine || processingResetPassword || invalid}
                customClass={`${resetPasswordClasses()}`}
                styleName={styleNames.additive}>
                {processingResetPassword
                  ? context.t('Resetting your password...')
                  : context.t('Reset Password')}
              </APButton>
            </form>
          )}

          {message && (
            <React.Fragment>
              <div className={`${baseClasses('message')}`}>
                <div className={`${baseClasses('message-text')}`}>
                  {context.t(message)}
                </div>
              </div>
              <div className={`${baseClasses('message-footer')}`}>
                {fetchError ? (
                  <Link
                    to={path('/problem-logging-in')}
                    className={baseClasses('btn', 'subtractive')}>
                    {context.t('Problem resetting your password?')}
                  </Link>
                ) : (
                  <Link
                    to={path('/login')}
                    className={baseClasses('btn', 'additive')}>
                    {context.t('Go to Exchange')}
                  </Link>
                )}
              </div>
            </React.Fragment>
          )}
        </div>
      </StandAloneLayout>
    );
  }
}

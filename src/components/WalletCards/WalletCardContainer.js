import { connect } from 'react-redux';
import WalletCardComponent from './WalletCardComponent';
import { getProductByIdSelector } from 'apex-web/lib/redux/selectors/productPositionSelectors';
import { baseCurrencyPriceSelector } from 'apex-web/lib/redux/selectors/baseCurrenciesSelectors';
import { selectPositionAndSave } from 'apex-web/lib/redux/actions/positionActions';

const mapStateToProps = (state, ownProps) => {
  const {
    iconFileName,
    ProductSymbol,
    ProductFullName,
    Amount,
    rawAmount,
    ProductType,
    ProductId,
    productColor
  } = getProductByIdSelector(state, ownProps.productId);
  const baseCurrency = baseCurrencyPriceSelector(state, ownProps.productId);
  const isCrypto = ProductType === 'CryptoCurrency';

  return {
    iconFileName,
    ProductSymbol,
    ProductFullName,
    Amount,
    rawAmount,
    isCrypto,
    ProductId,
    productColor,
    baseCurrency,
    detailsLink: ownProps.detailsLink
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    selectPositionAndSave: () => {
      dispatch(selectPositionAndSave(ownProps.productId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WalletCardComponent);

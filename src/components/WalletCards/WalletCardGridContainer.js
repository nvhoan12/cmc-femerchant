import { connect } from 'react-redux';
import {
  fetchingSelector,
  fundedPositionsSelector
} from 'apex-web/lib/redux/selectors/productPositionSelectors';
import WalletCardGridComponent from './WalletCardGridComponent';

const mapStateToProps = (state, ownProps) => {
  return {
    fetching: fetchingSelector(state),
    products: fundedPositionsSelector(state),
    detailsLink: ownProps.detailsLink
  };
};

export default connect(mapStateToProps)(WalletCardGridComponent);

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import path from 'apex-web/lib/helpers/path';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import ProductIconContainer from 'apex-web/lib/components/common/ProductIcon/ProductIconContainer';
import APIcon from 'apex-web/lib/components/common/APIcon';
import CryptoNavItemsContainer from 'apex-web/lib/components/WalletNavItems/CryptoNavItemsContainer';
import FiatNavItemsContainer from 'apex-web/lib/components/WalletNavItems/FiatNavItemsContainer';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import resize from 'apex-web/lib/hocs/resize';

import './WalletCard.css';

const walletCardClasses = getBEMClasses('wallet-card');

const WalletCard = (props, context) => {
  const {
    iconFileName,
    ProductSymbol,
    ProductFullName,
    Amount,
    rawAmount,
    isCrypto,
    ProductId,
    productColor,
    baseCurrency,
    width
  } = props;
  const iconSize = width > 1024 ? 44 : 28;

  return (
    <div className={`${walletCardClasses()} container-with-shadow`}>
      <div className={walletCardClasses('icon-label-row')}>
        <div
          style={{ borderTop: `4px solid ${productColor}` }}
          className={walletCardClasses('icon')}>
          <ProductIconContainer iconFileName={iconFileName} size={iconSize} />
        </div>
        <span className={walletCardClasses('symbol-label')}>
          {`${ProductSymbol} ${ProductFullName}`}
        </span>
      </div>
      <div className={walletCardClasses('balance-row')}>
        <div className={walletCardClasses('balance')}>
          <span className={walletCardClasses('label')}>
            {context.t('Current Balance')}
          </span>
          <div className={walletCardClasses('amount')}>
            <span className={walletCardClasses('amount-text')}>{Amount}</span>
          </div>
        </div>
        {baseCurrency && (
          <div className={walletCardClasses('balance')}>
            <span className={clsx(walletCardClasses('label'), 'text-right')}>
              {baseCurrency.Product} {context.t('Value')}
            </span>
            <div
              className={clsx(
                walletCardClasses('amount'),
                'justify-content-end'
              )}>
              <div className={clsx(walletCardClasses('amount-text'), 'p-0')}>
                {formatNumberToLocale(
                  rawAmount * baseCurrency.value,
                  baseCurrency.DecimalPlaces
                )}
              </div>
            </div>
          </div>
        )}
      </div>
      <div className={walletCardClasses('activity-link-row')}>
        <div className={walletCardClasses('activity-link-container')}>
          <Link
            className={walletCardClasses('activity-link')}
            to={path(props.detailsLink)}
            onClick={props.selectPositionAndSave}>
            {context.t('Details')}{' '}
            <APIcon
              name="arrowNext"
              customClass={walletCardClasses('next-icon')}
            />
          </Link>
        </div>
        <div className={walletCardClasses('icon-nav-buttons')}>
          {isCrypto ? (
            <CryptoNavItemsContainer
              product={{
                iconFileName,
                ProductSymbol,
                ProductFullName,
                ProductId
              }}
              showIconButtonLabel={false}
            />
          ) : (
            <FiatNavItemsContainer
              product={{
                iconFileName,
                ProductSymbol,
                ProductFullName,
                ProductId
              }}
              showIconButtonLabel={false}
            />
          )}
        </div>
      </div>
    </div>
  );
};

WalletCard.contextTypes = {
  t: PropTypes.func.isRequired
};

export default resize(WalletCard);

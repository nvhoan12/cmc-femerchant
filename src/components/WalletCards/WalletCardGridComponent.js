import React from 'react';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import WalletCard from './WalletCardContainer';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';

import './WalletCardGrid.css';

const walletCardGridClasses = getBEMClasses('wallet-card-grid');

const WalletCardGrid = props => {
  const items = props.products.map(item => {
    return (
      <WalletCard
        key={item.ProductId}
        productId={item.ProductId}
        detailsLink={props.detailsLink}
      />
    );
  });

  return (
    <React.Fragment>
      {props.fetching || !!items.length ? (
        <div className={walletCardGridClasses()}>
          {props.fetching ? <Spinner /> : items}
        </div>
      ) : (
        ''
      )}
    </React.Fragment>
  );
};

export default WalletCardGrid;

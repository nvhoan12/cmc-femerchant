/* global TradingView */

import React from 'react';
import PropTypes from 'prop-types';
import ChartAPI from 'apex-web/lib/services/tradingView/ChartAPI';
import jstz from 'jstimezonedetect';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import path from 'apex-web/lib/helpers/path';
import config from 'apex-web/lib/config';
import { getGateway } from 'apex-web/lib/helpers/wsHelper';
import { getCSSVar } from 'apex-web/lib/helpers/cssVarHelper';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import './TVChartSimpleComponent.css';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import { convertTickerIntervalToTradingViewResolution } from 'apex-web/lib/helpers/tickerHelper';

const tvChartClasses = getBEMClasses(['simple-tv-chart']);

class TVChartComponent extends React.Component {
  componentDidUpdate() {
    this.initChart();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      !this.props.selectedInstrument.InstrumentId &&
      nextProps.selectedInstrument.InstrumentId
    ) {
      return true;
    }
    return !!window.TradingView && !!nextState && !!nextState.chartInitialized;
  }

  componentDidMount() {
    this.cssVariable = getCSSVar();
    this.scripts = {};

    this.script('/vendor/jquery-3.3.1.min.js')
      .then(() => {
        return this.script('/vendor/tradingView/charting_library.min.js');
      })
      .then(() => {
        this.initChart();
      });
  }

  initChart() {
    if (!this.props.selectedInstrument.InstrumentId) return;
    this.wsUri = getGateway();
    const timezone = jstz.determine().name();
    const currentTimezone = timezone ? timezone : config.TradingView.timezone;

    if (!this.chart) {
      if (!window.tvChartDataFeed) {
        window.tvChartDataFeed = new ChartAPI.UDFCompatibleDatafeed(
          this.wsUri,
          null,
          this.props.customGateway,
          currentTimezone,
          increment => convertIncrementToIntDecimalPlaces(increment)
        );
      }

      this.createChart(window.tvChartDataFeed);
    } else {
      this.chart.setSymbol(
        this.props.selectedInstrument.Symbol,
        convertTickerIntervalToTradingViewResolution(
          config.TickerData.defaultInterval
        )
      );
    }
  }

  script = location => {
    return new Promise(resolve => {
      this.scripts[location] = document.createElement('script');
      this.scripts[location].src = path(location);
      this.scripts[location].type = 'text/javascript';
      this.scripts[location].async = true;
      this.scripts[location].onload = resolve;
      document.body.appendChild(this.scripts[location]);
    });
  };

  componentWillUnmount() {
    clearTimeout(this.timeout);
    if (this.chart) {
      try {
        this.chart.remove();
      } catch (e) {
        console.warn('silently handled chart error: ', e);
      }
      this.scripts['/vendor/tradingView/charting_library.min.js'].remove();
      this.scripts['/vendor/jquery-3.3.1.min.js'].remove();
    }
  }

  createChart = dataFeed => {
    const loading_BgColor = '#1c1c1c';
    const bgColor = '#1c1c1c';
    const gridColor = this.cssVariable('--tv-chart__grid-color');
    const gridColorVertical = this.cssVariable(
      '--tv-chart__grid-vertical-color'
    );
    const gridColorHorizontal = this.cssVariable(
      '--tv-chart__grid-horizontal-color'
    );
    const scaleFontColor = this.cssVariable('--tv-chart__scale-font-color');
    const scaleLineColor = this.cssVariable('--tv-chart__scale-line-color');
    const scaleBgColor = this.cssVariable('--tv-chart__scale-bg-color');
    const graphUpColor = '#26b367';
    const graphDownColor = '#c22945';
    const graphBorderUpColor = '#26b367';
    const graphBorderDownColor = '#c22945';
    const graphUpOpaqueColor = 'rgba(38, 179, 103, .3)';
    const graphDownOpaqueColor = 'rgba(194, 41, 69, .3)';

    const custom_css_url =
      this.props.theme === 'light'
        ? ''
        : '/vendor/tradingView/charting_library-dark-theme.css';
    const chartOptions = {
      symbol: this.props.selectedInstrument.Symbol,
      datafeed: dataFeed,
      library_path: path('/vendor/tradingView/'),
      autosize: true,
      interval: convertTickerIntervalToTradingViewResolution(
        config.TickerData.defaultInterval
      ),
      container_id: 'trading-view-chart',
      loading_screen: { backgroundColor: loading_BgColor },
      custom_css_url: path(custom_css_url),
      preset: 'mobile',
      overrides: {
        'paneProperties.background': bgColor,
        'paneProperties.gridProperties.color': gridColor,
        'paneProperties.vertGridProperties.color': gridColorVertical,
        'paneProperties.horzGridProperties.color': gridColorHorizontal,
        'paneProperties.topMargin': 15,
        'paneProperties.bottomMargin': 25,
        'scalesProperties.textColor': scaleFontColor,
        'scalesProperties.lineColor': scaleLineColor,
        'scalesProperties.showLeftScale': false,
        'scalesProperties.showRightScale': true,
        'scalesProperties.backgroundColor': scaleBgColor,
        volumePaneSize: 'large',
        'mainSeriesProperties.candleStyle.upColor': graphUpColor,
        'mainSeriesProperties.candleStyle.downColor': graphDownColor,
        'mainSeriesProperties.candleStyle.borderUpColor': graphBorderUpColor,
        'mainSeriesProperties.candleStyle.borderDownColor': graphBorderDownColor,
        'mainSeriesProperties.candleStyle.drawWick': true,
        'mainSeriesProperties.hollowCandleStyle.upColor': graphUpColor,
        'mainSeriesProperties.hollowCandleStyle.downColor': bgColor,
        'mainSeriesProperties.hollowCandleStyle.borderUpColor': graphBorderUpColor,
        'mainSeriesProperties.hollowCandleStyle.borderDownColor': graphBorderDownColor,
        'mainSeriesProperties.hollowCandleStyle.drawWick': false,
        'mainSeriesProperties.haStyle.upColor': graphUpColor,
        'mainSeriesProperties.haStyle.downColor': graphDownColor,
        'mainSeriesProperties.haStyle.borderUpColor': graphBorderUpColor,
        'mainSeriesProperties.haStyle.borderDownColor': graphBorderDownColor,
        'mainSeriesProperties.haStyle.drawWick': false,
        'mainSeriesProperties.barStyle.upColor': graphUpColor,
        'mainSeriesProperties.barStyle.downColor': graphDownColor,
        'mainSeriesProperties.lineStyle.color': graphUpColor,
        'mainSeriesProperties.lineStyle.linewidth': '4',
        'mainSeriesProperties.areaStyle.color1': graphUpColor,
        'mainSeriesProperties.areaStyle.color2': graphUpOpaqueColor,
        'mainSeriesProperties.areaStyle.linecolor': graphUpColor,
        'mainSeriesProperties.baselineStyle.topFillColor1': graphUpOpaqueColor,
        'mainSeriesProperties.baselineStyle.topFillColor2': graphUpOpaqueColor,
        'mainSeriesProperties.baselineStyle.bottomFillColor1': graphDownOpaqueColor,
        'mainSeriesProperties.baselineStyle.bottomFillColor2': graphDownOpaqueColor,
        'mainSeriesProperties.baselineStyle.topLineColor': graphUpColor,
        'mainSeriesProperties.baselineStyle.bottomLineColor': graphDownColor
      },
      studies_overrides: {
        'volume.volume.color.0': graphDownOpaqueColor,
        'volume.volume.color.1': graphUpOpaqueColor
      }
    };

    // For some reasons TradingView.onready is not fired, timeout is used as fallback to initialize chart after 1.5 sec
    this.timeout = setTimeout(() => {
      try {
        this.chart = new TradingView.widget({
          ...chartOptions,
          disabled_features: [
            'header_widget',
            'left_toolbar',
            'timeframes_toolbar',
            'control_bar',
            'use_localstorage_for_settings'
          ]
        });
        this.chart.onChartReady(() => {
          const activeChart = this.chart.activeChart();
          activeChart.createStudy('Moving Average', false, false, [7], null, {
            'plot.color': '#55e7e8'
          });
          activeChart.createStudy('Moving Average', false, false, [25], null, {
            'plot.color': '#e5a353'
          });
          activeChart.createStudy('Moving Average', false, false, [99], null, {
            'plot.color': '#ec59ed'
          });

          // minimized indicators
          const iframe = document.querySelector('#trading-view-chart > iframe');
          iframe.contentWindow.document
            .querySelector('div.pane-legend > div.pane-legend-line.main > a')
            .click();

          this.setState({ chartInitialized: true });
        });
      } catch (error) {}
    }, 1500);
  };

  render() {
    return (
      <div
        id="trading-view-chart"
        className={tvChartClasses('chart-container')}>
        <Spinner />
      </div>
    );
  }
}

TVChartComponent.propTypes = {
  selectedInstrument: PropTypes.object
};

TVChartComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default TVChartComponent;

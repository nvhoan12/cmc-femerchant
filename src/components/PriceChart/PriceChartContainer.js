import React from 'react';
import ApexChartContainer from 'apex-web/lib/components/PriceChart/ApexChartContainer';
import TVChartContainer from './TVChartContainer';
import LightWeightChartContainer from 'apex-web/lib/components/LightWeightCharts/LightWeightChartContainer';
import config from 'apex-web/lib/config';

const chartOptions = {
  APEX: <ApexChartContainer />,
  TradingView: <TVChartContainer />,
  LightWeight: <LightWeightChartContainer />
};

const PriceChartContainer = props => {
  let chart = config.TradingLayout.chart;
  if (props.chart) chart = props.chart;
  return chartOptions[chart];
};

export default PriceChartContainer;

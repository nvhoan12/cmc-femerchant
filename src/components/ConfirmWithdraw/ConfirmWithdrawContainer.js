import { connect } from 'react-redux';
import { fetchConfirmWithdraw } from 'apex-web/lib/redux/actions/confirmWithdrawActions';
import { getQueryParam } from 'apex-web/lib/helpers/queryParamsHelper';
import ConfirmWithdrawComponent from './ConfirmWithdrawComponent';

const mapStateToProps = state => {
  const {
    apiProcessing: { processingConfirmWithdraw },
    confirmWithdraw: { message, error }
  } = state;
  return {
    processingConfirmWithdraw,
    message,
    error
  };
};

const mapDispatchToProps = dispatch => {
  const d1 = getQueryParam('d1');
  const userid = getQueryParam('UserId');
  const verifycode = getQueryParam('verifycode');
  return {
    fetchConfirmWithdraw: () =>
      dispatch(fetchConfirmWithdraw({ verifycode, userid, d1 }))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmWithdrawComponent);

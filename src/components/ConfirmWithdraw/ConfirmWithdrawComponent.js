import React from 'react';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MobileDetect from 'mobile-detect';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import APLogo from 'apex-web/lib/components/common/APLogo/APLogo';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import path from 'apex-web/lib/helpers/path';
import { defaultPath } from 'apex-web/lib/routeTemplates';
import config from '../../config';
import doneIcon from '../../images/icons/done.png';
import bannersIosAndroid from '../../images/news-banners-ios-android.jpg';
import BTZIcon from '../../components/common/BTZIcon/BTZIcon';

import 'apex-web/lib/styles/components/common/StandaloneModal.css';
import './ConfirmWithdrawContainer.css';

const baseClasses = getBEMClasses('standalone-modal');
const classes = getBEMClasses('confirm-withdraw');

export default class ConfirmWithdrawComponent extends React.Component {
  static defaultProps = {
    fetchConfirmWithdraw: () => {},
    processingConfirmWithdraw: true,
    message: ''
  };

  static propTypes = {
    fetchConfirmWithdraw: PropTypes.func.isRequired,
    processingConfirmWithdraw: PropTypes.bool.isRequired,
    message: PropTypes.string
  };

  static contextTypes = {
    t: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchConfirmWithdraw();
  }

  render() {
    const {
      context,
      props: { processingConfirmWithdraw, message }
    } = this;

    const md = new MobileDetect(window.navigator.userAgent);
    const os = md.os(); // AndroidOS, iOS
    const isMobile = !!os;

    return (
      <div className={`${baseClasses('wrapper')}`}>
        <APLogo customClass={baseClasses('logo')} linkTo="/" />
        <div className={`${baseClasses('container')}`}>
          <div className="m-5">
            <div className="d-flex flex-column align-items-center justify-content-center">
              <img className="mb-3" src={doneIcon} alt="done-icon" />
              <p className="text-body-1 font-weight-bold text-primary text-center mb-5">
                {context.t(message)}
              </p>
            </div>
          </div>
          <img
            src={bannersIosAndroid}
            className="w-100"
            alt="bannersIosAndroid"
          />
          <div className={classes('footer')}>
            {isMobile ? (
              <React.Fragment>
                <a
                  href={
                    os === 'iOS'
                      ? config.global.appsStore
                      : config.global.googlePlay
                  }
                  className={clsx(baseClasses('btn', 'additive'))}>
                  <BTZIcon iconName="fa-download" className="mr-2" />
                  {context.t('Download App')}
                </a>
                <a
                  href="bitazzaapp://login"
                  className={clsx(baseClasses('btn'), classes('login-btn'))}>
                  {context.t('Login')}
                </a>
              </React.Fragment>
            ) : (
              <Link
                to={path(`${defaultPath.path}`)}
                className={clsx(baseClasses('btn', 'additive'))}>
                {context.t('Go to Exchange')}
              </Link>
            )}
          </div>
          <Spinner
            isVisible={processingConfirmWithdraw}
            text="Confirming your withdrawal"
          />
        </div>
      </div>
    );
  }
}

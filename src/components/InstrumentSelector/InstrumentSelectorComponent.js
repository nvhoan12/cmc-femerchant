import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'apex-web/lib/components/common/Popover/Popover';
import Tab from '../common/Tab/Tab';
import ProductIconContainer from 'apex-web/lib/components/common/ProductIcon/ProductIconContainer';
import InstrumentSelectorPopupComponent from './InstrumentSelectorPopupComponent';
import { instrumentPropType } from 'apex-web/lib/propTypes/instrumentPropTypes';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import BTZIcon from '../common/BTZIcon/BTZIcon';
import './InstrumentSelectorComponent.css';

const baseClasses = getBEMClasses('instrument-selector');

class InstrumentSelectorComponent extends React.Component {
  constructor(props) {
    super(props);
    this.popoverRef = null;
    this.updateLevel1 = true;
  }

  componentWillMount() {
    if (this.props.instruments.length === 0) return;

    const selectedInstrument = this.props.instruments.find(instrument => {
      return (
        instrument.InstrumentId === this.props.selectedInstrument.InstrumentId
      );
    });

    if (selectedInstrument) return;

    const instrumentId = this.props.instruments[0].InstrumentId;
    this.props.selectInstrument(instrumentId);
  }

  shouldComponentUpdate() {
    return this.updateLevel1;
  }

  runLevel1Updates = () => (this.updateLevel1 = true);

  stopLevel1Updates = () => (this.updateLevel1 = false);

  onRowClick = instrument => {
    this.popoverRef.toggleShow();
    this.props.selectInstrument(instrument.InstrumentId);
  };

  onToggleFavorite = instrument => {
    let newValue = [];
    if (this.props.favoriteInstrumentsID.includes(instrument.InstrumentId)) {
      newValue = this.props.favoriteInstrumentsID.filter(
        item => item !== instrument.InstrumentId
      );
    } else {
      newValue = [...this.props.favoriteInstrumentsID, instrument.InstrumentId];
    }
    const configValue = [
      { Key: 'favoriteInstrumentsID', Value: JSON.stringify(newValue) }
    ];
    this.props.setUserConfig(null, null, configValue);
  };

  render() {
    const {
      fetching,
      instruments,
      selectedInstrument,
      selectedRow,
      level1,
      customClass,
      showFullNameInstrument,
      showTriangles,
      productManifest,
      favoriteInstrumentsID,
      isAuthenticated
    } = this.props;

    let instrumentSymbol = '';
    if (showFullNameInstrument && selectedInstrument.Product1FullName) {
      instrumentSymbol = this.context.t(
        `${selectedInstrument.Product1FullName} / ${
          selectedInstrument.Product2FullName
        }`
      );
    } else {
      instrumentSymbol = selectedInstrument.Product1Symbol
        ? `${selectedInstrument.Product1Symbol}/${
            selectedInstrument.Product2Symbol
          }`
        : '';
    }
    const selectedInstrumentManifest =
      productManifest.manifest[selectedInstrument.Product1Symbol] || {};

    const getTabs = () => {
      const instrumentsGroup = instruments.reduce(
        (sum, item) => {
          let instrument = null;
          const manifest = productManifest.manifest[item.Product1Symbol] || {};
          if (favoriteInstrumentsID.includes(item.InstrumentId)) {
            instrument = { ...item, manifest, favorite: true };
            sum.favorite.push(instrument);
          } else {
            instrument = { ...item, manifest, favorite: false };
          }
          if (instrument.Product2Symbol === 'THB') {
            sum.thb.push(instrument);
          }
          if (instrument.Product2Symbol === 'USDT') {
            sum.usdt.push(instrument);
          }
          if (instrument.Product2Symbol === 'BTC') {
            sum.btc.push(instrument);
          }
          if (instrument.Product2Symbol === 'ETH') {
            sum.eth.push(instrument);
          }
          return sum;
        },
        {
          favorite: [],
          thb: [],
          usdt: [],
          btc: [],
          eth: []
        }
      );
      const tabs = [];
      if (isAuthenticated) {
        tabs.push({
          menuItem: <BTZIcon iconName="fa-star" size="lg" />,
          render: () => (
            <InstrumentSelectorPopupComponent
              onRowClick={this.onRowClick}
              fetching={fetching}
              instruments={instrumentsGroup.favorite}
              selectedRow={selectedRow}
              level1={level1}
              customClass={customClass}
              showFullNameInstrument={showFullNameInstrument}
              onToggleFavorite={this.onToggleFavorite}
              isAuthenticated={isAuthenticated}
            />
          )
        });
      }
      tabs.push({
        menuItem: 'THB',
        render: () => (
          <InstrumentSelectorPopupComponent
            onRowClick={this.onRowClick}
            fetching={fetching}
            instruments={instrumentsGroup.thb}
            selectedRow={selectedRow}
            level1={level1}
            customClass={customClass}
            showFullNameInstrument={showFullNameInstrument}
            onToggleFavorite={this.onToggleFavorite}
            isAuthenticated={isAuthenticated}
          />
        )
      });
      tabs.push({
        menuItem: 'USDT',
        render: () => (
          <InstrumentSelectorPopupComponent
            onRowClick={this.onRowClick}
            fetching={fetching}
            instruments={instrumentsGroup.usdt}
            selectedRow={selectedRow}
            level1={level1}
            customClass={customClass}
            showFullNameInstrument={showFullNameInstrument}
            onToggleFavorite={this.onToggleFavorite}
            isAuthenticated={isAuthenticated}
          />
        )
      });
      tabs.push({
        menuItem: 'BTC',
        render: () => (
          <InstrumentSelectorPopupComponent
            onRowClick={this.onRowClick}
            fetching={fetching}
            instruments={instrumentsGroup.btc}
            selectedRow={selectedRow}
            level1={level1}
            customClass={customClass}
            showFullNameInstrument={showFullNameInstrument}
            onToggleFavorite={this.onToggleFavorite}
            isAuthenticated={isAuthenticated}
          />
        )
      });
      tabs.push({
        menuItem: 'ETH',
        disabled: true,
        render: () => (
          <InstrumentSelectorPopupComponent
            onRowClick={this.onRowClick}
            fetching={fetching}
            instruments={instrumentsGroup.eth}
            selectedRow={selectedRow}
            level1={level1}
            customClass={customClass}
            showFullNameInstrument={showFullNameInstrument}
            onToggleFavorite={this.onToggleFavorite}
            isAuthenticated={isAuthenticated}
          />
        )
      });
      return {
        defaultActiveIndex:
          instrumentsGroup.favorite.length === 0 && isAuthenticated ? 1 : 0,
        panes: tabs
      };
    };

    return (
      <Popover
        ref={ref => (this.popoverRef = ref)}
        trigger={props => (
          <button
            className={baseClasses('trigger')}
            {...props}
            onMouseOver={this.stopLevel1Updates}
            onMouseOut={this.runLevel1Updates}>
            <span className={baseClasses('symbol')}>
              <ProductIconContainer
                iconFileName={selectedInstrumentManifest.iconFileName}
                size={20}
              />
              {instrumentSymbol}
            </span>
            {showTriangles ? (
              <div className={baseClasses('triangles-container')}>
                <span className={'triangle-up'} />
                <span className={'triangle-down'} />
              </div>
            ) : (
              <span className={baseClasses('triangle')} />
            )}
          </button>
        )}
        content={<Tab {...getTabs()} />}
        customClass="instrument-selector-popover"
        drawArrow
      />
    );
  }
}

InstrumentSelectorComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

InstrumentSelectorComponent.propTypes = {
  level1: PropTypes.object.isRequired,
  instruments: PropTypes.array.isRequired,
  fetching: PropTypes.bool.isRequired,
  selectedInstrument: instrumentPropType,
  selectedRow: PropTypes.object.isRequired
};

export default InstrumentSelectorComponent;

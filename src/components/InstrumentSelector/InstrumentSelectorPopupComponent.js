import React from 'react';
import PropTypes from 'prop-types';
import {
  formatPercentageValue,
  formatNumberToLocale
} from 'apex-web/lib/helpers/numberFormatter';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APTable from '../common/APTable';
import ProductIconContainer from 'apex-web/lib/components/common/ProductIcon/ProductIconContainer';
import BTZIcon from '../common/BTZIcon/BTZIcon';

import './InstrumentSelectorPopupComponent.css';

const InstrumentSelectorPopupComponent = (props, context) => {
  const {
    onRowClick,
    level1,
    instruments,
    fetching,
    selectedRow,
    customClass,
    // showFullNameInstrument,
    onToggleFavorite,
    isAuthenticated
  } = props;
  const baseClasses = getBEMClasses('instrument-selector-popup', customClass);
  const [sort, setSort] = React.useState({ name: '', order: '' }); // order => desc asc

  const handdleClickSort = name => {
    if (sort.name === name) {
      if (sort.order === 'desc') {
        return setSort({ name, order: 'asc' });
      }
      return setSort({ name, order: 'desc' });
    }
    return setSort({ name, order: 'asc' });
  };

  const columns = [
    {
      header: (
        <div
          className={baseClasses('btn-sort')}
          onClick={() => handdleClickSort('pairing')}>
          {context.t('Pairing')}
          {sort.name === 'pairing' ? (
            sort.order === 'desc' ? (
              <BTZIcon iconName="fa-sort-desc" />
            ) : (
              <BTZIcon iconName="fa-sort-asc" />
            )
          ) : (
            <BTZIcon iconName="fa-sort" />
          )}
        </div>
      ),
      classModifier: 'coin',
      cell: instrument => {
        return (
          <div
            className={baseClasses('coin')}
            onClick={() => onRowClick(instrument)}>
            <ProductIconContainer
              iconFileName={instrument.manifest.iconFileName}
              size={18}
            />
            {instrument.pairing}
          </div>
        );
      }
    },
    {
      header: (
        <div
          className={baseClasses('btn-sort')}
          onClick={() => handdleClickSort('price')}>
          {context.t('Price')}
          {sort.name === 'price' ? (
            sort.order === 'desc' ? (
              <BTZIcon iconName="fa-sort-desc" />
            ) : (
              <BTZIcon iconName="fa-sort-asc" />
            )
          ) : (
            <BTZIcon iconName="fa-sort" />
          )}
        </div>
      ),
      classModifier: 'price',
      cell: instrument => {
        return (
          <div onClick={() => onRowClick(instrument)}>
            {instrument.lastTrade}
          </div>
        );
      }
    },
    {
      header: (
        <div
          className={baseClasses('btn-sort')}
          onClick={() => handdleClickSort('24hr-chg')}>
          {context.t('24hr Chg')}
          {sort.name === '24hr-chg' ? (
            sort.order === 'desc' ? (
              <BTZIcon iconName="fa-sort-desc" />
            ) : (
              <BTZIcon iconName="fa-sort-asc" />
            )
          ) : (
            <BTZIcon iconName="fa-sort" />
          )}
        </div>
      ),
      classModifier: '24hr-chg',
      cell: instrument => {
        return (
          <div onClick={() => onRowClick(instrument)}>
            {instrument.Rolling24HrPxChangePercent}
          </div>
        );
      }
    }
  ];
  if (isAuthenticated) {
    columns.push({
      header: (
        <div
          className={baseClasses('btn-sort')}
          onClick={() => handdleClickSort('favorite')}>
          <BTZIcon iconName="fa-star" size="lg" />
          {sort.name === 'favorite' ? (
            sort.order === 'desc' ? (
              <BTZIcon iconName="fa-sort-desc" />
            ) : (
              <BTZIcon iconName="fa-sort-asc" />
            )
          ) : (
            <BTZIcon iconName="fa-sort" />
          )}
        </div>
      ),
      cell: instrument => {
        return (
          <div onClick={() => onToggleFavorite(instrument)}>
            {instrument.favorite ? (
              <BTZIcon iconName="fa-star" size="lg" />
            ) : (
              <BTZIcon iconName="fa-star-o" size="lg" />
            )}
          </div>
        );
      }
    });
  }

  const instrumentsRow = instruments
    .map(instrument => {
      const level1Data = level1[instrument.InstrumentId];
      return {
        ...instrument,
        pairing: `${instrument.Product1Symbol}/${instrument.Product2Symbol}`,
        lastTrade:
          level1Data &&
          formatNumberToLocale(
            level1Data.LastTradedPx,
            convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
          ),
        lastTradeNumber: level1Data && level1Data.LastTradedPx,
        Rolling24HrPxChangePercent: level1Data && (
          <span
            className={baseClasses(
              level1Data.Rolling24HrPxChangePercent >= 0
                ? '24h-positive'
                : '24h-negative'
            )}>
            {(level1Data.Rolling24HrPxChangePercent > 0 ? '+' : '') +
              formatPercentageValue(level1Data.Rolling24HrPxChangePercent)}
          </span>
        ),
        rolling24HrPxChangeNumber:
          level1Data && level1Data.Rolling24HrPxChangePercent
      };
    })
    .sort((a, b) => {
      if (sort.name === 'pairing') {
        if (sort.order === 'desc') {
          if (b.pairing < a.pairing) {
            return -1;
          }
          if (a.pairing < b.pairing) {
            return 1;
          }
          return 0;
        }
        if (b.pairing > a.pairing) {
          return -1;
        }
        if (a.pairing > b.pairing) {
          return 1;
        }
        return 0;
      }
      if (sort.name === 'price') {
        if (sort.order === 'desc') {
          return b.lastTradeNumber - a.lastTradeNumber;
        }
        return a.lastTradeNumber - b.lastTradeNumber;
      }
      if (sort.name === '24hr-chg') {
        if (sort.order === 'desc') {
          return b.rolling24HrPxChangeNumber - a.rolling24HrPxChangeNumber;
        }
        return a.rolling24HrPxChangeNumber - b.rolling24HrPxChangeNumber;
      }
      if (sort.name === 'favorite') {
        if (sort.order === 'desc') {
          return b.favorite - a.favorite;
        }
        return a.favorite - b.favorite;
      }
      return 0;
    });

  return (
    <APTable
      {...{
        fetching,
        columns,
        rows: instrumentsRow,
        baseClass: baseClasses,
        selectedRow,
        empty: context.t('No coin is available')
      }}
    />
  );
};

InstrumentSelectorPopupComponent.propTypes = {
  level1: PropTypes.object.isRequired,
  onRowClick: PropTypes.func.isRequired,
  instruments: PropTypes.array.isRequired,
  fetching: PropTypes.bool.isRequired,
  selectedRow: PropTypes.object.isRequired
};

InstrumentSelectorPopupComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default InstrumentSelectorPopupComponent;

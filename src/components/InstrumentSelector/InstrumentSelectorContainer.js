import { connect } from 'react-redux';
import InstrumentSelectorComponent from './InstrumentSelectorComponent';
import { selectInstrument } from 'apex-web/lib/redux/actions/instrumentActions';
import {
  selectedInstrumentSelector,
  instrumentSelectorInstruments
} from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import { isMarginActiveSelector } from 'apex-web/lib/redux/selectors/marginSelectors';
import { setUserConfig } from '../../redux/actions/userActions';

const mapStateToProps = state => {
  const isMarginActive = isMarginActiveSelector(state);
  let instrument = {};
  if (isMarginActive && !state.margin.instrument.fetching) {
    instrument = state.margin.instrument;
  } else {
    instrument = state.apexCore.instrument;
  }
  const { fetching } = instrument;
  const selectedInstrument = selectedInstrumentSelector(state);

  const config = state.user.userConfig.find(
    data => data.Key === 'favoriteInstrumentsID'
  );
  const favoriteInstrumentsID = config ? JSON.parse(config.Value) : [];

  return {
    level1: state.apexCore.level1,
    instruments: instrumentSelectorInstruments(state),
    selectedInstrument,
    selectedRow: {
      key: 'InstrumentId',
      value: selectedInstrument.InstrumentId
    },
    fetching,
    showFullNameInstrument: false,
    productManifest: state.productManifest,
    favoriteInstrumentsID,
    isAuthenticated: state.auth.isAuthenticated
  };
};

const mapDispatchToProps = {
  selectInstrument,
  setUserConfig
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InstrumentSelectorComponent);

import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import RecentActivityHeaderWithFilter from 'apex-web/lib/components/RecentActivityWithFilter/RecentActivityHeaderWithFilter';
import RecentActivityTableWithFilter from './RecentActivityTableWithFilter';

import '../RecentActivity/ActivityComponents.css';

const baseClasses = getBEMClasses('activity');

let RecentActivityWithFilterComponent = (props, context) => {
  const { width, themeModifier, pageSize, minRow } = props;
  return (
    <div className={baseClasses()}>
      <RecentActivityHeaderWithFilter />
      <RecentActivityTableWithFilter
        pageSize={pageSize}
        minRow={minRow}
        themeModifier={themeModifier}
        width={width}
        context={context}
      />
    </div>
  );
};

RecentActivityWithFilterComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default RecentActivityWithFilterComponent;

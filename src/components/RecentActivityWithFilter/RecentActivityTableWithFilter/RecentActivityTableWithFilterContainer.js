import { connect } from 'react-redux';
import RecentActivityTableComponent from '../../RecentActivity/RecentActivityTable/RecentActivityTableComponent';
import config from 'apex-web/lib/config';
import {
  formatActivity,
  getColumns
} from '../../../helpers/recentActivityHelper';
import { getFilteredActivity } from '../../../redux/selectors/recentActivitySelector';

const mapStateToProps = (state, ownProps) => {
  const { minRow, pageSize } = ownProps;
  const filteredActivity = getFilteredActivity(
    state,
    ownProps.filterForSelected
  );
  const content = formatActivity(filteredActivity, ownProps.context);

  const { usePagination } = config.OrderHistoryData;

  const columns = getColumns(
    ownProps.width,
    ownProps.themeModifier,
    ownProps.context
  );

  return {
    minRow,
    pageSize,
    usePagination,
    content,
    columns
  };
};

export default connect(mapStateToProps)(RecentActivityTableComponent);

import React from 'react';
import PropTypes from 'prop-types';
import APVerticalScroller from 'apex-web/lib/components/common/APVerticalScroller/APVerticalScroller';
// import APInlineButton from 'apex-web/lib/components/common/APInlineButton';
import BulkCancelButtonsComponent from 'apex-web/lib/components/OrderBookComponents/BulkCancelButtonsComponent/BulkCancelButtonsComponent';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { isFirefox } from 'apex-web/lib/helpers/browserHelper';
import PaddedDecimal from 'apex-web/lib/components/common/PaddedDecimal/PaddedDecimal';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import Level2DataTableComponent from './Level2DataTableComponent';

import 'apex-web/lib/styles/components/TradeComponent.css';
import './OrderBookComponent.css';

const tradeComponentClasses = getBEMClasses('trade-component');
const orderBookClasses = getBEMClasses('orderbook');
const flexTable = getBEMClasses('flex-table');

export default class OrderBookComponent extends React.Component {
  constructor(props, context) {
    super(props, context);
    // default state
    this.state = {
      showCenterButton: false //  will change to true when user scrolls away from rest position
    };
    // event handler bindings
    this.onClickCenter = this.onClickCenter.bind(this);
    this.resizeEventListener = this.resizeEventListener.bind(this);
    this.onScrollRestChange = this.onScrollRestChange.bind(this);
    this.handleScrollRestChange = this.handleScrollRestChange.bind(this);

    this.buyScroller = React.createRef();
    this.sellScroller = React.createRef();
  }

  onClickCenter(overrideUserScrolled = false) {
    if (this.sellScroller && this.buyScroller) {
      const scrollToRest = () => {
        this.sellScroller.current.scrollToRest();
        this.buyScroller.current.scrollToRest();
      };

      if (overrideUserScrolled)
        return this.sellScroller.current.resetUserScrolled(() =>
          this.buyScroller.current.resetUserScrolled(scrollToRest)
        );
      scrollToRest();
    }
  }

  resizeEventListener = () => this.onClickCenter(true);

  componentDidUpdate(prevProps) {
    this.onClickCenter(
      this.props.selectedInstrumentId !== prevProps.selectedInstrumentId
    );
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeEventListener);
    this.onClickCenter();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeEventListener);
  }

  onScrollRestChange(value) {
    // show "center" button when user scrolls away from rest position
    setTimeout(this.handleScrollRestChange, 230);
  }

  handleScrollRestChange() {
    if (!!this.sellScroller.current) {
      const sellIsAtRest = this.sellScroller.current.getIsAtRest();
      const buyIsAtRest = this.buyScroller.current.getIsAtRest();
      const showCenterButton = !sellIsAtRest || !buyIsAtRest;
      this.setState({
        showCenterButton
      });
    }
  }

  render() {
    const {
      context,
      onScrollRestChange,
      props: {
        // spread,
        cancelAllOrders,
        cancelBuyOrders,
        cancelSellOrders,
        openOrders,
        buyOpenOrders,
        sellOpenOrders,
        recentTrades
      }
    } = this;

    const {
      selectedInstrument,
      cancelOrders,
      level2: { buy, sell },
      buyPriceClicked,
      buyQuantityClicked,
      sellPriceClicked,
      sellQuantityClicked,
      fetching,
      hideBulkCancelButtons
    } = this.props;

    // const CenterButton = ({ state, context, onClickCenter, spread }) => (
    //   <div className={orderBookClasses('spread-row')}>
    //     <span>
    //       {context.t('Spread')} {spread}
    //     </span>
    //     {state &&
    //       state.showCenterButton && (
    //         <APInlineButton
    //           icon="center"
    //           iconModifier="small"
    //           text={context.t('Center')}
    //           onClick={() => onClickCenter(true)}
    //           customClass={orderBookClasses('center-button')}
    //         />
    //       )}
    //   </div>
    // );

    return (
      <div className={tradeComponentClasses()}>
        <div className={`${flexTable()} ${orderBookClasses()}`}>
          <div className={`${flexTable('body')} ${orderBookClasses('body')}`}>
            <div
              className={`${flexTable(
                'header',
                'background'
              )} ${orderBookClasses('header')}`}>
              <div className={flexTable('column')}>
                <div
                  className={`${flexTable('fixed')} ${orderBookClasses(
                    'table-header',
                    'price'
                  )}`}>
                  {context.t('Price')}
                </div>
              </div>
              <div className={flexTable('column')}>
                <div
                  className={`${flexTable('fixed')} ${orderBookClasses(
                    'table-header',
                    'qty'
                  )}`}>
                  {context.t('Qty')}
                </div>
              </div>
              {/* <div className={flexTable('column')}>
                <div
                  className={`${flexTable('fixed')} ${orderBookClasses(
                    'table-header',
                    'my-size'
                  )}`}>
                  {context.t('My Size')}
                </div>
              </div> */}
            </div>
            <div className={orderBookClasses('order-book-body')}>
              <APVerticalScroller
                ref={this.sellScroller}
                restPosition="bottom"
                onScrollRestChange={onScrollRestChange}
                customClass={orderBookClasses(
                  'vertical-scroller-body',
                  isFirefox() ? 'firefox' : ''
                )}>
                <div className="orderbook__table-filler">
                  <Level2DataTableComponent
                    quantityDecimalPlaces={
                      this.props.convertedQuantityIncrement
                    }
                    priceDecimalPlaces={this.props.convertedPriceIncrement}
                    selectedInstrument={selectedInstrument}
                    cancelOrders={cancelOrders}
                    onPriceClick={sellPriceClicked}
                    onQuantityClick={sellQuantityClicked}
                    level2Data={sell}
                    classModifier={'sell'}
                    fetching={fetching}
                  />
                </div>
              </APVerticalScroller>

              <div className={orderBookClasses('spread-row')}>
                <span>
                  {context.t('Last price')}{' '}
                  {recentTrades.length !== 0 && (
                    <PaddedDecimal
                      value={recentTrades[0].Price}
                      bemMod={recentTrades[0].TakerSide === 0 ? 'buy' : 'sell'}
                      decimals={convertIncrementToIntDecimalPlaces(
                        selectedInstrument.PriceIncrement
                      )}
                    />
                  )}
                </span>
              </div>

              <APVerticalScroller
                ref={this.buyScroller}
                restPosition="top"
                onScrollRestChange={onScrollRestChange}
                customClass={orderBookClasses(
                  'vertical-scroller-body',
                  isFirefox() ? 'firefox' : ''
                )}>
                <Level2DataTableComponent
                  quantityDecimalPlaces={this.props.convertedQuantityIncrement}
                  priceDecimalPlaces={this.props.convertedPriceIncrement}
                  selectedInstrument={selectedInstrument}
                  cancelOrders={cancelOrders}
                  onPriceClick={buyPriceClicked}
                  onQuantityClick={buyQuantityClicked}
                  level2Data={buy}
                  classModifier={'buy'}
                  fetching={fetching}
                />
              </APVerticalScroller>
              {!hideBulkCancelButtons && (
                <div>
                  <BulkCancelButtonsComponent
                    {...{
                      openOrders,
                      buyOpenOrders,
                      sellOpenOrders,
                      cancelAllOrders,
                      cancelBuyOrders,
                      cancelSellOrders
                    }}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OrderBookComponent.defaultProps = {
  hideBulkCancelButtons: false,
  level2: {
    buy: [],
    sell: [],
    short: [],
    unknown: []
  }
};

OrderBookComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

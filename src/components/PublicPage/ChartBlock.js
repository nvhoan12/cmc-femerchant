import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { connect } from 'react-redux';
import { AreaChart, Area, YAxis, ResponsiveContainer } from 'recharts';
import {
  formatPercentageValue,
  formatNumberToLocale
} from 'apex-web/lib/helpers/numberFormatter';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { BTZAnimationNumberCount } from '../common/BTZAnimation/BTZAnimation';
import './ChartBlock.css';
import btcIcon from './../../images/icons/btc.png';
import ethIcon from './../../images/icons/eth.png';
import xrpIcon from './../../images/icons/xrp.png';
import xlmIcon from './../../images/icons/xlm.png';
import daiIcon from './../../images/icons/dai.png';
import compIcon from './../../images/icons/comp.png';

const icons = {
  BTC: btcIcon,
  ETH: ethIcon,
  XRP: xrpIcon,
  XLM: xlmIcon,
  DAI: daiIcon,
  COMP: compIcon
};

const classes = getBEMClasses('btz-chart-block');

const ChartBlock = (props, context) => {
  const { Rolling24HrVolume, LastTradedPx, Rolling24HrPxChange } = props.level1;
  const { Product1Symbol, Product2Symbol } = props.instrument;
  return (
    <div className={classes()}>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <img
            className={classes('coin-icon')}
            src={icons[Product1Symbol]}
            alt={Product1Symbol}
          />
          <p className="text-lead m-0 ml-2">
            {Product1Symbol}/{Product2Symbol}
          </p>
        </div>
        <div>
          <p
            className={clsx(
              'text-lead text-right m-0',
              Rolling24HrPxChange >= 0
                ? classes('positive')
                : classes('negative')
            )}>
            <BTZAnimationNumberCount
              number={LastTradedPx}
              decimals={props.decimalPlaces[Product2Symbol]}
            />{' '}
            {Product2Symbol}
          </p>
        </div>
      </div>
      <div className="row mt-3">
        <div className={clsx('col-6', classes('divider'))}>
          <p className={classes('text-muted')}>
            {context.t('24 Hour Pct Chg')}
          </p>
          <p
            className={clsx(
              'font-weight-bold',
              Rolling24HrPxChange >= 0
                ? classes('positive')
                : classes('negative')
            )}>
            {formatPercentageValue(Rolling24HrPxChange)}
          </p>
        </div>
        <div className="col-6">
          <p className={classes('text-muted')}>{context.t('24 Hour Volume')}</p>
          <p className={clsx('font-weight-bold', classes('text-muted'))}>
            {formatNumberToLocale(Rolling24HrVolume, 2)} {Product1Symbol}
          </p>
        </div>
      </div>
      <div className={classes('chart-container')}>
        <ResponsiveContainer width="100%" height="100%">
          <AreaChart
            margin={{ top: 0, bottom: 0, right: 0, left: 0 }}
            data={props.recentTrades}>
            <defs>
              <pattern
                id="line"
                patternUnits="userSpaceOnUse"
                width="4"
                height="4">
                <line
                  x1="0"
                  y="0"
                  x2="0"
                  y2="4"
                  stroke="#000"
                  strokeWidth="4"
                />
              </pattern>
              <linearGradient id="gradient">
                <stop offset="0%" stopColor="#69A496" />
                <stop offset="100%" stopColor="#D7E07D" />
              </linearGradient>
            </defs>
            <YAxis type="number" hide={true} domain={['dataMin', 'dataMax']} />
            <Area
              type="monotone"
              dataKey="Price"
              isAnimationActive={false}
              stroke="url(#gradient)"
              fill="url(#gradient)"
            />
            <Area
              type="monotone"
              dataKey="Price"
              isAnimationActive={false}
              stroke="url(#gradient)"
              fill="url(#line)"
            />
          </AreaChart>
        </ResponsiveContainer>
      </div>
    </div>
  );
};

const mapStateToProps = (state, props) => {
  const instrument = state.instrument.instruments.find(
    instrument => instrument.Symbol === props.symbol
  );
  const level1 = instrument && state.level1[instrument.InstrumentId];
  const recentTradesChart =
    instrument &&
    state.recentTradesChart.find(
      item => item.InstrumentId === instrument.InstrumentId
    );

  return {
    instrument: instrument || {},
    level1: level1 || {},
    recentTrades: recentTradesChart ? recentTradesChart.recentTrades : [],
    decimalPlaces: state.apexCore.product.decimalPlaces
  };
};

ChartBlock.contextTypes = {
  t: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(ChartBlock);

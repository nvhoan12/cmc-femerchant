import React from 'react';
import clsx from 'clsx';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { BtnLink } from './Btn';
import './FAQBlock.css';

const faqBlockClasses = getBEMClasses('faq-block');

const FAQBlock = ({ item }) => {
  const [open, setOpen] = React.useState(false);
  return (
    <div className={faqBlockClasses()}>
      <BtnLink
        className={clsx(faqBlockClasses('header'), open && 'active')}
        onClick={() => setOpen(!open)}>
        {item.title}
        <i
          className={clsx('fa fa-lg', open ? 'fa-angle-up' : 'fa-angle-down')}
          aria-hidden="true"
        />
      </BtnLink>
      {open && (
        <div
          className={faqBlockClasses('des')}
          dangerouslySetInnerHTML={{ __html: item.des }}
        />
      )}
    </div>
  );
};

export default FAQBlock;

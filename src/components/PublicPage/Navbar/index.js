import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { throttle } from 'lodash';
import logo from '../../../images/logo-horizontal.svg';
import {
  getlocalLanguage,
  setlocalLanguage
} from '../../../helpers/localLanguage';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import { GolabelMessageLanding } from '../../GolabelMessage/GolabelMessage';
import LinkLINE from '../../LinkLINE/LinkLINE';
import { BtnOutline, BtnLink, Link, Select, SelectOption, Btn } from '../Btn';
import { Text } from '../Text';
import KnowledgeMenu from './KnowledgeMenu';
import BitazzaPlatformMenu from './BitazzaPlatformMenu';
import DownloadAppMenu from './DownloadAppMenu';
import './Navbar.css';

const navbarClasses = getBEMClasses('navbar');

const Navbar = (props, context) => {
  const [isScrollDown, setIsScrollDown] = React.useState(false);
  const [isOpenMenu, setOpenMenu] = React.useState(false);
  const language = getlocalLanguage();

  const menuItem = [
    <Link
      href={
        language === 'EN'
          ? 'https://partners.bitazza.com/en/'
          : 'https://partners.bitazza.com/th/'
      }>
      {context.t('BITAZZA PARTNERS')}
    </Link>,
    <BitazzaPlatformMenu />,
    <KnowledgeMenu />,
    <DownloadAppMenu />,
    <LinkLINE
      className="d-sm-none d-block"
      text={context.t('BITAZZAN COMMUNITY')}
    />
  ];

  const handleScroll = () => {
    if (window.matchMedia('(min-width: 1200px)').matches) {
      setIsScrollDown(!!window.scrollY);
    }
  };
  const throttled = throttle(handleScroll, 250);
  React.useEffect(() => {
    window.addEventListener('scroll', throttled);
    throttled();
    return () => window.removeEventListener('scroll', throttled);
  }, []);

  const handleChangeLanguage = e => {
    setlocalLanguage(e.target.value);
    window.location.reload();
  };

  return (
    <div
      className={clsx(
        navbarClasses(),
        isScrollDown && navbarClasses('scall-down')
      )}>
      <GolabelMessageLanding className={navbarClasses('golabel-message')} />
      <div className="container-fluid">
        <div className="d-flex justify-content-between align-items-center">
          <div className="d-flex align-items-center">
            <a href="/">
              <img className={navbarClasses('logo')} src={logo} alt="Bitazza" />
            </a>
            <Select
              className={navbarClasses('language')}
              onChange={handleChangeLanguage}
              value={language}>
              <SelectOption value="EN">English</SelectOption>
              <SelectOption value="TH">ไทย</SelectOption>
            </Select>
            <span className="d-none d-sm-inline-block">
              <LinkLINE />
            </span>
          </div>
          <BtnLink
            className={navbarClasses('fa-bars')}
            onClick={() => setOpenMenu(!isOpenMenu)}>
            <Text
              color="primary"
              noMargin
              fontSize={24}
              text={<i className="fa fa-bars" aria-hidden="true" />}
            />
          </BtnLink>
          <div className={navbarClasses('list-menu')}>
            {menuItem.map((item, index) => (
              <React.Fragment key={index}>{item}</React.Fragment>
            ))}
            <BtnOutline router href="/login">
              {context.t('Log in')}
            </BtnOutline>
            <Btn className="m-0" router href="/signup">
              {context.t('Sign Up')}
            </Btn>
          </div>
        </div>
        <div
          className={clsx(
            navbarClasses('list-menu-collapse'),
            isOpenMenu && navbarClasses('list-menu-collapse-open')
          )}>
          {menuItem.map((item, index) => (
            <React.Fragment key={index}>{item}</React.Fragment>
          ))}
          <Link router href="/login">
            {context.t('Log in')}
          </Link>
          <Btn router href="/signup">
            {context.t('Sign Up')}
          </Btn>
        </div>
      </div>
    </div>
  );
};

Navbar.contextTypes = {
  t: PropTypes.func.isRequired
};

export default Navbar;

import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { getlocalLanguage } from '../../../helpers/localLanguage';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import BTZDivider from '../../common/BTZDivider/BTZDivider';
import { Link } from '../Btn';
import './Navbar.css';

const navbarClasses = getBEMClasses('navbar');
const mobile = 'd-xl-none d-block';
const desktop = 'd-none d-xl-block';

const BitazzaPlatformMenuItem = (pros, context) => {
  const [open, setOpen] = React.useState(false);
  const container = React.useRef();
  const language = getlocalLanguage();

  const handleClickOutside = event => {
    if (container.current && !container.current.contains(event.target)) {
      setOpen(false);
    }
  };

  React.useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, []);

  const menuItem = [
    // <Link href="/" disabled={true} className={navbarClasses('link-sub-menu')}>
    //   {context.t('BITAZZA REWARDS')}
    // </Link>,
    // <BTZDivider className={clsx(desktop, navbarClasses('divider'))} />,
    <Link
      href="/exchange"
      router={true}
      className={navbarClasses('link-sub-menu')}>
      {context.t('Market')}
    </Link>,
    <BTZDivider className={clsx(desktop, navbarClasses('divider'))} />,
    <Link href="http://bitazzaelite.mainframe.vc">{context.t('OTC')}</Link>,
    <BTZDivider className={clsx(desktop, navbarClasses('divider'))} />,
    <Link
      href="/levels"
      router={true}
      className={navbarClasses('link-sub-menu')}>
      {context.t('BTZ')}
    </Link>,
    <BTZDivider className={clsx(desktop, navbarClasses('divider'))} />,
    <Link
      href={
        language === 'EN'
          ? 'https://partners.bitazza.com/en/foundation/'
          : 'https://partners.bitazza.com/th/foundation/'
      }
      className={navbarClasses('link-sub-menu')}>
      {context.t('BITAZZA FREEDOM FUND')}
    </Link>,
    <BTZDivider className={clsx(mobile, navbarClasses('divider'))} />
  ];

  return (
    <div ref={container}>
      <span
        role="button"
        className="bitazza-link cursor-pointer d-flex align-items-center"
        onClick={() => setOpen(!open)}>
        {context.t('BITAZZA PLATFORM')}
        {open ? (
          <i className="fa fa-caret-up ml-3" aria-hidden="true" />
        ) : (
          <i className="fa fa-caret-down ml-3" aria-hidden="true" />
        )}
      </span>
      {open && (
        <React.Fragment>
          <div className={mobile}>
            {menuItem.map((item, index) => (
              <React.Fragment key={index}>{item}</React.Fragment>
            ))}
          </div>
          <div
            className={clsx(
              desktop,
              navbarClasses('sub-list-menu-desktop-container')
            )}>
            <div className={navbarClasses('sub-list-menu-desktop')}>
              {menuItem.map((item, index) => (
                <React.Fragment key={index}>{item}</React.Fragment>
              ))}
            </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

BitazzaPlatformMenuItem.contextTypes = {
  t: PropTypes.func.isRequired
};

export default BitazzaPlatformMenuItem;

import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import config from '../../../config';
import BTZDivider from '../../common/BTZDivider/BTZDivider';
import { Link } from '../Btn';
import './Navbar.css';

const navbarClasses = getBEMClasses('navbar');
const mobile = 'd-xl-none d-block';
const desktop = 'd-none d-xl-block';

const DownloadAppMenu = (props, context) => {
  const [open, setOpen] = React.useState(false);
  const container = React.useRef();

  const handleClickOutside = event => {
    if (container.current && !container.current.contains(event.target)) {
      setOpen(false);
    }
  };

  React.useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, []);

  const menuItem = [
    <Link
      href={config.global.appsStore}
      disabled={!config.global.appsStore}
      className={navbarClasses('link-sub-menu')}>
      <i className="fa fa-apple fa-lg mr-3" aria-hidden="true" />
      {context.t('APP STORE')}
    </Link>,
    <BTZDivider className={clsx(desktop, navbarClasses('divider'))} />,
    <Link
      href={config.global.googlePlay}
      disabled={!config.global.googlePlay}
      className={navbarClasses('link-sub-menu')}>
      <i className="fa fa-android fa-lg mr-3" aria-hidden="true" />
      {context.t('GOOGLE PLAY')}
    </Link>,
    <BTZDivider className={clsx(mobile, navbarClasses('divider'))} />
  ];

  return (
    <div ref={container}>
      <span
        role="button"
        className="bitazza-link cursor-pointer d-flex align-items-center"
        onClick={() => setOpen(!open)}>
        <i
          className="fa fa-mobile fa-lg d-none d-xl-inline"
          aria-hidden="true"
        />
        <p className="d-xl-none d-inline m-0 ">{context.t('MOBILE APP')}</p>
        {open ? (
          <i className="fa fa-caret-up ml-3" aria-hidden="true" />
        ) : (
          <i className="fa fa-caret-down ml-3" aria-hidden="true" />
        )}
      </span>
      {open && (
        <React.Fragment>
          <div className={mobile}>
            {menuItem.map((item, index) => (
              <React.Fragment key={index}>{item}</React.Fragment>
            ))}
          </div>
          <div
            className={clsx(
              desktop,
              navbarClasses('sub-list-menu-desktop-container')
            )}>
            <div className={navbarClasses('sub-list-menu-desktop')}>
              {menuItem.map((item, index) => (
                <React.Fragment key={index}>{item}</React.Fragment>
              ))}
            </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

DownloadAppMenu.contextTypes = {
  t: PropTypes.func.isRequired
};

export default DownloadAppMenu;

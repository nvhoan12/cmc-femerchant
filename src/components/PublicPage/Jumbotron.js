import React from 'react';
import { pickBy } from 'lodash';
import clsx from 'clsx';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import './Jumbotron.css';

const jumbotronClass = getBEMClasses('jumbotron');

const Jumbotron = ({ backgrandImg, children, className }) => {
  const style = {
    backgroundImage: backgrandImg ? `url(${backgrandImg})` : null
  };
  return (
    <div style={pickBy(style)} className={clsx(jumbotronClass(), className)}>
      <div className="container">{children}</div>
    </div>
  );
};

export default Jumbotron;

import React from 'react';
import PropTypes from 'prop-types';
import { sortBy } from 'lodash';
import Slider from 'react-slick';
import YouTubePlayer from 'react-player/lib/players/YouTube';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { getlocalLanguage } from '../../helpers/localLanguage';
import config from '../../config';
import { AspectRatio16per9 } from './AspectRatio';
import { Link } from './Btn';
// import { Text } from './Text';

import './Announcement.css';

const classes = getBEMClasses('btz_announcement');

const settings = {
  dots: true,
  className: classes('slick'),
  infinite: true,
  fade: true,
  autoplay: true,
  autoplaySpeed: 2500,
  pauseOnHover: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
};

const Announcement = () => {
  const language = getlocalLanguage();
  const [slider, setSlider] = React.useState('');
  const [announces, setAnnounces] = React.useState([]);

  React.useEffect(
    () => {
      (async () => {
        const res = await fetch(
          config.services.getBanners(language.toLocaleLowerCase())
        );
        if (res.status === 200) {
          const now = new Date();
          const resJson = await res.json();
          const announcesRes = resJson.data.filter(item => {
            const start = new Date(item.start_date);
            const end = new Date(item.end_date);
            return start && end && start < now && now < end;
          });
          setAnnounces(sortBy(announcesRes, 'order_number'));
        }
      })();
    },
    [language]
  );

  const handleOnPlayYouTube = () => slider.slickPause();
  const handleOnEndedYouTube = () => slider.slickPlay();

  if (announces.length === 0) {
    return null;
  }

  return (
    <Slider {...settings} ref={setSlider}>
      {announces.map(item => (
        <div key={item.id} className={classes('banner-container')}>
          {item.type === 'video' && (
            <AspectRatio16per9>
              <YouTubePlayer
                url={item.url}
                onPlay={handleOnPlayYouTube}
                onEnded={handleOnEndedYouTube}
                width="100%"
                height="100%"
                controls
              />
            </AspectRatio16per9>
          )}
          {item.type === 'image' && (
            <Link className="d-block" href={item.link}>
              <img className={classes('img')} src={item.url} alt={item.title} />
              {/* <div className={classes('banner-text')}>
                <Text
                  noMargin
                  align="center"
                  black="block"
                  text={item.description}
                />
              </div> */}
            </Link>
          )}
        </div>
      ))}
    </Slider>
  );
};

Announcement.contextTypes = {
  t: PropTypes.func.isRequired
};

export default Announcement;

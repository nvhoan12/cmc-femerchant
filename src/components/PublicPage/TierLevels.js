import React from 'react';
import SVG from 'react-inlinesvg';

import { Text } from './Text';
import infoIcon from '../../images/icons/icon-info.svg';

import './TierLevel.css';

import { getValueFromRank, rankings } from './RankingTabs';

const ranks = Object.keys(rankings);

const TierLevels = ({ context }) => {
  return (
    <section className="tier-levels-section">
      <div className="tier-header-container">
        <div className="tier-header-empty" />
        <div className="tier-header">
          <div className="header-item bronze">BRONZE</div>
          <div className="header-item silver">SILVER</div>
          <div className="header-item gold">GOLD</div>
          <div className="header-item platinum">PLATINUM</div>
          <div className="header-item diamond">DIAMOND</div>
          <div className="header-item emerald">EMERALD</div>
          <div className="header-item early-adopter">EARLY ADOPTER</div>
        </div>
      </div>

      <div className="tier-levels">
        <div className="parent-wrapper">
          <div className="item ml-0 is-btz-holding">
            <div className="parent">
              <Text text="BTZ" fontSize="20" fontBold />
              <Text text="Holding Requirement" fontSize="18" />
            </div>
          </div>
          <div className="item ml-0 is-accumulation-rate">
            <div className="parent">
              <Text text="BTZ" fontSize="20" fontBold />
              <div>
                <Text text="Accumulation Rate" fontSize="18" display="inline" />
                <span
                  className="level-page__tooltip"
                  tooltip={context.t(
                    'Receive BTZ tokens (according to your tier) for every 1,000 baht traded'
                  )}
                  tooltip-position="top">
                  <SVG src={infoIcon} />
                </span>
              </div>
              <Text text="(3 Month Campaign)" fontSize="14" />
            </div>
          </div>
        </div>

        {ranks.map((rank, i) => {
          const { btz, percentText } = getValueFromRank(rankings[rank]);
          return (
            <div className="item" key={i}>
              <div className="child has-border-bottom">{btz}</div>
              <div className="child">{percentText}</div>
            </div>
          );
        })}
      </div>
    </section>
  );
};

export default TierLevels;

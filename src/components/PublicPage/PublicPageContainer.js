import React from 'react';
import Navbar from './Navbar';

const PublicPageContainer = ({ children }) => {
  return (
    <React.Fragment>
      <Navbar />
      {children}
    </React.Fragment>
  );
};

export default PublicPageContainer;

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { dateDiff } from '../../helpers/dateHelper';
import { getlocalLanguage } from '../../helpers/localLanguage';
import config from '../../config';
import './WaitlistPhase2.css';

const WaitlistPhase2 = (props, context) => {
  const [dateLeftText, setDateLeftText] = React.useState('');

  React.useEffect(() => {
    let daysText = 'd';
    let hoursText = 'h';
    let minutesText = 'm';
    let secondsText = 's';
    if (getlocalLanguage() === 'TH') {
      daysText = 'วัน';
      hoursText = 'ชม.';
      minutesText = 'น.';
      secondsText = 'วิ.';
    }
    const interval = setInterval(() => {
      const dateLeft = dateDiff(
        new Date(),
        new Date(config.global.bannerDateLeft)
      );
      if (dateLeft) {
        const text =
          `${dateLeft.days === 0 ? '' : `${dateLeft.days}${daysText} `}` +
          `${
            dateLeft.days === 0 && dateLeft.hours === 0
              ? ''
              : `${dateLeft.hours}${hoursText} `
          }` +
          `${
            dateLeft.days === 0 &&
            dateLeft.hours === 0 &&
            dateLeft.minutes === 0
              ? ''
              : `${dateLeft.minutes}${minutesText} `
          }` +
          `${dateLeft.seconds}${secondsText}`;
        setDateLeftText(text);
      } else {
        setDateLeftText('');
        clearInterval(interval);
      }
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div style={{ paddingTop: '70px' }}>
      <div id="waitlist-banner-container">
        <div className="container">
          <div className="row">
            <div className="col col-12 col-lg-10 offset-lg-1">
              <div id="waitlist-form-row">
                {/* footer banner */}
                {/* -------------------- */}
                <div id="waitlist-banner-message">
                  {context.t(
                    'Kickstart your Crypto journey with Bitcoin Giveaway, total value of THB 1.5M. Sign up now for chance to win every month of 2021!'
                  )}
                  {dateLeftText && (
                    <span className="date-left px-2">
                      {context.t('{dateLeft} left', {
                        dateLeft: dateLeftText
                      })}
                    </span>
                  )}
                </div>
                <Link id="Waitlist-submit" to="/signup">
                  {context.t('Sign Up')}
                </Link>
                {/* -------------------- */}

                {/* footer banner  maintenance*/}
                {/* -------------------- */}
                {/* <div id="waitlist-banner-message" style={{ color: 'darkred' }}>
                  {context.t(
                    'We are currently experiencing some technical issues with our deposit/withdrawal system and are working hard to rectify the problem. We apologize for the inconvenience and will update when wallets are fully functional. All funds are safe during this upgrade.'
                  )}
                </div> */}
                {/* -------------------- */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

WaitlistPhase2.contextTypes = {
  t: PropTypes.func.isRequired
};

export default WaitlistPhase2;

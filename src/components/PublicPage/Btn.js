import React from 'react';
import { NavLink } from 'react-router-dom';
import clsx from 'clsx';
import './Btn.css';

const buildDisplay = (style = {}, display) => {
  if (display) {
    if (display === 'block') style.display = 'block';
    if (display === 'inline') style.display = 'inline-block';
  }
  return style;
};

const Btn = ({ children, className, router, href, ...props }) => {
  if (router) {
    return (
      <NavLink to={href} className={clsx('bitazza-btn', className)} {...props}>
        {children}
      </NavLink>
    );
  }
  return (
    <button type="button" className={clsx('bitazza-btn', className)} {...props}>
      {children}
    </button>
  );
};

const BtnLink = ({ children, className, router, href, ...props }) => {
  if (router) {
    return (
      <NavLink
        to={href}
        className={clsx('bitazza-btn', 'bitazza-btn-link', className)}
        {...props}>
        {children}
      </NavLink>
    );
  }
  return (
    <button
      href={href}
      type="button"
      className={clsx('bitazza-btn', 'bitazza-btn-link', className)}
      {...props}>
      {children}
    </button>
  );
};

const BtnOutline = ({ children, className, router, href, ...props }) => {
  if (router) {
    return (
      <NavLink
        to={href}
        className={clsx('bitazza-btn', 'bitazza-btn-outline', className)}
        {...props}>
        {children}
      </NavLink>
    );
  }
  return (
    <button
      href={href}
      type="button"
      className={clsx('bitazza-btn', 'bitazza-btn-outline', className)}
      {...props}>
      {children}
    </button>
  );
};

const Select = ({ children, value, className, ...props }) => {
  return (
    <select
      value={value}
      className={clsx(
        'bitazza-btn',
        'bitazza-btn-outline',
        'bitazza-btn-select',
        className
      )}
      {...props}>
      {children}
    </select>
  );
};

const SelectOption = ({ children, value, ...props }) => {
  return (
    <option value={value} {...props}>
      {children}
    </option>
  );
};

const Link = ({
  children,
  className,
  router,
  href,
  display,
  disabled,
  ...props
}) => {
  let style = buildDisplay({}, display);
  if (router) {
    return (
      <NavLink
        to={href}
        style={style}
        activeClassName="active"
        className={clsx('bitazza-link', className, disabled && 'disabled')}
        {...props}>
        {children}
      </NavLink>
    );
  }
  return (
    <a
      role="button"
      href={href}
      style={style}
      className={clsx('bitazza-link', className, disabled && 'disabled')}
      {...props}>
      {children}
    </a>
  );
};

export { Btn, BtnLink, BtnOutline, Select, SelectOption, Link };

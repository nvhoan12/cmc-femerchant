import React from 'react';
import PropTypes from 'prop-types';
import logoHorizontal from '../../images/logo-horizontal.svg';
import facebookIcon from '../../images/social/ic-facebook.svg';
import linkedinIcon from '../../images/social/ic-linkedin.svg';
import twitterIcon from '../../images/social/ic-twitter.svg';
import lineIcon from '../../images/social/ic-line.png';
import { Link } from '../../components/PublicPage/Btn';
import { Text } from '../../components/PublicPage/Text';
import { getlocalLanguage } from '../../helpers/localLanguage';
import './Footer.css';

const Footer = (props, context) => {
  const languag = getlocalLanguage();
  return (
    <div className="container-fluid">
      <div className="row footer-container">
        <div className="col-md-8 px-lg-5 px-4 left-menu">
          <div className="row">
            <div className="col offset-xl-3">
              <div className="row">
                <div className="col-md-4">
                  <Text color="secondary" text={context.t('ABOUT')} />
                  <Link display="block" href="/about.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Who We Are')}
                    />
                  </Link>
                  <Link display="block" href="https://team.bitazza.com">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Our Team')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href="/Bitazza_Whitepaper.pdf"
                    target="_blank">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Whitepaper')}
                    />
                  </Link>
                </div>
                <div className="col-md-4">
                  <Text color="secondary" text={context.t('INFO')} />
                  <Link
                    display="block"
                    href="https://bitazzahelp.freshdesk.com/en/support/discussions/forums/44000000692"
                    target="_blank">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Announcements')}
                    />
                  </Link>
                  <Link display="block" href="/fees.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Fees & Limits')}
                    />
                  </Link>
                  <Link router display="block" href="/levels">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Bitazza Levels')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href="https://api-doc.bitazza.com/#marketmakerapi"
                    target="_blank">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('API Documentation')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href="https://bitazzahelp.freshdesk.com/en/support/solutions/44000812944"
                    target="_blank">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Coin Info')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href="https://bitazzahelp.freshdesk.com/en/support/discussions/topics/44001010153/page/last#post-44002263830"
                    target="_blank">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('BTZ Allocation')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href="https://bitazzahelp.freshdesk.com/th/support/home"
                    target="_blank">
                    <Text fontBold color="secondary" text={context.t('FAQ')} />
                  </Link>
                </div>
                <div className="col-md-4">
                  <Text color="secondary" text={context.t('SUPPORT')} />
                  <Link display="block" href="/tof.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('User Agreement')}
                    />
                  </Link>
                  <Link display="block" href="/privacy-policy.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Data Privacy Policy')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href="/conflict-of-interests-disclosure.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Conflict of Interests Disclosure')}
                    />
                  </Link>
                  <Link display="block" href="/best-execution-document.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('Best Execution Document')}
                    />
                  </Link>
                  <Link display="block" href="/aml-policy-statement.html">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t('AML Policy Statement')}
                    />
                  </Link>
                  <Link
                    display="block"
                    href={
                      languag === 'EN'
                        ? '/Draft_Fiancial Statement_Bittazza - 2020_unaudited version.pdf'
                        : '/Draft_งบการเงินบจก.บิทาซซ่า63_unaudited version.pdf'
                    }
                    target="_blank">
                    <Text
                      fontBold
                      color="secondary"
                      text={context.t(
                        'Draft Financial Statement 2020 (UnAudited Version)'
                      )}
                    />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 px-lg-5 px-4 right-menu">
          <Text
            fontSize={30}
            fontBold
            color="primary"
            text={
              <React.Fragment>
                <span>{context.t('keep up with')}</span>
                <img
                  className="footer-logo-bitazza"
                  src={logoHorizontal}
                  alt="logo"
                />
              </React.Fragment>
            }
          />
          <Link display="block" href="mailto:hello@bitazza.com">
            <Text color="publicPrimary" text="hello@bitazza.com" />
          </Link>
          <Link
            className="mr-3"
            href="https://www.facebook.com/bitazza/"
            rel="noopener noreferrer"
            target="_blank">
            <img alt="facebook" src={facebookIcon} />
          </Link>
          <Link
            className="mr-3"
            href="https://www.linkedin.com/company/bitazza/"
            rel="noopener noreferrer"
            target="_blank">
            <img alt="linkedin" src={linkedinIcon} />
          </Link>
          <Link
            className="mr-3"
            href="https://twitter.com/bitazzaofficial"
            rel="noopener noreferrer"
            target="_blank">
            <img alt="twitter" src={twitterIcon} />
          </Link>
          <Link
            className="mr-3"
            href="https://line.me/ti/p/%40bitazza"
            rel="noopener noreferrer"
            target="_blank">
            <img className="footer-logo-line" alt="line" src={lineIcon} />
          </Link>
        </div>
      </div>
    </div>
  );
};

Footer.contextTypes = {
  t: PropTypes.func.isRequired
};

export default Footer;

import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import SVG from 'react-inlinesvg';

import bronzeLogo from '../../images/levels/rank-bronze.png';
import goldLogo from '../../images/levels/rank-gold.png';
import silverLogo from '../../images/levels/rank-silver.png';
import platinumLogo from '../../images/levels/rank-platinum.png';
import diamondLogo from '../../images/levels/rank-diamond.png';
import emeraldLogo from '../../images/levels/rank-emerald.png';
import earlyAdopterLogo from '../../images/levels/badge-early-adopter.svg';
import iconCheck from '../../images/icons/icon-check-rounded.svg';
import iconCheckPrimary from '../../images/icons/icon-check-primary.svg';
import infoIcon from '../../images/icons/icon-info.svg';

import { Text } from './Text';

import 'react-tabs/style/react-tabs.css';
import './RankingTabs.css';

export const getValueFromRank = rank => {
  const percentage = Number(rank.accumulationRate * 100).toFixed(2);
  const feeDiscount = Number(rank.feeDiscount * 100).toFixed(0);
  const referralRate = rank.referralRate * 100;
  const btz = Number(rank.holdingRequirement).toLocaleString();

  const btzText = `${Number(rank.holdingRequirement).toLocaleString()} BTZ`;
  const percentText = `${percentage}%`;
  const feeDiscountText = `${feeDiscount}%`;
  const referralRateText = `${referralRate}%`;

  // case early adopter add + sign in front of value.
  const isEarly = rank.id === 1001;

  return {
    btz,
    referralRateText: isEarly ? `+${referralRateText}` : referralRateText,
    feeDiscountText: isEarly ? `+${feeDiscountText}` : feeDiscountText,
    btzText,
    percentText: isEarly ? `+${percentText}` : percentText
  };
};

const TabPanelContent = ({ rank, imageUrl }, context) => {
  const isVIPUrl = rank.isVIPSupport ? iconCheckPrimary : iconCheck;
  const isPersonalBrokerUrl = rank.personalBroker
    ? iconCheckPrimary
    : iconCheck;

  const {
    referralRateText,
    btzText,
    percentText,
    feeDiscountText
  } = getValueFromRank(rank);

  return (
    <div className="offset-lg-1 col-12 col-lg-10 text-center tab-content__inner">
      <div className="row">
        <div className="col col-12 col-lg-4">
          <Text text={btzText} fontSize="48" color="brandColor" fontBold />
          <Text
            text={context.t('BTZ HOLDING REQUIREMENT')}
            fontSize="24"
            fontBold={true}
          />
        </div>

        <div className="col col-12 col-lg-4">
          <img
            src={imageUrl}
            alt={rank.name}
            className={`rank-image ${rank.name.replace(' ', '').toLowerCase()}`}
          />
          <Text
            text={rank.name.toUpperCase()}
            fontSize="30"
            className={`${rank.name.toLowerCase()} mt-2 mb-0`}
            fontBold={true}
          />
          <Text
            color="lightdark"
            text={context.t('LEVEL')}
            fontSize="14"
            className="mb-0"
            fontBold={true}
          />
        </div>

        <div className="col col-12 col-lg-4">
          <Text text={percentText} fontSize="48" color="brandColor" fontBold />
          <div className="btz-accumulation-info">
            <div>
              <Text
                text={context.t('BTZ ACCUMULATION RATE')}
                fontSize="24"
                className="no-pb mb-0"
                display="inline"
                fontBold={true}
              />
              <span
                className="level-page__tooltip"
                tooltip={context.t(
                  'Receive BTZ tokens (according to your tier) for every 1,000 baht traded'
                )}
                tooltip-position="top">
                <SVG src={infoIcon} />
              </span>
            </div>

            <Text
              text={context.t('3 MONTH CAMPAIGN')}
              fontSize="24"
              color="secondary"
              className="no-pb"
            />
          </div>
        </div>

        <div className="col-12">
          <div className="tab-content__benefits">
            <div className="tab-content__benefits-box">
              <div className="text-center tab-content__benefits-box-wrap">
                <h2 className="tab-content__benefits-heading">
                  {context.t('KEY BENEFITS')}
                </h2>
              </div>

              <div className="row benefits-items">
                <div className="benefits-items__item">
                  <Text
                    text={feeDiscountText}
                    fontSize="60"
                    color="brandColor"
                    fontBold
                  />
                  <Text
                    text={context.t('BTZ Trading Fee Discount')}
                    align="center"
                    fontSize="24"
                    color="lightdark"
                  />
                </div>

                <div className="benefits-items__item">
                  <Text
                    text={referralRateText}
                    fontSize="60"
                    color="brandColor"
                    fontBold
                  />
                  <Text
                    text={context.t('Referral Income Share')}
                    align="center"
                    fontSize="24"
                    color="lightdark"
                  />
                </div>

                <div className="benefits-items__item">
                  <Text
                    text={rank.voteCount}
                    fontSize="60"
                    color="brandColor"
                    fontBold
                  />
                  <Text
                    text={context.t('Vote Count')}
                    align="center"
                    fontSize="24"
                    color="lightdark"
                  />
                </div>

                <div className="benefits-items__item">
                  <img className="icon" src={isVIPUrl} alt="icon vip support" />

                  <Text
                    text={context.t('Dedicated VIP Call Support')}
                    align="center"
                    fontSize="24"
                    color="lightdark"
                  />
                </div>

                <div className="benefits-items__item">
                  <img
                    className="icon"
                    src={isPersonalBrokerUrl}
                    alt="icon personal broker"
                  />
                  <Text
                    text={context.t('Personal Broker/ Voice Trades')}
                    align="center"
                    fontSize="22"
                    color="lightdark"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

TabPanelContent.contextTypes = {
  t: PropTypes.func.isRequired
};

const RankingTabs = () => (
  <Tabs className="bitazza-ranking-tabs">
    <TabList>
      <Tab className="react-tabs__tab bronze">
        <div className="tab-title">
          <div className="border-line" />
          <span>BRONZE</span>
        </div>
      </Tab>
      <Tab className="react-tabs__tab silver">
        <div className="tab-title">
          <div className="border-line" />
          <span>SILVER</span>
        </div>
      </Tab>
      <Tab className="react-tabs__tab gold">
        <div className="tab-title">
          <div className="border-line" />
          <span>GOLD</span>
        </div>
      </Tab>
      <Tab className="react-tabs__tab platinum">
        <div className="tab-title">
          <div className="border-line" />
          <span>PLATINUM</span>
        </div>
      </Tab>
      <Tab className="react-tabs__tab diamond">
        <div className="tab-title">
          <div className="border-line" />
          <span>DIAMOND</span>
        </div>
      </Tab>
      <Tab className="react-tabs__tab emerald">
        <div className="tab-title">
          <div className="border-line" />
          <span>EMERALD</span>
        </div>
      </Tab>
      <Tab className="react-tabs__tab early-adopter">
        <div className="tab-title">
          <div className="border-line" />
          <span>EARLY ADOPTER</span>
        </div>
      </Tab>
    </TabList>

    <TabPanel forceRender>
      <TabPanelContent rank={rankings.bronze} imageUrl={bronzeLogo} />
    </TabPanel>

    <TabPanel forceRender>
      <TabPanelContent rank={rankings.silver} imageUrl={silverLogo} />
    </TabPanel>

    <TabPanel forceRender>
      <TabPanelContent rank={rankings.gold} imageUrl={goldLogo} />
    </TabPanel>

    <TabPanel forceRender>
      <TabPanelContent rank={rankings.platinum} imageUrl={platinumLogo} />
    </TabPanel>

    <TabPanel forceRender>
      <TabPanelContent rank={rankings.diamond} imageUrl={diamondLogo} />
    </TabPanel>

    <TabPanel forceRender>
      <TabPanelContent rank={rankings.emerald} imageUrl={emeraldLogo} />
    </TabPanel>
    <TabPanel forceRender>
      <TabPanelContent
        rank={rankings.earlyAdopter}
        imageUrl={earlyAdopterLogo}
      />
    </TabPanel>
  </Tabs>
);

export default RankingTabs;

export const rankings = {
  bronze: {
    id: 1,
    name: 'Bronze',
    holdingRequirement: 0,
    accumulationRate: 0.0015,
    feeDiscount: 0.5,
    referralRate: 0.15,
    voteCount: 1,
    isVIPSupport: false,
    personalBroker: false
  },
  silver: {
    id: 2,
    name: 'Silver',
    holdingRequirement: 5000,
    accumulationRate: 0.0015,
    feeDiscount: 0.5,
    referralRate: 0.2,
    voteCount: 2,
    isVIPSupport: false,
    personalBroker: false
  },
  gold: {
    id: 3,
    name: 'Gold',
    holdingRequirement: 15000,
    accumulationRate: 0.002,
    feeDiscount: 0.55,
    referralRate: 0.25,
    voteCount: 5,
    isVIPSupport: false,
    personalBroker: false
  },
  platinum: {
    id: 4,
    name: 'Platinum',
    holdingRequirement: 50000,
    accumulationRate: 0.002,
    feeDiscount: 0.6,
    referralRate: 0.3,
    voteCount: 10,
    isVIPSupport: true,
    personalBroker: false
  },
  diamond: {
    id: 5,
    name: 'Diamond',
    holdingRequirement: 150000,
    accumulationRate: 0.0025,
    feeDiscount: 0.65,
    referralRate: 0.35,
    voteCount: 25,
    isVIPSupport: true,
    personalBroker: true
  },
  emerald: {
    id: 6,
    name: 'Emerald',
    holdingRequirement: 300000,
    accumulationRate: 0.0025,
    feeDiscount: 0.7,
    referralRate: 0.4,
    voteCount: 50,
    isVIPSupport: true,
    personalBroker: true
  },
  earlyAdopter: {
    id: 1001,
    name: 'Early Adopter',
    holdingRequirement: 500,
    accumulationRate: 0.0005,
    feeDiscount: 0.05,
    referralRate: 0,
    voteCount: 10,
    isVIPSupport: true,
    personalBroker: false
  }
};

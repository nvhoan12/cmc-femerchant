import React from 'react';
import clsx from 'clsx';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import './Divider.css';

const dividerClasses = getBEMClasses('bitazza-divider');

const Divider = ({ className, ...props }) => {
  return <div {...props} className={clsx(dividerClasses(), className)} />;
};

export default Divider;

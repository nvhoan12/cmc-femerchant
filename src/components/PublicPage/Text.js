import React from 'react';
import clsx from 'clsx';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import './Text.css';

const textClass = getBEMClasses('bitazza-text');

const buildColor = (style = {}, color) => {
  if (color) {
    if (color === 'publicPrimary') style.color = 'var(--public-page-primary)';
    if (color === 'primary') style.color = '#fff';
    if (color === 'secondary') style.color = 'rgba(255, 255, 255, 0.75)';
    if (color === 'black') style.color = '#454545';
    if (color === 'lightdark') style.color = '#c9c9c9';
    if (color === 'brandColor') style.color = '#7ec2b1';
  }
  return style;
};

const buildAlign = (style = {}, align) => {
  if (align) {
    if (align === 'left') style.textAlign = 'left';
    if (align === 'center') style.textAlign = 'center';
    if (align === 'right') style.textAlign = 'right';
  }
  return style;
};

const buildDisplay = (style = {}, display) => {
  if (display) {
    if (display === 'block') style.display = 'block';
    if (display === 'inline') style.display = 'inline-block';
  }
  return style;
};

const buildFontSize = (style = {}, fontSize) => {
  if (fontSize) {
    style.fontSize = `${fontSize}px`;
  }
  return style;
};

const buildFontBold = (style = {}, fontBold) => {
  if (fontBold) {
    style.fontFamily = '"db_airystd_bd", "Helvetica", sans-serif';
    style.fontWeight = 'bold';
  }
  return style;
};
const buildnoMargin = (style = {}, noMargin) => {
  if (noMargin) {
    style.margin = '0px';
  }
  return style;
};

const TextHeader = ({ text, className, color, align, display }) => {
  let style = buildColor({}, color);
  style = buildAlign(style, align);
  style = buildDisplay(style, display);
  return (
    <h1
      style={style}
      className={clsx(textClass(), textClass('header'), className)}>
      {text}
    </h1>
  );
};

const TextBlockHeader = ({ text, className, color, align, display }) => {
  let style = buildColor({}, color);
  style = buildAlign(style, align);
  style = buildDisplay(style, display);
  return (
    <h1
      style={style}
      className={clsx(textClass(), textClass('block-header'), className)}>
      {text}
    </h1>
  );
};

const TextBlockSubHeader = ({ text, className, color, align, display }) => {
  let style = buildColor({}, color);
  style = buildAlign(style, align);
  style = buildDisplay(style, display);
  return (
    <h1
      style={style}
      className={clsx(textClass(), textClass('block-sub-header'), className)}>
      {text}
    </h1>
  );
};

const TextLead = ({ text, className, color, align, display }) => {
  let style = buildColor({}, color);
  style = buildAlign(style, align);
  style = buildDisplay(style, display);
  return (
    <p
      style={style}
      className={clsx(textClass(), textClass('lead'), className)}>
      {text}
    </p>
  );
};

const Text = ({
  text,
  span,
  className,
  color,
  align,
  display,
  fontSize,
  fontBold,
  noMargin
}) => {
  let style = buildColor({}, color);
  style = buildAlign(style, align);
  style = buildDisplay(style, display);
  style = buildFontSize(style, fontSize);
  style = buildFontBold(style, fontBold);
  style = buildnoMargin(style, noMargin);

  if (span) {
    return (
      <span style={style} className={clsx(textClass(), className)}>
        {text}
      </span>
    );
  }
  return (
    <p style={style} className={clsx(textClass(), className)}>
      {text}
    </p>
  );
};

export { TextHeader, TextBlockHeader, TextBlockSubHeader, TextLead, Text };

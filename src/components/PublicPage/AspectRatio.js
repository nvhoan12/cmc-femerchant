import React from 'react';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import './AspectRatio.css';

const classes = getBEMClasses('aspect-ratio');

const AspectRatio16per9 = ({ children }) => {
  return (
    <div className={classes('16-9')}>
      <div className={classes('content')}>{children}</div>
    </div>
  );
};

export { AspectRatio16per9 };

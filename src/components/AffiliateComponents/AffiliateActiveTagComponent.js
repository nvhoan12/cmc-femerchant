import React from 'react';
import PropTypes from 'prop-types';
import APButton from '../common/APButton';

import './AffiliateComponent.css';
import copyToClipboard from 'apex-web/lib/helpers/clipboardHelper';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import AffiliateModalContainer from 'apex-web/lib/components/AffiliateComponents/AffiliateModalContainer';

const affiliateClasses = getBEMClasses('affiliate');

const AffiliateActiveTag = (props, context) => {
  const { route, affiliate, showSnack } = props;

  const affiliateTag = `${route}${affiliate.affiliateTag}`;

  const getModalButtonText = () => {
    return affiliate.affiliateTag ? context.t('Edit') : context.t('Create');
  };

  return (
    <React.Fragment>
      <section className={affiliateClasses('container-content')}>
        <p className={affiliateClasses('tag')}>{affiliate.affiliateTag}</p>
        {affiliate.affiliateTag && (
          <p id="affiliateTag" className={affiliateClasses('tag-description')}>
            {affiliateTag}
          </p>
        )}
        <div className={affiliateClasses('btn-container')}>
          {affiliate.affiliateTag && (
            <APButton
              customClass={affiliateClasses()}
              onClick={() => {
                showSnack(
                  context.t(
                    'The referral link has been copied to the clipboard.'
                  )
                );
                copyToClipboard(affiliateTag);
              }}>
              {context.t('Copy')}
            </APButton>
          )}
        </div>
      </section>
      <AffiliateModalContainer />
    </React.Fragment>
  );
};

AffiliateActiveTag.contextTypes = {
  t: PropTypes.func.isRequired
};

AffiliateActiveTag.propTypes = {
  route: PropTypes.string.isRequired,
  affiliate: PropTypes.object.isRequired,
  showSnack: PropTypes.func.isRequired,
  openAffiliateModal: PropTypes.func.isRequired
};

export default AffiliateActiveTag;

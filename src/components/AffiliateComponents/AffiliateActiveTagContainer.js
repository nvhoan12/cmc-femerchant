import { connect } from 'react-redux';
import AffiliateActiveTagComponent from './AffiliateActiveTagComponent';
import { openAffiliateModal } from 'apex-web/lib/redux/actions/affiliateActions';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';

const mapStateToProps = (state, ownProps) => {
  const route = ownProps.route;
  return {
    route,
    affiliate: state.affiliate
  };
};

const mapDispatchToProps = dispatch => ({
  openAffiliateModal: () => dispatch(openAffiliateModal()),
  showSnack: text =>
    dispatch(showSnack({ id: 'affiliateCopyTage', text, type: 'success' }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AffiliateActiveTagComponent);

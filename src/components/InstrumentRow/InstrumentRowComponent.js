import React from 'react';
import InstrumentSelectorContainer from '../InstrumentSelector/InstrumentSelectorContainer';
import InstrumentTableContainer from 'apex-web/lib/components/InstrumentTable/InstrumentTableContainer';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

import './InstrumentRowComponent.css';

const baseClasses = getBEMClasses('instrument-row');

const InstrumentRowComponent = () => {
  return (
    <div className={baseClasses()}>
      <InstrumentSelectorContainer />
      <InstrumentTableContainer />
    </div>
  );
};

export default InstrumentRowComponent;

import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { BtnLink, Link } from '../PublicPage/Btn';
import LinkLINE from '../LinkLINE/LinkLINE';
import Divider from '../common/BTZDivider/BTZDivider';
import config from '../../config';
import './MobileApp.css';

const classes = getBEMClasses('mobile-app');

const MobileApp = (props, context) => {
  const [open, setOpen] = React.useState(false);

  return (
    <React.Fragment>
      <BtnLink className={classes('btn-mobile')} onClick={() => setOpen(true)}>
        <i className="fa fa-mobile fa-lg" aria-hidden="true" />
      </BtnLink>
      <Modal isOpen={open} title="Mobile" close={() => setOpen(false)}>
        <div className={classes('container')}>
          <Link
            disabled={!config.global.appsStore}
            href={config.global.appsStore}
            className={classes('item')}>
            <i className="fa fa-apple fa-lg mr-3" aria-hidden="true" />
            {context.t('APP STORE')}
          </Link>
          <Link
            disabled={!config.global.googlePlay}
            href={config.global.googlePlay}
            className={classes('item')}>
            <i className="fa fa-android fa-lg mr-3" aria-hidden="true" />
            {context.t('GOOGLE PLAY')}
          </Link>
          <Divider className="m-0" />
          <LinkLINE
            className={classes('item-line')}
            text={context.t('BITAZZAN COMMUNITY')}
          />
        </div>
      </Modal>
    </React.Fragment>
  );
};

MobileApp.contextTypes = {
  t: PropTypes.func.isRequired
};

export default MobileApp;

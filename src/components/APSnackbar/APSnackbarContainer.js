import { connect } from 'react-redux';
import { dismissSnackByUniqueId } from 'apex-web/lib/redux/actions/snackbarActions';
import APSnackbar from '../common/APSnackbar/APSnackbar';

const mapStateToProps = state => ({
  snacks: state.snackbar.snacks
});

const mapDispatchToProps = dispatch => ({
  dismissSnackByUniqueId: payload => dispatch(dismissSnackByUniqueId(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(APSnackbar);

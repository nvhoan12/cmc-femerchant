import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import APSnackbar from '../common/APSnackbar/APSnackbar';
import APSnackbarContainer from './APSnackbarContainer';
import * as actions from '../../redux/actions/snackbarActions';
const mockStore = configureStore();

jest.mock('../common/APSnackbar/APSnackbar', () => {
  return () => {
    return <div className="component">Content</div>;
  };
});

jest.mock('../../redux/actions/snackbarActions', () => {
  return {
    dismissSnackByUniqueId: jest.fn(payload => ({ type: 'ACTION', payload }))
  };
});

describe('APSnackbarContainer', () => {
  const state = {
    snackbar: {
      snacks: [{ id: '1' }]
    }
  };
  let store, container;

  beforeEach(() => {
    store = mockStore(state);
    container = mount(<APSnackbarContainer store={store} />);
  });

  it('Should render APSnackbar', () => {
    expect(container.find('.component').length).toBe(1);
  });

  it('Should pass snacks to component', () => {
    expect(container.childAt(0).prop('snacks')).toBe(state.snackbar.snacks);
  });

  it('Should pass dismissSnackByUniqueId action to component', () => {
    expect(container.childAt(0).prop('dismissSnackByUniqueId')).toBeDefined();
  });

  it('Should pass dismissSnackByUniqueId action to component', () => {
    container
      .find(APSnackbar)
      .props()
      .dismissSnackByUniqueId();
    expect(actions.dismissSnackByUniqueId).toBeCalled();
  });
});

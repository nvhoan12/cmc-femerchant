export const formatGender = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('Male')
    return 'Male';
  }
  if (payload === '2') {
    // context.t('Female')
    return 'Female';
  }
  if (payload === '3') {
    // context.t('Others')
    return 'Others';
  }
  if (payload === '4') {
    // context.t('Not Specify')
    return 'Not Specify';
  }
};

export const formatMarriage = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('Single')
    return 'Single';
  }
  if (payload === '2') {
    // context.t('Married')
    return 'Married';
  }
  if (payload === '3') {
    // context.t('Divorced')
    return 'Divorced';
  }
  if (payload === '4') {
    // context.t('Widowed')
    return 'Widowed';
  }
  if (payload === '5') {
    // context.t('Not Specify')
    return 'Not Specify';
  }
};

export const formatEducation = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('None')
    return 'None';
  }
  if (payload === '2') {
    // context.t('High School')
    return 'High School';
  }
  if (payload === '3') {
    // context.t('College')
    return 'College';
  }
  if (payload === '4') {
    // context.t('University')
    return 'University';
  }
  if (payload === '5') {
    // context.t('Post Graduate')
    return 'Post Graduate';
  }
  if (payload === '6') {
    // context.t('Not Specify')
    return 'Not Specify';
  }
};

export const formatOccupation = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('Civil Servant')
    return 'Civil Servant';
  }
  if (payload === '2') {
    // context.t('Entrepreneur')
    return 'Entrepreneur';
  }
  if (payload === '3') {
    // context.t('Employee')
    return 'Employee';
  }
  if (payload === '4') {
    // context.t('Freelancer')
    return 'Freelancer';
  }
  if (payload === '5') {
    // context.t('Student')
    return 'Student';
  }
  if (payload === '6') {
    // context.t('Others')
    return 'Others';
  }
};

export const formatWorkBusiness = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('Government agencies/state enterprise')
    return 'Government agencies/state enterprise';
  }
  if (payload === '2') {
    // context.t('Advertising')
    return 'Advertising';
  }
  if (payload === '3') {
    // context.t('Design')
    return 'Design';
  }
  if (payload === '4') {
    // context.t('Education')
    return 'Education';
  }
  if (payload === '5') {
    // context.t('Engineering')
    return 'Engineering';
  }
  if (payload === '6') {
    // context.t('Electronics')
    return 'Electronics';
  }
  if (payload === '7') {
    // context.t('Energy')
    return 'Energy';
  }
  if (payload === '8') {
    // context.t('Entertainment')
    return 'Entertainment';
  }
  if (payload === '9') {
    // context.t('Financial services')
    return 'Financial services';
  }
  if (payload === '10') {
    // context.t('Food & beverage')
    return 'Food & beverage';
  }
  if (payload === '11') {
    // context.t('Logistics')
    return 'Logistics';
  }
  if (payload === '12') {
    // context.t('Health & beauty care')
    return 'Health & beauty care';
  }
  if (payload === '13') {
    // context.t('Hospitality & tourism')
    return 'Hospitality & tourism';
  }
  if (payload === '14') {
    // context.t('Information technology')
    return 'Information technology';
  }
  if (payload === '15') {
    // context.t('Jewellery/gems/watches')
    return 'Jewellery/gems/watches';
  }
  if (payload === '16') {
    // context.t('Legal services')
    return 'Legal services';
  }
  if (payload === '17') {
    // context.t('Manufacturing')
    return 'Manufacturing';
  }
  if (payload === '18') {
    // context.t('Media/publishing/printing')
    return 'Media/publishing/printing';
  }
  if (payload === '19') {
    // context.t('Medical/pharmaceutical')
    return 'Medical/pharmaceutical';
  }
  if (payload === '20') {
    // context.t('Mining & drilling')
    return 'Mining & drilling';
  }
  if (payload === '21') {
    // context.t('Real estate')
    return 'Real estate';
  }
  if (payload === '22') {
    // context.t('Retail & wholesale')
    return 'Retail & wholesale';
  }
  if (payload === '23') {
    // context.t('Security')
    return 'Security';
  }
  if (payload === '24') {
    // context.t('Telecommunications')
    return 'Telecommunications';
  }
  if (payload === '25') {
    // context.t('Trading & distribution')
    return 'Trading & distribution';
  }
  if (payload === '26') {
    // context.t('Transportation')
    return 'Transportation';
  }
  if (payload === '27') {
    // context.t('Utilities')
    return 'Utilities';
  }
  if (payload === '28') {
    // context.t('Antiques & Auction')
    return 'Antiques & Auction';
  }
  if (payload === '29') {
    // context.t('Currency Exchange & Transfer')
    return 'Currency Exchange & Transfer';
  }
  if (payload === '30') {
    // context.t('Casino & Gambling Business')
    return 'Casino & Gambling Business';
  }
  if (payload === '31') {
    // context.t('Entertainment facility under the law governing')
    return 'Entertainment facility under the law governing';
  }
  if (payload === '32') {
    // context.t('Arms & Ammunition')
    return 'Arms & Ammunition';
  }
  if (payload === '33') {
    // context.t('International Employment Agency')
    return 'International Employment Agency';
  }
  if (payload === '34') {
    // context.t('Travel Agency')
    return 'Travel Agency';
  }
  if (payload === '99') {
    // context.t('Others')
    return 'Others';
  }
};

export const formatSourceFund = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('Employment')
    return 'Employment';
  }
  if (payload === '2') {
    // context.t('Entrepreneur')
    return 'Entrepreneur';
  }
  if (payload === '3') {
    // context.t('Heritage')
    return 'Heritage';
  }
  if (payload === '4') {
    // context.t('Investment')
    return 'Investment';
  }
  if (payload === '5') {
    // context.t('Loan')
    return 'Loan';
  }
  if (payload === '6') {
    // context.t('Property')
    return 'Property';
  }
  if (payload === '7') {
    // context.t('Savings')
    return 'Savings';
  }
};

export const formatYesOrNo = payload => {
  if (payload === '0' || payload === 0) {
    // context.t('No')
    return 'No';
  }
  if (payload === '1' || payload === 1) {
    // context.t('Yes')
    return 'Yes';
  }
};

export const formatAccept = payload => {
  if (payload === '0' || payload === 0) {
    // context.t('No Accept')
    return 'No Accept';
  }
  if (payload === '1' || payload === 1) {
    // context.t('Accept')
    return 'Accept';
  }
};

export const formatMonthlyIncome = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('0 - 20,000 THB')
    return '0 - 20,000 THB';
  }
  if (payload === '2') {
    // context.t('20,001 - 50,000 THB')
    return '20,001 - 50,000 THB';
  }
  if (payload === '3') {
    // context.t('50,001-100,000 THB')
    return '50,001-100,000 THB';
  }
  if (payload === '4') {
    // context.t('100,001 - 200,000 THB')
    return '100,001 - 200,000 THB';
  }
  if (payload === '5') {
    // context.t('200,001 - 500,000 THB')
    return '200,001 - 500,000 THB';
  }
  if (payload === '6') {
    // context.t('500,001 - 1,000,000 THB')
    return '500,001 - 1,000,000 THB';
  }
  if (payload === '7') {
    // context.t('1,000,001 - 2,000,000 THB')
    return '1,000,001 - 2,000,000 THB';
  }
  if (payload === '8') {
    // context.t('more than 2,000,000 THB')
    return 'more than 2,000,000 THB';
  }
};

export const formatTotalAsset = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('less than 1,000,000 THB')
    return 'less than 1,000,000 THB';
  }
  if (payload === '2') {
    // context.t('1,000,001 - 5,000,000 THB')
    return '1,000,001 - 5,000,000 THB';
  }
  if (payload === '3') {
    // context.t('5,000,001 - 10,000,000 THB')
    return '5,000,001 - 10,000,000 THB';
  }
  if (payload === '4') {
    // context.t('10,000,001 - 20,000,000 THB')
    return '10,000,001 - 20,000,000 THB';
  }
  if (payload === '5') {
    // context.t('20,000,001 - 50,000,000 THB')
    return '20,000,001 - 50,000,000 THB';
  }
  if (payload === '6') {
    // context.t('more than 50,000,000 THB')
    return 'more than 50,000,000 THB';
  }
};

export const formatHaveCrytoInvest = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('Never')
    return 'Never';
  }
  if (payload === '2') {
    // context.t('Once')
    return 'Once';
  }
  if (payload === '3') {
    // context.t('A few times')
    return 'A few times';
  }
  if (payload === '4') {
    // context.t('Many times')
    return 'Many times';
  }
};

export const formatUnderstandLevel = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('No Understanding')
    return 'No Understanding';
  }
  if (payload === '2') {
    // context.t('Little Understanding')
    return 'Little Understanding';
  }
  if (payload === '3') {
    // context.t('Some Understanding')
    return 'Some Understanding';
  }
  if (payload === '4') {
    // context.t('Clear understanding')
    return 'Clear understanding';
  }
};

export const formatHaveCryptoCrash = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('No')
    return 'No';
  }
  if (payload === '2') {
    // context.t('Not sure')
    return 'Not sure';
  }
  if (payload === '3') {
    // context.t('Maybe')
    return 'Maybe';
  }
  if (payload === '4') {
    // context.t('Yes')
    return 'Yes';
  }
};

export const formatLegalRisk = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('No')
    return 'No';
  }
  if (payload === '2') {
    // context.t('Not likely')
    return 'Not likely';
  }
  if (payload === '3') {
    // context.t('Most likely')
    return 'Most likely';
  }
  if (payload === '4') {
    // context.t('Yes')
    return 'Yes';
  }
};

export const formatPrimaryGoal = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('To generate secure and constant but low earnings')
    return 'To generate secure and constant but low earnings';
  }
  if (payload === '2') {
    // context.t('To generate constant earnings with some risks of losing some investment principle')
    return 'To generate constant earnings with some risks of losing some investment principle';
  }
  if (payload === '3') {
    // context.t('To generate high earnings with high risks of losing some investment principle')
    return 'To generate high earnings with high risks of losing some investment principle';
  }
  if (payload === '4') {
    // context.t('To generate exorbitant earnings with high risks of losing most of investment principle')
    return 'To generate exorbitant earnings with high risks of losing most of investment principle';
  }
};

export const formatRiskLevel = payload => {
  if (payload === '0') {
    return null;
  }
  if (payload === '1') {
    // context.t('5% or less loss')
    return '5% or less loss';
  }
  if (payload === '2') {
    // context.t('5-10% loss')
    return '5-10% loss';
  }
  if (payload === '3') {
    // context.t('10-20% loss')
    return '10-20% loss';
  }
  if (payload === '4') {
    // context.t('20% or more loss')
    return '20% or more loss';
  }
};

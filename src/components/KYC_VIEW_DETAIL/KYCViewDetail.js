import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { getlocalLanguage } from '../../helpers/localLanguage';
import config from '../../config';
import * as helper from './helper';
import './KYCViewDetail.css';

const classes = getBEMClasses('kyc-view-detail');

const SubTitle = ({ title }) => {
  return <div className={classes('sub-title')}>{title}</div>;
};

const ColumnFeild = ({ primary, secondary }) => {
  return (
    <div className={classes('column-feild')}>
      <span className={classes('text-primary')}>{primary}</span>
      <span className={classes('text-secondary')}>{secondary}</span>
    </div>
  );
};

const RowFeild = ({ primary, secondary }) => {
  return (
    <div className={classes('row-feild')}>
      <span className={clsx(classes('text-primary'), 'mb-4')}>{primary}</span>
      <span className={classes('text-secondary')}>{secondary}</span>
    </div>
  );
};

const KYCViewDetail = ({ onSidePaneOpen, userId, token }, context) => {
  const [kycData, setKYCDate] = React.useState({});
  const language = getlocalLanguage();

  React.useEffect(() => {
    onSidePaneOpen({ title: context.t('KYC Details') });
  }, []);

  React.useEffect(() => {
    (async () => {
      const res = await fetch(config.services.getKYCInfo(userId), {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'X-BTZ-TOKEN': token
        }
      });
      const jsonRes = await res.json();
      setKYCDate(JSON.parse(jsonRes));
    })();
  }, []);

  return (
    <div className={classes()}>
      <SubTitle title={context.t('Personal Infomation')} />
      {language === 'TH' ? (
        <React.Fragment>
          <ColumnFeild
            primary={context.t('First Name')}
            secondary={kycData.first_name_th}
          />
          <ColumnFeild
            primary={context.t('Last Name')}
            secondary={kycData.last_name_th}
          />
          <ColumnFeild
            primary="ชื่อ (ภาษาอังกฤษ)"
            secondary={kycData.first_name}
          />
          <ColumnFeild
            primary="นามสกุล (ภาษาอังกฤษ)"
            secondary={kycData.last_name}
          />
          <ColumnFeild primary="เลขประจำตัวประชาชน" secondary={kycData.id_no} />
          <ColumnFeild
            primary="โค้ดเลเซอร์หลังบัตรประชาชน"
            secondary={kycData.laser_code}
          />
          <ColumnFeild
            primary="วันเกิด วัน/เดือน/ปี"
            secondary={kycData.birth_date}
          />
        </React.Fragment>
      ) : (
        <React.Fragment>
          <ColumnFeild
            primary={context.t('Nationality')}
            secondary={kycData.nationality}
          />
          <ColumnFeild
            primary={context.t('First Name')}
            secondary={kycData.first_name}
          />
          <ColumnFeild
            primary={context.t('Last Name')}
            secondary={kycData.last_name}
          />
        </React.Fragment>
      )}
      <ColumnFeild
        primary={context.t('Gender')}
        secondary={context.t(helper.formatGender(kycData.gender))}
      />
      <ColumnFeild
        primary={context.t('Bill Address')}
        secondary={kycData.billing_address}
      />
      {language === 'TH' && (
        <React.Fragment>
          <ColumnFeild primary="เขต" secondary={kycData.district} />
          <ColumnFeild primary="แขวง" secondary={kycData.sub_area} />
        </React.Fragment>
      )}
      <ColumnFeild primary={context.t('State')} secondary={''} />
      <ColumnFeild primary={context.t('City')} secondary={kycData.city} />
      <ColumnFeild
        primary={context.t('Zip Code')}
        secondary={kycData.zip_code}
      />
      <ColumnFeild
        primary={context.t('Contact Number')}
        secondary={kycData.contact_number}
      />
      <ColumnFeild
        primary={context.t('Marriage Status')}
        secondary={context.t(helper.formatMarriage(kycData.marriage_status))}
      />
      <ColumnFeild
        primary={context.t('Name of Spouse')}
        secondary={kycData.spouse_name}
      />
      <SubTitle title={context.t('Education and Experience Background')} />
      <ColumnFeild
        primary={context.t('Highest Education')}
        secondary={context.t(helper.formatEducation(kycData.education))}
      />
      <ColumnFeild
        primary={context.t('Occupation')}
        secondary={context.t(helper.formatOccupation(kycData.occupation))}
      />
      <ColumnFeild
        primary={context.t('If Others, please specify')}
        secondary={kycData.occupation_specify}
      />
      <ColumnFeild
        primary={context.t('Work Address')}
        secondary={kycData.work_address}
      />
      <ColumnFeild
        primary={context.t('Nature of Business')}
        secondary={context.t(helper.formatWorkBusiness(kycData.work_business))}
      />
      <ColumnFeild
        primary={context.t('If Others, please specify')}
        secondary={kycData.work_business_specify}
      />
      <SubTitle title={context.t('Other Disclosure')} />
      <ColumnFeild
        primary={context.t('Source of Fund')}
        secondary={context.t(helper.formatSourceFund(kycData.source_fund))}
      />
      <RowFeild
        primary={context.t(
          'Do you hold any political position in any jurisdiction?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.is_jurisdict))}
      />
      <RowFeild
        primary={context.t(
          'Have you been allegedly involve in any tax evasion case in any jurisdiction?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.is_tax_evasion))}
      />
      <RowFeild
        primary={context.t(
          'Have you been involved in any offense regarding laws on anti-money laundering and counter-terrorism and proliferation of weapons of mass destruction financing (AML/CFT)?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.is_aml_cft))}
      />
      <RowFeild
        primary={context.t('Are you the true beneficiary of this account?')}
        secondary={context.t(helper.formatYesOrNo(kycData.is_beneficial))}
      />
      <RowFeild
        primary={context.t(
          'If are not the true beneficiary of this account please provide the name of beneficiary and telephone number.'
        )}
        secondary={kycData.beneficial_name}
      />
      <SubTitle title={context.t('Financial Information')} />
      <ColumnFeild
        primary={context.t('Monthly Income')}
        secondary={context.t(
          helper.formatMonthlyIncome(kycData.monthly_income)
        )}
      />
      <ColumnFeild
        primary={context.t('Net Total Asset')}
        secondary={context.t(helper.formatTotalAsset(kycData.total_asset))}
      />
      <SubTitle title={context.t('Suitability Test')} />
      <RowFeild
        primary={context.t(
          'Have you ever made a cryptocurrency or token digital based investment before?'
        )}
        secondary={context.t(
          helper.formatHaveCrytoInvest(kycData.have_cryto_invest)
        )}
      />
      <RowFeild
        primary={context.t(
          'Do you feel like you have a clear understandings of the risks involved with a cryptocurrency?'
        )}
        secondary={context.t(
          helper.formatUnderstandLevel(kycData.understand_level)
        )}
      />
      <RowFeild
        primary={context.t(
          'Do you anticipate a cryptocurrency or token digital "crash" or "bubble burst"?'
        )}
        secondary={context.t(
          helper.formatHaveCryptoCrash(kycData.have_crypto_crash)
        )}
      />
      <RowFeild
        primary={context.t(
          'Are you willing to take on legal risk in pursuing your investment in cryptocurrency or token digital?'
        )}
        secondary={context.t(helper.formatLegalRisk(kycData.legal_risk))}
      />
      <RowFeild
        primary={context.t(
          'When I invest money in cryptocurrency or digital token, my primary goal is to?'
        )}
        secondary={context.t(helper.formatPrimaryGoal(kycData.primary_goal))}
      />
      <RowFeild
        primary={context.t(
          'The degree to which the value of an investment moves up and down is called volatility (risk). More volatile investments generally offer greater growth potential in the long term than less volatile investments, but they may produce greater losses. With how much volatility are you comfortable?'
        )}
        secondary={context.t(helper.formatRiskLevel(kycData.risk_level))}
      />
      <RowFeild
        primary={context.t(
          'I hereby declare that the information provided is true and accurate. I shall also immediately inform and update the Company if there is any change to the information provided.'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.declare_true))}
      />
      <RowFeild
        primary={context.t('Are you a U.S. Citizen?')}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca1))}
      />
      <RowFeild
        primary={context.t(
          'Are you a holder of any U.S. Permanent Resident Card (e.g. Green Card)?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca2))}
      />
      <RowFeild
        primary={context.t('Are you a U.S. resident for U.S. tax purposes?')}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca3))}
      />
      <RowFeild
        primary={context.t(
          'Were you born in the U.S. (or U.S. territory) but have surrendered U.S. citizenship?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca4))}
      />
      <RowFeild
        primary={context.t(
          'Do you have a current U.S. residence address or U.S. mailing address for the account opened with Bitazza?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca5))}
      />
      <RowFeild
        primary={context.t(
          'Do you have U.S. telephone number for contacting you or another person in relation to the account opened with or through or maintained with Bitazza?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca6))}
      />
      <RowFeild
        primary={context.t(
          'Do you have standing instructions to transfer funds from the account opened with or through or held with the Receiver to an account maintained in the U.S.?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca7))}
      />
      <RowFeild
        primary={context.t(
          'Do you have a power of attorney or signatory authority for the account opened with or through or held with the Receiver granted to a person with U.S. address?'
        )}
        secondary={context.t(helper.formatYesOrNo(kycData.fatca8))}
      />
      <RowFeild
        primary={context.t(
          'You confirm that the above information is true, correct, accurate and complete.'
        )}
        secondary={context.t(helper.formatAccept(kycData.fatca9))}
      />
      <RowFeild
        primary={context.t(
          'You acknowledge and agree that if you are a U.S. Person but the information provided on this form or Form W-9 is false, inaccurate or incomplete, Bitazza shall be entitled to terminate, at its sole discretion, the entire business relationship with you or part of such relationship as Bitazza may deem appropriate.'
        )}
        secondary={context.t(helper.formatAccept(kycData.fatca10))}
      />
      <RowFeild
        primary={context.t(
          'You agree to notify and provide relevant documents to Bitazza within 30 days after any change in circumstances that causes the information provided in this form to be incorrect.:'
        )}
        secondary={context.t(helper.formatAccept(kycData.fatca11))}
      />
      <RowFeild
        primary={context.t(
          'You acknowledge and agree that failure to comply with item 8 above, or provision of any false, inaccurate or incomplete information as to your status, shall entitle Bitazza to terminate, at its sole discretion, the entire business relationship with you or part of such relationship as Bitazza may deem appropriate.'
        )}
        secondary={context.t(helper.formatAccept(kycData.fatca12))}
      />
      <RowFeild
        primary={context.t('FATCA Terms and Conditions')}
        secondary={context.t('Agree')}
      />
      <RowFeild
        primary={context.t('Terms and Conditions')}
        secondary={context.t('Agree')}
      />
      <SubTitle
        title={context.t(
          "If you're willing to update your KYC information, please contact support@bitazza.com(opens in new tab)."
        )}
      />
    </div>
  );
};

KYCViewDetail.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const { token, userId } = state.auth;
  return { token, userId };
};

export default connect(
  mapStateToProps,
  null
)(KYCViewDetail);

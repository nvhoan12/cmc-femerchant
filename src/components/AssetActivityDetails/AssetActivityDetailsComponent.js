import React from 'react';
import AssetActivityDetailsHeaderContainer from 'apex-web/lib/components/AssetActivityDetails/AssetActivityDetailsHeader';
import MarginOrderHistoryExtended from 'apex-web/lib/components/OrderHistoryComponents/MarginOrderHistoryExtended';
import OrderHistoryComponent from '../OrderHistoryComponents';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';

import './AssetActivityDetailsComponent.css';

const classes = getBEMClasses('asset-activity-details-component');

class AssetActivityDetailsComponent extends React.Component {
  render() {
    const { isMarginActive } = this.props;
    const orderHistoryConfig = {
      filterMode: 'selectedProduct',
      usePagination: false
    };

    return (
      <div className={classes()}>
        <AssetActivityDetailsHeaderContainer />
        {isMarginActive ? (
          <MarginOrderHistoryExtended config={orderHistoryConfig} />
        ) : (
          <OrderHistoryComponent config={orderHistoryConfig} />
        )}
      </div>
    );
  }
}

export default AssetActivityDetailsComponent;

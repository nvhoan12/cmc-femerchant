import { connect } from 'react-redux';
import AssetActivityDetailsComponent from './AssetActivityDetailsComponent';
import { isMarginActiveSelector } from 'apex-web/lib/redux/selectors/marginSelectors';

const mapStateToProps = state => {
  return {
    isMarginActive: isMarginActiveSelector(state)
  };
};

export default connect(mapStateToProps)(AssetActivityDetailsComponent);

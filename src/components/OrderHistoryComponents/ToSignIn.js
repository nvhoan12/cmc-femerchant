import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import BTZButton from '../common/BTZButton/BTZButton';
import './ToSignIn.css';

const classes = getBEMClasses('to-sign-in');

const ToSignInOrSignUp = (props, context) => {
  const history = useHistory();
  const handleToSignIn = () => history.push('/login');
  const handleToSignup = () => history.push('/signup');

  return (
    <div className={classes()}>
      <BTZButton
        className={classes('btn')}
        onClick={handleToSignup}
        variant="outline">
        {context.t('Sign Up Now!')}
      </BTZButton>
      <BTZButton
        className={classes('btn')}
        onClick={handleToSignIn}
        variant="link">
        {context.t('Sign In to Trade')}
      </BTZButton>
    </div>
  );
};

ToSignInOrSignUp.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ToSignInOrSignUp;

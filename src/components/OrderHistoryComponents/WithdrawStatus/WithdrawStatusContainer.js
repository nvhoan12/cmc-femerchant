import { connect } from 'react-redux';
import WithdrawStatusComponent from './WithdrawStatusComponent';
import { filteredWithdrawalsByProductSelector } from 'apex-web/lib/redux/selectors/depositWithdrawalSelectors';
import { instrumentSelectorInstruments } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import { decimalsByProductId } from 'apex-web/lib/redux/selectors/productsSelectors';
import { cancelWithdrawTicket } from 'apex-web/lib/redux/actions/withdrawActions';

const mapStateToProps = (state, { config }) => {
  const withdrawTickets = filteredWithdrawalsByProductSelector(state, config);
  const instruments = instrumentSelectorInstruments(state);
  const decimalPlaces = decimalsByProductId(state);
  const { activeRequests } = state.apexCore.orderHistory;
  const fetching = activeRequests > 0;

  return {
    data: withdrawTickets,
    instruments,
    fetching,
    decimalPlaces
  };
};

const mapDispatchToProps = {
  cancelWithdrawTicket
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WithdrawStatusComponent);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Tab from 'apex-web/lib/components/common/Tab/Tab';
import OpenOrdersContainer from 'apex-web/lib/components/OrderHistoryComponents/OpenOrders';
import FilledOrdersContainer from 'apex-web/lib/components/OrderHistoryComponents/FilledOrders';
import InactiveOrdersContainer from 'apex-web/lib/components/OrderHistoryComponents/InactiveOrders';
import TradeReportsContainer from 'apex-web/lib/components/OrderHistoryComponents/TradeReports';
import DepositsStatusContainer from 'apex-web/lib/components/OrderHistoryComponents/DepositsStatus';
import WithdrawStatusContainer from './WithdrawStatus';
import HeaderWithFilterContainer from 'apex-web/lib/components/OrderHistoryComponents/HeaderWithFilter';

import './OrderHistoryComponent.css';
import './OrderHistoryTables.css';
import { selectedInstrumentSelector } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';

class OrderHistoryComponent extends Component {
  _getPanes() {
    const { config, selectedInstrument } = this.props;

    const panes = [
      {
        menuItem: OpenOrdersContainer.title,
        render: () => (
          <OpenOrdersContainer
            config={config}
            selectedInstrument={selectedInstrument}
          />
        )
      },
      {
        menuItem: FilledOrdersContainer.title,
        render: () => (
          <FilledOrdersContainer
            config={config}
            selectedInstrument={selectedInstrument}
          />
        )
      },
      {
        menuItem: InactiveOrdersContainer.title,
        render: () => (
          <InactiveOrdersContainer
            config={config}
            selectedInstrument={selectedInstrument}
          />
        )
      },
      {
        menuItem: TradeReportsContainer.title,
        render: () => (
          <TradeReportsContainer
            config={config}
            selectedInstrument={selectedInstrument}
          />
        )
      },
      {
        menuItem: DepositsStatusContainer.title,
        render: () => (
          <DepositsStatusContainer
            config={config}
            selectedInstrument={selectedInstrument}
          />
        )
      },
      {
        menuItem: WithdrawStatusContainer.title,
        render: () => (
          <WithdrawStatusContainer
            config={config}
            selectedInstrument={selectedInstrument}
          />
        )
      }
    ];

    return panes;
  }

  render() {
    return (
      <React.Fragment>
        {this.props.useHeaderWithFilter && <HeaderWithFilterContainer />}
        <Tab
          panes={this._getPanes()}
          classModifiers={this.props.classModifiers}
          customClass="order-history"
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, { config }) => ({
  selectedInstrument:
    config.filterMode === 'selectedInstrument'
      ? selectedInstrumentSelector(state)
      : undefined
});

export default connect(mapStateToProps)(OrderHistoryComponent);

import React from 'react';
import { connect } from 'react-redux';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import OrderHistoryComponent from './OrderHistoryComponent';
import ToSignIn from './ToSignIn';
import './OrderHistory.css';

const classes = getBEMClasses('order-history-wrapper');

const RecentTrades = ({ isAuthenticated, ...props }) => {
  return (
    <div className={classes()}>
      <OrderHistoryComponent {...props} />
      {!isAuthenticated && <ToSignIn />}
    </div>
  );
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  null
)(RecentTrades);

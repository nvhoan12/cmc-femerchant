import React from 'react';

import RetailPriceChartContainer from 'apex-web/lib/components/RetailPriceChart/RetailPriceChartContainer';
import InstrumentTableContainer from 'apex-web/lib/components/InstrumentTable/InstrumentTableContainer';
import InstrumentSelectorContainer from '../InstrumentSelector/InstrumentSelectorContainer';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import './MarketOverviewComponent.css';

const marketOverviewClasses = getBEMClasses('market-overview');

const MarketOverviewComponent = () => {
  return (
    <div className={marketOverviewClasses('body')}>
      <div className={marketOverviewClasses('chart-component-container')}>
        <div className={marketOverviewClasses('instrument-selector')}>
          <InstrumentSelectorContainer customClass="market-overview-popup" />
        </div>
        <RetailPriceChartContainer
          customClass={marketOverviewClasses('retail-price-chart-container')}
        />
      </div>
      <div className={marketOverviewClasses('market-overview-container')}>
        <InstrumentTableContainer customClass="market-overview-instrument" />
      </div>
    </div>
  );
};

export default MarketOverviewComponent;

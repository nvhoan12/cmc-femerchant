import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { buyValue } from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';
import BTZIcon from '../common/BTZIcon/BTZIcon';

import './ConfirmOrderModalComponent.css';

const bemClasses = getBEMClasses('confirm-order-modal');

const ConfirmOrderModalComponent = (props, context) => {
  const {
    formObj,
    isOpen,
    close,
    confirmReport,
    feeSymbol,
    selectedInstrument: {
      QuantityIncrement,
      PriceIncrement,
      Product1Symbol,
      Product2Symbol
    },
    marketPrice,
    price,
    orderTotal,
    isMarginActive
  } = props;
  const isBuySide = formObj.values.side === buyValue;
  const floatMarketPrice = parseFloat(marketPrice.replace(',', ''));

  const items = [
    {
      label: context.t('Order Type:'),
      text: formObj.values.orderType
    },
    {
      label: context.t('Quantity'),
      text: `${formatNumberToLocale(
        formObj.values.quantity,
        convertIncrementToIntDecimalPlaces(QuantityIncrement)
      )} ${Product1Symbol}`
    },
    {
      label: context.t('Price'),
      text: `${formatNumberToLocale(
        price,
        convertIncrementToIntDecimalPlaces(PriceIncrement)
      )} ${Product2Symbol}`
    },
    {
      label: context.t('Fee'),
      text: `${formatNumberToLocale(
        formObj.values.fee.OrderFee,
        feeSymbol
      )} ${feeSymbol}`
    },
    {
      label: context.t('Value'),
      text: `${formatNumberToLocale(
        orderTotal,
        convertIncrementToIntDecimalPlaces(PriceIncrement)
      )} ${Product2Symbol}`
    }
  ];

  if (isMarginActive) {
    items.splice(1, 0, {
      label: context.t('Leverage'),
      text: formObj.values.fee.Leverage
    });
  }

  return (
    <Modal
      isOpen={isOpen}
      title={
        isMarginActive
          ? context.t('Confirm Margin Order')
          : context.t('Confirm Order')
      }
      close={close}
      footer={{
        buttonText: isBuySide
          ? context.t('Confirm Buy Order')
          : context.t('Confirm Sell Order'),
        buttonStyle: isBuySide ? 'additive' : 'subtractive',
        onClick: () => confirmReport(formObj.values)
      }}
      customClass={bemClasses(
        formObj.values.side === buyValue ? 'bought' : 'sold'
      )}>
      <div className={bemClasses()}>
        {items.map(item => {
          return (
            <APLabelWithText
              key={item.label}
              label={item.label}
              text={item.text}
              customClass={bemClasses()}
            />
          );
        })}
      </div>
      {formObj.values.orderType === 'Limit Order' &&
        isBuySide &&
        parseFloat(price) > floatMarketPrice && (
          <div className={bemClasses('disclaimer')}>
            <p>
              {context.t(
                'Your Limit Price was set greter than the current market price. To confirm, the system will auto-match your order to the current market price.'
              )}
            </p>
          </div>
        )}
      {formObj.values.orderType === 'Limit Order' &&
        !isBuySide &&
        parseFloat(price) < floatMarketPrice && (
          <div className={bemClasses('disclaimer')}>
            <p>
              {context.t(
                'Your Limit Price was set lower than the current market price. To confirm, the system will auto-match your order to the current market price.'
              )}
            </p>
          </div>
        )}
      {[Product1Symbol, Product2Symbol].includes('YFI') && (
        <div className={bemClasses('disclaimer')}>
          <p>
            <BTZIcon
              iconName="fa-exclamation-triangle"
              className={bemClasses('disclaimer_icon')}
            />
            {context.t(
              'Investment in yearn.Finance (YFI) may contain high risks and market volatility. Please study all the provided information in relation to the list of smart contract audits performed on the yearn finance protocol before making an investment.'
            )}
          </p>
          <p>
            {context.t('Link:')}
            <a
              href="https://github.com/iearn-finance/audits"
              className="ml-2"
              target="_blank"
              rel="noopener noreferrer">
              https://github.com/iearn-finance/audits
            </a>
          </p>
        </div>
      )}
    </Modal>
  );
};

ConfirmOrderModalComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ConfirmOrderModalComponent;

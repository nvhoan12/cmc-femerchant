/* eslint-disable import/first */
import React, { Component } from 'react';
import packageJson from '../package.json';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router-dom';
import { initApex, setGateway } from 'apex-web/lib/apex';
import {
  connectionOpened,
  connectionClosed
} from 'apex-web/lib/redux/actions/wsConnectionActions';
import { getQueryParam } from './helpers/queryParamsHelper';
import { onInit } from 'apex-web/lib/redux/actions/lifeCycleActions';
import StandAloneLayout from './layouts/StandAloneLayout/StandAloneLayout';
import './styles/theme/dark.css';
import ProblemLoggingInPage from './pages/ProblemLoggingInPage';
import DepositConfirmation from 'apex-web/lib/components/DepositConfirmation/DepositConfirmationModalContainer';
import Snackbar from './components/APSnackbar/APSnackbarContainer';
import ResetPassword from './components/ResetPasswordForm/ResetPasswordFormContainer';
import TwoFactor from 'apex-web/lib/components/TwoFactorAuthForm/TwoFactorAuthFormContainer';
import SidePane from './components/common/SidePane/SidePaneContainer';
import { emailLinkRoutes } from 'apex-web/lib/helpers/emailLinksHelper';
import EmailLinks from './layouts/EmailLinksLayout.js';
import InteriorPage from './pages/InteriorPage';
import StandaloneLoginFormContainer from './pages/Login/StandaloneLoginFormContainer';
import StandaloneSignupFormContainer from './pages/Signup/StandaloneSignupFromContainer';
import LandingPage from './pages/LandingPage';
import { getGateway } from 'apex-web/lib/helpers/wsHelper';
import redirectIfAuthorized from 'apex-web/lib/hocs/redirectIfAuthorized';
import SimpleExchangePage from './pages/Exchange/SimpleExchangePage';
import config from './config';
import Require2FA from 'apex-web/lib/components/2FA/TwoFactorAuthContainer';
import Enable2FA from 'apex-web/lib/components/EnableTwoFactorAuth/StandaloneEnable2FAModalContainer';
import LevelPage from './pages/LevelPage';
import StandaloneTVChartPage from './pages/StandaloneTVChartPage';
import { getUserRole } from './helpers/userConfigHelper';

class App extends Component {
  constructor(props) {
    super(props);
    const gateway = getGateway();
    if (gateway) {
      setGateway(gateway);
      initApex(this.props.connectionOpened, this.props.connectionClosed);
    }

    const AffiliateTag = getQueryParam('aff');
    if (AffiliateTag) {
      window.localStorage.setItem('affiliateTag', AffiliateTag);
    }
  }

  render() {
    const { pathname } = this.props.location;
    const role = getUserRole(this.props.userConfig);
    return (
      <React.Fragment>
        <div className="App fluid">
          <Switch>
            <Route
              path="/version"
              render={() => (
                <div>
                  <p>v3implementation-template: {packageJson.version}</p>
                  <p>apex-web: {packageJson.dependencies['apex-web']}</p>
                </div>
              )}
            />
            <Route
              path="/standalone-tv-chart"
              component={StandaloneTVChartPage}
            />
            <Route exact path={'/'} component={LandingPage} />
            {emailLinkRoutes(pathname, EmailLinks)}
            <Route
              path={'/login'}
              component={redirectIfAuthorized(
                StandaloneLoginFormContainer,
                role.defaultPath
              )}
            />
            <Route path={'/signup'} component={StandaloneSignupFormContainer} />
            <Route
              path={'/problem-logging-in'}
              component={ProblemLoggingInPage}
            />
            <Route path={'/reset-password'} component={ResetPassword} />
            <Route
              path={'/twofactorauth'}
              render={() => (
                <StandAloneLayout>
                  <TwoFactor />
                </StandAloneLayout>
              )}
            />
            <Route path={'/enabletwofactorauth'} render={() => <Enable2FA />} />
            <Route path="/levels" component={LevelPage} />
            {config.SimpleExchange.active && (
              <Route
                path={config.SimpleExchange.route}
                component={SimpleExchangePage}
              />
            )}
            {/* passing props.location prevents Blocked Re-renders: https://reacttraining.com/react-router/web/guides/dealing-with-update-blocking */}
            <InteriorPage location={this.props.location} />
          </Switch>
        </div>
        <Snackbar />
        <SidePane
          options={{
            ReportBlockTrade: {
              useNewForm: true
            }
          }}
        />
        <DepositConfirmation />
        <Require2FA />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  userConfig: state.user.userConfig
});

const mapDispatchToProps = {
  onInit,
  connectionOpened,
  connectionClosed
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);

import VerifyEmailContainer from '../components/VerifyEmail/VerifyEmailContainer';
import ResetPasswordFormContainer from 'apex-web/lib/components/ResetPasswordForm/ResetPasswordFormContainer';
import ConfirmWithdrawContainer from '../components/ConfirmWithdraw/ConfirmWithdrawContainer';
import DepositConfirmationModalContainer from 'apex-web/lib/components/DepositConfirmation/DepositConfirmationModalContainer';
import StandaloneLoginContainer from 'apex-web/lib/components/Login/StandaloneLoginFormContainer';
import VerifyAddressContainer from 'apex-web/lib/components/VerifyAddress/VerifyAddressContainer';
import VerifyDeviceContainer from 'apex-web/lib/components/VerifyDevice/VerifyDeviceContainer';

export const STAND_ALONE_COMPONENTS = {
  VerifyEmailContainer: VerifyEmailContainer,
  ResetPasswordNoAuthFormContainer: ResetPasswordFormContainer,
  ConfirmWithdrawContainer: ConfirmWithdrawContainer,
  DepositConfirmationContainer: DepositConfirmationModalContainer,
  StandaloneLoginContainer: StandaloneLoginContainer,
  VerifyAddressContainer,
  VerifyDeviceContainer
};

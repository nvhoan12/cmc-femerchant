export const sellValue = '1';
export const buyValue = '0';

export const orderTypes = {
  //context.t('Market')
  market: {
    orderType: '1',
    displayName: 'Market Order'
  },
  //context.t('Limit')
  limit: {
    orderType: '2',
    displayName: 'Limit Order'
  },
  //context.t('Stop Market')
  stopMarket: {
    orderType: '3',
    displayName: 'Stop Market'
  },
  //context.t('Stop Limit')
  stopLimit: {
    orderType: '4',
    displayName: 'Stop Limit'
  },
  //context.t('Trailing Stop Market')
  trailingStopMarket: {
    orderType: '5',
    displayName: 'Trailing Stop Market'
  },
  //context.t('Trailing Stop Limit')
  trailingStopLimit: {
    orderType: '6',
    displayName: 'Trailing Stop Limit'
  },
  //context.t('Report Block Trade')
  reportBlockTrade: {
    orderType: '7',
    displayName: 'Report Block Trade'
  },
  //context.t('Fill Or Kill')
  fillOrKill: {
    orderType: '2',
    displayName: 'Fill Or Kill'
  },
  //context.t('Immediate or Cancel')
  immediateOrCancel: {
    orderType: '2',
    displayName: 'Immediate or Cancel'
  },
  //context.t('Reserve Order')
  reserveOrder: {
    orderType: '2',
    displayName: 'Reserve Order'
  }
};

export const advancedOrderFormTypes = {
  market: orderTypes.market,
  limit: orderTypes.limit,
  stopMarket: orderTypes.stopMarket,
  // stopLimit: orderTypes.stopLimit,
  trailingStopMarket: orderTypes.trailingStopMarket,
  // trailingStopLimit: orderTypes.trailingStopLimit,
  fillOrKill: orderTypes.fillOrKill,
  immediateOrCancel: orderTypes.immediateOrCancel
  // reserveOrder: orderTypes.reserveOrder
};

/**
 * context.t('Place Sell Order')
 * context.t('Place Buy Order')
 */
export const submitButtonTextOptions = {
  [sellValue]: 'Place Sell Order',
  [buyValue]: 'Place Buy Order'
};

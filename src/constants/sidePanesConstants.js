export const SIDE_PANES = {
  REPORTS: 'Reports',
  API_KEYS: 'ApiKeys',
  DEPOSITS: 'Deposits',
  WITHDRAWS: 'Withdraws',
  ADVANCED_ORDER: 'AdvancedOrder',
  KYC: 'KYCForm',
  KYC_IM: 'KYC_IMForm',
  KYC_IMLegacy: 'KYC_IMLegacyForm',
  KYC_Jumio: 'KYC_JumioForm',
  KYC_Manual: 'KYC_ManualForm',
  REPORT_BLOCK_TRADE: 'ReportBlockTrade',
  RETAIL_SEND_RECEIVE: 'SendReceive',
  RETAIL_FIAT: 'RetailFiat',
  WHITE_LIST_ADDRESS: 'WhiteListAddress',
  KYC_VIEW_DETAIL: 'KYCViewDetail'
};

import { APEX } from 'alphapoint';

const apex = { connection: {}, gateway: '' };

export const setGateway = gateway => {
  apex.gateway = gateway;
};

export const initApex = (onopen, onclose) => {
  const gateway = apex.gateway;
  apex.connection = new APEX(gateway, {
    onopen,
    onclose
  });
  apex.connection.standardCallback = () => {};
};

export const getApex = (onopen = () => {}, onclose = () => {}) => {
  if (!apex.connection.ws) {
    let server;
    const cookie = localStorage.getItem('tradingServer');
    if (apex.gateway) server = apex.gateway;
    if (cookie) server = cookie;

    if (!server) {
      // context.t("Gateway not selected")
      throw new Error('Gateway not selected');
    }

    setGateway(server);
    initApex(onopen, onclose);
  }

  return apex;
};

export default apex;

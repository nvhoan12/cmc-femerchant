import querystring from 'query-string';
import config from '../../config';

export const getExchangeRates = () => {
  const url = config.services.exchangeRates;

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': localStorage.getItem('token')
    }
  });
};

export const getBlocktradeHistory = () => {
  const url = config.services.blocktradeHistory;

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': localStorage.getItem('token')
    }
  });
};

/**
 * Example Payload for Request Quote
 * ?baseCurrency=BTC&quoteCurrency=USD&side=Buy&baseQty=0.4
 * @param {Object} payload
 */
export const requestQuote = payload => {
  const query = querystring.stringify(payload);

  const url = config.services.requestQuote + '?' + query;

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': localStorage.getItem('token')
    }
  });
};

export const executeQuote = async payload => {
  const url = config.services.requestQuote;

  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': localStorage.getItem('token')
    },
    body: JSON.stringify(payload)
  });
};

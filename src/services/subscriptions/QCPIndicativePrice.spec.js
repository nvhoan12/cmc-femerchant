const prices = [
  {
    Base: 'SPO',
    Quote: 'USDT',
    Buy: 0.048,
    Sell: 0.0473
  },
  {
    Base: 'BTC',
    Quote: 'TUSD',
    Buy: 9772.59,
    Sell: 9627.09
  },
  {
    Base: 'LTC',
    Quote: 'USDT',
    Buy: 72.42,
    Sell: 71.34
  },
  {
    Base: 'ETH',
    Quote: 'TUSD',
    Buy: 263.3,
    Sell: 259.38
  },
  {
    Base: 'XRP',
    Quote: 'USD',
    Buy: 0.28996,
    Sell: 0.28564
  },
  {
    Base: 'SPO',
    Quote: 'USD',
    Buy: 0.0504,
    Sell: 0.0496
  },
  {
    Base: 'EOS',
    Quote: 'PAX',
    Buy: 4.1375,
    Sell: 4.0759
  },
  {
    Base: 'BNB',
    Quote: 'PAX',
    Buy: 22.5862,
    Sell: 22.2499
  },
  {
    Base: 'EOS',
    Quote: 'USDC',
    Buy: 4.1364,
    Sell: 4.0748
  },
  {
    Base: 'BNB',
    Quote: 'USD',
    Buy: 23.5952,
    Sell: 23.2439
  },
  {
    Base: 'NEO',
    Quote: 'USDC',
    Buy: 14.241,
    Sell: 14.029
  },
  {
    Base: 'SPO',
    Quote: 'TUSD',
    Buy: 0.0481,
    Sell: 0.0474
  },
  {
    Base: 'SPO',
    Quote: 'PAX',
    Buy: 0.0481,
    Sell: 0.0473
  },
  {
    Base: 'USDT',
    Quote: 'USD',
    Buy: 1.05743,
    Sell: 1.04169
  },
  {
    Base: 'EOS',
    Quote: 'USD',
    Buy: 4.3224,
    Sell: 4.258
  },
  {
    Base: 'ETH',
    Quote: 'SGD',
    Buy: 393.62,
    Sell: 387.76
  },
  {
    Base: 'TRX',
    Quote: 'USDT',
    Buy: 0.02025,
    Sell: 0.01995
  },
  {
    Base: 'XRP',
    Quote: 'BTC',
    Buy: 0.00002862,
    Sell: 0.0000282
  },
  {
    Base: 'LTC',
    Quote: 'PAX',
    Buy: 72.51,
    Sell: 71.43
  },
  {
    Base: 'USDC',
    Quote: 'PAX',
    Buy: 1.00774,
    Sell: 0.99274
  },
  {
    Base: 'ETH',
    Quote: 'PAX',
    Buy: 263.21,
    Sell: 259.3
  },
  {
    Base: 'BNB',
    Quote: 'BTC',
    Buy: 0.0023292,
    Sell: 0.0022945
  },
  {
    Base: 'PAX',
    Quote: 'USDT',
    Buy: 1.00621,
    Sell: 0.99123
  },
  {
    Base: 'ETH',
    Quote: 'USD',
    Buy: 281.19,
    Sell: 277.01
  },
  {
    Base: 'TUSD',
    Quote: 'PAX',
    Buy: 1.00721,
    Sell: 0.99221
  },
  {
    Base: 'XLM',
    Quote: 'BTC',
    Buy: 0.00000743,
    Sell: 0.00000732
  },
  {
    Base: 'TRX',
    Quote: 'TUSD',
    Buy: 0.02028,
    Sell: 0.01998
  },
  {
    Base: 'USDC',
    Quote: 'USD',
    Buy: 1.05634,
    Sell: 1.04061
  },
  {
    Base: 'LTC',
    Quote: 'USDC',
    Buy: 72.49,
    Sell: 71.41
  },
  {
    Base: 'ADA',
    Quote: 'BTC',
    Buy: 0.00000613,
    Sell: 0.00000604
  },
  {
    Base: 'BNB',
    Quote: 'USDC',
    Buy: 22.58022,
    Sell: 22.24404
  },
  {
    Base: 'PAX',
    Quote: 'USDC',
    Buy: 1.00726,
    Sell: 0.99226
  },
  {
    Base: 'BTC',
    Quote: 'SGD',
    Buy: 14286.85,
    Sell: 14074.14
  },
  {
    Base: 'ADA',
    Quote: 'USD',
    Buy: 0.06211,
    Sell: 0.06118
  },
  {
    Base: 'NEO',
    Quote: 'USD',
    Buy: 14.881,
    Sell: 14.659
  },
  {
    Base: 'LTC',
    Quote: 'BTC',
    Buy: 0.007478,
    Sell: 0.007366
  },
  {
    Base: 'ADA',
    Quote: 'TUSD',
    Buy: 0.05947,
    Sell: 0.05858
  },
  {
    Base: 'LTC',
    Quote: 'TUSD',
    Buy: 72.53,
    Sell: 71.45
  },
  {
    Base: 'NEO',
    Quote: 'PAX',
    Buy: 14.244,
    Sell: 14.032
  },
  {
    Base: 'XRP',
    Quote: 'PAX',
    Buy: 0.27756,
    Sell: 0.27343
  },
  {
    Base: 'EOS',
    Quote: 'USDT',
    Buy: 4.1323,
    Sell: 4.0708
  },
  {
    Base: 'BTC',
    Quote: 'USDT',
    Buy: 9757.5,
    Sell: 9612.23
  },
  {
    Base: 'BTC',
    Quote: 'USDC',
    Buy: 9767.24,
    Sell: 9621.82
  },
  {
    Base: 'ETH',
    Quote: 'USDT',
    Buy: 262.85,
    Sell: 258.94
  },
  {
    Base: 'PAX',
    Quote: 'USD',
    Buy: 1.05608,
    Sell: 1.04036
  },
  {
    Base: 'XLM',
    Quote: 'PAX',
    Buy: 0.07205,
    Sell: 0.07098
  },
  {
    Base: 'NEO',
    Quote: 'USDT',
    Buy: 14.226,
    Sell: 14.015
  },
  {
    Base: 'BNB',
    Quote: 'USDT',
    Buy: 22.5577,
    Sell: 22.2219
  },
  {
    Base: 'ADA',
    Quote: 'USDC',
    Buy: 0.05943,
    Sell: 0.05855
  },
  {
    Base: 'XLM',
    Quote: 'USDC',
    Buy: 0.07203,
    Sell: 0.07096
  },
  {
    Base: 'XRP',
    Quote: 'USDC',
    Buy: 0.27749,
    Sell: 0.27336
  },
  {
    Base: 'PAX',
    Quote: 'TUSD',
    Buy: 1.00779,
    Sell: 0.99279
  },
  {
    Base: 'USDC',
    Quote: 'USDT',
    Buy: 1.00646,
    Sell: 0.99147
  },
  {
    Base: 'XLM',
    Quote: 'USDT',
    Buy: 0.07196,
    Sell: 0.07089
  },
  {
    Base: 'BTC',
    Quote: 'PAX',
    Buy: 9769.81,
    Sell: 9624.35
  },
  {
    Base: 'SPO',
    Quote: 'USDC',
    Buy: 0.048,
    Sell: 0.0473
  },
  {
    Base: 'TRX',
    Quote: 'USDC',
    Buy: 0.02027,
    Sell: 0.01997
  },
  {
    Base: 'TUSD',
    Quote: 'USDC',
    Buy: 1.00696,
    Sell: 0.99197
  },
  {
    Base: 'NEO',
    Quote: 'BTC',
    Buy: 0.001469,
    Sell: 0.001447
  },
  {
    Base: 'NEO',
    Quote: 'TUSD',
    Buy: 14.248,
    Sell: 14.036
  },
  {
    Base: 'TUSD',
    Quote: 'USD',
    Buy: 1.05578,
    Sell: 1.04006
  },
  {
    Base: 'USDC',
    Quote: 'TUSD',
    Buy: 1.00804,
    Sell: 0.99303
  },
  {
    Base: 'ADA',
    Quote: 'USDT',
    Buy: 0.05937,
    Sell: 0.05849
  },
  {
    Base: 'XRP',
    Quote: 'USDT',
    Buy: 0.27721,
    Sell: 0.27308
  },
  {
    Base: 'BTC',
    Quote: 'USD',
    Buy: 10206.29,
    Sell: 10054.33
  },
  {
    Base: 'XLM',
    Quote: 'USD',
    Buy: 0.07527,
    Sell: 0.07415
  },
  {
    Base: 'ETH',
    Quote: 'USDC',
    Buy: 263.18,
    Sell: 259.26
  },
  {
    Base: 'BNB',
    Quote: 'TUSD',
    Buy: 22.5926,
    Sell: 22.2562
  },
  {
    Base: 'TRX',
    Quote: 'PAX',
    Buy: 0.02027,
    Sell: 0.01997
  },
  {
    Base: 'XLM',
    Quote: 'TUSD',
    Buy: 0.07207,
    Sell: 0.071
  },
  {
    Base: 'EOS',
    Quote: 'TUSD',
    Buy: 4.1387,
    Sell: 4.0771
  },
  {
    Base: 'ETH',
    Quote: 'BTC',
    Buy: 0.02714,
    Sell: 0.026736
  },
  {
    Base: 'XRP',
    Quote: 'TUSD',
    Buy: 0.27764,
    Sell: 0.27351
  },
  {
    Base: 'TRX',
    Quote: 'BTC',
    Buy: 0.00000209,
    Sell: 0.00000206
  },
  {
    Base: 'EOS',
    Quote: 'BTC',
    Buy: 0.0004267,
    Sell: 0.0004203
  },
  {
    Base: 'ADA',
    Quote: 'PAX',
    Buy: 0.05945,
    Sell: 0.05856
  },
  {
    Base: 'TUSD',
    Quote: 'USDT',
    Buy: 1.00592,
    Sell: 0.99094
  },
  {
    Base: 'TRX',
    Quote: 'USD',
    Buy: 0.02118,
    Sell: 0.02086
  },
  {
    Base: 'LTC',
    Quote: 'USD',
    Buy: 75.75,
    Sell: 74.62
  }
];

const filterByBaseName = name => ({ Base }) => Base === name;

const LTC = prices.filter(filterByBaseName('ETH'));
const BTC = prices.filter(filterByBaseName('BTC'));
console.log('LTC', LTC);
console.log('bTC', BTC);

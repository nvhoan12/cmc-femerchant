import { APEX } from 'alphapoint';

import BaseSubscriptionService from 'apex-web/lib/services/subscriptions/BaseSubscriptionService';

const QCP_GATEWAY = 'wss://qcpotc.com/indicPrice';

class QCPIndicativePriceService extends BaseSubscriptionService {
  constructor() {
    super();

    this.wss = new APEX(QCP_GATEWAY, {
      onopen: () => this.subscription
    });
  }

  subscribe(callback) {
    this.unsubscribe();
    this.subscription = this.wss.ws.subscribe(response => callback(response));
  }

  stop() {}
}

export default new QCPIndicativePriceService();

import BaseSubscriptionService from 'apex-web/lib/services/subscriptions/BaseSubscriptionService';
import { Trade } from 'alphapoint';
import apex from 'apex-web/lib/apex';
import store from 'apex-web/lib/redux/store';

let queue = [];
let interval;

class TradesSubscriptionService extends BaseSubscriptionService {
  initSubscribtion(instrumentId, history = 40) {
    return new Promise((resolve, reject) => {
      apex.connection
        .RPCPromise('SubscribeTrades', {
          instrumentId,
          IncludeLastCount: history, // history
          OMSId: 1
        })
        .then(res => JSON.parse(res.o))
        .then(res => {
          if (res.result === false) {
            return reject(res);
          }

          return resolve(
            res.filter(i => i[1] === instrumentId).map(i => new Trade(i))
          );
        })
        .catch(e => console.error(e));
    });
  }

  subscribe(callback, instrumentId) {
    this.unsubscribe();
    queue = [];
    window.clearInterval(interval);

    this.subscription = apex.connection.ws
      .filter(x => x.n === 'TradeDataUpdateEvent')
      .subscribe(response => {
        const data = JSON.parse(response.o);
        queue = data.concat(queue);
      });

    interval = window.setInterval(() => {
      if (queue.length) {
        const data = queue
          .filter(i => i[1] === instrumentId)
          .map(x => new Trade(x));

        if (data.length) {
          callback(data);
        }
        queue = [];
      }
    }, 1000);
  }

  stop() {
    const { selectedInstrumentId } = store.getState().apexCore.instrument;
    this.stopOne(selectedInstrumentId);
  }

  stopOne(instrumentId) {
    window.clearInterval(interval);
    queue = [];
    apex.connection.unsubscribeTrades(instrumentId);
  }
}

export default new TradesSubscriptionService();

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import I18n from 'redux-i18n';
import history from './helpers/history';
import * as numberFormatter from 'apex-web/lib/helpers/numberFormatter';
import initAPEXWebActions from 'apex-web/lib/helpers/apexWebActions';
import { unregister } from './registerServiceWorker';
import config from './config';
import { initCacheLocaltion } from './helpers/cacheLocaltion';
import { initDataExchange } from './helpers/dataExchange';
import { initAdjust } from './helpers/adjust';
import cssVars from 'css-vars-ponyfill';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './styles/base/Fonts.css';
import './styles/base/Base.css';
import './styles/base/Grid.css';
import './styles/theme/variables.css';
import './styles/components/common/Divider.css';
import './styles/layout/app.css';
import './styles/components/common/index.css';

/* eslint-disable import/first */
import('./config').then(config =>
  import(`./styles/theme/${config.default.global.theme}.css`)
);

import App from './App';
import store from './redux/store';

initDataExchange();
initAPEXWebActions();
initCacheLocaltion();
initAdjust();

numberFormatter.init({
  locale: config.global.locale,
  getDecimalPlaces: () => store.getState().product.decimalPlaces
});

window.cssVariables = {};
cssVars({
  watch: true,
  silent: true,
  onComplete: function(cssText, styleNode, cssVariables) {
    window.cssVariables = cssVariables;
  }
});

const translations = () => {
  if (window.APEXTranslations) {
    window.APEXTranslations.options = {
      suppress_warnings: true
    };
    return window.APEXTranslations;
  }
  return { en: {} };
};

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <I18n translations={translations()}>
        <App />
      </I18n>
    </Router>
  </Provider>,
  document.getElementById('root')
);

unregister();

import medalBronze from '../images/levels/rank-bronze.png';
import medalSilver from '../images/levels/rank-silver.png';
import medalGold from '../images/levels/rank-gold.png';
import medalPlatinum from '../images/levels/rank-platinum.png';
import medalDiamond from '../images/levels/rank-diamond.png';
import medalEmerald from '../images/levels/rank-emerald.png';

import rankBronze from '../images/loyalty-token/rank-bronze.svg';
import rankSilver from '../images/loyalty-token/rank-silver.svg';
import rankGold from '../images/loyalty-token/rank-gold.svg';
import rankPlatinum from '../images/loyalty-token/rank-platinum.svg';
import rankDiamond from '../images/loyalty-token/rank-diamond.svg';
import rankEmerald from '../images/loyalty-token/rank-emerald.svg';

import icBdgIActive from '../images/loyalty-token/ic-bdg-i-inactive.png';
import icBdgIIActive from '../images/loyalty-token/ic-bdg-ii-inactive.png';
import icBdgIIIActive from '../images/loyalty-token/ic-bdg-iii-inactive.png';
import icBdgIVActive from '../images/loyalty-token/ic-bdg-iv-inactive.png';
import icBdgPromoteActive from '../images/loyalty-token/ic-bdg-promote-inactive.png';

import icLockedSilver from '../images/loyalty-token/ic-locked-silver.png';
import icLockedGold from '../images/loyalty-token/ic-locked-gold.png';
import icLockedPlatinum from '../images/loyalty-token/ic-locked-platinum.png';
import icLockedDiamond from '../images/loyalty-token/ic-locked-diamond.png';
import icLockedEmerald from '../images/loyalty-token/ic-locked-emerald.png';

import icBdgBronzeIActive from '../images/loyalty-token/ic-bdg-bronze-i-active.png';
import icBdgBronzeIIActive from '../images/loyalty-token/ic-bdg-bronze-ii-active.png';
import icBdgBronzeIIIActive from '../images/loyalty-token/ic-bdg-bronze-iii-active.png';
import icBdgBronzeIVActive from '../images/loyalty-token/ic-bdg-bronze-iv-active.png';
import icBdgBronzePromoteActive from '../images/loyalty-token/ic-bdg-bronze-promote-active.png';
import icRankBronze from '../images/loyalty-token/ic-rank-bronze.png';
import icBdgSilverIActive from '../images/loyalty-token/ic-bdg-silver-i-active.png';
import icBdgSilverIIActive from '../images/loyalty-token/ic-bdg-silver-ii-active.png';
import icBdgSilverIIIActive from '../images/loyalty-token/ic-bdg-silver-iii-active.png';
import icBdgSilverIVActive from '../images/loyalty-token/ic-bdg-silver-iv-active.png';
import icBdgSilverPromoteActive from '../images/loyalty-token/ic-bdg-silver-promote-active.png';
import icRankSilver from '../images/loyalty-token/ic-rank-silver.png';
import icBdgGoldIActive from '../images/loyalty-token/ic-bdg-gold-i-active.png';
import icBdgGoldIIActive from '../images/loyalty-token/ic-bdg-gold-ii-active.png';
import icBdgGoldIIIActive from '../images/loyalty-token/ic-bdg-gold-iii-active.png';
import icBdgGoldIVActive from '../images/loyalty-token/ic-bdg-gold-iv-active.png';
import icBdgGoldPromoteActive from '../images/loyalty-token/ic-bdg-gold-promote-active.png';
import icRankGold from '../images/loyalty-token/ic-rank-gold.png';
import icBdgPlatinumIActive from '../images/loyalty-token/ic-bdg-platinum-i-active.png';
import icBdgPlatinumIIActive from '../images/loyalty-token/ic-bdg-platinum-ii-active.png';
import icBdgPlatinumIIIActive from '../images/loyalty-token/ic-bdg-platinum-iii-active.png';
import icBdgPlatinumIVActive from '../images/loyalty-token/ic-bdg-platinum-iv-active.png';
import icBdgPlatinumPromoteActive from '../images/loyalty-token/ic-bdg-platinum-promote-active.png';
import icRankPlatinum from '../images/loyalty-token/ic-rank-platinum.png';
import icBdgDiamondIActive from '../images/loyalty-token/ic-bdg-diamond-i-active.png';
import icBdgDiamondIIActive from '../images/loyalty-token/ic-bdg-diamond-ii-active.png';
import icBdgDiamondIIIActive from '../images/loyalty-token/ic-bdg-diamond-iii-active.png';
import icBdgDiamondIVActive from '../images/loyalty-token/ic-bdg-diamond-iv-active.png';
import icBdgDiamondPromoteActive from '../images/loyalty-token/ic-bdg-diamond-promote-active.png';
import icRankDiamond from '../images/loyalty-token/ic-rank-diamond.png';
import icBdgEmeraldIActive from '../images/loyalty-token/ic-bdg-emerald-i-active.png';
import icBdgEmeraldIIActive from '../images/loyalty-token/ic-bdg-emerald-ii-active.png';
import icBdgEmeraldIIIActive from '../images/loyalty-token/ic-bdg-emerald-iii-active.png';
import icBdgEmeraldIVActive from '../images/loyalty-token/ic-bdg-emerald-iv-active.png';
import icBdgEmeraldPromoteActive from '../images/loyalty-token/ic-bdg-emerald-promote-active.png';
import icRankEmerald from '../images/loyalty-token/ic-rank-emerald.png';

import badgeAmbassadorLegendary from '../images/loyalty-token/badge-ambassador-legendary.png';
import badgeAmbassadorEpic from '../images/loyalty-token/badge-ambassador-epic.png';
import badgeAmbassadorRare from '../images/loyalty-token/badge-ambassador-rare.png';
import badgeAmbassadorNormal from '../images/loyalty-token/badge-ambassador-normal.svg';
import badgeAmbassadorNormalGrey from '../images/loyalty-token/badge-ambassador-normal-grey.png';
import badgeWhaleLegendary from '../images/loyalty-token/badge-whale-legendary.png';
import badgeWhaleKiller from '../images/loyalty-token/badge-whale-killer.png';
import badgeWhaleBaby from '../images/loyalty-token/badge-whale-baby.svg';
import badgeWhale from '../images/loyalty-token/badge-whale.png';
import badgeDolphin from '../images/loyalty-token/badge-dolphin.png';
import badgeDolphinGrey from '../images/loyalty-token/badge-dolphin-grey.png';
import badgeEarlyAdopter from '../images/loyalty-token/badge-early-adopter.png';
import badgeEarlyAdopterGrey from '../images/loyalty-token/badge-early-adopter-grey.png';

const rankImg = {
  bronze: rankBronze,
  silver: rankSilver,
  gold: rankGold,
  platinum: rankPlatinum,
  diamond: rankDiamond,
  emerald: rankEmerald
};

const bdgImg = {
  icBdgIActive,
  icBdgIIActive,
  icBdgIIIActive,
  icBdgIVActive,
  icBdgPromoteActive
};

const lockImg = {
  silver: icLockedSilver,
  gold: icLockedGold,
  platinum: icLockedPlatinum,
  diamond: icLockedDiamond,
  emerald: icLockedEmerald
};

const selectIcBdgImg = {
  bronze: {
    medal: medalBronze,
    icBdgIActive: icBdgBronzeIActive,
    icBdgIIActive: icBdgBronzeIIActive,
    icBdgIIIActive: icBdgBronzeIIIActive,
    icBdgIVActive: icBdgBronzeIVActive,
    icBdgPromoteActive: icBdgBronzePromoteActive,
    icRank: icRankBronze
  },
  silver: {
    medal: medalSilver,
    icBdgIActive: icBdgSilverIActive,
    icBdgIIActive: icBdgSilverIIActive,
    icBdgIIIActive: icBdgSilverIIIActive,
    icBdgIVActive: icBdgSilverIVActive,
    icBdgPromoteActive: icBdgSilverPromoteActive,
    icRank: icRankSilver
  },
  gold: {
    medal: medalGold,
    icBdgIActive: icBdgGoldIActive,
    icBdgIIActive: icBdgGoldIIActive,
    icBdgIIIActive: icBdgGoldIIIActive,
    icBdgIVActive: icBdgGoldIVActive,
    icBdgPromoteActive: icBdgGoldPromoteActive,
    icRank: icRankGold
  },
  platinum: {
    medal: medalPlatinum,
    icBdgIActive: icBdgPlatinumIActive,
    icBdgIIActive: icBdgPlatinumIIActive,
    icBdgIIIActive: icBdgPlatinumIIIActive,
    icBdgIVActive: icBdgPlatinumIVActive,
    icBdgPromoteActive: icBdgPlatinumPromoteActive,
    icRank: icRankPlatinum
  },
  diamond: {
    medal: medalDiamond,
    icBdgIActive: icBdgDiamondIActive,
    icBdgIIActive: icBdgDiamondIIActive,
    icBdgIIIActive: icBdgDiamondIIIActive,
    icBdgIVActive: icBdgDiamondIVActive,
    icBdgPromoteActive: icBdgDiamondPromoteActive,
    icRank: icRankDiamond
  },
  emerald: {
    medal: medalEmerald,
    icBdgIActive: icBdgEmeraldIActive,
    icBdgIIActive: icBdgEmeraldIIActive,
    icBdgIIIActive: icBdgEmeraldIIIActive,
    icBdgIVActive: icBdgEmeraldIVActive,
    icBdgPromoteActive: icBdgEmeraldPromoteActive,
    icRank: icRankEmerald
  }
};

const levelsInfo = [
  {
    title: 'bronze',
    subLevel: [1, 2, 3, 4, 5],
    accumulationRate: 0.15,
    tradingFeeDiscount: 50,
    referralIncomeRate: 15,
    dedicatedVIPCallSupport: 'No',
    voteCount: 'x1',
    personalBroker: 'No'
  },
  {
    title: 'silver',
    subLevel: [5, 7.5, 10, 12.5, 15],
    accumulationRate: 0.15,
    tradingFeeDiscount: 50,
    referralIncomeRate: 20,
    dedicatedVIPCallSupport: 'No',
    voteCount: 'x2',
    personalBroker: 'No'
  },
  {
    title: 'gold',
    subLevel: [15, 23.75, 32.5, 41.25, 50],
    accumulationRate: 0.2,
    tradingFeeDiscount: 55,
    referralIncomeRate: 25,
    dedicatedVIPCallSupport: 'No',
    voteCount: 'x5',
    personalBroker: 'No'
  },
  {
    title: 'platinum',
    subLevel: [50, 75, 100, 125, 150],
    accumulationRate: 0.2,
    tradingFeeDiscount: 60,
    referralIncomeRate: 30,
    dedicatedVIPCallSupport: 'Yes',
    voteCount: 'x10',
    personalBroker: 'No'
  },
  {
    title: 'diamond',
    subLevel: [150, 187.5, 225, 262.5, 300],
    accumulationRate: 0.25,
    tradingFeeDiscount: 65,
    referralIncomeRate: 35,
    dedicatedVIPCallSupport: 'Yes',
    voteCount: 'x25',
    personalBroker: 'Yes'
  },
  {
    title: 'emerald',
    subLevel: [300, 533.3, 766.6, 1000],
    accumulationRate: 0.25,
    tradingFeeDiscount: 70,
    referralIncomeRate: 40,
    dedicatedVIPCallSupport: 'Yes',
    voteCount: 'x50',
    personalBroker: 'Yes'
  }
];

const calProgress = (curentLevel, amountK) => {
  if (amountK < 1) {
    return 0;
  }
  if (amountK > 1000) {
    return 100;
  }
  const fullProgressNumber =
    curentLevel.subLevel[curentLevel.subLevel.length - 1] -
    curentLevel.subLevel[0];
  const curentProgressNumber = amountK - curentLevel.subLevel[0];
  return (curentProgressNumber / fullProgressNumber) * 100;
};

const cal = (curentLevel, amountK) => {
  // subLevel: [50, 75, 100, 125, 150],
  if (amountK < curentLevel.subLevel[1]) {
    curentLevel.subLevelValue = 4;
    curentLevel.subLevelText = 'IV';
    curentLevel.progress = calProgress(curentLevel, amountK);
    curentLevel.imgs = selectIcBdgImg[curentLevel.title];
    return curentLevel;
  }
  if (amountK < curentLevel.subLevel[2]) {
    curentLevel.subLevelValue = 3;
    curentLevel.subLevelText = 'III';
    curentLevel.progress = calProgress(curentLevel, amountK);
    curentLevel.imgs = selectIcBdgImg[curentLevel.title];
    return curentLevel;
  }
  if (amountK < curentLevel.subLevel[3]) {
    curentLevel.subLevelValue = 2;
    curentLevel.subLevelText = 'II';
    curentLevel.progress = calProgress(curentLevel, amountK);
    curentLevel.imgs = selectIcBdgImg[curentLevel.title];
    return curentLevel;
  }
  if (amountK < curentLevel.subLevel[4]) {
    curentLevel.subLevelValue = 1;
    curentLevel.subLevelText = 'I';
    curentLevel.progress = calProgress(curentLevel, amountK);
    curentLevel.imgs = selectIcBdgImg[curentLevel.title];
    return curentLevel;
  }
  curentLevel.subLevelValue = 1;
  curentLevel.subLevelText = 'I';
  curentLevel.progress = calProgress(curentLevel, amountK);
  curentLevel.imgs = selectIcBdgImg[curentLevel.title];
  return curentLevel;
};

const getCurentLevel = amount => {
  if (amount < 5) {
    return cal(
      levelsInfo.find(levelInfo => levelInfo.title === 'bronze'),
      amount
    );
  }
  if (amount < 15) {
    return cal(
      levelsInfo.find(levelInfo => levelInfo.title === 'silver'),
      amount
    );
  }
  if (amount < 50) {
    return cal(
      levelsInfo.find(levelInfo => levelInfo.title === 'gold'),
      amount
    );
  }
  if (amount < 150) {
    return cal(
      levelsInfo.find(levelInfo => levelInfo.title === 'platinum'),
      amount
    );
  }
  if (amount < 300) {
    return cal(
      levelsInfo.find(levelInfo => levelInfo.title === 'diamond'),
      amount
    );
  }
  return cal(
    levelsInfo.find(levelInfo => levelInfo.title === 'emerald'),
    amount
  );
};

const badge = {
  ambassador: {
    legendary: badgeAmbassadorLegendary,
    epic: badgeAmbassadorEpic,
    rare: badgeAmbassadorRare,
    normal: badgeAmbassadorNormal,
    grey: badgeAmbassadorNormalGrey
  },
  whale: {
    legendary: badgeWhaleLegendary,
    killer: badgeWhaleKiller,
    baby: badgeWhaleBaby,
    normal: badgeWhale,
    dolphin: badgeDolphin,
    grey: badgeDolphinGrey
  },
  ea: {
    normal: badgeEarlyAdopter,
    grey: badgeEarlyAdopterGrey
  }
};

export {
  rankImg,
  bdgImg,
  lockImg,
  levelsInfo,
  selectIcBdgImg,
  getCurentLevel,
  badge
};

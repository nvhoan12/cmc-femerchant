import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import LoginFormComponent from './LoginFormComponent';
import {
  login,
  AUTH_2FA_REQUIRED,
  AUTH_ERROR
} from 'apex-web/lib/redux/actions/authActions';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import path from 'apex-web/lib/helpers/path';
import { withRouter } from 'react-router';
import { adTrack } from '../../helpers/adjust';
import config from '../../config';

const configSignupForm = config.SignupForm;
const useEmailAsUsername = configSignupForm.useEmailAsUsername;
const active = configSignupForm.active;

const mapStateToProps = state => {
  return {
    errorMsg: state.auth.errorMsg,
    isConnected: state.wsConnection.isConnected,
    useEmailAsUsername: useEmailAsUsername,
    active: active
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    doSubmit: (payload, isConnected, context) => {
      adTrack('LoginAttempt');
      if (!isConnected || !navigator.onLine) {
        return dispatch(
          showSnack({
            id: 'loginNotConnectedError',
            text: context.t(
              'We were unable to login, check your internet connection and try refreshing the page.'
            ),
            type: 'warning'
          })
        );
      }
      return dispatch(login(payload)).then(action => {
        if (action.type !== AUTH_ERROR) {
          adTrack('LoginCompleted');
        }
        console.log(action);
        if (action.type === AUTH_2FA_REQUIRED) {
          ownProps.history.push(path('/twofactorauth'));
        }
      });
    }
  };
};

const LoginFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginFormComponent);

const LoginFormContainerForm = reduxForm({
  form: 'login',
  onSubmit: () => {}
})(LoginFormContainer);

export default withRouter(LoginFormContainerForm);

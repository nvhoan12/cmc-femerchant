import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import APInput from 'apex-web/lib/components/common/APInput';
import APButton from 'apex-web/lib/components/common/APButton';
import { required } from 'apex-web/lib/helpers/formValidations';
import APPasswordInput from 'apex-web/lib/components/common/APPasswordInput/APPasswordInput';
import redirectIfAuthorized from 'apex-web/lib/hocs/redirectIfAuthorized';
import { defaultPath } from 'apex-web/lib/routeTemplates';
import path from 'apex-web/lib/helpers/path';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { styleNames } from 'apex-web/lib/propTypes/commonComponent';

import logo2x from '../../images/bitazza-logo-02.png';
import '../../styles/components/common/StandaloneForm.css';
import './LoginFormComponent.css';

const baseClasses = getBEMClasses('standalone-form');
const loginFormClasses = getBEMClasses('login-form');

export const LoginFormComponent = (props, context) => {
  const {
    handleSubmit,
    errorMsg,
    submitting,
    doSubmit,
    isConnected,
    useEmailAsUsername,
    active
  } = props;

  return (
    <div
      className={clsx(baseClasses('container'), loginFormClasses('container'))}>
      <div className={loginFormClasses('container-left')}>
        <div className={loginFormClasses('container-left-header')}>
          <h1>{context.t('Sign In')}</h1>
        </div>
        <form
          onSubmit={handleSubmit(values =>
            doSubmit(values, isConnected, context)
          )}
          className={clsx(baseClasses('form'), loginFormClasses('form'))}>
          {errorMsg && (
            <p className={baseClasses('error')}>{context.t(errorMsg)}</p>
          )}
          <APInput
            type="text"
            name="username"
            customClass={loginFormClasses()}
            label={
              useEmailAsUsername ? context.t('Email') : context.t('Username')
            }
            validate={[required]}
          />
          <APPasswordInput
            type="password"
            name="password"
            customClass={loginFormClasses()}
            label={context.t('Password')}
            validate={[required]}
          />
          <APButton
            type="submit"
            disabled={submitting}
            customClass={loginFormClasses()}
            styleName={styleNames.additive}>
            {submitting ? context.t('Processing...') : context.t('Log In')}
          </APButton>
        </form>
        <div className={loginFormClasses('container-left-footer')}>
          {active && (
            <Link
              to={path('/signup')}
              className={loginFormClasses('link')}
              disabled={true}>
              {context.t('Sign Up')}
            </Link>
          )}
          <Link
            to={path('/problem-logging-in')}
            className={loginFormClasses('link')}
            disabled={true}>
            {context.t('Problem Logging In?')}
          </Link>
        </div>
      </div>
      <div className={loginFormClasses('container-right')}>
        <img src={logo2x} alt="logo" />
      </div>
    </div>
  );
};

LoginFormComponent.defaultProps = {
  handleSubmit: () => {},
  submitting: false
};

LoginFormComponent.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  referrer: PropTypes.string
};

LoginFormComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default redirectIfAuthorized(LoginFormComponent, defaultPath.path);

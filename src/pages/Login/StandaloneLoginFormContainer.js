import React from 'react';
import StandAloneLayout from '../../layouts/StandAloneLayout/StandAloneLayout';
import LoginFormContainer from './LoginFormContainer';

const StandaloneLoginFormContainer = () => {
  return (
    <StandAloneLayout>
      <LoginFormContainer />
    </StandAloneLayout>
  );
};

export default StandaloneLoginFormContainer;

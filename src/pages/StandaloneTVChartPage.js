import { connect } from 'react-redux';
import { getQueryParam } from 'apex-web/lib/helpers/queryParamsHelper';
import TVChartSimpleComponent from '../components/PriceChart/TVChartSimpleComponent';
import { changeTickersInterval } from 'apex-web/lib/redux/actions/tickersActions';
import { selectedInstrumentSelector } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';
import config from 'apex-web/lib/config';

const mapStateToProps = state => {
  const instrument = getQueryParam('instrument');
  document.getElementById('fc-btn').style.display = 'none';

  const selectedInstrument = state.apexCore.instrument.instruments.find(
    item => item.Symbol === instrument.toUpperCase()
  );
  const { decimalPlaces } = state.apexCore.product;

  const { theme } = config.global;
  const localConfig = JSON.parse(localStorage.getItem('apexTemplate')) || {};

  return {
    selectedInstrument: selectedInstrument || selectedInstrumentSelector(state),
    decimalPlaces,
    theme: localConfig.theme || theme
  };
};

const mapDispatchToProps = {
  changeTickersInterval
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TVChartSimpleComponent);

import React from 'react';
import clsx from 'clsx';

import { Text } from '../../components/PublicPage/Text';

import './EarnBTZBlock.css';

const EarnBTZBlock = ({ item }) => {
  const [open, setOpen] = React.useState(false);
  return (
    <div className="col-lg-4 col-md-6 pb-5 mb-5">
      <div className="px-md-5 text-center">
        <img src={item.img} className="img-fluid" alt={item.title} />

        <div className="earn-btz-wrapper">
          <Text
            className="mt-4 earn-btz-text"
            align="center"
            text={item.title}
            color="black"
            fontSize="24"
            fontBold
          />
          <i
            className={clsx(
              'earn-btz-caret fa fa-lg',
              open ? 'fa-caret-up' : 'fa-caret-down'
            )}
            onClick={() => setOpen(!open)}
            aria-hidden="true"
          />
        </div>
        {open && (
          <Text
            className="earn-btz-text"
            align="center"
            text={item.description}
            color="black"
            fontSize="18"
            fontBold
          />
        )}
      </div>
    </div>
  );
};

export default EarnBTZBlock;

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import PublicPageContainer from '../../components/PublicPage/PublicPageContainer';
import Jumbotron from '../../components/PublicPage/Jumbotron';
import FAQBlock from '../../components/PublicPage/FAQBlock';
import TierLevels from '../../components/PublicPage/TierLevels';
import Footer from '../../components/PublicPage/Footer';
import WaitlistPhase2 from '../../components/PublicPage/WaitlistPhase2';
import { AspectRatio16per9 } from '../../components/PublicPage/AspectRatio';

import { getlocalLanguage } from '../../helpers/localLanguage';

import { levelFaq, earnBTZs } from './LevelPageData';

import {
  TextHeader,
  TextLead,
  TextBlockHeader,
  TextBlockSubHeader,
  Text
} from '../../components/PublicPage/Text';

import { getBEMClasses } from '../../helpers/cssClassesHelper';
import RankingTabs from '../../components/PublicPage/RankingTabs';
import EarnBTZBlock from './EarnBTZBlock';

import rankingImg from '../../images/ranking.png';
import rankingBg from '../../images/levels/background-09.png';

import './index.css';

const levelPageClasses = getBEMClasses('level-page');

const LevelPage = (props, context) => {
  const language = getlocalLanguage();

  return (
    <PublicPageContainer>
      <Jumbotron className={levelPageClasses('jumbotron')}>
        <div className={levelPageClasses('container')}>
          <div className="row level-page__row">
            <div className="col-lg-6">
              <div className={levelPageClasses('hero-content')}>
                <h1 className={levelPageClasses('hero-heading')}>
                  <span className={levelPageClasses('gradient')}>
                    {context.t('BTZ')}
                  </span>{' '}
                  {context.t('LEVELS')}
                </h1>
                <h3 className={levelPageClasses('hero-subheading')}>
                  {context.t('Trade.Earn.Rewards')}
                </h3>

                <AspectRatio16per9>
                  {language === 'EN' ? (
                    <iframe
                      title="bitazza-vdo-en"
                      className={levelPageClasses('video-container')}
                      width="100%"
                      height="100%"
                      src="https://www.youtube.com/embed/WBZTQ-b2YEE"
                      frameborder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen
                    />
                  ) : (
                    <iframe
                      title="bitazza-vdo-en"
                      className={levelPageClasses('video-container')}
                      width="100%"
                      height="100%"
                      src="https://www.youtube.com/embed/9rk_dpdk7OI"
                      frameBorder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen
                    />
                  )}
                </AspectRatio16per9>
              </div>
            </div>
            <div className="col-lg-6">
              <div className={levelPageClasses('image-container')}>
                <img
                  src={rankingImg}
                  alt="ranking logo"
                  className={levelPageClasses('cover')}
                />
              </div>
            </div>
          </div>
        </div>
      </Jumbotron>

      <div className={levelPageClasses('info')}>
        <div className="container py-lg-5 py-4">
          <div className="row py-lg-5 py-4">
            <div className="col-lg-10 offset-lg-1 py-md-4 text-center">
              <div className="px-lg-4">
                <TextBlockHeader
                  color="black"
                  align="center"
                  text={context.t('THIS IS BITAZZA LEVELS')}
                />

                <p className={levelPageClasses('tagline')}>
                  <strong>{context.t('Welcome to Bitazza Levels')}</strong>
                  {` `}
                  {context.t('– where everyone can earn more BTZ just by')}
                  <span>
                    {` `}
                    {context.t('trading, referring, sharing')}
                  </span>
                  {` ${context.t('or just')} `}
                  <span>{`${context.t('playing games.')} `}</span>
                  {context.t(
                    'All to make your Bitazza experience even better.'
                  )}
                </p>
                <Text
                  className={levelPageClasses('text')}
                  fontSize="32"
                  text={context.t(
                    'The more BTZ you accumulate, the higher the tier you upgrade to with more benefits.'
                  )}
                />

                <TextBlockSubHeader text={context.t('TIER LEVELS')} />
              </div>
            </div>

            <div className="col-lg-12">
              <TierLevels context={context} />
            </div>
          </div>
        </div>
      </div>

      <Jumbotron
        backgrandImg={rankingBg}
        className={levelPageClasses('reward')}>
        <TextLead text={context.t('REWARDS AND BENEFITS')} align="center" />
        <TextHeader
          text={context.t('FOR EVERYONE, AT EVERY TIER')}
          align="center"
        />

        <Text
          text={context.t(
            'From Trading Fee Discounts to Dedicated VIP Call Support, enjoy a host of exclusive benefits at every tier.'
          )}
          align="center"
          fontSize="28"
          color="lightdark"
        />

        <RankingTabs />
      </Jumbotron>

      <Jumbotron className={levelPageClasses('earn-btz')}>
        <TextLead
          text={context.t('MORE WAYS TO EARN BTZ')}
          align="center"
          color="lightdark"
          fontBold
        />
        <TextHeader
          text={context.t('MORE BENEFITS TO ENJOY')}
          align="center"
          color="black"
        />

        <Text
          text={context.t(
            'At Bitazza Levels, there are so many ways to earn BTZ and we’ve also made it awesomely simple. The easier it is to earn, the more you get to enjoy.'
          )}
          align="center"
          fontSize="28"
          color="lightdark"
        />

        <div className={levelPageClasses('earn-btz-info')}>
          <div className="row text-center has-row-spacing">
            {earnBTZs(context).map((item, i) => {
              return <EarnBTZBlock item={item} key={i} />;
            })}
          </div>
        </div>

        <div className="row">
          <div className="col col-12">
            <div className={levelPageClasses('call-to-action')}>
              <Text
                text={context.t('GET STARTED ON BITAZZA LEVELS NOW!')}
                fontSize="28"
                fontBold
              />

              <Link to="/signup" className={levelPageClasses('btn-cta')}>
                {context.t('SIGN UP NOW!')}
              </Link>
            </div>
          </div>
        </div>
      </Jumbotron>

      <Jumbotron className={levelPageClasses('faq')}>
        <TextBlockHeader
          color="black"
          align="center"
          text={context.t('FAQs')}
        />
        <div className="row">
          {levelFaq(context).map((item, i) => (
            <div key={i} className="col-lg-6 pt-3">
              <FAQBlock item={item} />
            </div>
          ))}
        </div>
        <Text
          className="mt-5 pt-5"
          color="publicPrimary"
          align="center"
          text={
            <React.Fragment>
              {context.t(
                'If you have any further queries, please kindly email'
              )}
              <Text
                span
                className="pl-2"
                color="publicPrimary"
                display="inline"
                fontBold
                text="hello@bitazza.com"
              />
            </React.Fragment>
          }
        />
      </Jumbotron>
      <Footer />
      <WaitlistPhase2 />
    </PublicPageContainer>
  );
};

LevelPage.contextTypes = {
  t: PropTypes.func.isRequired
};

export default LevelPage;

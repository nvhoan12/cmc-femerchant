import exchangeIcon from '../../images/levels/exchange.svg';
import collaborationIcon from '../../images/levels/collaboration.png';
import podiumIcon from '../../images/levels/podium.svg';
import shoppingCartIcon from '../../images/levels/shopping-cart.svg';
import highFiveIcon from '../../images/levels/high-five.svg';
import medalIcon from '../../images/levels/medal.svg';

const getTextHere = text =>
  `<a style="color: #222; font-size: 18px; text-decoration: underline; padding: 0 2px;" href="https://bitazza.com/Bitazza_Whitepaper.pdf" target="_blank" rel="noopener noreferrer">${text}</a>`;

export const earnBTZs = context => [
  {
    title: context.t('TRADING VOLUME'),
    description: context.t(
      'The more you trade, the more BTZ you earn! Receive 0.15% for every trade per day at Bronze level, increasing all the way to 0.70% at Emerald status.'
    ),
    img: exchangeIcon
  },
  {
    title: context.t('SUPPORT OUR PARTNERS & AFFILIATES'),
    description: context.t(
      'Be a patron to our partners and affiliates. Check out our partners & affiliates here.'
    ),
    img: collaborationIcon
  },
  {
    title: context.t('BITAZZA CONTESTS'),
    description: context.t(
      'Take part in sharing contests, activations, or other social games and be rewarded with BTZ. Follow us on social media for the latest updates!'
    ),
    img: podiumIcon
  },
  {
    title: context.t('BADGE ACHIEVEMENTS'),
    description: context.t(
      'Complete tasks and challenges to rake up Bitazza Badges, rewarding you with BTZ each time.'
    ),
    img: shoppingCartIcon
  },
  {
    title: context.t('PURCHASE BTZ'),
    description: context.t(
      'The easiest way to level up is to, of course, just purchase yourself more BTZ! You can do so after our 3-month accumulation period from January 2020 to March 2020.'
    ),
    img: highFiveIcon
  },
  {
    title: context.t('REFER A FRIEND'),
    description: context.t(
      'Refer a buddy to join Bitazza for a referral fee on all their trades, for life! The referral fee starts at 15% for Bronze level and increases all the way to 40% at Emerald status! '
    ),
    img: medalIcon
  }
];

export const levelFaq = context => [
  {
    title: context.t('Why should I care about BTZ?'),
    des: context.t(
      "BTZ is fuel to the Bitazza Global platform & ecosystem. BTZ can be used as fees for all products & services that Bitazza offers. We intend to expand our offerings to an array of investment products from spot crypto, spot fx, futures trading as well as crypto options on our web and mobile platforms BTZ's supply is fixed and reducing due to our burn mechanism. As Bitazza receives fee payments in BTZ, it intends to burn a portion of the BTZ, which removes it from the supply forever. Find out more about BTZ and Bitazza’s future plans {here}  (link to Whitepaper)",
      {
        here: getTextHere(context.t('here'))
      }
    )
  },
  {
    title: context.t(
      'How long will the BTZ accumulation period last and what will happen next?'
    ),
    des: context.t(
      'The BTZ accumulation period will last for approximately 3 months or until the BTZ amount allocated for Ecosystem and Community (refer to Whitepaper {here}) has been fully distributed. Once the accumulation period is over, we will list BTZ for trading on the platform.',
      {
        here: getTextHere(context.t('here'))
      }
    )
  },
  {
    title: context.t('Will I ever be downgraded to a lower tier?'),
    des: context.t(
      'Yes, you must maintain the minimum amount of required for each tier (refer to the chart above) so you don’t lose your tier level.'
    )
  },
  {
    title: context.t('Can I use BTZ as fees?'),
    des: context.t(
      'Yes you can! In fact you will receive up to 70% discount in trading fees when you use them! But you will not accumulate more BTZ from these trades. Trading fee discounts are determined by your tier level. Early Adopters have a special discount bonus of 5% on top of their current tier level discount.'
    )
  },
  {
    title: context.t('How much BTZ do I receive for every THB I spend?'),
    des: context.t(
      'You will receive BTZ at a different accumulation rate based on your tier level. A higher tier level will give you a higher accumulation rate (refer to the chart above here). It can be from 0.15% up to 0.40% of every THB spent. Early Adopters have a special rate bonus of 0.05% on top of their current tier level rate.'
    )
  },
  {
    title: context.t(
      'When does my tier level get updated and how will I know if I reach a higher level?'
    ),
    des: context.t(
      'We will take a snapshot of your BTZ wallet balance at midnight 12:00am Thailand (GMT+7) everyday in order to determine your tier level. You will be notified via email when we have upgraded your tier level.'
    )
  },
  {
    title: context.t('I am an Early Adopter, will I lose my tier level?'),
    des: context.t(
      'Early Adopters are required to maintain a minimum of 500 BTZ at all times in order to maintain the Early Adopter badge. Once you lose your Early Adopter badge, we will not be able to restore it for you.'
    )
  },
  {
    title: context.t('What token pairings can I trade to accumulate more BTZ?'),
    des: context.t(
      'You can trade any pairing on the Bitazza platform to acquire more BTZ. However, you will not accumulate BTZ if you use BTZ as fees.'
    )
  },
  {
    title: context.t(
      'Can I transfer my benefits or level to another Bitazzan?'
    ),
    des: context.t(
      'Unfortunately, benefits and tier level are not transferable. Everybody has to earn it fair and square.'
    )
  },
  {
    title: context.t('Will my benefits expire if I don’t use them?'),
    des: context.t(
      'The key benefits of tier level will never expire i.e. discounted fees and referral fees. However, exclusive partnership benefits will vary over time depending on our negotiations with partners'
    )
  },
  {
    title: context.t(
      'How do I get access to Dedicated Call Support and Personal Broker?'
    ),
    des: context.t(
      'Once you have achieved the minimum tier level to qualify for these benefits, we will send you an email informing you of your new tier level and how to claim your rewards and benefits.'
    )
  },
  {
    title: context.t('How does my tier level affect my voting capacity?'),
    des: context.t(
      'All Bitazzans will have voting decision on product rollouts, coin listings, rewards, benefits and Bitazza Freedom Foundation CSR Projects. Every tier level has a fixed voting multiple from x1 (Bronze) to x50 (Emerald). Refer to the table above for reference.'
    )
  },
  {
    title: context.t('How do I claim my BTZ daily?'),
    des: context.t(
      'Your BTZ from daily accumulation, discount cash backs and referrals will be claimable everyday after the 12:00am snapshot (Thailand, GMT+7). Once you claim the BTZ reward, we will credit your account balance immediately.'
    )
  },
  {
    title: context.t('Will my accumulated BTZ expire if I don’t claim it?'),
    des: context.t(
      'There is no expiry date for your unclaimed BTZ. You will be able to claim them anytime in the future.'
    )
  },
  {
    title: context.t('What are BTZ rewards?'),
    des: context.t(
      'You will receive rewards for every Bitazza Levels you climb up. The rewards are our appreciation for your support, they may come in different forms such as Bitazza branded mugs, t-shirts, mousepads, umbrellas, hotel & restaurant vouchers and much more! Please refer to {bitazzaLink} for the various rewards up for grabs now.',
      {
        bitazzaLink:
          '<a style="color: #222; font-size: 18px; text-decoration: underline;" class="bitazza-text" href="https://levels.bitazza.com" target="_blank" rel="noopener noreferrer">https://levels.bitazza.com</a>'
      }
    )
  },
  {
    title: context.t('When will BTZ be available for trade on the platform?'),
    des: context.t(
      'BTZ will be available to trade on Bitazza Global (our international platform) once the BTZ accumulation period is over, which is targeted for 3 months from the launch of Bitazza Levels. Bitazza reserves the right to vary the trading start date as we see fit. '
    )
  },
  {
    title: context.t('How do I use BTZ for discounted fees?'),
    des: context.t(
      'You have to toggle the BTZ usage for fees in your ‘Settings’ page to activate your BTZ fee discount.'
    )
  },
  {
    title: context.t('How do you calculate daily BTZ accumulation?'),
    des: context.t(
      'BTZ accumulation is calculated once a day based on your trade volume and rewarded based on an indicative price of THB3.33 per BTZ (Private Placement Price).'
    )
  },
  {
    title: context.t('Can I buy BTZ outright?'),
    des: context.t(
      'We do not have any plans for a public sale of BTZ tokens yet. You may buy BTZ in the open market once BTZ begins trading on the platform in 3 months.'
    )
  },
  {
    title: context.t(
      'Does the ‘Refer a Friend’ contest during the Bitazza Pre-launch Campaign apply to my referrals?'
    ),
    des: context.t(
      'Definitely! All referrals during the Pre-launch Campaign will be tagged as your referral'
    )
  },
  {
    title: context.t(
      'How can I cash out my BTZ, especially being an overseas user without a Thai Bank  Account?'
    ),
    des: context.t(
      'If you do not have a Thai Bank Account, you will be able to trade BTZ for USDT or BTC and withdraw funds as USDT or BTC from the platform. You may also withdraw BTZ to be kept in an ERC20 wallet once BTZ withdrawals are activated. This will only be done when BTZ starts trading on Bitazza Global.'
    )
  },
  {
    title: context.t('What services does the Personal Broker include?'),
    des: context.t(
      'The Personal Broker service allows you to make trades from a secure chat system based on a request-for-quote price (similar to OTC) or via phone call to your personally assigned broker. The broker will be assigned to understand your trading requirements and be your first point of contact from Bitazza. As a Diamond or Emerald tier member, we value your individual trading requirements and your personal broker will ensure that we tailor our services to your needs.'
    )
  },
  {
    title: context.t(
      'On the Bitazza website it says “use BTZ tokens for 50% discount”. I am on Diamond tier, so what about my extra 15% discount?'
    ),
    des: context.t(
      'Discount cash backs are daily calculations of entitled discounted fees above the default 50% discount rate paid to you when you use BTZ as payment for fees For example, if you are on Diamond tier, you are entitled to receive a 65% discount on trading fees. When you make a trade, a default 50% discount will instantly be accounted for and the remaining 15% will be paid back to you at the end of each day after midnight Thailand (GMT+7)'
    )
  }
];

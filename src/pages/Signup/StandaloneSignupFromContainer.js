import React from 'react';
import StandAloneLayout from '../../layouts/StandAloneLayout/StandAloneLayout';
import SignupFormContainer from './SignupFormContainer';

const StandaloneSignupFormContainer = () => {
  return (
    <StandAloneLayout>
      <SignupFormContainer />
    </StandAloneLayout>
  );
};

export default StandaloneSignupFormContainer;

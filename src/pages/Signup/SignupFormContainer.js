import { connect } from 'react-redux';
import { reduxForm, formValueSelector, SubmissionError } from 'redux-form';
import get from 'lodash/get';
import SignupFormComponent from './SignupFormComponent';
import { preprocessErrorTextForReduxForm } from '../../helpers/errorTextFormatter';
import { getQueryParam } from '../../helpers/queryParamsHelper';
import { signup, clearSignupError } from '../../redux/actions/signupActions';
import config from '../../config';
import { getlocalLanguage } from '../../helpers/localLanguage';

const getAffiliateTag = () => {
  if (window.location.search) {
    return getQueryParam('kid') || getQueryParam('aff');
  }
  return window.localStorage.getItem('affiliateTag') || '';
};

const SignupFormForm = reduxForm({
  form: 'signup',
  initialValues: {
    language: getlocalLanguage() || 'EN',
    affiliateTag: getAffiliateTag()
  },
  onSubmit: (payload, dispatch) =>
    dispatch(signup(payload)).then(response => {
      if (response && response.errormsg) {
        /**
         * To leverage `redux-form` field-specific error functionality,
         *  we must throw a `SubmissionError` from decorated component's `handleSubmit` prop.
         */
        throw new SubmissionError(
          preprocessErrorTextForReduxForm(response.errormsg, payload)
        );
      }
    })
})(SignupFormComponent);

const selector = formValueSelector('signup');

const mapStateToProps = state => {
  return {
    passwordValue: selector(state, 'password'),
    errorMsg: get(state, 'form.signup.error', false),
    done: !!state.signup.userId,
    isAuthenticated: state.auth.isAuthenticated,
    siteName: config.global.siteName,
    language: selector(state, 'language'),
    ...config.SignupForm
  };
};

const mapDispatchToProps = {
  clearSignupError
};

const SignupFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupFormForm);

export default SignupFormContainer;

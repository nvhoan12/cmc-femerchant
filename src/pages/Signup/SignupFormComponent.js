import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Link } from 'react-router-dom';
import APInput from '../../components/common/APInput';
import APSelect from '../../components/common/APSelect';
import APPasswordInput from '../../components/common/APPasswordInput/APPasswordInput';
import APButton from '../../components/common/APButton';
import NotifySingaporean from '../../components/notification/NotifySingaporean';
import {
  required,
  email,
  matchingPassword,
  validatePasswordStricted,
  noWhiteSpace,
  alphanumeric,
  length
} from '../../helpers/formValidations';
import { getlocalLanguage } from '../../helpers/localLanguage';

import { renderFormInputs } from '../../helpers/formGeneratorHelper';
import path from '../../helpers/path';
import passwordScore from '../../helpers/passwordScore';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { styleNames } from '../../propTypes/commonComponent';

import '../../styles/components/common/StandaloneForm.css';
import './SignupFormComponent.css';

const baseClasses = getBEMClasses('standalone-form');
const signupFormClasses = getBEMClasses('signup-form');

class SignupFormComponent extends Component {
  getPasswordStrength = () => {
    const { context } = this;
    const score = passwordScore(this.props.passwordValue);
    const passwordStrengthValues = {
      0: context.t('Very Weak'),
      1: context.t('Weak'),
      2: context.t('Medium'),
      3: context.t('Strong'),
      4: context.t('Very Strong')
    };

    return passwordStrengthValues[score];
  };

  handleChange = () => {
    if (this.props.errorMsg) {
      this.props.clearSignupError();
    }
  };

  render() {
    const { context } = this;
    const {
      handleSubmit,
      errorMsg,
      submitting,
      pristine,
      isAuthenticated,
      passwordValue,
      invalid,
      done,
      useEmailAsUsername,
      additionalFields,
      termsAndServicesLink,
      privacyPolicyLink,
      language,
      siteName
    } = this.props;

    const termsLink = (
      <a
        className="link-text"
        href={path(termsAndServicesLink)}
        target="_blank"
        rel="noopener noreferrer">
        <p>{context.t('Terms and Conditions')}</p>
      </a>
    );

    const privacyLink = (
      <a
        className="link-text"
        href={path(privacyPolicyLink)}
        target="_blank"
        rel="noopener noreferrer">
        <p>{context.t('Privacy Policy')}</p>
      </a>
    );

    if (isAuthenticated) {
      return <Redirect to={path('/')} />;
    }

    if (done) {
      const userLang = language || getlocalLanguage();
      const launchUrl = `https://launch.bitazza.com/signupthankyou${userLang.toLowerCase()}`;
      window.location = launchUrl;
    } else {
      return (
        <div
          className={`${baseClasses('container')} ${signupFormClasses(
            'form-container'
          )}`}>
          <div
            className={`${baseClasses('header')} ${signupFormClasses(
              'header'
            )}`}>
            <div
              className={`${baseClasses('header-text')} ${signupFormClasses(
                'header-text'
              )}`}>
              {!done && context.t('{n} Sign Up', { n: siteName })}
            </div>
          </div>
          {!done && (
            <form
              onSubmit={handleSubmit}
              className={`${baseClasses('form')} ${signupFormClasses('form')}`}>
              <div className={signupFormClasses('wrapper')}>
                <div className={signupFormClasses('container')}>
                  {!useEmailAsUsername && (
                    <APInput
                      type="text"
                      name="username"
                      label={context.t('Username')}
                      validate={[required, noWhiteSpace]}
                      customClass={signupFormClasses()}
                      onChange={this.handleChange}
                    />
                  )}
                  <APInput
                    type="text"
                    name="email"
                    label={context.t('Email')}
                    validate={[required, email]}
                    customClass={signupFormClasses()}
                    onChange={this.handleChange}
                  />
                  <APPasswordInput
                    type="password"
                    name="password"
                    label={context.t('Password')}
                    validate={[required]}
                    info={passwordValue && this.getPasswordStrength()}
                    customClass={signupFormClasses()}
                    onChange={this.handleChange}
                  />
                  <APPasswordInput
                    type="password"
                    name="matchingPassword"
                    label={context.t('Retype Password')}
                    classes="signup"
                    validate={[
                      required,
                      matchingPassword,
                      validatePasswordStricted
                    ]}
                    customClass={signupFormClasses()}
                    onChange={this.handleChange}
                  />
                  <APSelect
                    name="language"
                    label={`${context.t('Select Language')}:`}
                    customClass={signupFormClasses()}
                    onChange={this.handleChange}
                    showTriangles={true}
                    options={[
                      {
                        value: 'EN',
                        label: context.t('English')
                      },
                      {
                        value: 'TH',
                        label: context.t('Thai')
                      }
                    ]}
                  />

                  <APInput
                    type="text"
                    name="affiliateTag"
                    label={context.t('Affiliate')}
                    validate={[alphanumeric, length(6)]}
                    customClass={signupFormClasses()}
                    onChange={this.handleChange}
                  />

                  {renderFormInputs(
                    additionalFields,
                    signupFormClasses(),
                    context
                  )}
                  {errorMsg && (
                    <p
                      className={`${baseClasses('error')} ${signupFormClasses(
                        'error'
                      )}`}>
                      {context.t(errorMsg)}
                    </p>
                  )}
                </div>
              </div>

              <div className={signupFormClasses('footer')}>
                <APButton
                  type="submit"
                  disabled={pristine || submitting || invalid}
                  customClass={`${baseClasses()} ${signupFormClasses()}`}
                  styleName={styleNames.additive}>
                  {submitting
                    ? context.t('Processing...')
                    : context.t('Sign Up')}
                </APButton>

                <div className={signupFormClasses('text')}>
                  {context.t('By Clicking')}
                  <span className={signupFormClasses('bold-text')}>
                    {context.t(' Sign up ')}
                  </span>
                  {context.t('you accept our {t} and {p}', {
                    t: termsLink,
                    p: privacyLink
                  })}
                </div>
              </div>
            </form>
          )}

          <div
            className={`${baseClasses('footer')} ${signupFormClasses(
              'links'
            )}`}>
            <Link to={path('/login')} className={signupFormClasses('link')}>
              {context.t('Login')}
            </Link>
            <div className={`${baseClasses('footer-separator')}`} />
            <Link
              to={path('/problem-logging-in')}
              className={signupFormClasses('link')}
              disabled>
              {context.t('Forgot Password?')}
            </Link>
          </div>
          <NotifySingaporean />
        </div>
      );
    }
  }
}

SignupFormComponent.defaultProps = {
  handleSubmit: () => {},
  submitting: false
};

SignupFormComponent.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  referrer: PropTypes.string,
  search: PropTypes.string,
  shouldRedirect: PropTypes.bool,
  isAuthenticated: PropTypes.bool
};

SignupFormComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default SignupFormComponent;

import React from 'react';
import { getlocalLanguage } from '../helpers/localLanguage';
import whyBitazzaDeep from '../images/why-bitazza-deep.png';
import whyBitazzaLicense from '../images/why-bitazza-license.png';
import whyBitazzaMobile from '../images/why-bitazza-mobile.png';
import whyBitazzaSecurity from '../images/why-bitazza-security.png';

import whatAwesomeImg01 from '../images/what-awesome-img-01.png';
import whatAwesomeImg02 from '../images/what-awesome-img-02.png';
import whatAwesomeImg03 from '../images/what-awesome-img-03.png';
import whatAwesomeImg04 from '../images/what-awesome-img-04.png';
import whatAwesomeImg05 from '../images/what-awesome-img-05.png';
import whatAwesomeImg06 from '../images/what-awesome-img-06.png';
import whatBtzIcon1 from '../images/what-btz-icon-1.png';
import whatBtzIcon2 from '../images/what-btz-icon-2.png';
import whatBtzIcon3 from '../images/what-btz-icon-3.png';

const language = getlocalLanguage();
const secLink =
  language === 'TH'
    ? 'https://www.sec.or.th/TH/Pages/Shortcut/DigitalAsset.aspx'
    : 'https://www.sec.or.th/EN/Pages/Shortcut/DigitalAsset.aspx';

export const whyBitazza = context => [
  {
    title: context.t('Regulated License'),
    img: whyBitazzaLicense,
    des: (
      <React.Fragment>
        {context.t('Bitazza has been awarded the')}
        <a href={secLink}>{context.t('Digital Asset Broker license')}</a>
        {context.t(
          'regulated by the Securities and Exchange Commission and issued by Thailand’s Ministry of Finance.'
        )}
      </React.Fragment>
    )
  },
  {
    title: context.t('Security & Stability'),
    img: whyBitazzaSecurity,
    des: context.t(
      'The platform is protected by a highly reputable legal council to ensure stable operations and regulatory compliance.'
    )
  },
  {
    title: context.t('Superior Mobile App'),
    img: whyBitazzaMobile,
    des: context.t(
      'Fully optimised web and mobile app with a quick and easy onboarding process.'
    )
  },
  {
    title: context.t('Deep Liquidity'),
    img: whyBitazzaDeep,
    des: context.t(
      'Bitazza provides you with deep liquidity rates, as well as a convenient, safe and secure digital banking experience.'
    )
  }
];

export const whatAwesome = context => [
  {
    title: context.t('Deep Liquidity'),
    des: context.t(
      'We share our liquidity pool with leading exchanges around the world to ensure that you get the best prices'
    ),
    img: whatAwesomeImg01
  },
  {
    title: context.t('Simple Buy & Sell'),
    des: context.t(
      'Uncomplicated and effortless way to trade the cryptocurrency of your choice'
    ),
    img: whatAwesomeImg02
  },
  {
    title: context.t('Superior Security'),
    des: context.t(
      'Advanced risk control that’s optimised for institutional, professional and consumer-facing trading'
    ),
    img: whatAwesomeImg03
  },
  {
    title: context.t('Advanced Order Types'),
    des: context.t(
      'Wide range of order types to help you customise your trading strategy'
    ),
    img: whatAwesomeImg04
  },
  {
    title: context.t('User Friendly Mobile App'),
    des: context.t('Simple and easy to use mobile app for traders on the go'),
    img: whatAwesomeImg05
  },
  {
    title: context.t('24/7 Customer Support'),
    des: context.t(
      'Help is just an email, chat or phone call away. Anytime of the day'
    ),
    img: whatAwesomeImg06
  }
];

export const whatIsBtz = context => [
  {
    title: context.t('Trading Fee Discounts'),
    des: context.t(
      'Lifetime discounts on trading fees! Up to 70% depending on token holder level.'
    ),
    img: whatBtzIcon1
  },
  {
    title: context.t('Priority Allocation'),
    des: context.t('Priority access to future Initial Platform Offerings.'),
    img: whatBtzIcon2
  },
  {
    title: context.t('Voting Rights'),
    des: context.t('Voting rights for platform enhancements.'),
    img: whatBtzIcon3
  }
];

export const faq = context => [
  {
    title: context.t('Has BTZ been approved by SEC Thailand?'),
    des: context.t(
      'BTZ is a utility token that Bitazza Company Limited (Thailand) accepts as payments for trading fees in the Bitazza ecosystem. It has never gone through a public sale and do not fall under Thailand’s ICO law.'
    )
  },
  {
    title: context.t(
      'If Bitazza is a Southeast Asian platform, which other countries will you be operating in?'
    ),
    des: context.t(
      'Apart from Thailand, Bitazza has already submitted an application to the Securities Commission of Malaysia for the digital asset exchange license. We aim to expand to various countries in Asia, and are actively working on obtaining the required licences or exemptions for operations.'
    )
  },
  {
    title: context.t('What is the price of 1 BTZ?'),
    des: context.t(
      'BTZ is currently valued at US$0.111 based on the latest private placement of tokens completed.'
    )
  },
  {
    title: context.t('How do you determine the fiat value of BTZ?'),
    des: context.t(
      'BTZ has never undergone a public sale, so its price of US$0.111 was determined at the latest private placement round. If and when BTZ is listed in the secondary market, its fiat value will be determined by market forces.'
    )
  },
  {
    title: context.t('What do you mean by tokenholder level?'),
    des: context.t(
      'There are 6 different levels of BTZ tokenholders depending on the amount of BTZ owned. Higher level tokenholders will be entitled to VIP benefits and special offers on Bitazza’s platform.'
    )
  },
  {
    title: context.t('Do you accept signups from foreigners?'),
    des: context.t(
      'Bitazza generally accepts all signups from foreigners, except for customers from the United States and all Financial Action Task Force (FATF) blacklisted countries, or countries where we are not able to obtain required licences or exemptions for operations. However, it is required that all customers pass our KYC process before using our services.'
    )
  },
  {
    title: context.t(
      'Do I need a Thai bank account to use Bitazza’s services?'
    ),
    des: context.t(
      'A Thai bank account is needed if the customer wants to withdraw Thai baht. However, customers can already start trading by passing the KYC process and depositing digital assets.'
    )
  },
  {
    title: context.t('What are the discounted fees for using BTZ?'),
    des: context.t(
      'The use of BTZ entitles our customers a discount of 50% on all trading fees on the 1st year after Bitazza’s launch. BTZ tokenholders will enjoy a discount of 35% on Year 2, 20% on Year 3 and 10% on Year 4 onwards.'
    )
  },
  {
    title: context.t('When and how will I receive my BTZ rewards?'),
    des: context.t(
      'Your BTZ rewards will be transferred to your account upon the date of the platform launch and will be usable as fuel for trading fees right away.'
    )
  },
  {
    title: context.t('What’s the difference between a broker and an exchange?'),
    des: context.t(
      'Digital asset brokers and exchanges are both regulated business categories in Thailand. An exchange is licensed to match its customers’ orders in its own centralized orderbook. A broker is licensed to route its customers’ orders to various liquidity pools globally, as well as execute orders on behalf of its customers.'
    )
  },
  {
    title: context.t('Where is Bitazza’s liquidity coming from?'),
    des: context.t(
      'Bitazza’s liquidity will come from various international sources with high security and deep markets. Liquidity sources are required to also be compliant with local laws and regulations. Bitazza then aggregates the liquidity and allow customers to execute with best pricing and minimal market impact.'
    )
  },
  {
    title: context.t('How is Bitazza different from other trading platforms?'),
    des: context.t(
      'As the only institutional grade digital asset broker in Thailand, Bitazza is able to provide customers access to the deepest and most active digital asset markets, all on one platform.'
    )
  },
  {
    title: context.t('How do I buy more BTZ?'),
    des: context.t(
      'BTZ tokens are currently being airdropped to early participants for free. We do not offer a public sale of BTZ tokens, and any direct solicitation is potentially a scam and not endorsed by the company. We urge you to get in touch with us as soon as you find any evidence of such attempt. BTZ tokens will be available in the secondary market only after a listing on an exchange. Please only refer to our official statements for updates.'
    )
  },
  {
    title: context.t("What is Bitazza's trade commission fee?"),
    des: context.t(
      "Bitazza's base trade commission fee is 0.25%. However it will vary depending on discount level awarded to individual users by tokenholder ranking."
    )
  }
];

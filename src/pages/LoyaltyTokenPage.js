import React from 'react';
import PropTypes from 'prop-types';
import BigNumber from 'bignumber.js';
import { connect } from 'react-redux';
import SVG from 'react-inlinesvg';
import clsx from 'clsx';
import { getProductByIdSelector } from 'apex-web/lib/redux/selectors/productPositionSelectors';
import {
  loyaltyTokenSelector,
  currentAccountloyaltyTokenSelector
} from 'apex-web/lib/redux/selectors/loyaltyTokenSelectors';
import { updateAccount } from '../redux/actions/userActions';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { formatDateTime } from 'apex-web/lib/helpers/dateHelper';
import infoIcon from '../images/icons/icon-info.svg';
import config from '../config';
import Switch from '../components/common/APSwitch/Switch';
import APButton from '../components/common/APButton';
import BTZPanel from '../components/common/BTZPanel/BTZPanel';
import BTZDivider from '../components/common/BTZDivider/BTZDivider';
import BTZButton from '../components/common/BTZButton/BTZButton';
import { getBEMClasses } from '../helpers/cssClassesHelper';
import { getlocalLanguage } from '../helpers/localLanguage';
import { claimedBTZ } from '../redux/actions/btzLevelActions';

import {
  rankImg,
  bdgImg,
  lockImg,
  getCurentLevel,
  levelsInfo,
  badge
} from './LoyaltyTokenPageData';
import './LoyaltyTokenPage.css';

const classes = getBEMClasses('loyalty-token-page');

const percentFormat = (value) => `${value}%`;
const discountCal = (discount, ea) => discount + (ea ? 5 : 0);
const currentFeeCal = (discountNet) => {
  const percent = new BigNumber(discountNet).div(100);
  const feeDiscount = new BigNumber(config.global.orderFee).times(percent);
  const feeAmount = new BigNumber(config.global.orderFee).plus(
    feeDiscount.negated()
  );
  return feeAmount.toFixed(3, BigNumber.ROUND_CEIL);
};

const LoyaltyTokenPage = (
  {
    rawAmount,
    localAmount,
    loyaltyOMSEnabled,
    loyaltyOMSProducts,
    affiliate,
    usedBTZ,
    curentLevel,
    btzLevel,
    updateAccount,
    claimedBTZ
  },
  context
) => {
  const cardRankBlockEle = React.useRef(null);
  const [onHover, setOnHover] = React.useState(null);

  const displayLevelInfo = onHover
    ? levelsInfo.find((levelInfo) => levelInfo.title === onHover)
    : curentLevel;

  React.useEffect(
    () => {
      cardRankBlockEle.current.childNodes.forEach((childNode) => {
        if (childNode.id === curentLevel.title) {
          cardRankBlockEle.current.scrollLeft =
            childNode.offsetLeft - cardRankBlockEle.current.offsetLeft;
        }
      });
    },
    [curentLevel.title]
  );

  const handleHoverCard = (level) => setOnHover(level);
  const handleLeaveHover = () => setOnHover(null);
  const handleChangefeeToken = () => {
    if (!usedBTZ) {
      const btz = loyaltyOMSProducts.find(
        (product) => product.ProductSymbol === 'BTZ'
      );
      updateAccount(btz);
      return;
    }
    updateAccount();
  };

  const handleShare = () => {
    const language = getlocalLanguage();
    window.open(
      `https://share.bitazza.com/${language}/?kolid=${affiliate.affiliateTag}`,
      '_blank'
    );
  };

  const handleLearnMore = () => window.open('/levels', '_blank');

  return (
    <div className={classes()}>
      <div className="d-flex justify-content-between">
        {loyaltyOMSEnabled && (
          <div className="d-flex align-items-center pb-3">
            <Switch
              disabled={!rawAmount}
              value={usedBTZ}
              customClass={classes('switch')}
              onClick={handleChangefeeToken}
            />
            <p className="text-body-1 d-inline-block m-0 ml-4">
              {context.t(
                'Use BTZ to pay for fees ({x}% discount). Your current fee is {y}%.',
                {
                  x: discountCal(
                    curentLevel.tradingFeeDiscount,
                    btzLevel.showEA
                  ),
                  y: usedBTZ
                    ? currentFeeCal(
                        discountCal(
                          curentLevel.tradingFeeDiscount,
                          btzLevel.showEA
                        )
                      )
                    : config.global.orderFee
                }
              )}
            </p>
          </div>
        )}
        <BTZButton
          onClick={handleLearnMore}
          className="float-right"
          variant="link">
          {context.t('Learn More')}
        </BTZButton>
      </div>
      <BTZPanel className="d-flex flex-column justify-content-start flex-xl-row justify-content-xl-between">
        <div className="d-flex">
          <img
            className={classes('avatar')}
            src={curentLevel.imgs.medal}
            alt="avatar"
          />
          <div className="d-flex flex-column justify-content-center d-xl-block ml-3 mt-4 mt-xl-0">
            <p className="text-body-1 text-secondary">
              {context.t('Your Rank')}
            </p>
            <p className="text-header-1">
              <img
                src={curentLevel.imgs.icRank}
                className={clsx('d-none d-lg-inline-block', classes('ic-rank'))}
                alt="ic-rank"
              />
              {curentLevel.title.toUpperCase()} {curentLevel.subLevelText}
            </p>
            <div>
              <div className={classes('badge')}>
                <img
                  src={btzLevel.showEA ? badge.ea.normal : badge.ea.grey}
                  className={btzLevel.showEA ? 'active' : ''}
                  alt="badge early adopter"
                />
                <div
                  className={clsx(
                    classes('badge-info'),
                    classes('early-adopter-info')
                  )}>
                  <p className="text-lead mt-3 mb-4">
                    <span className={classes('color-early-adopter')}>
                      {context.t('EARLY ADOPTER')}
                    </span>{' '}
                    {context.t('BADGE')}
                  </p>
                  <p className="text-body-1 text-secondary">
                    {context.t(
                      'Receive a lifetime of benefits if you earn the Early Adopter badge. You need to earn this badge by contributing to the Bitazzan Community. Early Adopters must maintain a BTZ holding of at least 500 BTZ or the badge will be lost.'
                    )}
                  </p>
                  <BTZDivider dark />
                  <p className="text-body-1 text-secondary">
                    {context.t('BTZ Accumulation Rate')}
                  </p>
                  <p className="text-lead m-0">+0.05%</p>
                  <BTZDivider dark />
                  <p className="text-body-1 text-secondary">
                    {context.t('BTZ Trading Fee Discount')}
                  </p>
                  <p className="text-lead m-0">+5%</p>
                  <BTZDivider dark />
                  <p className="text-body-1 text-secondary">
                    {context.t('Dedicated VIP Call Support')}
                  </p>
                  <p className="text-lead m-0">{context.t('Yes')}</p>
                  <BTZDivider dark />
                  <p className="text-body-1 text-secondary">
                    {context.t('Vote Count')}
                  </p>
                  <p className="text-lead m-0">x5</p>
                </div>
              </div>
              <div className={classes('badge')}>
                <img
                  src={badge.ambassador[btzLevel.ambassador]}
                  className={btzLevel.ambassador !== 'grey' ? 'active' : ''}
                  alt="badge ambassador"
                />
                <div
                  className={clsx(
                    classes('badge-info'),
                    classes('am-info'),
                    classes(`border-color-am-${btzLevel.ambassador}`)
                  )}>
                  <p className="text-lead mt-3 mb-4 badge-main-title">
                    {context.t('Ambassador Badge')}
                  </p>
                  <div className={classes('am-info-header')}>
                    <div />
                    <div>
                      <p className="text-body-2 px-1">
                        {context.t('Referrals')}
                      </p>
                    </div>
                    <div>
                      <p className="text-body-2 px-1">
                        {context.t('BTZ Rewards')}
                      </p>
                    </div>
                    <div>
                      <p className="text-body-2 px-1">
                        {context.t('Referral Income')}
                      </p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('am-info-body'),
                      classes('color-am-legendary')
                    )}>
                    <div className={classes('am-info-body-badge')}>
                      <img
                        src={badge.ambassador.legendary}
                        alt="badge ambassador"
                      />
                      <span>LEGENDARY</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">200</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">1,000</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+2%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('am-info-body'),
                      classes('color-am-epic')
                    )}>
                    <div className={classes('am-info-body-badge')}>
                      <img src={badge.ambassador.epic} alt="badge ambassador" />
                      <span>EPIC</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">100</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">500</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+1%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('am-info-body'),
                      classes('color-am-rare')
                    )}>
                    <div className={classes('am-info-body-badge')}>
                      <img src={badge.ambassador.rare} alt="badge ambassador" />
                      <span>RARE</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">50</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">250</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+0.5%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('am-info-body'),
                      classes('color-am-normal')
                    )}>
                    <div className={classes('am-info-body-badge')}>
                      <img
                        src={badge.ambassador.normal}
                        alt="badge ambassador"
                      />
                      <span>NORMAL</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">25</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">100</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+0.25%</p>
                    </div>
                  </div>
                  <div>
                    <p className="text-body-1 text-secondary">
                      * {context.t('Referrals must complete KYC to be counted')}
                    </p>
                    <p className="text-body-1 text-secondary">
                      **{' '}
                      {context.t(
                        'Platinum level required to achieve Epic and Legendary badges'
                      )}
                    </p>
                  </div>
                </div>
              </div>
              <div className={classes('badge')}>
                <img
                  src={badge.whale[btzLevel.whale]}
                  className={btzLevel.whale !== 'grey' ? 'active' : ''}
                  alt="badge whale"
                />
                <div
                  className={clsx(
                    classes('badge-info'),
                    classes('whale-info'),
                    classes(`border-color-whale-${btzLevel.whale}`)
                  )}>
                  <p className="text-lead mt-3 mb-4 badge-main-title">
                    {context.t('Whale Badge')}
                  </p>
                  <div className={classes('whale-info-header')}>
                    <div />
                    <div>
                      <p className="text-body-2">
                        {context.t('Total THB Voume / Day')}
                      </p>
                    </div>
                    <div>
                      <p className="text-body-2 px-1">
                        {context.t('Accumulation')}
                      </p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('whale-info-body'),
                      classes('color-whale-legendary')
                    )}>
                    <div className={classes('whale-info-body-badge')}>
                      <img src={badge.whale.legendary} alt="badge whale" />
                      <span>LEGENDARY WHALE</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">50M</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+0.06%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('whale-info-body'),
                      classes('color-whale-killer')
                    )}>
                    <div className={classes('whale-info-body-badge')}>
                      <img src={badge.whale.killer} alt="badge whale" />
                      <span>KILLER WHALE</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">30M</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+0.04%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('whale-info-body'),
                      classes('color-whale-normal')
                    )}>
                    <div className={classes('whale-info-body-badge')}>
                      <img src={badge.whale.normal} alt="badge whale" />
                      <span>WHALE</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">10M</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+0.02%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('whale-info-body'),
                      classes('color-whale-baby')
                    )}>
                    <div className={classes('whale-info-body-badge')}>
                      <img src={badge.whale.baby} alt="badge whale" />
                      <span>BABY WHALE</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">5M</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">+0.01%</p>
                    </div>
                  </div>
                  <div
                    className={clsx(
                      classes('whale-info-body'),
                      classes('color-dolphin')
                    )}>
                    <div className={classes('whale-info-body-badge')}>
                      <img src={badge.whale.dolphin} alt="badge dolphin" />
                      <span>DOLPHIN</span>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">1M</p>
                    </div>
                    <div>
                      <p className="text-body-1 mb-0">0.005%</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="d-none d-xl-block ml-3 ml-md-4">
            <p className="text-body-1 text-secondary">
              {context.t('Earn BTZ')}
            </p>
            <APButton onClick={handleShare} customClass={classes('level')}>
              {context.t('Share')}
            </APButton>
          </div>
        </div>
        <div className="d-flex flex-column flex-xl-row">
          <div className="d-block d-xl-none mt-4">
            <p className="text-body-1 text-secondary">
              {context.t('Earn BTZ')}
            </p>
            <APButton onClick={handleShare} customClass={classes('level')}>
              {context.t('Share')}
            </APButton>
          </div>
          <BTZDivider className="d-none d-xl-block" />
          <div className="d-none d-xl-block">
            <p className="text-body-1 text-secondary">
              <br />
            </p>
            <p className="text-header-3">
              <APButton
                disabled={
                  btzLevel.btzToClaim === '-' || btzLevel.btzToClaim === 0
                }
                onClick={claimedBTZ}
                customClass={classes('level')}>
                {context.t('Claim BTZ')}
              </APButton>
            </p>
          </div>
          <BTZDivider className="d-xl-none" />
          <div className="ml-xl-5">
            <p className="text-body-1 text-secondary">
              {context.t('BTZ to claim')}
              <span className="d-inline-block d-xl-none ml-3">
                <APButton
                  disabled={
                    btzLevel.btzToClaim === '-' || btzLevel.btzToClaim === 0
                  }
                  onClick={claimedBTZ}
                  customClass={classes('level')}>
                  {context.t('Claim BTZ')}
                </APButton>
              </span>
              <span className="d-block d-xl-none float-right text-body-2 text-secondary">
                {context.t('Last updated')}: <br />
                {btzLevel.lastUpdatedClaim &&
                  formatDateTime(btzLevel.lastUpdatedClaim)}
              </span>
            </p>
            <p className="text-header-3 d-inline-block">
              {' '}
              {formatNumberToLocale(btzLevel.btzToClaim, 2)}
            </p>
            <span
              className={clsx(
                'd-inline-block ml-2',
                classes('btz-to-claim-tooltip')
              )}>
              <SVG src={infoIcon} />
              <div className="tooltip-body">
                <div className="d-flex justify-content-between">
                  <p>{context.t('Accumulation:')}</p>
                  <p className="text-right">
                    {formatNumberToLocale(btzLevel.accumulation, 2)}
                  </p>
                </div>
                <div className="d-flex justify-content-between">
                  <p>{context.t('Referral:')}</p>
                  <p className="text-right">
                    {formatNumberToLocale(btzLevel.referral, 2)}
                  </p>
                </div>
                <div className="d-flex justify-content-between">
                  <p>{context.t('BTZ Discount Back:')}</p>
                  <p className="text-right">
                    {formatNumberToLocale(btzLevel.btzDiscountBack, 2)}
                  </p>
                </div>
                <div className="d-flex justify-content-between">
                  <p>{context.t('Rewards:')}</p>
                  <p className="text-right">
                    {formatNumberToLocale(btzLevel.rewards, 2)}
                  </p>
                </div>
                <div className="d-flex justify-content-between">
                  <p>{context.t('Reward KYC:')}</p>
                  <p className="text-right">
                    {formatNumberToLocale(btzLevel.rewardKYC, 2)}
                  </p>
                </div>
                <div className="d-flex justify-content-between mt-2 font-weight-bold">
                  <p>{context.t('Total:')}</p>
                  <p className="text-right">
                    {formatNumberToLocale(btzLevel.btzToClaim, 2)}
                  </p>
                </div>
              </div>
            </span>

            <p className="d-none d-xl-block text-body-2 text-secondary">
              {context.t('Last updated')}: <br />
              {formatDateTime(btzLevel.lastUpdatedClaim)}
            </p>
          </div>
          <BTZDivider className="d-xl-none" />
          <div className="ml-xl-5">
            <p className="text-body-1 text-secondary">
              {context.t('BTZ Mined')}
            </p>
            <p className="text-header-3">{localAmount}</p>
          </div>
          <BTZDivider className="d-xl-none" />
          <div className="ml-xl-5">
            <p className="text-body-1 text-secondary">
              {context.t('Global Rank')}
            </p>
            <p className="text-header-3">{btzLevel.rank}</p>
          </div>
        </div>
      </BTZPanel>
      <BTZPanel>
        <p className="text-body-1 text-secondary">{context.t('Your Rank')}</p>
        <div className="d-flex justify-content-between">
          <img
            className={classes(
              'ic-bdg',
              curentLevel.subLevelValue === 4 ? 'active' : ''
            )}
            src={
              curentLevel.subLevelValue <= 4
                ? curentLevel.imgs.icBdgIVActive
                : bdgImg.icBdgIVActive
            }
            alt="ic bdg IV active"
          />
          <img
            className={classes(
              'ic-bdg',
              curentLevel.subLevelValue === 3 ? 'active' : ''
            )}
            src={
              curentLevel.subLevelValue <= 3
                ? curentLevel.imgs.icBdgIIIActive
                : bdgImg.icBdgIIIActive
            }
            alt="ic bdg III active"
          />
          <img
            className={classes(
              'ic-bdg',
              curentLevel.subLevelValue === 2 ? 'active' : ''
            )}
            src={
              curentLevel.subLevelValue <= 2
                ? curentLevel.imgs.icBdgIIActive
                : bdgImg.icBdgIIActive
            }
            alt="ic bdg II active"
          />
          <img
            className={classes(
              'ic-bdg',
              curentLevel.subLevelValue === 1 ? 'active' : ''
            )}
            src={
              curentLevel.subLevelValue <= 1
                ? curentLevel.imgs.icBdgIActive
                : bdgImg.icBdgIActive
            }
            alt="ic bdg I active"
          />
          {curentLevel.title !== 'emerald' && (
            <img
              className={classes('ic-bdg')}
              src={bdgImg.icBdgPromoteActive}
              alt="ic bdg promote active"
            />
          )}
        </div>
        <div className={classes('progress')}>
          <span
            className={clsx('line', classes(`bg-${curentLevel.title}`))}
            style={{ width: `${curentLevel.progress}%` }}
          />
        </div>
        <div className="d-flex justify-content-between">
          {curentLevel.subLevel.map((item) => (
            <p
              key={item}
              className={clsx(
                'text-body-1 text-secondary',
                classes('ic-bdg-amount')
              )}>
              {item !== 1000 ? `${item}K` : '1M+'}
            </p>
          ))}
        </div>
      </BTZPanel>
      <div ref={cardRankBlockEle} className={classes('card-rank-block')}>
        <div
          id="bronze"
          onMouseEnter={() => handleHoverCard('bronze')}
          onMouseLeave={handleLeaveHover}
          className={clsx(
            classes('card-rank-bronze'),
            curentLevel.title === 'bronze' ? 'active' : ''
          )}>
          <img src={rankImg.bronze} alt="card rank bronze" />
          <div className="head-card">
            <p className="text-body-1 text-secondary">1K</p>
          </div>
          <div className="boby-card">
            <p className={clsx('text-lead', classes('color-bronze'))}>BRONZE</p>
          </div>
          <div className="foot-card">{context.t('CURRENT RANK')}</div>
        </div>
        <div
          id="silver"
          onMouseEnter={() => handleHoverCard('silver')}
          onMouseLeave={handleLeaveHover}
          className={clsx(
            classes('card-rank-silver'),
            curentLevel.title === 'silver' ? 'active' : ''
          )}>
          <img src={rankImg.silver} alt="card rank silver" />
          <div className="head-card">
            <p className="text-body-1 text-secondary">5K</p>
            {['bronze'].includes(curentLevel.title) && (
              <img src={lockImg.silver} alt="lock Img silver" />
            )}
          </div>
          <div className="boby-card">
            <p className={clsx('text-lead', classes('color-silver'))}>SILVER</p>
            <p className="text-body-2 text-secondary">Rewards</p>
            <p className="text-body-2 text-secondary">IV</p>
          </div>
          <div className="foot-card">{context.t('CURRENT RANK')}</div>
        </div>
        <div
          id="gold"
          onMouseEnter={() => handleHoverCard('gold')}
          onMouseLeave={handleLeaveHover}
          className={clsx(
            classes('card-rank-gold'),
            curentLevel.title === 'gold' ? 'active' : ''
          )}>
          <img src={rankImg.gold} alt="card rank gold" />
          <div className="head-card">
            <p className="text-body-1 text-secondary">15K</p>
            {['bronze', 'silver'].includes(curentLevel.title) && (
              <img src={lockImg.gold} alt="lock Img gold" />
            )}
          </div>
          <div className="boby-card">
            <p className={clsx('text-lead', classes('color-gold'))}>GOLD</p>
            <p className="text-body-2 text-secondary">Rewards</p>
            <p className="text-body-2 text-secondary">II</p>
            <p className="text-body-2 text-secondary">IV</p>
          </div>
          <div className="foot-card">{context.t('CURRENT RANK')}</div>
        </div>
        <div
          id="platinum"
          onMouseEnter={() => handleHoverCard('platinum')}
          onMouseLeave={handleLeaveHover}
          className={clsx(
            classes('card-rank-platinum'),
            curentLevel.title === 'platinum' ? 'active' : ''
          )}>
          <img src={rankImg.platinum} alt="card rank platinum" />
          <div className="head-card">
            <p className="text-body-1 text-secondary">50K</p>
            {['bronze', 'silver', 'gold'].includes(curentLevel.title) && (
              <img src={lockImg.platinum} alt="lock Img platinum" />
            )}
          </div>
          <div className="boby-card">
            <p className={clsx('text-lead', classes('color-platinum'))}>
              PLATINUM
            </p>
            <p className="text-body-2 text-secondary">Rewards</p>
            <p className="text-body-2 text-secondary">I</p>
            <p className="text-body-2 text-secondary">II</p>
            <p className="text-body-2 text-secondary">IV</p>
          </div>
          <div className="foot-card">{context.t('CURRENT RANK')}</div>
        </div>
        <div
          id="diamond"
          onMouseEnter={() => handleHoverCard('diamond')}
          onMouseLeave={handleLeaveHover}
          className={clsx(
            classes('card-rank-diamond'),
            curentLevel.title === 'diamond' ? 'active' : ''
          )}>
          <img src={rankImg.diamond} alt="card rank diamond" />
          <div className="head-card">
            <p className="text-body-1 text-secondary">150K</p>
            {['bronze', 'silver', 'gold', 'platinum'].includes(
              curentLevel.title
            ) && <img src={lockImg.diamond} alt="lock Img diamond" />}
          </div>
          <div className="boby-card">
            <p className={clsx('text-lead', classes('color-diamond'))}>
              DIAMOND
            </p>
            <p className="text-body-2 text-secondary">Rewards</p>
            <p className="text-body-2 text-secondary">I</p>
            <p className="text-body-2 text-secondary">II</p>
            <p className="text-body-2 text-secondary">III</p>
            <p className="text-body-2 text-secondary">IV</p>
          </div>
          <div className="foot-card">{context.t('CURRENT RANK')}</div>
        </div>
        <div
          id="emerald"
          onMouseEnter={() => handleHoverCard('emerald')}
          onMouseLeave={handleLeaveHover}
          className={clsx(
            classes('card-rank-emerald'),
            curentLevel.title === 'emerald' ? 'active' : ''
          )}>
          <img src={rankImg.emerald} alt="card rank emerald" />
          <div className="head-card">
            <p className="text-body-1 text-secondary">300K</p>
            {['bronze', 'silver', 'gold', 'platinum', 'diamond'].includes(
              curentLevel.title
            ) && <img src={lockImg.emerald} alt="lock Img emerald" />}
          </div>
          <div className="boby-card">
            <p className={clsx('text-lead', classes('color-emerald'))}>
              EMERALD
            </p>
            <p className="text-body-2 text-secondary">Rewards</p>
            <p className="text-body-2 text-secondary">I</p>
            <p className="text-body-2 text-secondary">II</p>
            <p className="text-body-2 text-secondary">III</p>
            <p className="text-body-2 text-secondary">IV</p>
          </div>
          <div className="foot-card">{context.t('CURRENT RANK')}</div>
        </div>
      </div>
      <BTZPanel
        className={clsx(
          classes('info-rank'),
          classes(`info-rank-${displayLevelInfo.title}`)
        )}>
        <p className="text-lead mt-3 mb-4">
          <span className={classes(`color-${displayLevelInfo.title}`)}>
            {displayLevelInfo.title.toUpperCase()}
          </span>{' '}
          {context.t('BENEFITS')}
        </p>
        <BTZDivider className="d-none d-lg-block" />
        <div className="d-flex flex-column flex-lg-row ">
          <div className="flex-grow-1 mr-3">
            <p className="text-body-1 text-secondary">
              {context.t('BTZ Accumulation Rate')}
              <span
                className={clsx('ml-2', classes('tooltip'))}
                tooltip={context.t(
                  'Receive BTZ tokens (according to your tier) for every 1000 baht traded'
                )}
                tooltip-position="top">
                <SVG src={infoIcon} />
              </span>
            </p>
            <p className="text-lead m-0">
              {percentFormat(displayLevelInfo.accumulationRate)}
            </p>
          </div>
          <BTZDivider className="d-lg-block" />
          <div className="flex-grow-1 mr-3">
            <p className="text-body-1 text-secondary">
              {context.t('BTZ Trading Fee Discount')}
            </p>
            <p className="text-lead m-0">
              {percentFormat(displayLevelInfo.tradingFeeDiscount)}
            </p>
          </div>
          <BTZDivider className="d-lg-none" />
          <div className="flex-grow-1 mr-3">
            <p className="text-body-1 text-secondary">
              {context.t('Referral Income Share')}
            </p>
            <p className="text-lead m-0">
              {percentFormat(displayLevelInfo.referralIncomeRate)}
            </p>
          </div>
          <BTZDivider className="d-lg-none" />
          <div className="flex-grow-1 mr-3">
            <p className="text-body-1 text-secondary">
              {context.t('Dedicated VIP Call Support')}
            </p>
            <p className="text-lead m-0">
              {displayLevelInfo.dedicatedVIPCallSupport}
            </p>
          </div>
          <BTZDivider className="d-lg-none" />
          <div className="flex-grow-1 mr-3">
            <p className="text-body-1 text-secondary">
              {context.t('Vote Count')}
            </p>
            <p className="text-lead m-0">{displayLevelInfo.voteCount}</p>
          </div>
          <BTZDivider className="d-lg-none" />
          <div className="flex-grow-1 mr-3">
            <p className="text-body-1 text-secondary">
              {context.t('Personal Broker')}
            </p>
            <p className="text-lead m-0">{displayLevelInfo.personalBroker}</p>
          </div>
        </div>
      </BTZPanel>
    </div>
  );
};

const mapStateToProps = (state) => {
  const { rawAmount } = getProductByIdSelector(state, 8);
  const localAmount = formatNumberToLocale(rawAmount, 2);
  const { loyaltyOMSEnabled, loyaltyOMSProducts } = loyaltyTokenSelector(state);
  const currentAccount = currentAccountloyaltyTokenSelector(state);

  const loyaltyProductId =
    currentAccount && currentAccount.LoyaltyEnabled
      ? currentAccount.LoyaltyProductId
      : 0;
  const usedBTZ = loyaltyProductId === 8;
  const amountK = rawAmount ? rawAmount / 1000 : 0;
  const curentLevel = getCurentLevel(amountK);

  return {
    rawAmount,
    localAmount,
    loyaltyOMSEnabled,
    loyaltyOMSProducts,
    btzLevel: state.btzLevel,
    affiliate: state.affiliate,
    usedBTZ,
    curentLevel
  };
};

const mapDispatchToProps = {
  updateAccount,
  claimedBTZ
};

LoyaltyTokenPage.contextTypes = {
  t: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoyaltyTokenPage);

import React from 'react';
import './KYCSettingsComponent.css';
import './KYCFormSidePaneComponent.css';

const KYCSettingsComponent = props => {
  const { LevelsComponent } = props;

  return <LevelsComponent />;
};

export default KYCSettingsComponent;

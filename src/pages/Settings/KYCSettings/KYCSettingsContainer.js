import { connect } from 'react-redux';
import KYCSettingsComponent from './KYCSettingsComponent';

import KYC_IMContainer from '../../../components/KYC_IM';

const mapStateToProps = () => {
  const LevelsComponent = KYC_IMContainer;

  return {
    LevelsComponent
  };
};

const KYCSettingsContainer = connect(mapStateToProps)(KYCSettingsComponent);

export default KYCSettingsContainer;

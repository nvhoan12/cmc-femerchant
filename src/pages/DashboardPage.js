import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PageSubHeaderLayout from '../layouts/PageSubHeaderLayout/PageSubHeaderLayout';
import PageFooterLayout from '../layouts/PageFooterLayout/PageFooterLayout';
import BalancesListContainer from 'apex-web/lib/components/BalancesList';
import MarketOverviewComponent from '../components/MarketOverview';
import RecentActivityContainer from '../components/RecentActivity/RecentActivityContainer';
import TransferRequestNotificationList from 'apex-web/lib/components/TransferRequestsNotification/';
import VerificationRequiredAtDashboard from '../components/VerificationRequiredAtDashboard/VerificationRequiredAtDashboard';
import { getBEMClasses } from '../helpers/cssClassesHelper';
import './DashboardPage.css';

const layoutClasses = getBEMClasses('dashboard-page');

const DashboardPage = (props, context) => {
  return (
    <React.Fragment>
      <div className={layoutClasses('accent-bar')} />
      <PageSubHeaderLayout />
      <div className={layoutClasses()}>
        <div className={layoutClasses('notification-row')}>
          <VerificationRequiredAtDashboard
            TransferRequestNotificationList={TransferRequestNotificationList}
          />
        </div>
        <div className={layoutClasses('market-overview-container')}>
          <div className={layoutClasses('market-overview-container-left')}>
            <div className={layoutClasses('market-header')}>
              {context.t('Markets Overview')}
            </div>
            <MarketOverviewComponent />
          </div>
          <div className={layoutClasses('market-overview-container-right')}>
            <div className={layoutClasses('balances-header')}>
              {context.t('Balances')}
            </div>
            <BalancesListContainer />
          </div>
        </div>
        <div className={layoutClasses('recent-activity-container')}>
          <RecentActivityContainer filterForSelected={false} />
        </div>
      </div>
      <div className={layoutClasses('footer')}>
        <PageFooterLayout />
      </div>
    </React.Fragment>
  );
};

DashboardPage.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  userConfig: state.user.userConfig
});

export default connect(mapStateToProps)(DashboardPage);

import React from 'react';
import InstrumentSelectorContainer from '../../components/InstrumentSelector/InstrumentSelectorContainer';
import InstrumentTableContainer from 'apex-web/lib/components/InstrumentTable/InstrumentTableContainer';
import INSTRUMENT_TABLE_ITEMS from 'apex-web/lib/components/InstrumentTable/InstrumentTableItems';
import { getBEMClasses } from '../../helpers/cssClassesHelper';

import '../../components/InstrumentRow/InstrumentRowComponent.css';

const baseClasses = getBEMClasses('instrument-row');

const InstrumentRowComponent = () => {
  return (
    <div className={baseClasses()}>
      <InstrumentSelectorContainer />
      <InstrumentTableContainer />
    </div>
  );
};

export default InstrumentRowComponent;

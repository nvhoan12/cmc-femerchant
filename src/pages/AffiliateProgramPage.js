import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { formatDateTime } from 'apex-web/lib/helpers/dateHelper';
import config from '../config';
import BTZPanel from '../components/common/BTZPanel/BTZPanel';
import BTZDivider from '../components/common/BTZDivider/BTZDivider';
import BTZSpinner from '../components/common/BTZSpinner/BTZSpinner';
import BTZButton from '../components/common/BTZButton/BTZButton';
import { getBEMClasses } from '../helpers/cssClassesHelper';
import { getlocalLanguage } from '../helpers/localLanguage';
import { selectIcBdgImg } from './LoyaltyTokenPageData';
import './AffiliateProgramPage.css';

const classes = getBEMClasses('affiliate');

const AffiliateBlock = ({ data }, context) => {
  const [open, setOpen] = React.useState(false);
  const handleToggle = () => setOpen(!open);

  return (
    <div className={clsx('mb-4', classes('affiliate-block'))}>
      <BTZPanel
        onClick={handleToggle}
        className="d-flex justify-content-between align-items-center mb-0 tags-and-image-container">
        <div className="d-flex align-items-center">
          <img
            className={classes('rank-medal')}
            src={data.img.medal}
            alt={data.rank}
          />
          <div className="d-inline-block tag-and-comm-container">
            <p className="text-body-1 font-weight-bolder">
              {data.commissionEarned}{' '}
              <span className="text-secondary">BTZ</span>
            </p>
            <p className="text-body-1 text-secondary">{data.tag}</p>
          </div>
        </div>
        <i
          className={clsx(
            'fa fa-lg fa-2x',
            open ? 'fa-angle-up' : 'fa-angle-down'
          )}
          aria-hidden="true"
        />
      </BTZPanel>
      {open && (
        <BTZPanel
          bg="secondary"
          className="d-flex flex-column flex-xl-row justify-content-xl-between mt-0">
          <div>
            <p className="text-body-1 text-secondary">
              {context.t('Commissions Earned')}
            </p>
            <p className="text-lead">
              {data.commissionEarned}{' '}
              <span className="text-secondary">BTZ</span>
            </p>
          </div>
          <BTZDivider dark className="d-xl-none" />
          <div>
            <p className="text-body-1 text-secondary">{context.t('Email')}</p>
            <p className="text-lead">{data.email}</p>
          </div>
          <BTZDivider dark className="d-xl-none" />
          <div>
            <p className="text-body-1 text-secondary">{context.t('Ranked')}</p>
            <p className="text-lead">{data.level}</p>
          </div>
          <BTZDivider dark className="d-xl-none" />
          <div>
            <p className="text-body-1 text-secondary">
              {context.t('Friend Accepted Date')}
            </p>
            <p className="text-lead">{data.acceptedDate}</p>
          </div>
        </BTZPanel>
      )}
    </div>
  );
};

AffiliateBlock.contextTypes = {
  t: PropTypes.func.isRequired
};

const AffiliateProgramPage = (props, context) => {
  const [loading, setLoading] = React.useState(true);
  const [affiliates, setAffiliates] = React.useState({
    commissionRate: '-',
    totalCommissionEarned: '-',
    referralCode: '-',
    items: []
  });
  const affiliateLink = `https://bitazza.com/signup?aff=${
    affiliates.referralCode
  }`;

  React.useEffect(
    () => {
      (async () => {
        const res = await fetch(config.services.getAffiliates, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'X-BTZ-TOKEN': props.token
          }
        });
        if (res.status === 200) {
          const jsonRes = await res.json();
          setLoading(false);
          setAffiliates({
            commissionRate: jsonRes.commission_rate.text,
            totalCommissionEarned: formatNumberToLocale(
              jsonRes.total_commission_earned,
              2
            ),
            referralCode: jsonRes.referral_code,
            items: jsonRes.affiliates.items
              .sort((a, b) => b.commission_earned - a.commission_earned)
              .map(item => ({
                commissionEarned: formatNumberToLocale(
                  item.commission_earned,
                  2
                ),
                level: item.level,
                email: item.email,
                acceptedDate: formatDateTime(item.accepted_date),
                tag: item.username,
                img: selectIcBdgImg[item.level.toLowerCase()]
              }))
          });
        }
      })();
    },
    [props.user.userInfo.UserId]
  );

  const handleClipboard = async () => {
    await navigator.clipboard.writeText(affiliateLink);
    props.showSnack(
      context.t('The referral link has been copied to the clipboard.')
    );
  };

  const handleShare = () => {
    const language = getlocalLanguage();
    window.open(
      `https://share.bitazza.com/${language}/?kolid=${
        props.affiliate.affiliateTag
      }`,
      '_blank'
    );
  };

  return (
    <div className={classes()}>
      <p className="text-header-1">{context.t('Referral Program')}</p>
      <div className="d-flex flex-column flex-lg-row">
        <BTZPanel className="flex-grow-1">
          <p className="text-header-2">
            <span className="text-primary">
              {context.t('Invite your friends.')}
            </span>{' '}
            {context.t('Earn up to 40%')}
          </p>
          <p className="text-body-1 text-secondary">
            {context.t(
              'Earn up to 40% commision every time your friends make a trade on Bitazza.'
            )}
          </p>
          <p className="text-body-1 mt-5">
            {context.t('Use your referral code')}
          </p>
          <div className={classes('share-box')}>
            <div className={classes('copy-box')}>
              <p className="text-body-1 font-weight-bold px-4 m-0">
                <BTZSpinner loading={loading}>{affiliateLink}</BTZSpinner>
              </p>
              <BTZButton
                onClick={handleClipboard}
                disabled={loading}
                variant="outline"
                className={classes('copy-custom')}
                size="large">
                {context.t('Copy')}
              </BTZButton>
            </div>
            <BTZButton
              onClick={handleShare}
              disabled={loading}
              className="mt-3 font-weight-bold"
              size="large"
              fullWidth>
              {context.t('Share on Social Media')}
            </BTZButton>
          </div>
        </BTZPanel>
        {affiliates.items.length > 0 && (
          <BTZPanel className={clsx('flex-grow-1', classes('right-panel'))}>
            <p className="text-body-1 text-secondary">
              {context.t('Base Commission Rate')}
            </p>
            <p className="text-header-2">{affiliates.commissionRate}</p>
            <BTZDivider className={classes('right-panel', 'divider')} />
            <p className="text-body-1 text-secondary">
              {context.t('Commissions Earned')}
            </p>
            <p className="text-header-2">
              {affiliates.totalCommissionEarned} BTZ
            </p>
            <BTZDivider
              className={clsx('d-lg-none', classes('right-panel', 'divider'))}
            />
          </BTZPanel>
        )}
      </div>
      <BTZPanel bg="no">
        <BTZSpinner loading={loading}>
          {affiliates.items.length > 0 ? (
            <p className="text-body-1 text-secondary mt-4 mb-3">
              {context.t('Your Affiliates ({x})', {
                x: affiliates.items.length
              })}
            </p>
          ) : (
            <p className="text-body-1 text-secondary mt-4 mb-3 text-center">
              {context.t(
                'You don’t have any affiliates yet Earn commission by inviting your friends'
              )}
            </p>
          )}
        </BTZSpinner>
      </BTZPanel>
      <div className={classes('affiliate-block-container')}>
        {affiliates.items.map((item, index) => (
          <AffiliateBlock key={index} data={item} />
        ))}
      </div>
    </div>
  );
};

AffiliateProgramPage.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    user: state.user,
    token: state.auth.token,
    affiliate: state.affiliate
  };
};

const mapDispatchToProps = dispatch => ({
  showSnack: text =>
    dispatch(showSnack({ id: 'affiliateCopyTage', text, type: 'success' }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AffiliateProgramPage);

import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import config from '../config';
import { getBEMClasses } from '../helpers/cssClassesHelper';
import { getlocalLanguage } from '../helpers/localLanguage';
import PublicPageContainer from '../components/PublicPage/PublicPageContainer';
import {
  TextBlockHeader,
  TextBlockSubHeader,
  TextLead,
  Text
} from '../components/PublicPage/Text';
import Jumbotron from '../components/PublicPage/Jumbotron';
import Announcement from '../components/PublicPage/Announcement';
import { Btn } from '../components/PublicPage/Btn';
import Divider from '../components/PublicPage/Divider';
import FAQBlock from '../components/PublicPage/FAQBlock';
import ChartBlock from '../components/PublicPage/ChartBlock';
import Footer from '../components/PublicPage/Footer';
import WaitlistPhase2 from '../components/PublicPage/WaitlistPhase2';
import NotifySingaporean from '../components/notification/NotifySingaporean';
import homepageBannerImg from '../images/homepage-banner.jpg';
import homepageWhyUsBgImg from '../images/homepage-why-us-bg.jpg';
import bitazzaLogoHorizontalDarkImg from '../images/bitazza-logo-horizontal-dark.svg';
import bitazzaIconGradientImg from '../images/bitazza-icon-gradient.svg';
import appStoreImg from '../images/download-app-store.png';
import playStoreImg from '../images/download-play-store.png';
import { whyBitazza, whatAwesome, whatIsBtz, faq } from './LandingPageData';
import './LandingPage.css';

const landingPageClasses = getBEMClasses('landing-page');

const settings = {
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 2500,
  speed: 500,
  className: landingPageClasses('slick'),
  slidesToScroll: 1,
  slidesToShow: 4,
  responsive: [
    {
      breakpoint: 1024 + 1,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 768 + 1,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480 + 1,
      settings: {
        slidesToShow: 1
      }
    }
  ]
};

const LandingPage = (props, context) => {
  const language = getlocalLanguage();
  return (
    <PublicPageContainer>
      <Jumbotron
        backgrandImg={homepageBannerImg}
        className={landingPageClasses('jumbotron')}>
        <div className="row d-flex align-items-center">
          <div className="col-lg-6 order-2 order-lg-1">
            <TextBlockSubHeader
              className="text-center text-lg-left"
              color="primary"
              text={context.t(
                'Discover the next-level digital asset trading experience on a regulated platform.'
              )}
            />
            <TextLead
              className="text-center text-lg-left"
              color="secondary"
              text={context.t(
                "Activate your Digital Wallet now and buy-sell Crypto on Southeast Asia's premier digital asset platform."
              )}
            />
            <div className="d-flex justify-content-center justify-content-lg-start">
              <Btn className="mt-5 lets-go-btn" router href="/signup">
                {context.t('Let’s go')}
              </Btn>
            </div>
          </div>
          <div className="col-lg-6 my-5 order-1 order-lg-2">
            <Announcement />
          </div>
        </div>
      </Jumbotron>
      <Jumbotron backgrandImg={homepageWhyUsBgImg}>
        <div className={landingPageClasses('block-overlap')}>
          <div className={landingPageClasses('chart')}>
            <Slider {...settings}>
              <div className="px-2">
                <ChartBlock symbol="BTCTHB" />
              </div>
              <div className="px-2">
                <ChartBlock symbol="ETHTHB" />
              </div>
              <div className="px-2">
                <ChartBlock symbol="XRPTHB" />
              </div>
              <div className="px-2">
                <ChartBlock symbol="XLMTHB" />
              </div>
              <div className="px-2">
                <ChartBlock symbol="BTCUSDT" />
              </div>
              <div className="px-2">
                <ChartBlock symbol="ETHUSDT" />
              </div>
              {/* <div className="px-2">
                <ChartBlock symbol="DAITHB" />
              </div> */}
              <div className="px-2">
                <ChartBlock symbol="COMPTHB" />
              </div>
            </Slider>
          </div>
          <div className={landingPageClasses('announce')}>
            <TextLead
              align="center"
              text={
                <React.Fragment>
                  <img
                    className="logo"
                    src={bitazzaIconGradientImg}
                    alt="logo"
                  />
                  {context.t(
                    'Bitazza now supports trading for BTC, ETH, XRP, XLM, USDT, DAI, COMP, YFI, OMG, BNT, LINK, SNX and REN. More to come soon!'
                  )}
                </React.Fragment>
              }
            />
          </div>
          <TextBlockHeader
            color="black"
            align="center"
            text={
              <React.Fragment>
                {context.t('Why')}{' '}
                <img
                  className={landingPageClasses('heading-logo')}
                  src={bitazzaLogoHorizontalDarkImg}
                  alt="logo"
                />{' '}
                ?
              </React.Fragment>
            }
          />
          <Divider className="mb-5" />
          <div className="row">
            {whyBitazza(context).map((item, i) => (
              <div key={i} className="col-xl-6">
                <div className={landingPageClasses('why-bitazza-block')}>
                  <img className="logo" src={item.img} alt={item.title} />
                  <div className="text">
                    <TextBlockSubHeader color="black" text={item.title} />
                    <TextLead color="black" text={item.des} />
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </Jumbotron>
      <Jumbotron>
        <TextBlockHeader
          align="center"
          text={context.t('Here’s what makes bitazza awesome')}
        />
        <div className="row">
          {whatAwesome(context).map((item, i) => (
            <div key={i} className="col-lg-4 col-md-6 pb-5 mb-5">
              <div className="px-md-5 text-center">
                <img
                  src={item.img}
                  className="img-fluid why-bitazza-logos"
                  alt="what awesome img"
                />
                <TextBlockSubHeader align="center" text={item.title} />
                <TextLead align="center" color="secondary" text={item.des} />
              </div>
            </div>
          ))}
        </div>
      </Jumbotron>
      <Jumbotron className={landingPageClasses('what-is-btz')}>
        <TextBlockHeader
          color="black"
          align="center"
          text={
            <React.Fragment>
              {language === 'th' ? 'BTZ' : context.t('What is')}{' '}
              <img
                className={landingPageClasses('heading-logo')}
                src={bitazzaIconGradientImg}
                alt="logo"
              />{' '}
              {language === 'th' ? context.t('What is') : 'BTZ'}
              {'?'}
            </React.Fragment>
          }
        />
        <Divider className="mb-5" />
        <TextLead
          align="center"
          color="black"
          text={context.t(
            'BTZ is fuel to Bitazza’s Platform. It is a utility token that allows users to pay for products and services on the platform at a discounted rate. BTZ will be ready to use once Bitazza goes live. Tokenholders enjoy many benefits including:'
          )}
        />
        <div className="row">
          {whatIsBtz(context).map((item, i) => (
            <div
              key={i}
              className="col-md-10 offset-md-1 col-lg-4 offset-lg-0 text-center mb-5">
              <div className="px-md-2">
                <img src={item.img} alt="what btz icon 1" />
                <TextBlockSubHeader
                  align="center"
                  color="black"
                  text={item.title}
                />
                <TextLead align="center" color="black" text={item.des} />
              </div>
            </div>
          ))}
        </div>
      </Jumbotron>
      <Jumbotron className={landingPageClasses('what-is-btz')}>
        <TextBlockHeader
          color="black"
          align="center"
          text={context.t('Bitazza Mobile Application. Freedom In Your Hands.')}
        />
        <TextBlockSubHeader
          align="center"
          color="black"
          text={context.t('DOWNLOAD TODAY!')}
        />
        <div className={landingPageClasses('app-download-container')}>
          <a
            href={config.global.appsStore}
            className={landingPageClasses('app-download')}>
            <img src={appStoreImg} alt="app store" />
          </a>
          <a
            href={config.global.googlePlay}
            className={landingPageClasses('app-download')}>
            <img src={playStoreImg} alt="play store" />
          </a>
        </div>
        <Divider className="mb-5" />
      </Jumbotron>
      <div className={landingPageClasses('iframe')}>
        <iframe
          title="owl.carousel"
          frameBorder="0"
          height="630px"
          width="100%"
          src="/vendor/owl.carousel/index.html"
        />
      </div>
      <Jumbotron className={landingPageClasses('faq')}>
        <TextBlockHeader color="black" align="center" text="FAQs" />
        <div className="row">
          {faq(context).map((item, i) => (
            <div key={i} className="col-lg-6 pt-3">
              <FAQBlock item={item} />
            </div>
          ))}
        </div>
        <Text
          className="mt-5 pt-5"
          color="publicPrimary"
          align="center"
          text={
            <React.Fragment>
              {context.t(
                'If you have any further queries, please kindly email'
              )}
              <Text
                span
                className="pl-2"
                color="publicPrimary"
                display="inline"
                fontBold
                text="hello@bitazza.com"
              />
            </React.Fragment>
          }
        />
      </Jumbotron>
      <Footer />
      <WaitlistPhase2 />
      <NotifySingaporean />
    </PublicPageContainer>
  );
};

LandingPage.contextTypes = {
  t: PropTypes.func.isRequired
};

export default LandingPage;

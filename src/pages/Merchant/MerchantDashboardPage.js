import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import PageFooterLayout from '../../layouts/PageFooterLayout/PageFooterLayout';
import withAuthentication from 'apex-web/lib/hocs/withAuthentication';
import BalancesListContainer from 'apex-web/lib/components/BalancesList';
import RecentActivityContainer from './components/RecentActivity/RecentActivityContainer';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import MerchantTransferBlock from './components/MerchantTransferBlock';

import './MerchantDashboardPage.css';

const layoutClasses = getBEMClasses('dashboard-page');

const DashboardPage = (props, context) => {
  useEffect(() => {
    document.title = 'Bitazza Partners';
  }, []);
  return (
    <React.Fragment>
      <div className={layoutClasses('accent-bar')} />
      <div className={layoutClasses()}>
        <div className={layoutClasses('merchant-overview-container')}>
          <div
            className={layoutClasses(
              'merchant-overview-container-left bg-dark'
            )}>
            <div className={layoutClasses('merchant-header')}>
              {context.t('Calculator')}
            </div>

            <MerchantTransferBlock />
          </div>
          <div className={layoutClasses('merchant-overview-container-right')}>
            <div className={layoutClasses('balances-header')}>
              {context.t('Balances')}
            </div>
            <BalancesListContainer />
          </div>
        </div>
        <div className={layoutClasses('recent-activity-container')}>
          <RecentActivityContainer filterForSelected={false} />
        </div>
      </div>
      <div className={layoutClasses('footer')}>
        <PageFooterLayout />
      </div>
    </React.Fragment>
  );
};

DashboardPage.contextTypes = {
  t: PropTypes.func.isRequired
};

export default withAuthentication(DashboardPage);

import React, { useRef, useState } from 'react';
import BigNumber from 'bignumber.js';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import destinationTagHelper from 'apex-web/lib/helpers/destinationTagHelper';
import copyToClipboard from 'apex-web/lib/helpers/clipboardHelper';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import BTZSpinner from '../../../components/common/BTZSpinner/BTZSpinner';
import { getRecentTrade } from '../../../redux/selectors/merchantSelectors';
import { decimalPlace } from './MerchantProductSelector';
import APButton from '../../../components/common/APButton';

import config from '../../../config';

import './MerchantModal.css';

const layoutClasses = getBEMClasses('merchant-modal');

const sendRequestForInvoice = async payload => {
  const url = config.services.generateInvoice;

  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': localStorage.getItem('token')
    },
    body: JSON.stringify(payload)
  });
};

const validateCryptoAddress = async (type, address) => {
  const url =
    config.services.validateCryptoAddress +
    '?type=' +
    type +
    '&address=' +
    address;
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': localStorage.getItem('token')
    }
  }).then(res => res.json());
  return response;
};

const MerchantModal = (
  {
    form,
    currentUser,
    instrument,
    depositInfo,
    transactionId,
    merchantProfile,
    onModalClose,
    recentPrice,
    dispatch
  },
  context
) => {
  const [loading, setLoading] = useState(false);
  const [buttonClicked, setButtonClicked] = useState('');

  const invoiceElement = useRef(null);
  const { values = {} } = form;

  const { side, to, from } = values;

  const selectedInstrumentId = instrument.selectedInstrumentId;
  const invoiceUrl = config.services.getInvoiceUrl(transactionId);

  const instrumentData = instrument.instruments.find(
    data => data.InstrumentId === selectedInstrumentId
  );

  if (!instrumentData) return null;

  let selectedAddress;

  if (depositInfo && depositInfo.length) {
    selectedAddress = depositInfo[depositInfo.length - 1];
  }

  const [address, destinationTag] = destinationTagHelper(selectedAddress);
  const symbol = instrumentData.Product1Symbol;

  const onSubmitError = () => {
    setLoading(false);
    dispatch(
      showSnack({
        id: 'GENERATE_INVOICE_ERROR',
        text: context.t(
          'There was an issue creating the invoice, please try again.'
        ),
        type: 'warning'
      })
    );
    onModalClose();
  };

  const onModalSubmit = async (isOpenLink = false) => {
    var isValidAddress = await validateCryptoAddress(
      symbol.toUpperCase(),
      address
    );
    if (side === 'external' && !isValidAddress.result) {
      onModalClose();
      dispatch(
        showSnack({
          id: 'INVALID_ADDRESS',
          text: context.t(
            'There was an issue while generating the invoice, please refresh the page.'
          )
        })
      );
      return false;
    }

    setLoading(true);
    const payload = {
      transactionId,
      instrumentId: selectedInstrumentId,
      merchantId: parseInt(merchantProfile.merchantId),
      accountId: parseInt(currentUser.AccountId),
      userId: parseInt(currentUser.UserId),
      productId: instrumentData.Product1,
      amount: BigNumber(from)
        .decimalPlaces(decimalPlace(instrumentData.Product1))
        .toNumber(),
      baht: to,
      side,
      price: recentPrice.price,
      username: merchantProfile.username,
      displayName: merchantProfile.displayName,
      address,
      destinationTag
    };

    console.log('payload', payload);

    try {
      const response = await sendRequestForInvoice(payload);
      setLoading(false);

      if (response.status === 200) {
        dispatch(
          showSnack({
            id: 'GENERATE_INVOICE_COMPLETE',
            text: context.t('The invoice has been generated.')
          })
        );

        if (isOpenLink) {
          window.open(invoiceUrl, '_blank');
        }
      } else {
        onSubmitError();
      }
    } catch (error) {
      console.log('error', error);
      onSubmitError();
    }
  };

  return (
    <div className={layoutClasses('container')}>
      <div ref={invoiceElement} className={layoutClasses('invoice-content')}>
        <div className={layoutClasses('row')}>
          <p className={layoutClasses('row-title')}>
            {context.t('Partner Name')}
          </p>
          <p className={layoutClasses('row-content')}>
            {merchantProfile.displayName}
          </p>
        </div>

        <div className={layoutClasses('row')}>
          <p className={layoutClasses('row-title')}>{context.t('Product')}</p>
          <p className={layoutClasses('row-content')}>{symbol}</p>
        </div>

        <div className={layoutClasses('row')}>
          <p className={layoutClasses('row-title')}>
            {context.t('Crypto Amount')}
          </p>
          <p className={layoutClasses('row-content')}>
            {formatNumberToLocale(from, symbol)} {symbol}
          </p>
        </div>

        <div className={layoutClasses('row')}>
          <p className={layoutClasses('row-title')}>
            {context.t('Total Baht')}
          </p>
          <p className={layoutClasses('row-content')}>
            {formatNumberToLocale(to, 'THB')} Baht
          </p>
        </div>

        <div className={layoutClasses('row')}>
          <p className={layoutClasses('row-title')}>
            {context.t('Transaction ID')}
          </p>
          <p className={layoutClasses('row-content')}>{transactionId}</p>
        </div>

        <div className={layoutClasses('row')}>
          <p className={layoutClasses('row-title')}>Bitazza ID</p>
          <p className={layoutClasses('row-content text-bold')}>
            @{merchantProfile.username}
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <button
            className={layoutClasses(`btn-link`)}
            onClick={() => {
              setButtonClicked('link');
              onModalSubmit(true);
            }}>
            {loading && buttonClicked === 'link' ? (
              <BTZSpinner loading={loading}>loading...</BTZSpinner>
            ) : (
              invoiceUrl
            )}
          </button>
        </div>
        <div className="col-12">
          <APButton
            onClick={() => {
              setButtonClicked('copy');
              copyToClipboard(invoiceUrl);
              onModalSubmit();
            }}
            className={layoutClasses(`btn-confirm`)}>
            {loading && buttonClicked === 'copy' ? (
              <BTZSpinner loading={loading}>loading...</BTZSpinner>
            ) : (
              context.t('Copy & Share')
            )}
          </APButton>
        </div>
      </div>
    </div>
  );
};

MerchantModal.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  recentPrice: getRecentTrade(state),
  form: state.form.merchant,
  currentUser: state.user.userInfo,
  instrument: state.instrument,
  depositInfo: state.deposit.depositInfo
});

export default connect(mapStateToProps)(MerchantModal);

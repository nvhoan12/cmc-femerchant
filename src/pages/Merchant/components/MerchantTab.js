import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { change } from 'redux-form';

import APIcon from '../../../components/common/APIcon';
import { clearMerchantForm } from '../../../redux/actions/merchantActions';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';

import './MerchantTab.css';

const baseClasses = getBEMClasses('buy-sell');
const iconClasses = getBEMClasses('merchant-tabs');

const MerchantTab = (props, context) => {
  const { clearForm, isInternal, setInternalValue, setExternalValue } = props;
  return (
    <div className={baseClasses('tab-wrapper')}>
      <div
        className={baseClasses('tab', {
          'buy-selected': isInternal
        })}
        onClick={() => {
          clearForm();
          setInternalValue();
        }}>
        <APIcon
          name="bitazza"
          classModifiers="small"
          customClass={iconClasses('icon')}
        />
        {context.t('BITAZZA WALLET')}
      </div>
      <div className={baseClasses('vertical-divider')} />
      <div
        className={baseClasses('tab', {
          'sell-selected': !isInternal
        })}
        onClick={() => {
          clearForm();
          setExternalValue();
        }}>
        {context.t('External Wallet')}
      </div>
    </div>
  );
};

MerchantTab.propTypes = {
  baseClasses: PropTypes.func.isRequired,
  clearForm: PropTypes.func.isRequired
};

MerchantTab.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return {
    formObj: state.form.merchant,
    isInternal:
      (state.form.merchant && state.form.merchant.values.side === 'internal') ||
      false,
    isExternal:
      (state.form.merchant && state.form.merchant.values.side === 'external') ||
      false
  };
};

const mapDispatchToProps = {
  setInternalValue: () => change('merchant', 'side', 'internal'),
  setExternalValue: () => change('merchant', 'side', 'external'),
  clearForm: clearMerchantForm
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantTab);

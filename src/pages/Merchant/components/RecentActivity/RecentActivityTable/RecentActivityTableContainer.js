import get from 'lodash/get';
import { connect } from 'react-redux';
import {
  getColumnsOfMerchant,
  receivedAmountCal
} from '../../../../../helpers/recentActivityHelper';
import RecentActivityTableComponent from './RecentActivityTableComponent';

const mapStateToProps = (state, ownProps) => {
  const content = state.merchantTransactions.map(item => {
    const product = state.apexCore.product.products.find(
      product => product.ProductId === item.product_id
    );
    const receivedAmount = receivedAmountCal(
      item.blocktrade_amount,
      item.blocktrade_price
    );
    const iconFileName = get(
      state.productManifest.manifest,
      [product.Product, 'iconFileName'],
      `${product.Product}DEFAULT_ICON`
    );

    return {
      Action: { params: { ProductType: '' } },
      ID: { primaryText: item.id },
      TransactionID: { primaryText: item.transaction_id },
      Product: { primaryText: product.Product, iconFileName },
      Username: { primaryText: `@${item.username}` },
      AmountCrypto: { Value: item.amount, ProductSymbol: product.Product },
      Amount: { Value: item.baht, ProductSymbol: 'THB' },
      ReceivedAmount: { Value: receivedAmount, ProductSymbol: 'THB' },
      Status: { Value: item.status },
      Side: { primaryText: item.side },
      TimeStamp: { raw: item.created_at }
    };
  });

  const columns = getColumnsOfMerchant(
    ownProps.width,
    ownProps.themeModifier,
    ownProps.context
  );

  return {
    columns,
    content,
    usePagination: true
  };
};

export default connect(mapStateToProps)(RecentActivityTableComponent);

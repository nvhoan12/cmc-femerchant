import React from 'react';
import Modal from 'apex-web/lib/components/common/Modal/Modal';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import APTable from 'apex-web/lib/components/common/APTable';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { formatDate, formatTime } from 'apex-web/lib/helpers/dateHelper';
import config from '../../../../../config';
import { invoiceStatusParser } from '../../../../../components/RecentActivity/ActivityCellComponents';

const baseClasses = getBEMClasses('merchant-activity');

const RecentActivityTableComponent = props => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [row, setRow] = React.useState({});
  const { columns, content, context, usePagination } = props;

  const handleClickRow = row => {
    setRow(row);
    setIsOpen(true);
  };

  return (
    <React.Fragment>
      <APTable
        {...{
          componentName: 'RecentActivityComponent',
          columns,
          pageSize: 6,
          minRow: 6,
          rows: content,
          onRowClicked: handleClickRow,
          baseClass: baseClasses,
          headerOutside: true,
          usePagination: usePagination,
          empty: context.t('No data is available')
        }}
      />
      {isOpen && (
        <Modal
          isOpen={isOpen}
          title={context.t('Transaction Details')}
          close={() => setIsOpen(false)}>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('ID')}</label>
            <span>{row.ID.primaryText}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Bitazza ID')}</label>
            <span>{row.Username.primaryText}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Transaction ID')}</label>
            <span>{row.TransactionID.primaryText}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Side')}</label>
            <span>{row.Side.primaryText}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Product')}</label>
            <span>{row.Product.primaryText}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Amount')}</label>
            <span>
              {`${formatNumberToLocale(
                row.AmountCrypto.Value,
                row.AmountCrypto.ProductSymbol
              )} ${row.AmountCrypto.ProductSymbol}`}
            </span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Invoice Amount')}</label>
            <span>
              {`${formatNumberToLocale(
                row.Amount.Value,
                row.Amount.ProductSymbol
              )} ${row.Amount.ProductSymbol}`}
            </span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Received Amount THB')}</label>
            <span>
              {`${formatNumberToLocale(
                row.ReceivedAmount.Value,
                row.ReceivedAmount.ProductSymbol
              )} ${row.ReceivedAmount.ProductSymbol}`}
            </span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Status')}</label>
            <span>{context.t(invoiceStatusParser(row.Status.Value).text)}</span>
          </div>
          <div className={baseClasses('modal-text')}>
            <label>{context.t('Invoice Date')}</label>
            <span>
              {`${formatTime(row.TimeStamp.raw)} ${formatDate(
                row.TimeStamp.raw
              )}`}
            </span>
          </div>
          <div className={baseClasses('modal-invoice')}>
            <span>
              <a
                rel="noopener noreferrer"
                target="_blank"
                href={config.global.invoice(row.TransactionID.primaryText)}>
                {config.global.invoice(row.TransactionID.primaryText)}
              </a>
            </span>
          </div>
        </Modal>
      )}
    </React.Fragment>
  );
};

export default RecentActivityTableComponent;

import React from 'react';
import PropTypes from 'prop-types';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import RecentActivityHeaderComponent from 'apex-web/lib/components/RecentActivity/RecentActivityHeader';
import RecentActivityTableComponent from './RecentActivityTable';

import './ActivityComponents.css';

const baseClasses = getBEMClasses('merchant-activity');

const RecentActivityComponent = (props, context) => {
  const { width, themeModifier } = props;

  React.useEffect(() => {
    props.startIntervalFetcht();
    return props.stopIntervalFetcht;
  }, []);

  return (
    <div className={baseClasses()}>
      <RecentActivityHeaderComponent />
      <RecentActivityTableComponent
        themeModifier={themeModifier}
        width={width}
        context={context}
      />
    </div>
  );
};

RecentActivityComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default RecentActivityComponent;

import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import RecentActivityComponent from './RecentActivityComponent';
import resize from 'apex-web/lib/hocs/resize';
import {
  startIntervalFetcht,
  stopIntervalFetcht
} from '../../../../redux/actions/merchantTransactionsActions';

const RecentActivityComponentForm = reduxForm({
  form: 'recent-activity',
  destroyOnUnmount: false,
  initialValues: {
    type: 'all',
    startDate: undefined,
    endDate: undefined
  }
})(RecentActivityComponent);

const mapDispatchToProps = {
  startIntervalFetcht,
  stopIntervalFetcht
};

export default connect(
  null,
  mapDispatchToProps
)(resize(RecentActivityComponentForm));

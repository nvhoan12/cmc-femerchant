import React from 'react';

import BigNumber from 'bignumber.js';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import PropTypes from 'prop-types';
import { InnerSelect } from '../../../components/common/APSelect';
import APNumberInput from '../../../components/common/APNumberInput';
import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import path from '../../../helpers/path';
import config from '../../../config';
import { getRecentTrade } from '../../../redux/selectors/merchantSelectors';
import { FIAT_PRODUCTS, CRYPTO_PRODUCTS } from './MerchantTransferBlock';
import './MerchantProductSelector.css';

const baseClasses = getBEMClasses('merchant-selector');

export const decimalPlace = (productId) => {
  if (productId === 1) return 6;
  if (productId === 2) return 5;
  if (productId === 3 || productId === 4) return 2;
  return 2;
};

const MerchantProductSelector = (
  {
    label,
    onProductSelect,
    inputName,
    selectOptions,
    productId,
    recentPrice,
    isDisabled = false,
    side,
    instrumentId,
    dispatch
  },
  context
) => {
  const products = [...CRYPTO_PRODUCTS, ...FIAT_PRODUCTS];

  const selectedProduct = products.find(({ id }) => id === productId);

  const handleQualityFromChange = (value) => {
    const newPrice = BigNumber(value)
      .times(recentPrice.price)
      .decimalPlaces(2);
    dispatch(change('merchant', 'to', newPrice));
  };

  const handleQuantityToChange = (value) => {
    const newPrice = BigNumber(value)
      .div(recentPrice.price)
      .decimalPlaces(8);

    const extraFee = config.global.merchant[side] || 0;
    const fee = newPrice.times(extraFee);

    const finalPrice = newPrice.plus(fee);

    dispatch(
      change(
        'merchant',
        'from',
        finalPrice.decimalPlaces(decimalPlace(instrumentId))
      )
    );
  };

  const handleChange =
    inputName === 'to' ? handleQuantityToChange : handleQualityFromChange;

  const matchedInstrumentId = () => {
    return recentPrice && recentPrice.id === instrumentId;
  };

  return (
    <div className={baseClasses('container')}>
      <div className={baseClasses('container-item')}>
        <div className={baseClasses('product-info')}>
          <img
            alt="Product icon"
            className="icon"
            src={
              path('/local/product-icons/') +
              (selectedProduct ? selectedProduct.icon : 'btc-48px.svg')
            }
          />
        </div>
        <InnerSelect
          label={context.t(label)}
          disabled={isDisabled}
          showTriangles={!isDisabled}
          customClass={baseClasses()}
          onSelect={onProductSelect}
          value={selectedProduct && selectedProduct.id}
          options={selectOptions.map((product) => ({
            value: product.id,
            label: product.symbol
          }))}
        />
      </div>

      <div className={baseClasses('container-item-input')}>
        <APNumberInput
          type="text"
          name={inputName}
          disabled={inputName === 'from' || !matchedInstrumentId()}
          placeholder="0"
          customClass={baseClasses()}
          // validate={[biggerThanZero]}
          decimalPlaces={decimalPlace(productId)}
          customChange={handleChange}
        />
      </div>
    </div>
  );
};

MerchantProductSelector.propTypes = {
  label: PropTypes.string.isRequired,
  onProductSelect: PropTypes.func.isRequired
};

MerchantProductSelector.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  recentPrice: getRecentTrade(state),
  side: state.form.merchant.values.side,
  instrumentId: state.instrument.selectedInstrumentId
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onProductSelect: (id) => dispatch(ownProps.onSelect(id)),
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantProductSelector);

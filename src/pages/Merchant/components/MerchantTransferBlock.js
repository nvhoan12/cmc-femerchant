import React, { useState, Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { customAlphabet } from 'nanoid';

import { openRetailFiatSidePane } from 'apex-web/lib/redux/actions/sidePaneActions';

import APCheckbox from '../../../components/common/APCheckbox';

import { getBEMClasses } from '../../../helpers/cssClassesHelper';
import Modal from '../../../components/common/Modal/Modal';
import MerchantTab from './MerchantTab';
import MerchantProductSelector from './MerchantProductSelector';
import MerchantModal from './MerchantModal';

import BTZSpinner from '../../../components/common/BTZSpinner/BTZSpinner';
import { onProductSelect } from '../../../redux/actions/merchantActions';
import { getRecentTrade } from '../../../redux/selectors/merchantSelectors';
import {
  isMerchantRole,
  getMerchantIdFromUserConfig
} from '../../../helpers/userConfigHelper';

import config from '../../../config';

import './MerchantTransferBlock.css';

const baseClasses = getBEMClasses('merchant');

const nanoid = customAlphabet('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', 10);

// id is instrumentId
// use for dispatch({ type: 'SELECT_INSTRUMENT' }) to get recentTrade price.
export const CRYPTO_PRODUCTS = config.global.merchant.instruments;

// for fiat id is product id.
export const FIAT_PRODUCTS = [
  {
    id: 0,
    symbol: 'THB',
    icon: 'thb-48px.svg'
  }
];

const MerchantTransferBlock = (
  {
    handleSubmit,
    baseSelectOptions,
    quoteSelectOptions,
    selectedInstrumentId,
    formObj,
    user,
    recentPrice,
    isDepositInfoLoading,
    token,
    dispatch
  },
  context
) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [tranId, setTranId] = useState(nanoid());
  const [merchantProfile, setMerchantProfile] = useState({
    displayName: '',
    username: '',
    merchantId: ''
  });
  useEffect(() => {
    // Set default instrument to BTC.
    const instrumentIdForBTC = 1;
    dispatch(onProductSelect(instrumentIdForBTC));
  }, []);

  useEffect(
    () => {
      if (user.userConfig) {
        const merchantId = getMerchantIdFromUserConfig(user.userConfig);
        (async () => {
          const res = await fetch(config.services.getPartnerInfo, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'X-BTZ-TOKEN': token
            }
          });
          if (res.status === 200) {
            const jsonRes = await res.json();

            setMerchantProfile({
              displayName: jsonRes.display_name,
              username: jsonRes.username,
              merchantId
            });
          }
        })();
      }
    },
    [user.userInfo.UserId]
  );

  const toggleModal = () => {
    setTranId(nanoid());

    if (isModalOpen) {
      dispatch(onProductSelect(selectedInstrumentId));
    }

    setIsModalOpen(!isModalOpen);
  };

  const isUnavailableToGenerate = () => {
    return !(formObj && (formObj.values.from > 0 && formObj.values.to > 0));
  };

  const handleWithdraw = () => {
    dispatch(
      openRetailFiatSidePane(false, {
        iconFileName: 'thb-48px.svg',
        ProductSymbol: 'THB',
        ProductFullName: 'Thai Baht',
        ProductId: 3
      })
    );
  };

  return (
    <Fragment>
      <form className={baseClasses('container')} onSubmit={handleSubmit}>
        <MerchantTab baseClasses={baseClasses} />

        <div className={baseClasses('content-wrapper')}>
          <div className="row">
            <div className="col-12">
              <MerchantProductSelector
                label={context.t('Customer pays with')}
                inputName="from"
                selectOptions={baseSelectOptions}
                onSelect={onProductSelect}
                productId={selectedInstrumentId}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <MerchantProductSelector
                baseClasses={baseClasses}
                label={context.t('Partner Receives')}
                inputName="to"
                selectOptions={quoteSelectOptions}
                onSelect={() => {}}
                productId={0}
                isDisabled={true}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <div className={baseClasses('btn-wrapper')}>
                <button
                  type="button"
                  disabled={isUnavailableToGenerate()}
                  className={baseClasses('merchant-quote')}
                  onClick={toggleModal}>
                  {recentPrice &&
                  recentPrice.id === selectedInstrumentId &&
                  !isDepositInfoLoading ? (
                    context.t('Generate Invoice')
                  ) : (
                    <BTZSpinner loading={true}>loading...</BTZSpinner>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>

      {isMerchantRole(user && user.userConfig) && (
        <div className="merchant__withdraw-container">
          <div className="row merchant__withdraw-row">
            <div className="col-6">
              <APCheckbox
                name="autoConvert"
                label={context.t('Auto Convert to THB')}
                id="autoConvert"
                inline={true}
                isChecked={true}
                disabled={true}
                readOnly={true}
              />
            </div>

            <div className="col-6">
              <button
                type="button"
                className={baseClasses('merchant-quote')}
                onClick={handleWithdraw}>
                {context.t('Withdraw THB')}
              </button>
            </div>
          </div>
        </div>
      )}

      {isModalOpen &&
        !isDepositInfoLoading && (
          <Modal
            customClass="merchant-modal"
            isOpen={isModalOpen}
            title={context.t('Confirm Order')}
            onCancle={toggleModal}
            close={toggleModal}>
            <MerchantModal
              onModalClose={toggleModal}
              transactionId={tranId}
              merchantProfile={merchantProfile}
            />
          </Modal>
        )}
    </Fragment>
  );
};

MerchantTransferBlock.contextTypes = {
  t: PropTypes.func.isRequired
};

const MerchantTransferBlockForm = reduxForm({
  form: 'merchant',
  onSubmit: (payload, dispatch, values) => {
    console.log('values', values);
  },
  destroyOnUnmount: false
})(MerchantTransferBlock);

const mapStateToProps = state => {
  return {
    formObj: state.form.merchant,
    initialValues: {
      side: 'internal', // internal or external
      from: 0,
      to: 0
    },
    // mock
    quoteSelectOptions: FIAT_PRODUCTS,
    baseSelectOptions: CRYPTO_PRODUCTS,
    selectedInstrumentId: state.instrument.selectedInstrumentId,
    user: state.user,
    recentPrice: getRecentTrade(state),
    isDepositInfoLoading: state.deposit.isLoading,
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(MerchantTransferBlockForm);

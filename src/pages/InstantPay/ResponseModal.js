import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import Modal from '../../components/common/Modal/Modal';
import './ResponseModal.css';

const modalClasses = getBEMClasses('instant-pay-response-modal');

const ResponseModal = (props, context) => {
  const history = useHistory();

  const handleClick = () => {
    history.push(props.then);
  };

  return (
    <Modal
      isOpen={true}
      title={context.t('Bitazza Transfer')}
      customClass={modalClasses()}
      close={() => {}}
      footer={{
        buttonText: context.t('OK'),
        buttonStyle: 'additive',
        onClick: handleClick
      }}>
      <h3 className={modalClasses('content-text')}>{context.t(props.text)}</h3>
    </Modal>
  );
};

ResponseModal.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ResponseModal;

import React from 'react';
import clsx from 'clsx';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  transferFunds,
  getTransfers
} from '../../redux/actions/transferActions';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import StandAloneLayout from 'apex-web/lib/layouts/StandAloneLayout/StandAloneLayout';
import { BTZLogoSpinner } from '../../components/common/BTZSpinner/BTZSpinner';
import config from '../../config';
import SendReceiveConfirmModal from './ConfirmModal';
import ResponseModal from './ResponseModal';
import './InstantPayPage.css';

const classes = getBEMClasses('instant-pay-page');
const baseClasses = getBEMClasses('standalone-form');

const InstantPayComponent = ({
  products,
  selectedAccountId,
  token,
  sendFunds
}) => {
  const { transactionID } = useParams();
  const [tx, setTx] = React.useState({ status: 'loading' });
  const [product, setProduct] = React.useState(null);

  const getUserDetail = async (type, search) => {
    const searchParam = new URLSearchParams({ type, search }).toString();
    const res = await fetch(config.services.getUserDetail(searchParam), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-BTZ-TOKEN': token
      }
    });
    const jsonRes = await res.json();
    return jsonRes;
  };

  React.useEffect(
    () => {
      (async () => {
        try {
          const headers = { 'X-BTZ-TOKEN': token };
          const res = await fetch(
            config.services.getTransaction(transactionID),
            { headers }
          );
          const resJson = await res.json();
          if (resJson['status'] !== 'new' || resJson['side'] !== 'internal') {
            throw new Error('status is not new or side is not new internal');
          }
          const btzID = resJson['username'];
          const jsonResInfo = await getUserDetail('userTag', btzID);
          const receiverAccountId = jsonResInfo['default_account_id'];
          if (!receiverAccountId) {
            throw new Error('Not found receiver account id');
          }

          const productTarget = products.find(
            item => item.ProductId === resJson['product_id']
          );
          setProduct({
            ProductId: productTarget.ProductId,
            ProductSymbol: productTarget.Product,
            ProductFullName: productTarget.ProductFullName,
            decimalPlaces: productTarget.DecimalPlaces
          });
          setTx({
            status: 'ready',
            Amount: resJson['amount'],
            AmountTHB: resJson['baht'],
            Notes: resJson['transaction_id'],
            ReceiverUsername: `@${btzID}`,
            displayName: resJson['display_name'],
            receiverAccountId: receiverAccountId
          });
        } catch (error) {
          setTx({ status: 'error' });
        }
      })();
    },
    [transactionID]
  );

  const submitData = async () => {
    const payload = {
      Notes: tx.Notes || '',
      ReceiverAccountId: tx.receiverAccountId,
      ReceiverUsername: '',
      Amount: tx.Amount,
      ProductId: product.ProductId,
      selectedAccountId
    };
    const res = await sendFunds(payload);
    if (res.type !== 'success') {
      setTx({ status: 'error', errorMes: res.text });
      return null;
    }
    setTx({ status: 'success' });
    return null;
  };

  return (
    <StandAloneLayout>
      <div className={clsx(baseClasses('container'), classes('container'))}>
        {tx.status === 'ready' && (
          <SendReceiveConfirmModal
            product={product}
            values={tx}
            onConfirm={submitData}
          />
        )}
        {tx.status === 'error' && (
          // context.t('Invalid Transaction or Transaction has expired.')
          <ResponseModal
            text={
              tx.errorMes || 'Invalid Transaction or Transaction has expired.'
            }
            then="/dashboard"
          />
        )}
        {tx.status === 'success' && (
          // context.t('Transaction completed successfully.')
          <ResponseModal
            text="Transaction completed successfully."
            then="/asset-activity-details"
          />
        )}
        {tx.status === 'loading' && <BTZLogoSpinner />}
      </div>
    </StandAloneLayout>
  );
};

const mapStateToProps = state => {
  const { products } = state.apexCore.product;
  const { selectedAccountId } = state.user;
  const { token } = state.auth;

  return {
    products,
    selectedAccountId,
    token
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  sendFunds: ({ selectedAccountId, ...rest }) =>
    dispatch(transferFunds(rest, ownProps.name)).then(res => {
      dispatch(getTransfers(selectedAccountId));
      return res;
    })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InstantPayComponent);

import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../components/common/Modal/Modal';
import APLabelWithText from 'apex-web/lib/components/common/APLabelWithText';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { formatDateTime } from 'apex-web/lib/helpers/dateHelper';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import SendReceiveConfirmHeaderComponent from 'apex-web/lib/components/SendReceiveSidePane/SendReceiveConfirmModal/SendReceiveConfirmHeaderComponent';

import './ConfirmModal.css';

const bemClasses = getBEMClasses('instant-pay-confirm-modal');

const ConfirmModal = (props, context) => {
  const { product, values, onConfirm } = props;

  return (
    <Modal
      isOpen={true}
      title={
        <SendReceiveConfirmHeaderComponent isSend={true} product={product} />
      }
      close={() => {}}
      footer={{
        buttonText: context.t('Confirm'),
        buttonStyle: 'additive',
        onClick: onConfirm
      }}
      customClass={bemClasses()}>
      <header className={bemClasses('title')}>
        {context.t('Confirmation')}
      </header>
      <section className={bemClasses('details')}>
        <APLabelWithText
          label={context.t('Recipient’s')}
          text={values.ReceiverUsername}
          customClass={bemClasses()}
        />
        <APLabelWithText
          label={`${product.ProductSymbol} ${context.t('Amount')}`}
          text={`${formatNumberToLocale(
            values.Amount,
            product.ProductSymbol
          )} ${product.ProductSymbol}`}
          customClass={bemClasses()}
        />
        <APLabelWithText
          label={context.t('THB Amount')}
          text={`${formatNumberToLocale(values.AmountTHB, 'THB')} THB`}
          customClass={bemClasses()}
        />
        <APLabelWithText
          label={context.t('Transfer Fee')}
          text={`${formatNumberToLocale(0, 'BTZ')} BTZ`}
          customClass={bemClasses()}
        />
        <APLabelWithText
          label={context.t('Transaction ID')}
          text={values.Notes}
          customClass={bemClasses()}
        />
        <APLabelWithText
          label={context.t('Time')}
          text={formatDateTime(new Date())}
          customClass={bemClasses()}
        />
      </section>
    </Modal>
  );
};

ConfirmModal.contextTypes = {
  t: PropTypes.func.isRequired
};

export default ConfirmModal;

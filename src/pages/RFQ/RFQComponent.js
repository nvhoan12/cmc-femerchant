import React, { useEffect, useState, Fragment } from 'react';
import BigNumber from 'bignumber.js';
import { change } from 'redux-form';
import PropTypes from 'prop-types';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import './RFQComponent.css';

import RFQTabs from './components/RFQTabs';
import RFQProductSelector from './components/RFQProductSelector';
import RFQQuoteButton from './components/RFQQuoteButton';
import BTZSpinner from '../../components/common/BTZSpinner/BTZSpinner';
import Modal from '../../components/common/Modal/Modal';
import RFQModalContainer from './components/RFQModalContainer';
import { showSnack } from 'apex-web/lib/redux/actions/snackbarActions';
import {
  subscribeToQCPIndicativePrice,
  unsubscribeToQCPIndicativePrice
} from '../../redux/actions/qcpActions';

import {
  onBaseProductSelect,
  onQuoteProductSelect,
  handleExecuteQuote,
  resetRFQFormAndState,
  fetchBlocktradeHistory
} from '../../redux/actions/rfqActions';

import { decimalPlace } from './components/RFQProductSelector/RFQProductSelector';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';

import config from '../../config';

const minimumQuoteTHB = config.global.minimumBlocktrade.thb;

const baseClasses = getBEMClasses('rfq');
const rfqSelectorClasses = getBEMClasses('rfq-selector');

const getOrderPrice = (qty, side, currency) => {
  let fee = config.global.blocktradeFee.crypto || 0.015;

  if (currency === 'USD' || currency === 'THB') {
    fee = config.global.blocktradeFee.fiat;
  }

  let unit = 1;
  if (side === 'Sell') {
    unit = -1;
  }

  const additionalFee = BigNumber(qty)
    .times(fee)
    .times(unit);

  const total = BigNumber(qty)
    .plus(additionalFee)
    .decimalPlaces(2)
    .toNumber();
  return total;
};

const RFQComponent = (
  {
    indicativePriceSelected,
    handleSubmit,
    themeModifier,
    formHeader,
    quote,
    isRequestQuote,
    rates,
    isFetching,
    isRFQAvailable,
    baseSelectOptions,
    quoteSelectOptions,
    quotePosition,
    basePosition,
    selectedBaseId,
    selectedQuoteId,
    formObj,
    rfqError,
    balances,
    prices,
    dispatch
  },
  context
) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  useEffect(
    () => {
      dispatch(subscribeToQCPIndicativePrice());

      return () => {
        dispatch(unsubscribeToQCPIndicativePrice());
      };
    },
    [dispatch]
  );

  const isIndicPriceValid = () => {
    return prices && prices.length;
  };

  const isOutOfBalance = () => {
    if (formObj) {
      if (formObj.values.quote === 0 || formObj.values.base === 0) {
        return true;
      }
      // Buy
      if (formObj.values.side === '0') {
        return (
          (formObj.values.quote || 0) >
          ((quotePosition && quotePosition.Amount) || -1)
        );
      } else {
        return (
          (formObj.values.base || 0) >
          ((basePosition && basePosition.Amount) || -1)
        );
      }
    }
    return true;
  };

  const isInvalidForMinimumQuote = () => {
    const { minimumBlocktrade } = config.global;
    if (formObj) {
      if (selectedQuoteId === 'THB' && selectedBaseId === 'USDT') {
        return parseInt(formObj.values.quote) < minimumBlocktrade.usdtthb;
      }

      if (selectedQuoteId === 'THB') {
        return parseInt(formObj.values.quote) < minimumBlocktrade.thb;
      }

      if (selectedQuoteId === 'BTC') {
        return parseInt(formObj.values.quote) < minimumBlocktrade.btc;
      }

      if (selectedQuoteId === 'ETH') {
        return parseInt(formObj.values.quote) < minimumBlocktrade.eth;
      }

      return false;
    }
    return false;
  };

  const toggleModal = () => setIsModalOpen(!isModalOpen);

  const onHandleReset = () => {
    setIsModalOpen(false);
    dispatch(resetRFQFormAndState());
  };

  const handleInputBase = (value) => {
    // If side == buy
    const { side } = formObj.values;
    const { Buy, Sell } = indicativePriceSelected;
    const price = side === '0' ? Buy : Sell;

    const decimal = decimalPlace(selectedQuoteId);

    const newQuote = BigNumber(value)
      .times(price)
      .decimalPlaces(decimal);
    dispatch(change('rfq', 'quote', newQuote));
  };

  const handleInputQuote = (value) => {
    // If side == buy
    const { side } = formObj.values;
    const { Buy, Sell } = indicativePriceSelected;
    const price = side === '0' ? Buy : Sell;

    const decimal = decimalPlace(selectedBaseId);

    const newBase = BigNumber(value)
      .div(price)
      .decimalPlaces(decimal);

    dispatch(change('rfq', 'base', newBase));
  };

  const executeQuote = async () => {
    setIsModalOpen(false);
    try {
      const orderPrice = getOrderPrice(
        quote.QuoteQty,
        quote.Side,
        quote.QuoteCurrency
      );
      await dispatch(
        handleExecuteQuote({
          quote,
          order_price: orderPrice
        })
      );

      dispatch(fetchBlocktradeHistory());
    } catch (error) {
      return dispatch(
        showSnack({
          id: 'executeQuote',
          timeout: 6000,
          text: context.t(error.message),
          type: 'warning'
        })
      );
    }
  };

  const baseFileName = balances.find((item) => {
    return item.ProductSymbol === selectedBaseId;
  });

  const quoteFileName = balances.find((item) => {
    return item.ProductSymbol === selectedQuoteId;
  });

  return (
    <Fragment>
      <form
        className={baseClasses('container', themeModifier)}
        onSubmit={handleSubmit}>
        {formHeader && (
          <div className={baseClasses('form-header', themeModifier)}>
            {formHeader}
          </div>
        )}

        <RFQTabs
          baseClasses={baseClasses}
          themeModifier={themeModifier}
          isRequestQuote={isRequestQuote && quote}
        />

        <div className={baseClasses('content-wrapper')}>
          <div className="row">
            <div className="col-12">
              <RFQProductSelector
                isRequestQuote={isRequestQuote && quote}
                baseClasses={rfqSelectorClasses}
                label={formObj && formObj.values.side === '0' ? 'BUY' : 'SELL'}
                inputName="base"
                handleInput={handleInputBase}
                selectOptions={baseSelectOptions}
                onSelect={onBaseProductSelect}
                product={baseFileName}
                productId={selectedBaseId}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <RFQProductSelector
                isRequestQuote={isRequestQuote && quote}
                formObj={formObj}
                baseClasses={rfqSelectorClasses}
                label={formObj && formObj.values.side === '0' ? 'WITH' : 'FOR'}
                inputName="quote"
                handleInput={handleInputQuote}
                selectOptions={quoteSelectOptions}
                onSelect={onQuoteProductSelect}
                product={quoteFileName}
                productId={selectedQuoteId}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <span className="text-info">
                {context.t('Required {minimumQuoteTHB} THB minimum.', {
                  minimumQuoteTHB: formatNumberToLocale(minimumQuoteTHB, 2)
                })}
              </span>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              {!quote ? (
                <div className="rfq__btn-wrapper">
                  {isRFQAvailable ? (
                    <button
                      disabled={
                        isOutOfBalance() ||
                        isInvalidForMinimumQuote() ||
                        !isIndicPriceValid()
                      }
                      onClick={async (payload, dispatch) => {
                        handleSubmit(payload, dispatch);
                      }}
                      className={baseClasses(
                        `request-quote ${formObj &&
                          formObj.values.side === '1' &&
                          'is-sell'}`
                      )}>
                      {isFetching ? (
                        <BTZSpinner loading={true}>Loading...</BTZSpinner>
                      ) : (
                        context.t('Request Quote')
                      )}
                    </button>
                  ) : (
                    <span className="text-danger">
                      {context.t(
                        'Blocktrade is available between Monday 8:00 AM to Friday 4:00 PM only.'
                      )}
                    </span>
                  )}
                </div>
              ) : (
                <RFQQuoteButton
                  dispatch={dispatch}
                  onReset={onHandleReset}
                  onClick={toggleModal}
                  side={formObj && formObj.values.side}
                  context={context}
                />
              )}
            </div>
          </div>
        </div>
      </form>
      {isModalOpen &&
        quote &&
        rates &&
        isRequestQuote && (
          <Modal
            isOpen={isModalOpen && quote}
            title="Confirm Order"
            onCancle={toggleModal}
            close={toggleModal}>
            <RFQModalContainer
              side={formObj.values.side}
              onModalSubmit={executeQuote}
              thaiRateCurrency={rates.THB}
              quote={quote}
            />
          </Modal>
        )}
    </Fragment>
  );
};

RFQComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  displayInstrumentSelector: PropTypes.bool,
  extraInfoComponent: PropTypes.element,
  themeModifier: PropTypes.string,
  submitting: PropTypes.bool.isRequired,
  hideKYC: PropTypes.bool,
  customKYCmessage: PropTypes.string
};

RFQComponent.defaultProps = {
  displayInstrumentSelector: true,
  hideKYC: false,
  amountHeader: 'Amount'
};

RFQComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default RFQComponent;

import React, { useEffect } from 'react';
import BigNumber from 'bignumber.js';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import PageFooterLayout from '../../layouts/PageFooterLayout/PageFooterLayout';
import withAuthentication from 'apex-web/lib/hocs/withAuthentication';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import {
  getBalanceFromSelected,
  getIndicativePriceSelected
} from '../../redux/selectors/rfqSelectors';
import {
  startIntervalFetchRFQ,
  stopIntervalFetchRFQ
} from '../../redux/actions/rfqActions';

import { getBEMClasses } from '../../helpers/cssClassesHelper';
import { Text } from '../../components/PublicPage/Text';

import RFQContainer from './RFQContainer';
import config from '../../config';

import './RFQPage.css';
import RFQLatestTransactions from './components/RFQLatestTransactions';

const layoutClasses = getBEMClasses('rfq-page');

export const showQuoteQty = (qty, side, currency, rate = undefined) => {
  let fee = config.global.blocktradeFee.crypto || 0.015;

  let newQty = qty;

  if (currency === 'USD' && rate) {
    newQty = BigNumber(qty).dividedBy(rate);
  }

  if (currency === 'USD' || currency === 'THB') {
    fee = config.global.blocktradeFee.fiat;
  }

  let unit = 1;
  if (side === 'Sell') {
    unit = -1;
  }

  const additionalFee = BigNumber(newQty)
    .times(fee)
    .times(unit);

  const total = BigNumber(newQty)
    .plus(additionalFee)
    .decimalPlaces(2);

  return formatNumberToLocale(total, currency);
};

const RFQPage = (
  {
    dispatch,
    indicativePriceSelected,
    basePosition,
    quotePosition,
    quote,
    isRequestQuote,
    rates,
    startFetchRFQ,
    stopFetchRFQ
  },
  context
) => {
  useEffect(() => {
    startFetchRFQ();
    return stopFetchRFQ;
  }, []);

  const indicativeText = () => {
    if (quote) {
      return ` per ${basePosition && basePosition.ProductSymbol}`;
    } else {
      return ` (${basePosition &&
        basePosition.ProductSymbol} - ${quotePosition &&
        quotePosition.ProductSymbol})`;
    }
  };

  return (
    <React.Fragment>
      <div className={layoutClasses('container')}>
        <div className={layoutClasses('header')}>
          <Text
            text={context.t('Request for Quote')}
            align="left"
            fontSize="40"
            color="light"
          />
        </div>

        <div className={layoutClasses('wallet-card-container')}>
          <div className="row mb-4">
            <div className="col-12">
              <div className="col-12 rfq-card is-show-on-mobile">
                <Text
                  text={context.t('Balances')}
                  align="left"
                  fontSize="28"
                  fontBold={true}
                  color="light"
                  className={layoutClasses('heading')}
                />

                {basePosition && (
                  <div className={layoutClasses('balance-wrapper')}>
                    <Text
                      text={basePosition.ProductSymbol}
                      align="left"
                      fontSize="18"
                      className={layoutClasses('text-lightgray')}
                    />
                    <Text
                      text={formatNumberToLocale(
                        basePosition.Amount,
                        basePosition.ProductSymbol
                      )}
                      align="left"
                      fontSize="18"
                      color="primary"
                    />
                  </div>
                )}

                <hr className={layoutClasses('divider')} />

                {quotePosition && (
                  <div className={layoutClasses('balance-wrapper')}>
                    <Text
                      text={quotePosition.ProductSymbol}
                      align="left"
                      fontSize="18"
                      className={layoutClasses('text-lightgray')}
                    />
                    <Text
                      text={formatNumberToLocale(
                        quotePosition.Amount,
                        quotePosition.ProductSymbol
                      )}
                      align="left"
                      fontSize="18"
                      color="primary"
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row rfq-page__wrapper">
            <div className="col-12 col-md-7">
              <RFQContainer
                indicativePriceSelected={indicativePriceSelected}
                basePosition={basePosition}
                quotePosition={quotePosition}
              />
            </div>

            <div className="col-12 col-md-5">
              <div className="row">
                <div className="col-12 rfq-card is-hidden-on-mobile">
                  <Text
                    text={context.t('Balances')}
                    align="left"
                    fontSize="28"
                    fontBold={true}
                    color="light"
                    className={layoutClasses('heading')}
                  />

                  {basePosition && (
                    <div className={layoutClasses('balance-wrapper')}>
                      <Text
                        text={basePosition.ProductSymbol}
                        align="left"
                        fontSize="18"
                        className={layoutClasses('text-lightgray')}
                      />
                      <Text
                        text={formatNumberToLocale(
                          basePosition.Amount,
                          basePosition.ProductSymbol
                        )}
                        align="left"
                        fontSize="18"
                        color="primary"
                      />
                    </div>
                  )}

                  <hr className={layoutClasses('divider')} />

                  {quotePosition && (
                    <div className={layoutClasses('balance-wrapper')}>
                      <Text
                        text={quotePosition.ProductSymbol}
                        align="left"
                        fontSize="18"
                        className={layoutClasses('text-lightgray')}
                      />
                      <Text
                        text={formatNumberToLocale(
                          quotePosition.Amount,
                          quotePosition.ProductSymbol
                        )}
                        align="left"
                        fontSize="18"
                        color="primary"
                      />
                    </div>
                  )}
                </div>
              </div>

              <div
                className={`row mt-4 rfq-indicative__container ${quote &&
                  isRequestQuote &&
                  'is-quote'}`}>
                <div className={layoutClasses('flipper')}>
                  <div className={layoutClasses('front')}>
                    <div className="col-12 rfq-card">
                      <Text
                        text={`${context.t('Indicative Price')} ${context.t(
                          indicativeText()
                        )}`}
                        align="left"
                        fontSize="28"
                        fontBold={true}
                        color="light"
                        className={layoutClasses('heading')}
                      />

                      {(!quote || !isRequestQuote) && (
                        <div className="row">
                          <div className="col-6">
                            <p
                              className={layoutClasses(
                                'indicative-price-sell is-text'
                              )}>
                              {context.t('Sell')}
                            </p>
                            <p
                              className={layoutClasses(
                                'indicative-price-sell'
                              )}>
                              {indicativePriceSelected
                                ? showQuoteQty(
                                    indicativePriceSelected.Sell,
                                    'Sell',
                                    indicativePriceSelected.Quote
                                  )
                                : '-'}
                            </p>
                          </div>
                          <div className="col-6">
                            <p
                              className={layoutClasses(
                                'indicative-price-buy is-text'
                              )}>
                              {context.t('Buy')}
                            </p>
                            <p
                              className={layoutClasses('indicative-price-buy')}>
                              {indicativePriceSelected
                                ? showQuoteQty(
                                    indicativePriceSelected.Buy,
                                    'Buy',
                                    indicativePriceSelected.Quote
                                  )
                                : '-'}
                            </p>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className={layoutClasses('back')}>
                    <div className="col-12 rfq-card">
                      <div className="test row">
                        <div className="col-9">
                          <Text
                            text={`${context.t('Price Quote')} ${context.t(
                              indicativeText()
                            )}`}
                            align="left"
                            fontSize="28"
                            fontBold={true}
                            color="light"
                            className={layoutClasses('heading')}
                          />
                        </div>

                        <div className="col-3">
                          <Text
                            text={context.t('Total')}
                            align="right"
                            fontSize="28"
                            fontBold={true}
                            color="light"
                            className={layoutClasses('heading')}
                          />
                        </div>
                      </div>

                      {quote &&
                        isRequestQuote && (
                          <div className="row">
                            <div className="col-6">
                              <p
                                className={layoutClasses(
                                  `indicative-price-summary ${quote.Side}`
                                )}>
                                {showQuoteQty(
                                  quote.Price,
                                  quote.Side,
                                  quote.QuoteCurrency,
                                  rates.THB
                                )}{' '}
                              </p>
                            </div>

                            <div className="col-6 text-right">
                              <p
                                className={layoutClasses(
                                  `indicative-price-summary total ${quote.Side}`
                                )}>
                                {showQuoteQty(
                                  quote.QuoteQty,
                                  quote.Side,
                                  quote.QuoteCurrency,
                                  rates.THB
                                )}
                              </p>
                            </div>
                          </div>
                        )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row my-4">
            <div className="col-12">
              <div className="transactions">
                <h2 className="rfq__latest-transactions-header">
                  {context.t('Latest Transactions')}
                </h2>

                <RFQLatestTransactions test={true} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={layoutClasses('footer')}>
        <PageFooterLayout />
      </div>
    </React.Fragment>
  );
};

RFQPage.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  const { basePosition, quotePosition } = getBalanceFromSelected(state);

  return {
    accountId: state.user.userInfo.AccountId,
    indicativePriceSelected: getIndicativePriceSelected(state),
    basePosition,
    quotePosition,
    quote: state.rfq.quote,
    isRequestQuote: state.rfq.isRequestQuote,
    rates: state.rfq.rates
  };
};

const mapDispatchToProps = {
  startFetchRFQ: startIntervalFetchRFQ,
  stopFetchRFQ: stopIntervalFetchRFQ
};

export default withAuthentication(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RFQPage)
);

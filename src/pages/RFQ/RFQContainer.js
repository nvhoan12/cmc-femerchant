import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import BigNumber from 'bignumber.js';
import RFQComponent from './RFQComponent';
import { buyValue } from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import { productPositionAssetsSelector } from 'apex-web/lib/redux/selectors/productPositionSelectors';

import { decimalPlace } from './components/RFQProductSelector/RFQProductSelector';

import {
  getProductListForQuote,
  selectOptionByCondition
} from '../../redux/selectors/rfqSelectors';
import { handleRequestQuote } from '../../redux/actions/rfqActions';
import config from '../../config';

const toUSD = (id) => {
  return id === 'THB' || id === 'BTZ';
};

const addSpread = (amount, side, currency) => {
  let spread = config.global.blocktradeFee.crypto || 0.015;

  if (currency === 'USD' || currency === 'THB') {
    spread = config.global.blocktradeFee.fiat;
  }

  const fee = BigNumber(amount).times(spread);

  // Buy
  if (side === '0') {
    return BigNumber(amount).plus(fee);
  } else {
    return BigNumber(amount).minus(fee);
  }
};

const RFQComponentForm = reduxForm({
  form: 'rfq',
  onSubmit: (payload, dispatch, values) => {
    const currency = toUSD(values.selectedQuoteId)
      ? 'USD'
      : values.selectedQuoteId;
    const data = {
      baseCurrency: values.selectedBaseId,
      quoteCurrency: currency,
      side: payload.side === '0' ? 'Buy' : 'Sell',
      baseQty: new BigNumber(payload.base).decimalPlaces(
        decimalPlace(values.selectedBaseId)
      ),
      amount: addSpread(payload.quote, payload.side, currency)
    };

    dispatch(handleRequestQuote(data));
  },
  destroyOnUnmount: false
})(RFQComponent);

const mapStateToProps = (state) => {
  return {
    formObj: state.form.rfq,
    initialValues: {
      side: buyValue,
      base: 0,
      quote: 0,
      input_base: 0,
      input_quote: 0
    },
    quote: state.rfq.quote,
    isRequestQuote: state.rfq.isRequestQuote,
    rfqError: state.rfq.error,
    isFetching: state.rfq.isFetching,
    isRFQAvailable: state.rfq.isRFQAvailable,
    rates: state.rfq.rates,
    prices: state.rfq.prices,
    selectedBaseId: state.rfq.selectedBaseId,
    selectedQuoteId: state.rfq.selectedQuoteId,
    baseSelectOptions: getProductListForQuote(state),
    quoteSelectOptions: selectOptionByCondition(state),
    balances: productPositionAssetsSelector(state)
  };
};

export default connect(mapStateToProps)(RFQComponentForm);

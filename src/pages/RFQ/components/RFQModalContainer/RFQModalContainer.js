import React from 'react';
import { getBEMClasses } from '../../../../helpers/cssClassesHelper';
import './RFQModalContainer.css';

import { showQuoteQty } from '../../RFQPage';

const layoutClasses = getBEMClasses('rfq-modal');

const showQuoteSymbol = (quote) => {
  if (quote === 'USD') return 'THB';
  return quote;
};

const RFQModalContainer = ({
  onModalSubmit,
  side,
  quote = {},
  thaiRateCurrency
}) => {
  const isBuy = side === '0';

  const sideClass = isBuy ? 'is-buy' : 'is-sell';
  const confirmText = isBuy ? 'Confirm Buy Order' : 'Confirm Sell Order';

  let price = quote.Price;
  let quoteQty = quote.QuoteQty;
  let symbol = quote.QuoteCurrency;

  if (quote.QuoteCurrency === 'USD') {
    symbol = 'THB';
  }

  return (
    <div className={layoutClasses('container')}>
      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Order Type</p>
        <p className={layoutClasses('row-content')}>Request for Quote</p>
      </div>

      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Pair</p>
        <p className={layoutClasses('row-content')}>
          {quote.BaseCurrency} - {showQuoteSymbol(quote.QuoteCurrency)}
        </p>
      </div>

      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Side</p>
        <p className={layoutClasses(`row-content ${sideClass}`)}>
          {quote.Side}
        </p>
      </div>

      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Quantity</p>
        <p className={layoutClasses('row-content')}>
          {quote.BaseQty} {quote.BaseCurrency}
        </p>
      </div>

      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Price</p>
        <p className={layoutClasses('row-content')}>
          {showQuoteQty(
            price,
            quote.Side,
            quote.QuoteCurrency,
            thaiRateCurrency
          )}{' '}
          {symbol}
        </p>
      </div>

      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Fee</p>
        <p className={layoutClasses('row-content')}>0</p>
      </div>

      <div className={layoutClasses('row')}>
        <p className={layoutClasses('row-title')}>Total</p>
        <p className={layoutClasses('row-content')}>
          {showQuoteQty(
            quoteQty,
            quote.Side,
            quote.QuoteCurrency,
            thaiRateCurrency
          )}{' '}
          {symbol}
        </p>
      </div>

      <div className="row">
        <button
          onClick={onModalSubmit}
          className={layoutClasses(`btn-confirm ${sideClass}`)}>
          {confirmText}
        </button>
      </div>
    </div>
  );
};

export default RFQModalContainer;

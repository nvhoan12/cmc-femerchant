import React, { useRef, useEffect, useState, Fragment } from 'react';
import { getBEMClasses } from '../../../../helpers/cssClassesHelper';
import { requestQuoteTimeout } from '../../../../redux/actions/rfqActions';
import './index.css';

const useInterval = (callback, delay) => {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(
    () => {
      function tick() {
        savedCallback.current();
      }

      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    },
    [delay]
  );
};

const baseClasses = getBEMClasses('rfq');
const PERCENTAGE = 100;
const COUNTER_IN_SECONDS = 10; // 10 seconds

const RFQQuoteButton = ({ dispatch, onReset, onClick, side, context }) => {
  const [expiry, setExpiry] = useState(10);
  const [isRunning, setIsRunning] = useState(true);
  const [isDisabled, setIsDisabled] = useState(false);

  const isBuy = side === '0';

  useInterval(() => {
    setExpiry(expiry - 1);
  }, isRunning ? 1000 : null);

  const resetExpiry = () => {
    setExpiry(10);
    setIsRunning(false);
    setIsDisabled(true);
    dispatch(requestQuoteTimeout());
  };

  if (expiry <= 0) {
    resetExpiry();
  }

  let width =
    expiry <= 0 ? '100%' : PERCENTAGE - expiry * COUNTER_IN_SECONDS + '%';

  return (
    <div className={baseClasses('btn-wrapper')}>
      <button
        type="button"
        className={baseClasses('btn-reset')}
        onClick={onReset}>
        {context.t('Reset')}
      </button>
      <button
        type="button"
        className={baseClasses(`request-quote ${isBuy ? '' : 'is-sell'}`)}
        onClick={onClick}
        disabled={isDisabled}>
        {isDisabled ? (
          `Expired`
        ) : (
          <Fragment>
            <span className={baseClasses('request-quote-label')}>
              {isBuy ? context.t('Buy') : context.t('Sell')}
            </span>
            <div
              className={baseClasses('request-quote-timer-overlay')}
              style={{ width }}
            />
            <span className={baseClasses('request-quote-timer')}>
              {context.t('Expires in {expiry} seconds', {
                expiry
              })}
            </span>
          </Fragment>
        )}
      </button>
    </div>
  );
};

export default RFQQuoteButton;

import { connect } from 'react-redux';

import RFQProductSelector from './RFQProductSelector';

const mapDispatchToProps = (dispatch, ownProps) => ({
  onProductSelect: id => dispatch(ownProps.onSelect(id))
});

export default connect(
  undefined,
  mapDispatchToProps
)(RFQProductSelector);

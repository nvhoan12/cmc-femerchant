import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { InnerSelect } from '../../../../components/common/APSelect';
import APNumberInput from '../../../../components/common/APNumberInput';
import path from '../../../../helpers/path';

import './RFQProductSelector.css';

export const decimalPlace = (productId) => {
  if (productId === 'BTC') return 4;
  if (productId === 'XRP') return 0;
  if (productId === 'XLM') return 1;
  return 2;
};

const RFQProductSelector = (
  {
    label,
    baseClasses,
    onProductSelect,
    inputName,
    selectOptions,
    handleInput,
    product = {},
    productId,
    isRequestQuote
  },
  context
) => {
  useEffect(() => {
    const preSelectedProduct = selectOptions.find(
      (i) => i.Quote === product && product.ProductSymbol
    );

    if (preSelectedProduct || !selectOptions.length) return;

    const product = selectOptions[0].Quote;
    onProductSelect(product);
  }, []);

  const labelText = context.t(label);

  return (
    <div className={baseClasses('container')}>
      <div className={baseClasses('container-item')}>
        <div className={baseClasses('product-info')}>
          <img
            alt="Product icon"
            className="icon"
            src={path('/local/product-icons/') + product.iconFileName}
          />
        </div>
        <InnerSelect
          showTriangles
          label={labelText}
          customClass={baseClasses()}
          onSelect={onProductSelect}
          value={product.ProductSymbol}
          options={selectOptions.map((product) => ({
            value: product.Quote,
            label: product.Quote
          }))}
          disabled={isRequestQuote}
        />
      </div>

      <div className={baseClasses('container-item-input')}>
        <APNumberInput
          type="hidden"
          name={inputName}
          placeholder="0"
          customClass={baseClasses()}
          decimalPlaces={decimalPlace(productId)}
          customChange={handleInput}
          disabled={isRequestQuote}
        />
      </div>
    </div>
  );
};

RFQProductSelector.contextTypes = {
  t: PropTypes.func.isRequired
};

export default RFQProductSelector;

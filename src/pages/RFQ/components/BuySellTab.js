import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Text } from '../../../components/PublicPage/Text';

import 'react-tabs/style/react-tabs.css';

const TabPanelContent = (props, context) => {
  return (
    <div className="row">
      <div className="col col-12 col-lg-4">
        <Text text={context.t('BUY')} fontSize="24" fontBold={true} />
      </div>
    </div>
  );
};

TabPanelContent.contextTypes = {
  t: PropTypes.func.isRequired
};

const BuySellTab = (props, context) => (
  <Tabs className="bitazza-ranking-tabs">
    <TabList>
      <Tab className="react-tabs__tab buy">
        <span>icon</span>
        <p>{context.t('Buy')}</p>
      </Tab>

      <Tab className="react-tabs__tab sell">
        <span>icon</span>
        <p>{context.t('Sell')}</p>
      </Tab>
    </TabList>

    <TabPanel forceRender>
      <TabPanelContent />
    </TabPanel>

    <TabPanel forceRender>
      <TabPanelContent />
    </TabPanel>
  </Tabs>
);

BuySellTab.contextTypes = {
  t: PropTypes.func.isRequired
};

export default BuySellTab;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import APTable from 'apex-web/lib/components/common/APTable';
import { formatDateTime } from 'apex-web/lib/helpers/dateHelper';
import { formatNumberToLocale } from 'apex-web/lib/helpers/numberFormatter';
import { getOrderPriceToDisplay } from 'apex-web/lib/helpers/orderHistoryHelper';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import { instrumentSelectorInstruments } from 'apex-web/lib/redux/selectors/instrumentPositionSelectors';

import { blocktradeHistorySelector } from '../../../../redux/selectors/rfqSelectors';

// const baseClass = getBEMClasses('order-history-table');
const baseClass = getBEMClasses('rfq-transaction-table');

const LatestTransactionsComponent = (
  { tradeReports, instruments, tableConfig },
  context
) => {
  const { maxLines, usePagination } = tableConfig;

  const columns = [
    {
      header: context.t('Date/Time'),
      dataTest: 'Date/Time',
      cell: (row) => {
        return (
          <div className={baseClass('datetime')}>
            {formatDateTime(row.ReceiveTime || row.TradeTimeMS)}
          </div>
        );
      }
    },
    {
      header: context.t('Pair'),
      dataTest: 'Pair',
      cell: (row) => {
        const instrument = instruments.find(
          (item) => item.InstrumentId === (row.Instrument || row.InstrumentId)
        );
        return instrument ? instrument.Symbol : 'USDTTHB';
      }
    },
    {
      header: context.t('Side'),
      dataTest: 'Side',
      cell: (row) => (
        <div className={baseClass(row.Side === 'Buy' ? 'buy' : 'sell')}>
          {row.Side}
        </div>
      )
    },
    {
      header: context.t('Size'),
      dataTest: 'Size',
      cell: (row) => (
        <div className={baseClass('size')}>
          {row.Quantity &&
            formatNumberToLocale(row.Quantity, row.Quantity.symbol)}
        </div>
      )
    },
    {
      header: context.t('Price'),
      dataTest: 'Price',
      cell: (row) => (
        <div className={baseClass('price')}>{getOrderPriceToDisplay(row)}</div>
      )
    },
    {
      header: context.t('Fee'),
      dataTest: 'Fee',
      cell: (row) => {
        return (
          (row.Fee && formatNumberToLocale(row.Fee, row.Quantity.symbol)) || 0
        );
      }
    },
    {
      header: context.t('Status'),
      dataTest: 'Status',
      cell: () => {
        return context.t('Complete');
      }
    }
  ];

  return (
    <APTable
      {...{
        columns,
        rows: tradeReports,
        baseClass,
        fetching: false,
        pageSize: maxLines,
        usePagination,
        empty: context.t('No data is available'),
        componentName: 'LatestTransactionsComponent'
      }}
    />
  );
};

/*
 this.context.t('Latest Transactions');
 */
LatestTransactionsComponent.title = 'Transactions';

LatestTransactionsComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  const instruments = instrumentSelectorInstruments(state);

  const tableConfig = {
    filterMode: 'selectedProduct', // selectedProduct select all and selectInstrument is pair
    maxLines: 5,
    usePagination: true
  };

  const tradeReports = blocktradeHistorySelector(state);

  return {
    tradeReports,
    instruments,
    tableConfig
  };
};

const LatestTransactionComponentMemo = React.memo(
  LatestTransactionsComponent,
  (prevProps, nextProps) => {
    return prevProps.tradeReports === nextProps.tradeReports;
  }
);

export default connect(mapStateToProps)(LatestTransactionComponentMemo);

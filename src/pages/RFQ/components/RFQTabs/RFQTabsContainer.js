import { connect } from 'react-redux';
import { change } from 'redux-form';
import {
  buyValue,
  sellValue
} from '../../../../constants/sendOrder/orderEntryFormConstants';
import { isBuySideRFQFormSelector } from '../../../../redux/selectors/rfqSelectors';
import { clearRFQForm } from '../../../../redux/actions/rfqActions';
import BuySellTabsComponent from './RFQTabsComponent';

const mapStateToProps = state => {
  return {
    isBuySide: isBuySideRFQFormSelector(state)
  };
};

const mapDispatchToProps = {
  setBuyValue: () => change('rfq', 'side', buyValue),
  setSellValue: () => change('rfq', 'side', sellValue),
  clearForm: clearRFQForm
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuySellTabsComponent);

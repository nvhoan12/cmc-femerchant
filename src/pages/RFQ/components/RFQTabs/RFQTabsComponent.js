import React from 'react';
import PropTypes from 'prop-types';
import APIcon from '../../../../components/common/APIcon';

const RFQTabsComponent = (props, context) => {
  const {
    baseClasses,
    clearForm,
    setBuyValue,
    setSellValue,
    isBuySide,
    themeModifier,
    isRequestQuote
  } = props;
  return (
    <div className={baseClasses('tab-wrapper')}>
      <div
        className={baseClasses('tab', {
          'buy-selected': isBuySide
        })}
        onClick={() => {
          if (isRequestQuote) return;
          clearForm();
          setBuyValue();
        }}>
        <APIcon
          name="sell"
          classModifiers="big"
          customClass={baseClasses('icon')}
        />
        {context.t('Buy')}
      </div>
      <div className={baseClasses('vertical-divider', themeModifier)} />
      <div
        className={baseClasses('tab', {
          'sell-selected': !isBuySide
        })}
        onClick={() => {
          if (isRequestQuote) return;
          clearForm();
          setSellValue();
        }}>
        <APIcon
          name="buy"
          classModifiers="big"
          customClass={baseClasses('icon')}
        />
        {context.t('Sell')}
      </div>
    </div>
  );
};

RFQTabsComponent.propTypes = {
  baseClasses: PropTypes.func.isRequired,
  setBuyValue: PropTypes.func.isRequired,
  setSellValue: PropTypes.func.isRequired,
  clearForm: PropTypes.func.isRequired,
  isBuySide: PropTypes.bool.isRequired,
  themeModifier: PropTypes.string
};

RFQTabsComponent.contextTypes = {
  t: PropTypes.func.isRequired
};

export default RFQTabsComponent;

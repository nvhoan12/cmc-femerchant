import React from 'react';
import { connect } from 'react-redux';
import { Route, useRouteMatch } from 'react-router-dom';
import { compose } from 'redux';
import PageHeaderLayout from '../layouts/PageHeaderLayout/PageHeaderLayout';
import withAuthentication from '../hocs/withAuthentication';
import withAllowPage from '../hocs/withAllowPage';

import DashboardPage from './DashboardPage';
import MarketDataPage from './MarketDataPage';
import BuySellPage from './BuySellPage';
import WalletsPage from './WalletsPage';
import ProductDetailsPage from './ProductDetailsPage';
// import RFQPage from './RFQ/RFQPage';

// import ComponentExhibitPage from './ComponentExhibitPage/ComponentExhibitPage';
import SettingsPage from './SettingsPage';
import MerchantDashboardPage from './Merchant/MerchantDashboardPage';
import InstantPayPage from './InstantPay/InstantPayPage';

// import MarginBalancesLayout from 'apex-web/lib/layouts/MarginBalancesLayout/MarginBalancesLayoutContainer';
import AssetActivityDetailsLayout from '../layouts/AssetActivityDetailsLayout/AssetActivityDetailsLayout';

import FixedLeftFluidLayout from 'apex-web/lib/layouts/FixedLeftFluidLayout/FixedLeftFluidLayout';
import WalletDetailsBackButtonComponent from 'apex-web/lib/components/WalletDetails/WalletDetailsBackButtonComponent';
import BalancesMenuContainer from 'apex-web/lib/components/BalancesMenu/BalanceMenuContainer';
import WalletDetailsLayout from 'apex-web/lib/layouts/WalletDetailsLayout/WalletDetailsLayout';

// import DigitalAssetsPage from 'apex-web/lib/components/DigitalAssets/DigitalAssetsPageContainer';
// import DigitalAssetsOfferingProfilePage from 'apex-web/lib/components/DigitalAssets/DigitalAssetsOfferingProfilePageContainer';
// import IBALayout from 'apex-web/lib/components/IBA/IbaLayout';

import ExchangePage from './Exchange/ExchangePage';
// import StreamingPage from './StreamingPage';

import { GolabelMessageInterior } from '../components/GolabelMessage/GolabelMessage';
import SendCryptoSuccessModal from '../components/SendReceiveSidePane/SendCryptoSuccessModal';
import PdpaNotification from '../components/notification/PDPA';
import '../styles/components/AssetActivityDetailsHeaderComponent.css';
import { getUserRole } from '../helpers/userConfigHelper';

const instantPayPath = '/transfer/:transactionID';
const standAlonePages = [instantPayPath];

const HeaderComponent = ({ role }) => {
  const standAloneMatch = useRouteMatch(standAlonePages);
  if (standAloneMatch) {
    return null;
  }

  const isMerchant = ['merchant', 'staff'].includes(role.name);
  return (
    <React.Fragment>
      <PageHeaderLayout />
      {!isMerchant && <GolabelMessageInterior />}
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  role: getUserRole(state.user.userConfig)
});

const Header = connect(
  mapStateToProps,
  null
)(HeaderComponent);

const InteriorPage = () => (
  <React.Fragment>
    <Header />
    <Route exact path={'/dashboard'} component={DashboardPage} />
    <Route exact path="/partners" component={MerchantDashboardPage} />
    <Route path={'/dashboard/market-data'} component={MarketDataPage} />
    <Route exact path={'/wallets'} component={WalletsPage} />
    <Route
      path="/wallets/wallet-detail"
      component={() => (
        <FixedLeftFluidLayout
          left={[
            <WalletDetailsBackButtonComponent key="1" />,
            <BalancesMenuContainer key="2" />
          ]}>
          <WalletDetailsLayout />
        </FixedLeftFluidLayout>
      )}
    />
    <Route path={'/exchange'} component={ExchangePage} />
    <Route path={'/buy-sell'} component={BuySellPage} />
    {/* <Route path={`/blocktrade`} component={RFQPage} /> */}
    <Route path={'/wallets/product-details'} component={ProductDetailsPage} />
    <Route path={'/settings'} component={SettingsPage} />
    {/* <Route path={'/component'} component={ComponentExhibitPage} /> */}
    {/* <Route
      path={'/margin-balances'}
      component={() => (
        <MarginBalancesLayout detailsLink="/wallets/wallet-detail" />
      )}
    /> */}
    <Route
      path={'/asset-activity-details'}
      component={AssetActivityDetailsLayout}
    />
    {/* <Route path={'/iba'} component={IBALayout} /> */}
    {/* <Route path="/eotc" component={StreamingPage} /> */}

    {/* Investor portal Routes */}
    {/* <Route exact path={'/digital-assets'} component={DigitalAssetsPage} />
    <Route
      path={'/digital-assets/:id'}
      component={DigitalAssetsOfferingProfilePage}
    /> */}

    <Route path={instantPayPath} component={InstantPayPage} />
    <SendCryptoSuccessModal />
    <PdpaNotification />
  </React.Fragment>
);

export default compose(
  withAllowPage,
  withAuthentication
)(InteriorPage);

export const appendEach = (arr, suffix) => {
  return arr.filter(c => c).map(text => text + suffix);
};

export const distinct = (array, keySelector = item => item) => {
  const unique = new Map();
  const distinctArray = [];
  array.forEach(item => {
    const itemKey = keySelector(item);
    if (!unique.has(itemKey)) {
      distinctArray.push(item);
      unique.set(itemKey, item);
    }
  });

  return distinctArray;
};

export const findIndexOfObject = (array, key, value) => {
  for (var i = 0; i < array.length; i += 1) {
    if (array[i][key] === value) {
      return i;
    }
  }
  return undefined;
};

export const updateObjectInArray = (array, action) => {
  if (typeof action.index !== 'number') {
    return [...array, action.item];
  }
  return array.map((item, index) => {
    if (index !== action.index) {
      return item;
    }
    return {
      ...item,
      ...action.item
    };
  });
};

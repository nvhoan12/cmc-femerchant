import BigNumber from 'bignumber.js';
export const getTotalWithdrawValue = (balance, amount, fee) => {
  return amount + fee >= balance ? balance - fee : amount;
};

export const getFinalBalanceWithdrawValue = (balance, amount, fee) => {
  return amount + fee <= balance ? balance - (amount + fee) : balance;
};

export const getTotalRender = (balance, amount, fee) => {
  amount = parseFloat(amount);

  if (!amount) return 0;

  const amountBigNumber = new BigNumber(amount);
  const feeBigNumber = new BigNumber(fee);
  return Number(amountBigNumber.minus(feeBigNumber));
};

export const showTemplateSelect = ({
  templateTypes,
  templateInfo,
  isLoading
}) =>
  templateTypes.length > 1 && !Object.keys(templateInfo).length && !isLoading;

export const getAvailableBalance = (balance, hold) => {
  return balance ? balance - hold : 0;
};

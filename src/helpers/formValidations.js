import { isValidNumber } from 'libphonenumber-js/custom';
import metadata from 'libphonenumber-js/metadata.min.json';
import { formatDate, formatTime } from './dateHelper';
import { formatNumberToLocale } from './numberFormatter';

export const required = value => (value ? undefined : 'Required');
// context.t('Required')

export const requiredNumeric = value =>
  value !== undefined && value !== '' ? undefined : 'Required';
// context.t('Required')

export const matchingPassword = (value, allValues) =>
  value !== allValues.password ? 'Passwords do not match' : undefined;
// context.t('Passwords do not match')

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)
    ? 'Invalid email address'
    : undefined;
// context.t('Invalid email address')

export const validateReceiverEmail = (value, userEmail) =>
  value === userEmail
    ? `You can't use your own email as receiver address`
    : undefined;
// context.t(`You can't use your own email as receiver address`)

export const noWhiteSpace = value =>
  value && /\s/.test(value) ? 'Must not contain whitespace' : undefined;
// context.t('Must not contain whitespace')

export const alphanumeric = value =>
  value && !/^[a-z0-9]*$/i.test(value)
    ? 'Input should only contain alphanumeric characters'
    : undefined;
// context.t('Input should only contain alphanumeric characters')

export const alphabetic = value =>
  value && !/^[a-z]*$/i.test(value)
    ? 'Input should only contain alphabetic characters'
    : undefined;
// context.t('Input should only contain alphabetic characters')

export const alphanumericspace = value =>
  !/^[a-z\d\s]*$/i.test(value)
    ? 'Input should only contain alphanumeric and space characters'
    : undefined;
// context.t('Input should only contain alphanumeric and space characters')

export const numeric = value =>
  value && !/^\d*$/i.test(value)
    ? 'Input should only contain numeric characters'
    : undefined;
// context.t('Input should only contain numeric characters')

export const biggerThanValue = min => value =>
  value !== undefined && value <= min
    ? `Must be bigger than ${min}`
    : undefined;
/* context.t('Must be bigger than {a}', {
   a: min
}) */

export const biggerThanOrEqualToValue = min => value =>
  value !== undefined && value !== '' && value < min
    ? `Must be bigger than or equal to ${min}`
    : undefined;
/* context.t('Must be bigger than or equal to {a}', {
   a: min
}) */

export const lessThanValue = (max, value) =>
  value && value > max ? `Must be less than ${max}` : undefined;
/* context.t('Must be less than {a}', {
   a: max
}) */

export const biggerThanZero = biggerThanValue(0);

export const biggerThanOrEqualToZero = biggerThanOrEqualToValue(0);

export const validatePassword = value =>
  value && !/^(?=.{8,})(?=.*[A-Z])(?=.*[0-9]).*$/g.test(value)
    ? 'Password must contain at least 8 characters, one number, and at least one capital letter'
    : undefined;
// context.t('Password must contain at least 8 characters, one number, and at least one capital letter')

export const validatePasswordStricted = value =>
  value &&
  // eslint-disable-next-line
  !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[.,:;<*+!?=#$%&@^`~{[()\]}"'\\_|\-\/])[0-9a-zA-Z.,:;<*+!?=#$%&@^`~{[()\]}"'\\_|\-\/]{8,40}$/g.test(
    value
  )
    ? 'Password should be 8-40 alphanumeric. Contain at least 1 capital letter, 1 number and 1 symbol.'
    : undefined;
// context.t('Password should be 8-40 alphanumeric. Contain at least 1 capital letter, 1 number and 1 symbol.')

export const isDate = value => {
  let isInvalid =
    Object.prototype.toString.call(value) === '[object Date]'
      ? isNaN(value.getTime())
      : true;
  return value && isInvalid ? 'Invalid date' : undefined;
};
// context.t('Invalid date')

export const maxLength = max => value => {
  return value && value.length > max
    ? `Must be ${max} characters or less`
    : undefined;
};
/* context.t('Must be {a} characters or less', {
   a: max
}) */

export const isValidPhone = value =>
  isValidNumber(value, metadata) ? undefined : 'Must be a valid phone number';
// context.t('Must be a valid phone number')

export const maxFileSize = (value, maxSize) =>
  value && value.file && maxSize <= value.file.size / 1024 / 1024
    ? `File size must be under ${maxSize}mb`
    : undefined;

/* context.t('File size must be under {a}mb', {
   a: maxSize
}) */

export const maxDate = (value, maxDate) =>
  value && value > maxDate
    ? `Date must be before ${formatDate(maxDate)}`
    : undefined;

/* context.t('Date must be before {a}', {
   a: maxDate
}) */

export const minDate = (value, minDate) =>
  value && value < minDate
    ? `Date must be after ${formatDate(minDate)}`
    : undefined;

/* context.t('Date must be after {a}', {
   a: minDate
}) */

export const minTime = (value, minTime) => {
  if (!value) return;
  const time = getTimeObj(value);
  const min = getTimeObj(minTime);

  return time.hours < min.hours ||
    (time.hours === min.hours && time.minutes < min.minutes)
    ? `Time must be after ${formatTime(minTime)}`
    : undefined;
};

/* context.t('Time must be after {a}', {
   a: maxDate
}) */

export const maxTime = (value, maxTime) => {
  if (!value) return;
  const time = getTimeObj(value);
  const max = getTimeObj(maxTime);

  return time.hours > max.hours ||
    (time.hours === max.hours && time.minutes > max.minutes)
    ? `Time must be before ${formatTime(maxTime)}`
    : undefined;
};

/* context.t('Time must be before {a}', {
   a: minDate
}) */

const getTimeObj = date => ({
  hours: date.getHours(),
  minutes: date.getMinutes()
});

export const length = length => value => {
  return value && value.length !== length
    ? `Must be ${length} characters`
    : undefined;
};

// context.t('Invalid Bitazza ID')

export const btzID = value =>
  value && !/^@/g.test(value) ? 'Invalid Bitazza ID' : undefined;

export const btzIDOrEmail = value => {
  const emailResult = email(value);
  const btzIDResult = btzID(value);
  if (emailResult === undefined || btzIDResult === undefined) {
    return undefined;
  }
  return btzIDResult || emailResult;
};

export const lessThanOrEqualToValue = max => value =>
  value && value > max
    ? `Must be less than or equal to ${formatNumberToLocale(max, 2)}`
    : undefined;
/* context.t(Must be less than or equal to ${max}', {
   max: max
}) */

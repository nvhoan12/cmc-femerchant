import config from '../config';

/**
 * Get User Config to determine user is merchant or normal user.
 * @param {*} userConfig - userConfig from AP user.userConfig state.
 * @returns true or false
 */
export const isMerchantRole = (userConfig = []) => {
  const config = userConfig.find(data => data.Key === 'user_role');
  return !!(config && config.Value === 'merchant');
};

export const isStaffRole = (userConfig = []) => {
  const config = userConfig.find(data => data.Key === 'user_role');
  return !!(config && config.Value === 'staff');
};

export const isBrokerRole = (userConfig = []) => {
  const config = userConfig.find(data => data.Key === 'user_role');
  return !!(config && config.Value === 'broker');
};

export const isPermitToMerchantPage = (userConfig = []) => {
  return isMerchantRole(userConfig) || isStaffRole(userConfig);
};

export const getMerchantIdFromUserConfig = (userConfig = []) => {
  const merchantId = userConfig.find(data => data.Key === 'merchant_id');

  return (merchantId && merchantId.Value) || 'NOT_FOUND';
};

export const getUserRole = (userConfig = []) => {
  if (isMerchantRole(userConfig)) {
    return config.global.roles.find(role => role.name === 'merchant');
  }
  if (isStaffRole(userConfig)) {
    return config.global.roles.find(role => role.name === 'staff');
  }
  if (isBrokerRole(userConfig)) {
    return config.global.roles.find(role => role.name === 'broker');
  }
  return config.global.roles.find(role => role.name === 'default');
};

export const isGlobalUser = (userConfig = []) => {
  const config = userConfig.find(data => data.Key === 'is_global');
  return !!(config && config.Value === true);
};

import BigNumber from 'bignumber.js';
// import { getOrdersBySide, getQuantityForPrice } from './orderHelper';
import {
  buyValue,
  orderTypes
} from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import { orderFormTypes } from 'apex-web/lib/constants/sendOrder/orderFormTypes';
import { timeInForceForOrderType } from 'apex-web/lib/constants/sendOrder/timeInForceConstants';
import { pegPriceType } from 'apex-web/lib/constants/sendOrder/pegPriceTypeConstants';
// import { truncateToDecimals } from './numberFormatter';
// import config from 'apex-web/lib/config';
import { getOrderFormInfoSelector } from 'apex-web/lib/redux/selectors/buySellSelectors';

export const getFormValuesForSubmittingOrder = (orderFormType, options) => {
  const { instrument, userInfo, selectedAccountId } = options;

  const baseOptions = {
    InstrumentId: instrument.selectedInstrumentId,
    OMSId: userInfo.OMSId,
    AccountId: selectedAccountId,
    TimeInForce: 1,
    ClientOrderId: 0,
    OrderIdOCO: 0,
    UseDisplayQuantity: false
  };

  if (orderFormType === orderFormTypes.default) {
    return getFormValuesForDefaultForm(baseOptions, options);
  } else if (orderFormType === orderFormTypes.advanced) {
    return getFormValuesForAdvancedForm(baseOptions, options);
  } else if (orderFormType === orderFormTypes.block) {
    return getFormValuesForReportBlockTradeForm(baseOptions, options);
  } else if (orderFormType === orderFormTypes.buySell) {
    return getFormValuesForBuySellForm(baseOptions, options);
  } else {
    console.error(`order form type ${orderFormType} does not exists`);
    return {};
  }
};

export const getOrderEventSnackbar = order => {
  let type, text, textVars;
  const isCancelled = order.OrderState === 'Canceled';

  if (['Working', 'FullyExecuted'].some(i => order.OrderState === i)) {
    // context.t('Your order has been successfully added')
    text = 'Your order has been successfully added';
    type = 'success';
  } else if (order.OrderState === 'Rejected' || isCancelled) {
    if (order.RejectReason || order.CancelReason || order.ChangeReason) {
      text = order.RejectReason || order.CancelReason || order.ChangeReason;
      type = 'warning';
    } else {
      // context.t('Your Order has been {n}')
      text = 'Your Order has been {n}';
      textVars = { n: order.OrderState };
      type = isCancelled ? 'success' : 'warning';
    }
  }

  return { type, text, textVars };
};

export const isLimitPriceFieldInForm = orderType => {
  return [
    orderTypes.limit.displayName,
    orderTypes.stopLimit.displayName,
    orderTypes.fillOrKill.displayName,
    orderTypes.immediateOrCancel.displayName,
    orderTypes.reserveOrder.displayName
  ].some(i => i === orderType);
};

export const isTimeInForceFieldInForm = orderType => {
  return [
    orderTypes.limit.displayName,
    orderTypes.stopLimit.displayName,
    orderTypes.trailingStopLimit.displayName
  ].some(i => i === orderType);
};

export const isStopPriceFieldInForm = orderType => {
  return [
    orderTypes.stopMarket.displayName,
    orderTypes.stopLimit.displayName
  ].some(i => i === orderType);
};

export const isLimitOffsetFieldInForm = orderType => {
  return [orderTypes.trailingStopLimit.displayName].some(i => i === orderType);
};

export const isDisplayQuantityFieldInForm = orderType => {
  return [orderTypes.reserveOrder.displayName].some(i => i === orderType);
};

export const isTrailingAmountFieldInForm = orderType => {
  return [
    orderTypes.trailingStopLimit.displayName,
    orderTypes.trailingStopMarket.displayName
  ].some(i => i === orderType);
};

export const isPegPriceFieldInForm = orderType => {
  return [
    orderTypes.trailingStopLimit.displayName,
    orderTypes.trailingStopMarket.displayName
  ].some(i => i === orderType);
};

export const getOrderTypeByName = displayName => {
  const key = Object.keys(orderTypes).find(
    key => orderTypes[key].displayName === displayName
  );

  return orderTypes[key].orderType;
};

const getFormValuesForDefaultForm = (baseOptions, options) => {
  const { form } = options;
  const formValues = form;

  const defaultForm = {
    ...baseOptions,
    Side: Number(formValues.side),
    Quantity: formValues.quantity,
    OrderType: Number(getOrderTypeByName(formValues.orderType)),
    PegPriceType: getPegPriceTypeBySide(formValues.side)
  };

  if (isLimitPriceFieldInForm(formValues.orderType)) {
    defaultForm.LimitPrice = formValues.limitPrice;
  }

  if (isTimeInForceFieldInForm(formValues.orderType)) {
    defaultForm.TimeInForce = Number(formValues.timeInForce);
  }

  if (isStopPriceFieldInForm(formValues.orderType)) {
    defaultForm.StopPrice = formValues.stopPrice;
  }

  return defaultForm;
};

const getFormValuesForAdvancedForm = (baseOptions, options) => {
  const { form } = options;
  const formValues = form;

  const advancedForm = {
    ...baseOptions,
    Side: Number(formValues.side),
    Quantity: formValues.quantity,
    OrderType: Number(getOrderTypeByName(formValues.orderType)),
    TimeInForce: getTimeInForceValue(formValues.orderType),
    PegPriceType: getPegPriceTypeBySide(formValues.side)
  };

  if (isLimitPriceFieldInForm(formValues.orderType)) {
    advancedForm.LimitPrice = formValues.limitPrice;
  }
  if (isTimeInForceFieldInForm(formValues.orderType)) {
    advancedForm.TimeInForce = Number(formValues.timeInForce);
  }
  if (isStopPriceFieldInForm(formValues.orderType)) {
    advancedForm.StopPrice = formValues.stopPrice;
  }
  if (isTrailingAmountFieldInForm(formValues.orderType)) {
    advancedForm.TrailingAmount = formValues.trailingAmount;
  }
  if (isPegPriceFieldInForm(formValues.orderType)) {
    advancedForm.PegPriceType = formValues.pegPriceType;
  }
  if (isDisplayQuantityFieldInForm(formValues.orderType)) {
    advancedForm.DisplayQuantity = formValues.displayQuantity;
    advancedForm.UseDisplayQuantity = true;
  }
  if (isLimitOffsetFieldInForm(formValues.orderType)) {
    advancedForm.LimitOffset = formValues.limitOffset;
  }
  return advancedForm;
};

const getFormValuesForReportBlockTradeForm = (baseOptions, options) => {
  const { form } = options;
  const formValues = form;

  const price =
    form.side === buyValue
      ? new BigNumber(formValues.price)
      : new BigNumber(formValues.quantity);
  const quantity =
    form.side === buyValue
      ? new BigNumber(formValues.quantity)
      : new BigNumber(formValues.price);

  let LimitPrice = price.toFixed(options.priceDecimalPlaces).toString();
  if (!options.useNewForm) {
    LimitPrice = price
      .div(quantity)
      .toFixed(options.priceDecimalPlaces)
      .toString();
  }

  return {
    LimitPrice,
    InstrumentId: baseOptions.InstrumentId,
    AccountId: baseOptions.AccountId,
    Side: parseInt(formValues.side, 10),
    counterPartyId: formValues.counterparty.toString(),
    Quantity: quantity,
    OMSId: baseOptions.OMSId,
    LockedIn: formValues.lockedIn,
    Timestamp: Date.now()
  };
};

const getFormValuesForBuySellForm = (baseOptions, options) => {
  const { form, level2, state } = options; // eslint-disable-line
  // const { roundBuyLimitPrices } = config.RetailBuySell;
  const formValues = form;
  const selectedValues = getOrderFormInfoSelector(state);

  // const orders = getOrdersBySide(formValues.side, level2);
  // const { OrderCalcPadding: { quantityPadding } = {} } = config;
  // let LimitPrice = getQuantityForPrice(
  //   selectedValues.price,
  //   quantityPadding,
  //   orders,
  //   false,
  //   formValues.side
  // ).LimitPrice;
  // const buy = !Number(formValues.side);
  // LimitPrice = truncateToDecimals(
  //   LimitPrice,
  //   options.priceDecimalPlaces,
  //   roundBuyLimitPrices && buy ? BigNumber.ROUND_CEIL : null
  // ).toNumber();

  return {
    ...baseOptions,
    TimeInForce: timeInForceForOrderType['Good till Cancel'],
    Side: Number(formValues.side),
    Quantity: selectedValues.quantity,
    OrderType: Number(orderTypes.market.orderType),
    PegPriceType: pegPriceType.ask
  };
};

const getTimeInForceValue = orderType => {
  let timeInForceValue = 1;
  if (timeInForceForOrderType[orderType] !== undefined) {
    timeInForceValue = timeInForceForOrderType[orderType];
  }
  return timeInForceValue;
};

export const getPegPriceByType = (level1, form) => {
  const level1ForInstrument = level1[form.instrument];

  if (form.pegPriceType === pegPriceType.ask) {
    return level1ForInstrument.BestOffer;
  } else if (form.pegPriceType === pegPriceType.bid) {
    return level1ForInstrument.BestBid;
  } else if (form.pegPriceType === pegPriceType.last) {
    return level1ForInstrument.LastTradedPx;
  }
};

const getPegPriceTypeBySide = side => {
  return side === buyValue ? pegPriceType.ask : pegPriceType.bid;
};

const {
  isMerchantRole,
  isStaffRole,
  isPermitToMerchantPage,
  getMerchantIdFromUserConfig
} = require('./userConfigHelper');

const staffConfig = [
  {
    Key: 'user_role',
    Value: 'staff'
  }
];

const merchantConfig = [
  {
    Key: 'user_role',
    Value: 'merchant'
  }
];

describe('userConfigHelper', () => {
  describe('#getMerchantIdFromUserConfig', () => {
    it('should get merchant_id key when user is staff or merchant', () => {
      const userConfig = [
        {
          Key: 'merchant_id',
          Value: 100
        },
        {
          Key: 'user_role',
          Value: 'staff'
        }
      ];

      expect(getMerchantIdFromUserConfig(userConfig)).toBe(100);
    });

    it('should return NOT_FOUND when do not have merchant_id in userConfig', () => {
      const userConfig = [];

      expect(getMerchantIdFromUserConfig(userConfig)).toEqual('NOT_FOUND');
    });
  });

  describe('#isPermitToMerchantPage', () => {
    it('should return TRUE when user role is staff or merchant', () => {
      expect(isPermitToMerchantPage(staffConfig)).toBe(true);
      expect(isPermitToMerchantPage(merchantConfig)).toBe(true);
    });

    it('should return FALSE when user role not found or neither staff or merchant', () => {
      const userConfig = [
        {
          Key: 'user_role',
          Value: 'superman'
        }
      ];
      expect(isPermitToMerchantPage([])).toBe(false);
      expect(isPermitToMerchantPage(userConfig)).toBe(false);
    });
  });

  describe('#isStaffRole', () => {
    it('should check isStaffRole properly', () => {
      expect(isStaffRole(staffConfig)).toBe(true);
      expect(isStaffRole(merchantConfig)).toBe(false);
      expect(isStaffRole([])).toBe(false);
    });
  });

  describe('#isMerchantRole', () => {
    it('should check isStaffRole properly', () => {
      expect(isMerchantRole(staffConfig)).toBe(false);
      expect(isMerchantRole(merchantConfig)).toBe(true);
      expect(isMerchantRole([])).toBe(false);
    });
  });
});

const cacheLocaltionKey = 'cache-localtion';

const cacheLocaltion = async () => {
  const geoApi =
    'https://api.ipdata.co/?api-key=14bbe4fe9a1e1f36435f11782e020a8879e563e53013a9b09d68bdc1';
  const res = await fetch(geoApi, {
    method: 'GET'
  });
  const jsonRes = await res.json();
  if (res.status === 200) {
    const country = jsonRes.country_name;
    const cacheDate = new Date();
    localStorage.setItem(
      cacheLocaltionKey,
      JSON.stringify({ country, cacheDate })
    );
  } else {
    localStorage.removeItem(cacheLocaltionKey);
  }
};

const getCountry = () => {
  const local = localStorage.getItem(cacheLocaltionKey);
  if (local) {
    const localJson = JSON.parse(local);
    return localJson.country;
  }
  return null;
};

const initCacheLocaltion = () => {
  const local = localStorage.getItem(cacheLocaltionKey);
  if (local) {
    const localJson = JSON.parse(local);
    if (new Date() - new Date(localJson.cacheDate) > 86400000) {
      // 1 day
      cacheLocaltion();
    }
  } else {
    cacheLocaltion();
  }
};

export { initCacheLocaltion, getCountry };

import React from 'react';
import {
  Action,
  Amount,
  AmountValue,
  Status,
  DateTime,
  Text,
  Product,
  InvoiceStatus
} from '../components/RecentActivity/ActivityCellComponents';
import {
  recentActivitySelector,
  selectedRecentActivitySelector
} from '../redux/selectors/recentActivitySelector';

export const formatActivity = (activity, context) => {
  return activity.map(item => {
    const { Type } = item;
    switch (Type) {
      case 'deposit':
      case 'withdraw': {
        const { Product, ProductType, Status } = item.Action.params;
        item.Action = {
          ...item.Action,
          primaryText:
            Type === 'deposit'
              ? context.t('Deposited {a}', { a: Product })
              : context.t('Withdrew {a}', { a: Product }),
          secondaryText:
            ProductType === 'CryptoCurrency'
              ? Type === 'deposit'
                ? context.t('from External Address')
                : context.t('to External Address')
              : Type === 'deposit'
                ? context.t('from Bank')
                : context.t('to Bank')
        };
        item.Status = {
          ...item.Status,
          status: context.t('{a}', { a: Status })
        };
        return item;
      }
      case 'send':
      case 'receive': {
        const {
          Product,
          ReceiverUserName,
          SenderUserName
        } = item.Action.params;
        item.Action = {
          ...item.Action,
          primaryText:
            Type === 'send'
              ? context.t('Sent {a}', { a: Product })
              : context.t('Received {a}', { a: Product }),
          secondaryText:
            Type === 'send'
              ? context.t(`to ${ReceiverUserName}`)
              : context.t(`to ${SenderUserName}`)
        };
        item.Status = {
          ...item.Status,
          status: context.t('Complete')
        };
        return item;
      }
      case 'trades': {
        const { Product1Symbol, Product2Symbol, Side } = item.Action.params;
        item.Action = {
          ...item.Action,
          primaryText:
            Side === 'Buy'
              ? context.t('Bought {a}', { a: Product1Symbol })
              : context.t('Sold {a}', { a: Product1Symbol }),
          secondaryText:
            Side === 'Buy'
              ? context.t('with {a}', { a: Product2Symbol })
              : context.t('for {a}', { a: Product2Symbol })
        };
        item.Status = {
          ...item.Status,
          status: context.t('Complete')
        };
        return item;
      }
      default:
        return item;
    }
  });
};

export const getColumns = (width, themeModifier, context) => {
  const columns = [
    {
      header: context.t('Action'),
      cell: row => (
        <Action
          value={row.Action}
          showProductIcon
          themeModifier={themeModifier}
        />
      )
    },
    {
      header: context.t('Amount'),
      width: '20rem',
      cell: row => <Amount value={row.Amount} />
    },
    {
      header: context.t('Status / ID'),
      width: '40rem',
      cell: row => <Status value={row.Status} />
    },
    {
      header: context.t('Time & Date'),
      width: '15rem',
      cell: row => <DateTime value={row.TimeStamp} />
    }
  ];

  return columns;
};

export const getColumnsOfMerchant = (width, themeModifier, context) => {
  const columns = [
    {
      header: context.t('Transaction ID'),
      cell: row => <Text value={row.TransactionID} />
    },
    {
      header: context.t('Product'),
      cell: row => <Product value={row.Product} />
    },
    {
      header: context.t('Crypto Amount'),
      width: '20rem',
      cell: row => <AmountValue value={row.AmountCrypto} />
    },
    {
      header: context.t('Invoice Amount'),
      width: '20rem',
      cell: row => <AmountValue value={row.Amount} />
    },
    {
      header: context.t('Received Amount'),
      cell: row => <AmountValue value={row.ReceivedAmount} />
    },
    {
      header: context.t('Status'),
      cell: row => <InvoiceStatus value={row.Status} />
    },
    {
      header: context.t('Invoice Date'),
      cell: row => <DateTime value={row.TimeStamp} />
    }
  ];

  return columns;
};

export const receivedAmountCal = (blocktradeAmount, blocktradePrice) => {
  if (blocktradeAmount && blocktradePrice) {
    return blocktradeAmount * blocktradePrice;
  }
  return 0;
};

export const getRecentActivity = (state, filter) => {
  if (filter) {
    return selectedRecentActivitySelector(state);
  }

  return recentActivitySelector(state);
};

import React from 'react';

import padStart from 'lodash/padStart';
import config from '../config';
import APInput from '../components/common/APInput';
import APNumberInput from '../components/common/APNumberInput';
import APSelect from '../components/common/APSelect';
import APDatePicker from '../components/common/APDatePicker/APDatePicker';
import APPhoneInput from '../components/common/APPhoneInput';
import APDocumentUpload from '../components/common/APDocumentUpload/APDocumentUpload';
import APImageUpload from '../components/common/APImageUpload/APImageUpload';
import APUpload from '../components/common/APUpload/APUpload';
import APTimePickerDropdown from '../components/common/APTimePickerDropdown/APTimePickerDropdown';
import APDatePickerDropdown from '../components/common/APDatePickerDropdown/APDatePickerDropdown';
import {
  required,
  email,
  alphanumeric,
  isDate,
  maxLength,
  isValidPhone,
  numeric
} from './formValidations';

export const renderFormInputs = (fields = [], customClass, context, change) => {
  const inputTypes = {
    text: renderInput,
    email: renderInput,
    number: renderNumberInput,
    select: renderSelect,
    date: renderDatePicker,
    timeDropdown: renderDatePickerDropdown,
    dateDropdown: renderTimePickerDropdown,
    phone: renderPhoneInput,
    image: renderImageUpload,
    document: renderDocumentUpload,
    country: renderCountry,
    upload: renderUpload
  };
  return fields
    .filter(field => {
      if (!inputTypes[field.type])
        console.error(
          `Unsupported ${field.type} input type on field: ${field.name}`
        );
      return inputTypes[field.type];
    })
    .map(field => inputTypes[field.type](field, customClass, context, change));
};

const renderInput = (field, customClass, context) => {
  const validators = getInputValidators(field);
  return (
    <APInput
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      type={field.type}
      placeholder={context.t(field.placeholder)}
      validate={validators}
      info={context.t(field.info)}
      customClass={customClass}
    />
  );
};

const renderNumberInput = (field, customClass, context) => {
  const validators = getInputValidators(field);
  return (
    <APNumberInput
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      type={field.type}
      placeholder={context.t(field.placeholder)}
      validate={validators}
      info={context.t(field.info)}
      customClass={customClass}
    />
  );
};

const renderSelect = (field, customClass, context) => {
  return (
    <APSelect
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      type={field.type}
      info={context.t(field.info)}
      customClass={customClass}
      options={
        Array.isArray(field.options)
          ? field.options.map(item => ({ value: item, label: context.t(item) }))
          : Object.keys(field.options).map(prop => ({
              value: prop,
              label: context.t(field.options[prop])
            }))
      }
    />
  );
};

const renderCountry = (field, customClass, context) => {
  let options = config.Countries;
  const keys = Object.keys(config.Countries);

  if (field.whiteList) {
    options = keys.filter(country =>
      field.whiteList.includes(options[country])
    );
  } else if (field.blackList) {
    options = keys.filter(
      country => !field.blackList.includes(options[country])
    );
  }

  field.options = options;

  return renderSelect(field, customClass, context);
};

const renderImageUpload = (field, customClass, context, change) => {
  const validators = getInputValidators(field);
  return (
    <APImageUpload
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      alt={field.alt ? context.t(field.alt) : context.t(field.label)}
      info={context.t(field.info)}
      customClass={customClass}
      validate={validators}
      placeholder={field.placeholder}
      maxSize={field.maxSize}
      onChange={change}
    />
  );
};

const renderDocumentUpload = (field, customClass, context, change) => {
  const validators = getInputValidators(field);
  return (
    <APDocumentUpload
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      info={context.t(field.info)}
      customClass={customClass}
      validate={validators}
      placeholder={field.placeholder}
      maxSize={field.maxSize}
      onChange={change}
    />
  );
};

const renderUpload = (field, customClass, context, change) => {
  const validators = getInputValidators(field);
  return (
    <APUpload
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      info={context.t(field.info)}
      customClass={customClass}
      validate={validators}
      placeholder={field.placeholder}
      maxSize={field.maxSize}
      acceptedFiles={field.acceptedFiles}
      customChange={change}
    />
  );
};

const renderDatePicker = (field, customClass, context) => {
  const validators = getInputValidators(field);
  return (
    <APDatePicker
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      type={field.type}
      info={context.t(field.info)}
      validate={validators}
      customClass={customClass}
      maxDate={new Date()}
    />
  );
};

const renderDatePickerDropdown = (field, customClass, context) => {
  const validators = getInputValidators(field);
  return (
    <APDatePickerDropdown
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      info={context.t(field.info)}
      validate={validators}
      customClass={customClass}
      maxDate={new Date()}
      leadingZero={field.leadingZero}
    />
  );
};

const renderTimePickerDropdown = (field, customClass, context) => {
  const validators = getInputValidators(field);
  return (
    <APTimePickerDropdown
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      type={field.type}
      info={context.t(field.info)}
      validate={validators}
      customClass={customClass}
      leadingZero={field.leadingZero}
    />
  );
};

const renderPhoneInput = (field, customClass, context) => {
  const validators = getInputValidators(field);
  return (
    <APPhoneInput
      key={field.name}
      name={field.name}
      label={context.t(field.label)}
      info={context.t(field.info)}
      validate={validators}
      customClass={customClass}
    />
  );
};

const mapStringToValidators = {
  required,
  email,
  alphanumeric,
  isDate,
  maxLength,
  isValidPhone,
  numeric
};

const getInputValidators = field => {
  if (!field.validators) return;

  let result =
    field.validators &&
    field.validators.map(validator => {
      const [validatorName, validatorArgs] = validator.split(':');
      if (validatorArgs) {
        const passedArgs = validatorArgs.split(',');
        return mapStringToValidators[validatorName](...passedArgs);
      } else {
        return mapStringToValidators[validatorName];
      }
    });

  return result.filter(item => item);
};

export const generateOptionsFromNumber = (min = 0, max, pad) => {
  const options = [];

  for (var i = min; i < max; i++) {
    options.push(
      <option key={i} value={i}>
        {padStart(i, pad, '0')}
      </option>
    );
  }

  return options;
};

export default renderFormInputs;

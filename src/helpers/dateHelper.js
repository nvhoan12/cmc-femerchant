import config from '../config';
import format from 'date-fns/format';
import isDate from 'date-fns/is_date';

/**
 * converts .NET ticks to UTC timestamp
 */
export function ticksToDateString(ticks) {
  if (typeof ticks !== 'number') return false;
  if (ticks === 0) return 'N/A';

  const ticksSinceEpoch = ticks - 621355968000000000;
  const millisecondsSinceEpoch = ticksSinceEpoch / 10000;
  const date = new Date(millisecondsSinceEpoch);

  return formatDateTime(date);
}

// Formats supported are any ordering of M/MM, D/DD and YYYY with -, /, or . separators.
export function parseDate(dateString) {
  if (dateString.length === 0) {
    return '';
  }
  const { dateFormat } = config.global;
  const regex = /[^.\-/]+/g;
  const keys = dateFormat.match(regex);
  const parsed = dateString.match(regex);
  const dateObj = {};
  keys.forEach((key, index) => {
    dateObj[key.charAt(0)] = parseInt(parsed[index], 10);
  });
  return new Date(dateObj['Y'], dateObj['M'] - 1, dateObj['D']);
}

export function parseTime(timeArg) {
  if (!timeArg) return;
  if (isDate(timeArg)) return timeArg;

  const [time, amPm] = timeArg.split(' ');
  let [hours, minutes] = time.split(':');

  if (amPm && amPm.toLowerCase() === 'pm') {
    hours = +hours + 12;
  }

  return new Date(0, 0, 0, +hours, +minutes);
}

export function formatDate(date, customFormat) {
  return format(date, customFormat || config.global.dateFormat);
}

export function formatTime(time) {
  return format(time, config.global.timeFormat);
}

export function formatDateTime(dateTime) {
  return format(dateTime, config.global.dateTimeFormat);
}

export function createInputDateMask() {
  // Divide dateFormat to groups like [M/,D/,YYYY]
  let formatMask = '',
    inputMask = '';
  const separator = config.global.dateFormat.match(/(\W)/g)[0];
  const reg = new RegExp(
    `([A-Za-z]+${separator})([A-Za-z]+${separator})([A-Za-z]+)`
  );
  const regArray = config.global.dateFormat.match(reg);
  const maxGroupsSize = 3;

  for (let i = 1; i <= maxGroupsSize; i++) {
    if (regArray[i].length === 2 && regArray[i].match(`${separator}`)) {
      // Check option like M/ and replace with MM/
      formatMask += regArray[i].slice(0, -1) + regArray[i];
    } else if (regArray[i].length === 1) {
      formatMask += regArray[i].repeat(2);
    } else {
      formatMask += regArray[i];
    }
  }

  // Replace all symbols to 9
  inputMask = formatMask.replace(/[A-Za-z]/g, '9');
  return { inputMask, formatMask };
}

export function dateDiff(dateStart, dateEnd) {
  const diff = dateEnd - dateStart;
  if (diff <= 0) {
    return null;
  }
  let delta = diff / 1000;

  const days = Math.floor(delta / 86400);
  delta -= days * 86400;

  const hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;

  const minutes = Math.floor(delta / 60) % 60;
  delta -= minutes * 60;

  const seconds = parseInt(delta % 60);
  return { days, hours, minutes, seconds };
}

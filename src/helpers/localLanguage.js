const getFirstBrowserLanguage = () => {
  let nav = window.navigator;
  let browserLanguagePropertyKeys = [
    'language',
    'browserLanguage',
    'systemLanguage',
    'userLanguage'
  ];
  let i;
  let language;
  // support for HTML 5.1 "navigator.languages"
  if (Array.isArray(nav.languages)) {
    for (i = 0; i < nav.languages.length; i++) {
      language = nav.languages[i];
      if (language && language.length) {
        return language.replace(/-[a-zA-Z]+/g, '');
      }
    }
  }
  // support for other well known properties in browsers
  for (i = 0; i < browserLanguagePropertyKeys.length; i++) {
    language = nav[browserLanguagePropertyKeys[i]];
    if (language && language.length) {
      return language.replace(/-[a-zA-Z]+/g, '');
    }
  }
  return null;
};

const getlocalLanguage = () => {
  const language = localStorage.getItem('language');
  if (language) {
    return language;
  }
  const browserLanguage = getFirstBrowserLanguage();
  if (browserLanguage) {
    localStorage.setItem('language', browserLanguage.toUpperCase());
    return browserLanguage.toUpperCase();
  }
  return 'TH';
};

const setlocalLanguage = language => {
  localStorage.setItem('language', language.toUpperCase());
};

export { getlocalLanguage, setlocalLanguage };

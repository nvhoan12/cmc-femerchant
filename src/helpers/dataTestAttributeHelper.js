import { isString } from './stringHelper';

export const getDataTestAttribute = (props, fieldName) => {
  return props.dataTest || (isString(props[fieldName]) ? props[fieldName] : '');
};

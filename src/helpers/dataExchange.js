import config from '../config';
import { getQueryParam } from './queryParamsHelper';

const dataExchangeParamKey = 'dataExchange';

const initDataExchange = async () => {
  const paramToken = getQueryParam(dataExchangeParamKey);
  if (paramToken) {
    const res = await fetch(config.services.getDataExchange(paramToken), {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'X-BTZ-TOKEN': localStorage.getItem('token')
      }
    });
    try {
      const jsonRes = await res.json();
      const dataObj = JSON.parse(jsonRes.data);
      Object.keys(dataObj).map(key => {
        localStorage.setItem(key, dataObj[key]);
      });
      window.location.replace(
        window.location.origin + window.location.pathname
      );
    } catch (error) {
      console.log(error);
    }
  }
};

const saveDataExchange = async (token, data) => {
  const res = await fetch(config.services.saveDataExchange, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-BTZ-TOKEN': token
    },
    body: JSON.stringify({ data })
  });
  const jsonRes = await res.json();
  return jsonRes.token;
};

export { initDataExchange, saveDataExchange, dataExchangeParamKey };

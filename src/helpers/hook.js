import React from 'react';
import { debounce } from 'lodash';

const useDebounce = (callback, delay) => {
  const d = callback;
  const callbackfunc = React.useCallback(debounce(d, delay), []);
  return callbackfunc;
};

const useDidUpdate = (callback, deps = []) => {
  const hasMount = React.useRef(false);
  React.useEffect(() => {
    if (hasMount.current) {
      callback();
    } else {
      hasMount.current = true;
    }
    // eslint-disable-next-line
  }, deps);
};

export { useDebounce, useDidUpdate };

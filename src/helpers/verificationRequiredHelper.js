import config from '../config';

export const getVerifiedToTradeStatus = (
  verificationLevel,
  levelIncreaseStatus
) => {
  const minimumVerificationLevel = config.KYC.MinimumVerificationLevel;
  let disableTrading = true;
  let isUnderReview = false;

  if (verificationLevel >= minimumVerificationLevel) disableTrading = false;
  if (levelIncreaseStatus === 'underReview') isUnderReview = true;
  if (verificationLevel === levelIncreaseStatus - 1) isUnderReview = true;

  return {
    disableTrading,
    isUnderReview
  };
};

import BigNumber from 'bignumber.js';
import {
  buyValue,
  orderTypes,
  sellValue,
  submitButtonTextOptions
} from 'apex-web/lib/constants/sendOrder/orderEntryFormConstants';
import {
  makerValue,
  takerValue
} from 'apex-web/lib/constants/sendOrder/makerTakerConstants';
import {
  formatNumberToLocale,
  parseNumberToLocale,
  truncateToDecimals
} from './numberFormatter';
import config from 'apex-web/lib/config';
import { convertIncrementToIntDecimalPlaces } from 'apex-web/lib/helpers/decimalPlaces/decimalPlacesHelper';

/* This is reusable, project independent helper. Here shouldn't be store, or specific forms (except place order forms),
 only pure helpers */
export const getQuantityForPrice = (
  price,
  padding = 0,
  orders = [],
  fill = false,
  side = sellValue
) => {
  const parsedPrice = new BigNumber(price);
  const parsedPadding = new BigNumber(padding);
  const paddedPrice = parsedPrice.minus(parsedPadding.times(parsedPrice));
  let remainingPrice = new BigNumber(paddedPrice);
  let sum = new BigNumber(0);
  let quantity = new BigNumber(0);
  let lastPrice;
  let lastOrder;
  orders = [...orders];

  if (side === buyValue) {
    orders.reverse();
  }

  if (orders.length) {
    orders.some(order => {
      const orderQty = new BigNumber(order.Quantity);
      const orderPx = new BigNumber(order.Price);
      const orderTotal = orderQty.times(orderPx);

      sum = sum.plus(orderTotal);
      if (sum.lt(paddedPrice)) {
        quantity = quantity.plus(orderQty);
      } else {
        const remainder = paddedPrice.minus(sum.minus(orderTotal));
        quantity = quantity.plus(remainder.div(orderPx));
        lastPrice = orderPx;
      }
      remainingPrice = remainingPrice.minus(orderTotal);
      lastOrder = order;
      return sum.gte(paddedPrice);
    });
  }
  if (fill && remainingPrice.gt(0) && orders.length) {
    quantity = quantity.plus(remainingPrice.div(lastOrder.Price));
    sum = paddedPrice;
  }
  if (sum.lt(paddedPrice)) {
    return { Price: parsedPrice, Quantity: '-' };
  }
  return {
    Price: parsedPrice,
    Quantity: quantity,
    LimitPrice: lastPrice || (lastOrder ? `${lastOrder.Price}` : '0')
  };
};

export const getPriceForQuantity = (
  quantity,
  padding = 0,
  orders = [],
  fill = false,
  side
) => {
  const parsedQuantity = new BigNumber(quantity);
  const parsedPadding = new BigNumber(padding);
  const paddedQty = parsedQuantity.plus(parsedPadding.times(quantity));
  let price = new BigNumber(0);
  let remainingQty = new BigNumber(paddedQty);
  let lastOrderPrice = new BigNumber(0);
  let sortedOrders = [...orders];

  if (side === buyValue) {
    sortedOrders.sort((a, b) => a.Price - b.Price);
  } else {
    sortedOrders.sort((a, b) => b.Price - a.Price);
  }

  sortedOrders.some(order => {
    const orderQuantity = new BigNumber(order.Quantity);
    const orderPrice = new BigNumber(order.Price);

    if (remainingQty.gt(orderQuantity)) {
      price = price.plus(orderQuantity.times(orderPrice));
    } else {
      price = price.plus(remainingQty.times(orderPrice));
    }
    remainingQty = remainingQty.minus(orderQuantity);
    lastOrderPrice = orderPrice;
    return remainingQty.lte(0);
  });

  if (fill && remainingQty.gt(0)) {
    price = price.plus(remainingQty.times(lastOrderPrice));
    remainingQty = 0;
  }
  if (remainingQty > 0) {
    return { Quantity: parsedQuantity, Price: '-', LimitPrice: lastOrderPrice };
  }

  return { Quantity: parsedQuantity, Price: price, LimitPrice: lastOrderPrice };
};

/**
 * @param {String} quantity
 * @param {Object} options
 * @param {Object} options.level2
 * @param {Array} options.level2.buy
 * @param {Array} options.level2.sell
 * @param {Object} options.selectedInstrument
 * @param {String} options.side
 */
export const syncPriceWithQuantity = (quantity, options) => {
  const { level2, selectedInstrument, side } = options;
  const orders = getOrdersBySide(side, level2);
  const { OrderCalcBuffer: { priceBuffer } = {} } = config;

  if (orders.length) {
    const priceForQuantity = getPriceForQuantity(
      quantity,
      priceBuffer,
      orders,
      true,
      side
    ).Price;
    return truncateToDecimals(
      priceForQuantity,
      convertIncrementToIntDecimalPlaces(selectedInstrument.PriceIncrement)
    );
  }

  return 0;
};

/**
 * @param {String} price
 * @param {Object} options
 * @param {Object} options.level2
 * @param {Array} options.level2.buy
 * @param {Array} options.level2.sell
 * @param {Object} options.selectedInstrument
 * @param {String} options.side
 */
export const syncQuantityWithPrice = (price, options) => {
  const { level2, selectedInstrument, side } = options;
  const orders = getOrdersBySide(side, level2);
  const { OrderCalcBuffer: { quantityBuffer } = {} } = config;

  if (orders.length) {
    return truncateToDecimals(
      getQuantityForPrice(price, quantityBuffer, orders, true, side).Quantity,
      convertIncrementToIntDecimalPlaces(selectedInstrument.QuantityIncrement)
    );
  }

  return 0;
};

export const isMakerOrTaker = (
  buyOrSell,
  price,
  bestBid,
  bestOffer,
  orderType
) => {
  const takerOrderTypes = [
    orderTypes.market.displayName,
    orderTypes.immediateOrCancel.displayName,
    orderTypes.stopMarket.displayName,
    orderTypes.trailingStopMarket.displayName
  ];
  if (takerOrderTypes.includes(orderType)) {
    return takerValue;
  }
  if (buyOrSell === buyValue) {
    return price < bestOffer || Number(bestOffer) === 0
      ? makerValue
      : takerValue;
  } else if (buyOrSell === sellValue) {
    return price > bestBid || Number(bestBid) === 0 ? makerValue : takerValue;
  }

  console.error(`Incorrect buyOrSell value is provided: ${buyOrSell}`);

  return takerValue;
};

export const getOrderFee = (form, positions, symbolPosition = 'beginning') => {
  const { fee } = form.values;
  if (fee) {
    const { OrderFee, ProductId, FeeProductId } = fee;
    const feeProductId = FeeProductId ? FeeProductId : ProductId;
    const { ProductSymbol } = positions[feeProductId] || { ProductSymbol: '' };

    const formattedFee = formatNumberToLocale(OrderFee, ProductSymbol);
    if (symbolPosition === 'beginning')
      return `${ProductSymbol} ${formattedFee}`;
    if (symbolPosition === 'end') return `${formattedFee} ${ProductSymbol}`;
  }
  return '0';
};

export const getOrderLeverage = form => {
  const { fee } = form.values;
  if (fee) {
    return fee.Leverage || '—';
  }
  return '—';
};

export const getOrderBorrowingPower = (
  form,
  selectedInstrument,
  products = []
) => {
  const { fee, side } = form.values;

  if (fee && fee.BorrowingPower) {
    return fee.BorrowingPower;
  } else if (products.length > 0) {
    const productSymbol =
      side === buyValue
        ? selectedInstrument.Product2Symbol
        : selectedInstrument.Product1Symbol;
    const product = products.find(
      product => product.ProductSymbol === productSymbol
    );
    const borrowingPower = product ? product.BorrowingPower : '0';

    return borrowingPower;
  }
  return '0';
};

export const getOrderPricePerUnit = (
  pricePerUnit,
  ProductSymbol,
  decimalPlaces
) => {
  if (!pricePerUnit) {
    return '0';
  }

  return `${formatNumberToLocale(
    parseNumberToLocale(pricePerUnit),
    decimalPlaces
  )} per ${ProductSymbol}`;
};

export const getOrderTotal = (form, instrument) => {
  const { totalAmount } = form.values;
  const { Product2Symbol } = instrument;
  if (!totalAmount) {
    return '0';
  }
  return `${Product2Symbol} ${formatNumberToLocale(
    totalAmount,
    convertIncrementToIntDecimalPlaces(instrument.PriceIncrement)
  )}`;
};

export const getOrderNet = (
  form,
  instrument,
  orderTotal,
  displaySymbol = true
) => {
  const { quantity, fee, side } = form.values;
  const {
    PriceIncrement,
    QuantityIncrement,
    Product1,
    Product2,
    Product1Symbol,
    Product2Symbol
  } = instrument;
  const productDecimalPlaces = convertIncrementToIntDecimalPlaces(
    side === buyValue ? QuantityIncrement : PriceIncrement
  );
  const productSymbol = side === buyValue ? Product1Symbol : Product2Symbol;
  let orderNet;

  if (!orderTotal || !quantity) {
    return '0';
  }

  if (side === buyValue) {
    orderNet =
      quantity -
      (fee.ProductId === Product1 || fee.FeeProductId === Product1
        ? fee.OrderFee
        : 0);
  } else {
    orderNet =
      orderTotal -
      (fee.ProductId === Product2 || fee.FeeProductId === Product2
        ? fee.OrderFee
        : 0);
  }

  if (!displaySymbol) {
    return formatNumberToLocale(orderNet, productDecimalPlaces);
  }

  return `${productSymbol} ${formatNumberToLocale(
    orderNet,
    productDecimalPlaces
  )}`;
};

export const getOrdersBySide = (side, level2) => {
  return side === buyValue ? level2.sell : level2.buy;
};

export const getSubmitButtonText = form => {
  const { side } = form.values;
  return side
    ? submitButtonTextOptions[side]
    : submitButtonTextOptions[buyValue];
};

export const getMarketPrice = (side, instrument, l1, decimalPlacesInt) => {
  if (side && instrument && l1) {
    const marketPrice = (side === buyValue ? l1.BestOffer : l1.BestBid) || 0;

    return truncateToDecimals(marketPrice, decimalPlacesInt);
  }

  return '0';
};

export const mergeOpenOrders = orders => {
  const userOrders = [...orders];
  const uniqOrders = [];

  userOrders.sort((a, b) => a.Price.minus(b.Price));

  userOrders.forEach(item => {
    const itemBefore = uniqOrders[uniqOrders.length - 1];

    if (itemBefore && item.Price.eq(itemBefore.Price)) {
      const mergeItem = {
        ...item,
        DisplayQuantity: new BigNumber(item.DisplayQuantity).plus(
          new BigNumber(itemBefore.DisplayQuantity)
        ),
        OrigQuantity: item.OrigQuantity.plus(itemBefore.OrigQuantity),
        OrderIds: [item.OrderId].concat(itemBefore.OrderIds)
      };

      uniqOrders.splice(uniqOrders.length - 1, 1, mergeItem);
    } else {
      uniqOrders.push({ ...item, OrderIds: [item.OrderId] });
    }
  });

  return uniqOrders;
};

export const getValuesFromDealValues = (
  dealValues,
  value,
  fieldToSearch = 'price'
) => {
  return (
    dealValues.find(i => parseFloat(i[fieldToSearch]) === parseFloat(value)) ||
    {}
  );
};

export const getOrderPrice = (form, marketPrice) => {
  if (form.values.orderType === orderTypes.limit.displayName) {
    return form.values.limitPrice;
  } else if (form.values.orderType === orderTypes.stopMarket.displayName) {
    return form.values.stopPrice;
  } else if (form.values.orderType === orderTypes.market.displayName) {
    return marketPrice;
  }
  return '—';
};

import Adjust from '@adjustcom/adjust-web-sdk';

// Deposit_attempt: z07d8n
// Deposit_fulfilled: zh3cvv
// Email_verified: r3uh2m
// KYC_approved: 5gos0m
// KYC_attempt: r2bf9z
// KYC_submitted: 70c580
// Login_attempt: c4sdmf
// Login_completed: 93u0vb
// Signup_Attempt: 7o9b4a
// Signup_completed: skn8m7
// test_event: g2ys56
// Trade_attempt: vm6q0y
// Trade_fulfilled: rktwfy
// Withdraw_attempt: xhx16j
// Withdraw_fulfilled: xzfeg3

const appToken = 'pnbfxcnpzjeo';
const trackEventList = {
  DepositAttempt: 'z07d8n',
  KYC: 'r2bf9z',
  WithdrawAttempt: 'xhx16j',
  SignupAttempt: '7o9b4a',
  SignupCompleted: 'skn8m7',
  TradeAttempt: 'vm6q0y',
  EmailVerified: 'r3uh2m',
  LoginAttempt: 'c4sdmf',
  LoginCompleted: '93u0vb'
};

const initAdjust = () => {
  const env = process.env.NODE_ENV === 'production' ? 'production' : 'sandbox';
  Adjust.initSdk({
    appToken,
    environment: env
  });
};

const adTrack = eventName => {
  const eventToken = trackEventList[eventName];
  if (!eventToken) {
    return console.warn(`Not fount event name: ${eventName}`);
  }
  Adjust.trackEvent({ eventToken });
};

export { initAdjust, adTrack };

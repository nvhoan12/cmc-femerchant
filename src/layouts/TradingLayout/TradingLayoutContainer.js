import { connect } from 'react-redux';
import { isMarginActiveSelector } from 'apex-web/lib/redux/selectors/marginSelectors';
import TradingLayout from './TradingLayout';
import config from 'apex-web/lib/config';

const mapStateToProps = state => {
  const { MarginBorrowerEnabled } = state.user.userInfo;

  return {
    MarginBorrowerEnabled,
    isMarginActive: isMarginActiveSelector(state),
    config
  };
};

export default connect(mapStateToProps)(TradingLayout);

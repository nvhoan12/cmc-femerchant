import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';
import AssetActivityDetailsComponent from '../../components/AssetActivityDetails';
import { getBEMClasses } from 'apex-web/lib/helpers/cssClassesHelper';
import EstimatedSettlementModal from 'apex-web/lib/components/MarginClosePosition/EstimatedSettlementModal';
import MarginClosePositionModal from 'apex-web/lib/components/MarginClosePosition/MarginClosePositionModal';
import { selectPositionAndSave } from 'apex-web/lib/redux/actions/positionActions';

const classes = getBEMClasses('asset-activity-details-layout');

const BuySellDesktopLayout = ({
  MarginBorrowerEnabled,
  selectPositionAndSave
}) => {
  useEffect(() => {
    selectPositionAndSave(null);
  }, []);

  return (
    <div className={classes()}>
      <div className={classes('content-wrapper')}>
        <AssetActivityDetailsComponent />
      </div>
      {MarginBorrowerEnabled && <MarginClosePositionModal />}
      {MarginBorrowerEnabled && <EstimatedSettlementModal />}
    </div>
  );
};

const mapStateToProps = state => ({
  MarginBorrowerEnabled: get(
    state,
    'user.userInfo.MarginBorrowerEnabled',
    false
  )
});

const mapDispatchToProps = {
  selectPositionAndSave
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuySellDesktopLayout);

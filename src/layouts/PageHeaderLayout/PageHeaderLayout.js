import React from 'react';
import { connect } from 'react-redux';
import { getBEMClasses } from '../../helpers/cssClassesHelper';
import {
  isMerchantRole,
  isStaffRole,
  isBrokerRole
} from '../../helpers/userConfigHelper';
import Spinner from 'apex-web/lib/components/common/Spinner/Spinner';
import APLogo from '../../components/common/APLogo/APLogo';
// import SiteSelector from '../../components/SiteSelector/SiteSelector';
import MobileApp from '../../components/MobileApp/MobileApp';
import PageHeaderNavComponent from '../../components/PageHeader/PageHeaderNavComponent';
import UserSummaryContainer from '../../components/UserSummary/UserSummaryContainer';
import './PageHeaderLayout.css';
import './UserSummaryOverrides.css';

const pageHeaderClasses = getBEMClasses('page-header');

const MainNavItems = [
  { text: 'Dashboard', icon: 'dashboard', path: '/dashboard' },
  { text: 'Exchange', icon: 'trading', path: '/exchange' },
  // { text: 'Balances', icon: 'balance', path: '/margin-balances' },
  // { text: 'Streaming', icon: 'activity', path: '/eotc' },
  // { text: 'Lending', icon: 'sell', path: '/iba' },
  { text: 'Buy & Sell', icon: 'buy-sell', path: '/buy-sell' },
  { text: 'Wallets', icon: 'wallet', path: '/wallets' },
  // Hide BlockTrade menu
  // { text: 'Block Trade', icon: 'buy-sell', path: '/blocktrade', isNew: true },
  { text: 'Activity', icon: 'activity', path: '/asset-activity-details' },
  { text: 'BTZ', path: '/settings/loyalty-token', icon: 'heart' }
  // { text: 'User Settings', icon: 'user-settings', path: '/settings/user' }
  // { text: 'Components', icon: 'star', path: '/component' }
];

const merchantMainNavItems = [
  {
    text: 'Partners',
    icon: 'dashboard',
    path: '/partners'
  },
  { text: 'Wallets', icon: 'wallet', path: '/wallets' }
  // { text: 'User Settings', icon: 'user-settings', path: '/settings/user' }
];

const staffMainNavItems = [
  {
    text: 'Partners',
    icon: 'dashboard',
    path: '/partners'
  }
];

const brokerMainNavItems = [
  { text: 'Exchange', icon: 'trading', path: '/exchange' },
  { text: 'Wallets', icon: 'wallet', path: '/wallets' }
];

const anonymousNavItems = [
  { text: 'Exchange', icon: 'trading', path: '/exchange' }
];

const renderMainNavItems = (userConfig, isAuthenticated) => {
  if (isMerchantRole(userConfig)) {
    return merchantMainNavItems;
  }
  if (isStaffRole(userConfig)) {
    return staffMainNavItems;
  }
  if (isBrokerRole(userConfig)) {
    return brokerMainNavItems;
  }
  if (!isAuthenticated) {
    return anonymousNavItems;
  }
  return MainNavItems;
};

const PageHeaderComponent = ({ userConfig, isAuthenticated }) => {
  return (
    <React.Fragment>
      <div className={pageHeaderClasses('container')}>
        <div className={pageHeaderClasses('content-wrapper')}>
          <div className={pageHeaderClasses('header-item', 'logo')}>
            <APLogo linkTo="/" />
          </div>
          {userConfig.length > 0 || !isAuthenticated ? (
            <PageHeaderNavComponent
              navItems={renderMainNavItems(userConfig, isAuthenticated)}
            />
          ) : (
            <Spinner isInline customClass={pageHeaderClasses} />
          )}
          <div className={pageHeaderClasses('right-panel')}>
            {/* <SiteSelector /> */}
            <MobileApp />
            {isAuthenticated && (
              <UserSummaryContainer
                customClass="page-header-user-summary"
                settingsRoute="/settings/user"
                activityRoute="/asset-activity-details"
              />
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    userConfig: state.user.userConfig,
    isAuthenticated: state.auth.isAuthenticated
  };
};

export default connect(mapStateToProps)(PageHeaderComponent);

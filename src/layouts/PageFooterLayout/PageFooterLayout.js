import React from 'react';
import PropTypes from 'prop-types';

import { getBEMClasses } from '../../helpers/cssClassesHelper';
import './PageFooterLayout.css';
import config from '../../config';

const pageFooterClasses = getBEMClasses('page-footer');

const PageFooterLayout = (props, context) => {
  return (
    <footer className={pageFooterClasses()}>
      <div className={pageFooterClasses('footer-left-content')}>
        {context.t('{n}™ {y}. All Rights Reserved.', {
          n: 'Bitazza',
          y: new Date().getFullYear()
        })}
      </div>
      <div className={pageFooterClasses('footer-right-content')}>
        {config.Footer.links.map((link, i) => (
          <a
            key={i}
            href={link.link}
            target={link.openInNewWindow ? '_blank' : '_self'}>
            {context.t(link.text)}
          </a>
        ))}
      </div>
    </footer>
  );
};

PageFooterLayout.contextTypes = {
  t: PropTypes.func.isRequired
};

export default PageFooterLayout;

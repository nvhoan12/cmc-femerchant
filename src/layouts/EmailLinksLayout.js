import React from 'react';
import redirectIfAuthorized from 'apex-web/lib/hocs/redirectIfAuthorized';
import {
  checkForVerifyEmailParam,
  checkForResetPassParam,
  checkForConfirmWithdrawParam,
  checkForDepositConfirmationParam,
  checkForLoginParam,
  checkForVerifyDeviceParam
} from 'apex-web/lib/helpers/emailLinksHelper';

import { checkForVerifyAddressParam } from 'apex-web/lib/helpers/homePageLayoutHelper';
import { defaultPath } from 'apex-web/lib/routeTemplates';
import { STAND_ALONE_COMPONENTS } from '../constants/emailLinksLayoutsConstants';

class EmailLinksLayout extends React.Component {
  state = {
    StandAloneComponent: undefined
  };

  componentWillMount() {
    if (checkForVerifyEmailParam())
      return this.setState({
        StandAloneComponent: STAND_ALONE_COMPONENTS.VerifyEmailContainer
      });
    else if (checkForConfirmWithdrawParam())
      return this.setState({
        StandAloneComponent: STAND_ALONE_COMPONENTS.ConfirmWithdrawContainer
      });
    else if (checkForResetPassParam())
      return this.setState({
        StandAloneComponent:
          STAND_ALONE_COMPONENTS.ResetPasswordNoAuthFormContainer
      });
    else if (checkForDepositConfirmationParam())
      return this.setState({
        StandAloneComponent: STAND_ALONE_COMPONENTS.DepositConfirmationContainer
      });
    else if (checkForLoginParam())
      return this.setState({
        StandAloneComponent: STAND_ALONE_COMPONENTS.StandaloneLoginContainer
      });
    else if (checkForVerifyAddressParam())
      return this.setState({
        StandAloneComponent: STAND_ALONE_COMPONENTS.VerifyAddressContainer
      });
    else if (checkForVerifyDeviceParam())
      return this.setState({
        StandAloneComponent: STAND_ALONE_COMPONENTS.VerifyDeviceContainer
      });
  }

  render() {
    const { StandAloneComponent } = this.state;
    return StandAloneComponent ? <StandAloneComponent /> : null;
  }
}

export default redirectIfAuthorized(EmailLinksLayout, defaultPath.path);

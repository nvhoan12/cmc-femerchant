import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { redirectFromLogin } from 'apex-web/lib/redux/actions/authActions';
import FullPageSpinner from 'apex-web/lib/components/common/FullPageSpinner/FullPageSpinner';
import path from 'apex-web/lib/helpers/path';

function withAuthentication(WrappedComponent) {
  const ModifiedComponent = props => {
    const { isAuthenticated, location, pendingAuth, redirectFromLogin } = props;

    if (location.pathname === '/exchange') {
      return <WrappedComponent {...props} />;
    }

    if (pendingAuth) {
      return <FullPageSpinner />;
    }
    if (!isAuthenticated) {
      redirectFromLogin(location);
      return <Redirect to={path('/login')} />;
    }

    return <WrappedComponent {...props} />;
  };

  const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    pendingAuth: state.auth.pending
  });

  const mapDispatchToProps = dispatch => ({
    redirectFromLogin: location => dispatch(redirectFromLogin(location))
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(ModifiedComponent);
}

export default withAuthentication;

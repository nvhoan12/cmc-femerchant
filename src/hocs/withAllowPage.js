import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import path from '../helpers/path';
import { getUserRole } from '../helpers/userConfigHelper';

function withAllowPage(WrappedComponent) {
  const ModifiedComponent = props => {
    const { userConfig, location } = props;

    if (location.pathname === '/exchange') {
      return <WrappedComponent {...props} />;
    }

    const role = getUserRole(userConfig);
    const allow = role.allowPages.find(allowPage =>
      location.pathname.includes(allowPage)
    );
    if (!allow) {
      return <Redirect to={path(role.defaultPath)} />;
    }
    return <WrappedComponent {...props} />;
  };
  const mapStateToProps = state => ({
    userConfig: state.user.userConfig
  });

  return connect(mapStateToProps)(ModifiedComponent);
}

export default withAllowPage;

import PropTypes from 'prop-types';

export const styleNames = {
  general: 'general',
  additive: 'additive',
  subtractive: 'subtractive'
};

export const styleNamePropType = PropTypes.oneOf([
  styleNames.general,
  styleNames.additive,
  styleNames.subtractive
]);

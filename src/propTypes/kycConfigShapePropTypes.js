import PropTypes from 'prop-types';

export const kycConfigLevelShape = {
  type: PropTypes.string,
  levels: PropTypes.arrayOf(
    PropTypes.shape({
      level: PropTypes.number,
      button: PropTypes.string,
      label: PropTypes.string,
      description: PropTypes.shape({
        benefits: PropTypes.array,
        requirements: PropTypes.string
      }),
      verifiedMessage: PropTypes.string,
      underReviewMessage: PropTypes.string,
      form: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string,
          type: PropTypes.string
        })
      )
    })
  )
};

jQuery(document).ready(function() {
	
	"use strict";
	
	/*----------------------------------------
     Slider Section
     ----------------------------------------*/
	var screenshot = $(".screenshot-carousel");
	screenshot.owlCarousel({
		loop:true,
		margin:30,
		nav:false,
		dots:false,
		center:true,
		autoplay: true,
		autoplayTimeout: 8000,
		responsive:{
			0:{
				items:2,
			},
			767:{
				items:2,
			},
			768:{
				items:3,
			},
			992:{
				items:4,
			},
			1200:{
				items:5
			}
		}
	});

});
const API_BASE_URL = 'https://api.bitazza.com'; // production
// const API_BASE_URL = 'http://tanasin2.ddns.net:8200'; // staging

const WSS_BASE_URL = 'wss://apexapi.bitazza.com/WSGateway'; // production
// const WSS_BASE_URL = 'wss://apibitazzastage.cdnhop.net/WSGateway'; // staging

const TRANSFER_BASE_URL = 'http://transfer.bitazza.com'; // production
// const TRANSFER_BASE_URL = 'http://178.128.96.163:4042'; // staging

window.APEXWebConfig = {
  services: {
    resetPassword: `${API_BASE_URL}/ap/reset_password`,
    signup: `${API_BASE_URL}/user/signup`,
    getLevel: `${API_BASE_URL}/me/level`,
    getPartnerInfo: `${API_BASE_URL}/partner/info`,
    getInfoWithEmail: (email) => `${API_BASE_URL}/user/data/${email}`,
    getInfoWithBTZID: (btzID) => `${API_BASE_URL}/users/info/${btzID}`,
    getUserDetail: (searchParam) => `${API_BASE_URL}/users/detail?${searchParam}`,
    getAffiliates: `${API_BASE_URL}/me/affiliates`,
    claimedBTZ: `${API_BASE_URL}/me/level/claimed`,
    getKYCInfo: (userID) => `${API_BASE_URL}/users/kyc_info/${userID}`,
    fetchGolabelMessageInterior: `${API_BASE_URL}/announcements/web`,
    fetchGolabelMessageLaning: `${API_BASE_URL}/announcements/landing`,
    generateInvoice: `${API_BASE_URL}/merchant/invoices`,
    validateCryptoAddress: `${API_BASE_URL}/validateCrypto`,
    getInvoiceUrl: (transactionId) =>
      `${TRANSFER_BASE_URL}/merchant/invoice?id=${transactionId}`,
    getMerchantTransactions: (accountID, userID) =>
      `${API_BASE_URL}/merchant/${accountID}/transactions/${userID}`,
    getTransaction: (transactionId) =>
      `${API_BASE_URL}/merchant/invoices/${transactionId}`,
    getQrDeposit: `${API_BASE_URL}/transfer/thb/deposit/qr`,
    getBankAcc: `${API_BASE_URL}/me/bank_accounts`,
    addBankAcc: `${API_BASE_URL}/me/bank_accounts`,
    cancelDeposit: (ticketNumber) =>
      `${API_BASE_URL}/transfer/thb/deposit/${ticketNumber}`,
    exchangeRates: `${API_BASE_URL}/global/exchangerates`,
    requestQuote: `${API_BASE_URL}/qcp/quotes`,
    blocktradeHistory: `${API_BASE_URL}/blocktrades`,
    saveDataExchange: `${API_BASE_URL}/data_exchange`,
    getDataExchange: (token) => `${API_BASE_URL}/data_exchange?token=${token}`,
    emailVerified: `${API_BASE_URL}/user/email_verified`,
    getBanners: (language) => `${API_BASE_URL}/banners/web/${language}`,
    pdpa: `${API_BASE_URL}/me/pdpa`,
  },
  sites: {
    global: 'https://bitazza.com',
    thailand: 'https://th.bitazza.com',
  },
  global: {
    site: 'thailand',
    siteName: 'Bitazza',
    theme: 'dark',
    gateway: WSS_BASE_URL,
    siteLogo: 'site-logo.png',
    locale: 'EN-TH',
    dateFormat: 'D/M/YYYY',
    dateTimeFormat: 'D-M-YYYY h:mma',
    products: [
      'THB',
      'BTZ',
      'BTC',
      'ETH',
      'XRP',
      'XLM',
      'USDT',
      'DAI',
      'COMP',
      'YFI',
      'OMG',
      'LINK',
      'BNT',
      'SNX',
      'REN',
      'UNI',
      'USDC',
      'PAX',
      'BUSD',
      'AAVE',
      'ALPHA',
      'BAL',
      'SUSHI',
      'CRV',
      'MKR',
      'GRT',
      'wNXM',
      'SOL',
      'LUNA',
      'DOGE',
      'MATIC',
      'ADA',
      'PAXG',
      'VET',
      'SHIB',
      'CAKE',
      'RUNE',
      'VELO',
      'HBAR',
      'AXS',
      'SLP',
      '1INCH',
      'BNB'
    ],
    disableDepositProducts: ['SOL','LUNA','DOGE','MATIC','ADA','PAXG','VET','SHIB','CAKE','RUNE','VELO','HBAR','1INCH'],
    disableWithdrawProducts: ['SOL','LUNA','DOGE','MATIC','ADA','PAXG','VET','SHIB','CAKE','RUNE','VELO','HBAR','iINCH'],
    activeWarningExternalTransferProducts: [
      'AXS',
      'SLP',
      'BNB',
    ],
    instruments: [
      'BTCTHB',
      'ETHTHB',
      'XRPTHB',
      'XLMTHB',
      'USDTTHB',
      'COMPTHB',
      'DAITHB',
      'YFITHB',
      'OMGTHB',
      'LINKTHB',
      'BNTTHB',
      'SNXTHB',
      'RENTHB',
      'UNITHB',
      'USDCTHB',
      'BUSDTHB',      
      'AAVETHB',
      'ALPHATHB',
      'BALTHB',
      'SUSHITHB',
      'CRVTHB',
      'MKRTHB',
      'GRTTHB',
      'wNXMTHB',
      'SOLTHB',
      'LUNATHB',
      'DOGETHB',
      'MATICTHB',
      'PAXGTHB',
      'VETTHB',
      'ADATHB',
      'SHIBTHB',
      'CAKETHB',
      'RUNETHB',
      'VELOTHB',
      'HBARTHB',
      'AXSTHB',
      'SLPTHB',
      '1INCHTHB',
      'BNBTHB',
      'BTCUSDT',
      'ETHUSDT',
      'XRPUSDT',
      'XLMUSDT',
      'ETHBTC',
      'XRPBTC',
      'XLMBTC',
      'COMPBTC',
      'YFIBTC',
      'OMGBTC',
      'LINKBTC',
      'BNTBTC',
      'SNXBTC',
      'RENBTC',
      'UNIBTC'
    ],
    baseCurrency: 'THB',
    StreamingInstruments: {
      Symbol: [],
      additionalSlippage: 0
    },
    qcp: {
      instruments: [
        {
          id: 16,
          symbol: 'QUSDTTHB',
          product1: 'USDT',
          product2: 'THB'
        },
        {
          id: 12,
          symbol: 'QBTCTHB',
          product1: 'BTC',
          product2: 'THB'
        },
        {
          id: 13,
          symbol: 'QETHTHB',
          product1: 'ETH',
          product2: 'THB'
        },
        {
          id: 14,
          symbol: 'QXRPTHB',
          product1: 'XRP',
          product2: 'THB'
        },
        {
          id: 15,
          symbol: 'QXLMTHB',
          product1: 'XLM',
          product2: 'THB'
        }
      ]
    },
    blocktradeFee: {
      fiat: 0.005,
      crypto: 0.008
    },

    // for RFQ minimum quote.
    minimumBlocktrade: {
      btc: 1,
      eth: 50,
      thb: 100000,
      usdtthb: 100000
    },
    orderFee: 0.25,
    bannerDateLeft: '2020-09-30T00:00:00.000Z',
    // Require2FA: true,
    minimumTransection: {
      thb: 350,
      usdt: 15
    },
    merchant: {
      internal: 0.01, // +1%
      external: 0.025, // +2.5%
      instruments: [
        {
          id: 1,
          symbol: 'BTC',
          icon: 'btc-48px.svg'
        },
        {
          id: 2,
          symbol: 'ETH',
          icon: 'eth-48px.svg'
        },
        {
          id: 5,
          symbol: 'USDT',
          icon: 'usdt-48px.svg'
        }
      ]
    },
    customDecimalPlacesAtField: {
      XRP: 1,
      XLM: 1
    },
    roles: [
      {
        name: 'default',
        defaultPath: '/dashboard',
        allowPages: [
          '/dashboard',
          '/exchange',
          '/buy-sell',
          '/wallets',
          '/asset-activity-details',
          '/settings',
          '/transfer'
        ]
      },
      {
        name: 'merchant',
        defaultPath: '/partners',
        allowPages: ['/partners', '/wallets', '/settings']
      },
      {
        name: 'staff',
        defaultPath: '/partners',
        allowPages: ['/partners']
      },
      {
        name: 'broker',
        defaultPath: '/exchange',
        allowPages: ['/exchange', '/wallets']
      }
    ],
    lineLink:
      'https://line.me/ti/g2/XNIJmH98kyHBdlN0GZVMBA?utm_source=invitation&utm_medium=QR_code&utm_campaign=default',
    invoice: (txID) => `${TRANSFER_BASE_URL}/merchant/invoice?id=${txID}`,
    appsStore: 'https://apps.apple.com/th/app/bitazza/id1476944844?l=th',
    googlePlay:
      'https://play.google.com/store/apps/details?id=com.bitazza.android'
  },
  Snackbar: {
    timeout: 4000,
    depositWithdrawTimeout: 10000,
    type: 'info',
    showMultiple: true,
    showProgressBar: true
  },
  ApiKeys: {
    documentationLink: 'https://api-doc.bitazza.com/#marketmakerapi'
  },
  TradingLayout: {
    showDepthChart: false,
    chart: 'TradingView'
  },
  TradingView: {
    timezone: 'Asia/Bangkok',
    locale: 'en',
    disabled_features: []
  },
  TickerData: {
    defaultInterval: 14400
  },
  Settings: {
    affiliate: false,
    tradeSettings: false,
    kyc: true
  },
  Deposit: {
    MinimumVerificationLevel: 1,
    showDepositIdModal: true,
    USD: {
      bankName: 'My USD Bank',
      bankWireNumber: '1234567890',
      bankAccountNumber: '123456'
    }
  },
  Withdraw: {
    MinimumVerificationLevel: 1,
    MinimumAmount: 100,
    MaximumAmount: 2000000,
    WithdrawDetails: [
      {
        text:
          "Note - The withdrawal and transfer of these funds might take up to 4 business hours, and it's subject to operator review.",
        useLink: false,
        linkAddress: ''
      },
      {
        text:
          'THB withdrawals during 11:30pm to 12:30am will be delayed due to bank maintenance during these hours.',
        useLink: false,
        linkAddress: ''
      }
    ]
  },
  LandingPage: {
    forceLogIn: false
  },
  SignupForm: {
    useEmailAsUsername: true,
    additionalFields: [],
    privacyPolicyLink: '/privacy-policy.html',
    termsAndServicesLink: '/tof.html'
  },
  RetailBuySell: {
    roundBuyLimitPrices: false,
    dealPrices: {
      BTCTHB: [1000, 2000, 5000, 10000],
      ETHTHB: [1000, 2000, 5000, 10000],
      XRPTHB: [1000, 2000, 5000, 10000],
      XLMTHB: [1000, 2000, 5000, 10000],
      USDTTHB: [1000, 2000, 5000, 10000],
      COMPTHB: [1000, 2000, 5000, 10000],
      DAITHB: [1000, 2000, 5000, 10000],
      YFITHB: [1000, 2000, 5000, 10000],
      OMGTHB: [1000, 2000, 5000, 10000],
      LINKTHB: [1000, 2000, 5000, 10000],
      BNTTHB: [1000, 2000, 5000, 10000],
      SNXTHB: [1000, 2000, 5000, 10000],
      RENTHB: [1000, 2000, 5000, 10000],
      UNITHB: [1000, 2000, 5000, 10000],
      USDCTHB: [1000, 2000, 5000, 10000],
      BUSDTHB: [1000, 2000, 5000, 10000],
      AAVETHB: [1000, 2000, 5000, 10000],
      ALPHATHB: [1000, 2000, 5000, 10000],
      BALTHB: [1000, 2000, 5000, 10000],
      SUSHITHB: [1000, 2000, 5000, 10000],
      CRVTHB: [1000, 2000, 5000, 10000],
      MKRTHB: [1000, 2000, 5000, 10000],
      GRTTHB: [1000, 2000, 5000, 10000],
      wNXMTHB: [1000, 2000, 5000, 10000],
      SOLTHB: [1000, 2000, 5000, 10000],
      LUNATHB: [1000, 2000, 5000, 10000],
      DOGETHB: [1000, 2000, 5000, 10000],
      MATICTHB: [1000, 2000, 5000, 10000],
      ADATHB: [1000, 2000, 5000, 10000],
      PAXGTHB: [1000, 2000, 5000, 10000],
      VETTHB: [1000, 2000, 5000, 10000],
      SHIBTHB: [1000, 2000, 5000, 10000],
      CAKETHB: [1000, 2000, 5000, 10000],
      RUNETHB: [1000, 2000, 5000, 10000],
      VELOTHB: [1000, 2000, 5000, 10000],
      HBARTHB: [1000, 2000, 5000, 10000],
      AXSTHB: [1000, 2000, 5000, 10000],
      SLPTHB: [1000, 2000, 5000, 10000],
      "1INCHTHB": [1000, 2000, 5000, 10000],
      BNBTHB: [1000, 2000, 5000, 10000],
      BTCUSDT: [50, 100, 250, 500],
      ETHUSDT: [50, 100, 250, 500],
      XRPUSDT: [50, 100, 250, 500],
      XLMUSDT: [50, 100, 250, 500],
      ETHBTC: [0.1, 0.2, 0.3, 0.4],
      XRPBTC: [0.1, 0.2, 0.3, 0.4],
      XLMBTC: [0.1, 0.2, 0.3, 0.4],
      COMPBTC: [0.1, 0.2, 0.3, 0.4],
      YFIBTC: [0.1, 0.2, 0.3, 0.4],
      OMGBTC: [0.1, 0.2, 0.3, 0.4],
      LINKBTC: [0.1, 0.2, 0.3, 0.4],
      BNTBTC: [0.1, 0.2, 0.3, 0.4],
      SNXBTC: [0.1, 0.2, 0.3, 0.4],
      RENBTC: [0.1, 0.2, 0.3, 0.4],
      UNIBTC: [0.1, 0.2, 0.3, 0.4]
    }
  },
  highlightStyle: 'star',
  Footer: {
    links: [
      {
        text: 'About Bitazza',
        link: '/about.html',
        openInNewWindow: true
      },
      {
        text: 'FAQ',
        link: 'https://bitazzahelp.freshdesk.com/th/support/home',
        openInNewWindow: true
      },
      {
        text: 'Support',
        link:
          'https://bitazzahelp.freshdesk.com/en/support/discussions/topics/44001010428',
        openInNewWindow: true
      },
      {
        text: 'Terms & Conditions',
        link: '/tof.html',
        openInNewWindow: true
      },
      {
        text: 'Privacy Policy',
        link: '/privacy-policy.html',
        openInNewWindow: true
      }
    ]
  },
  KYC: {
    MinimumVerificationLevel: 1,
    type: 'IM3.2',
    requestIdentifier: '',
    levels: [
      {
        level: 0,
        label: 'Level 0',
        button: 'Sign Up',
        description: {
          benefits: ['Navigate the web and mobile app but cannot trade'],
          requirements: ['Register as an exchange user and verify email.'],
        },
        verifiedMessage: 'Email Verified',
        underReviewMessage: 'Email pending verification',
        steps: [
          {
            nextPageLabel: 'Continue to Other Disclosure',
            sections: [
              {
                label: 'General Information',
                name: 'kyc_lvl1_gi_',
                form: [
                  {
                    label: 'Nationality',
                    name: 'billingCountry',
                    validators: ['required']
                  },
                  {
                    label: 'Bank Country',
                    name: 'kyc_bank_country',
                    type: 'select',
                    options: {
                      Malaysia: 'Malaysia',
                      Thailand: 'Thailand',
                      Vietnam: 'Vietnam'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'First Name(EN)',
                    name: 'firstName',
                    type: 'text',
                    validators: ['required']
                  },
                  {
                    label: 'Middle Name(EN)',
                    name: 'middleName',
                    type: 'text',
                    validators: []
                  },
                  {
                    label: 'Last Name(EN)',
                    name: 'lastName',
                    type: 'text',
                    validators: ['required']
                  },
                  {
                    label: 'First Name(TH)',
                    name: 'kyc_lvl1_gi_firstname_th',
                    type: 'text',
                    info: 'Thai National Only',
                    validators: ['required']
                  },
                  {
                    label: 'Last Name(TH)',
                    name: 'kyc_lvl1_gi_lastname_th',
                    type: 'text',
                    info: 'Thai National Only',
                    validators: ['required']
                  },
                  {
                    label: 'ID / Passport Number ',
                    name: 'kyc_lvl1_gi_idnumber',
                    type: 'text',
                    validators: ['required']
                  },
                  {
                    label: 'Laser Code',
                    name: 'kyc_lvl1_gi_lasercode',
                    type: 'text',
                    info: 'Thai National Only',
                    validators: []
                  },
                  {
                    label: 'Date of Birth',
                    name: 'dob',
                    type: 'date',
                    validators: ['required', 'isDate']
                  },
                  {
                    label: 'Gender',
                    name: 'kyc_lvl1_gi_gender',
                    type: 'select',
                    options: {
                      M: 'Male',
                      F: 'Female'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'Billing Address',
                    name: 'billingStreetAddress',
                    info: ' ',
                    validators: ['required']
                  },
                  {
                    label: 'State',
                    name: 'state',
                    info: ' ',
                    validators: []
                  },
                  {
                    label: 'City',
                    name: 'billingCity',
                    type: 'text',
                    validators: ['required']
                  },
                  {
                    label: 'Zip Code',
                    name: 'billingZip',
                    type: 'text',
                    validators: ['required']
                  },
                  {
                    label: 'Contact Number',
                    name: 'phone',
                    type: 'phone',
                    validators: ['required', 'isValidPhone']
                  },
                  {
                    label: 'Marriage status',
                    name: 'kyc_lvl1_gi_marriagestatus',
                    type: 'select',
                    options: {
                      S: 'Single',
                      M: 'Married',
                      D: 'Divorced',
                      W: 'Widowed'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'Name of spouse (if any)',
                    name: 'kyc_lvl1_gi_nameofspouse',
                    type: 'text',
                    validators: []
                  }
                ]
              }
            ]
          },
          {
            prevPageLabel: 'Go Back',
            nextPageLabel: 'Next step',
            sections: [
              // Education NEXT STEP 2
              {
                label: 'Education and Experience Background',
                name: 'kyc_lvl1_eeb_',
                form: [
                  {
                    label: 'Highest Education',
                    name: 'kyc_lvl1_eeb_education',
                    type: 'select',
                    options: {
                      none: 'None',
                      highSchool: 'High School',
                      college: 'College',
                      university: 'University',
                      postGraduate: 'Post Graduate'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'Occupation',
                    name: 'kyc_lvl1_eeb_occupation',
                    type: 'select',
                    options: {
                      none: 'None',
                      civilServant: 'Civil Servant',
                      entrepreneur: 'Entrepreneur',
                      employee: 'Employee',
                      freelancer: 'Freelancer',
                      student: 'Student',
                      others: 'Others'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'Work Address',
                    name: 'kyc_lvl1_eeb_workaddress',
                    type: 'text',
                    validators: ['required']
                  },
                  {
                    label: 'Nature of Business',
                    name: 'kyc_lvl1_eeb_natureofbusiness',
                    type: 'select',
                    options: {
                      nob_1: 'Government Agencies, State Enterprise',
                      nob_2: 'Advertising',
                      nob_3: 'Design',
                      nob_4: 'Education',
                      nob_5: 'Engineering',
                      nob_6: 'Electronics',
                      nob_7: 'Energy',
                      nob_8: 'Entertainment',
                      nob_10: 'Financial Services',
                      nob_11: 'Food & Beverage',
                      nob_12: 'Logistics',
                      nob_13: 'Health & Beauty Care',
                      nob_14: 'Hospitality & Tourism',
                      nob_15: 'Information Technology',
                      nob_16: 'Jewelry, Gems and Watches',
                      nob_17: 'Legal Services',
                      nob_18: 'Manufacturing',
                      nob_19: 'Media, Publishing and Printing',
                      nob_20: 'Medical, Pharmaceutical',
                      nob_21: 'Mining and Drilling',
                      nob_23: 'Real Estate',
                      nob_24: 'Retail and Wholesale',
                      nob_25: 'Recruitment and HR',
                      nob_26: 'Security',
                      nob_27: 'Telecommunications',
                      nob_28: 'Trading & Distribution',
                      nob_29: 'Transportation',
                      nob_30: 'Utilities',
                      nob_31: 'Others'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'If Others, please specify.',
                    name: 'kyc_lvl1_eeb_natureofbusiness2',
                    type: 'text'
                  }
                ]
              },
              // other disclosure
              {
                label: 'Other Disclosure',
                form: [
                  {
                    label: 'Source of Fund',
                    name: 'kyc_lvl1_od_sourceoffund',
                    type: 'select',
                    options: {
                      sof_1: 'Employment',
                      sof_2: 'Entrepreneur',
                      sof_3: 'Heritage',
                      sof_4: 'Investment',
                      sof_5: 'Loan',
                      sof_6: 'Property',
                      sof_7: 'Saving'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'Do you hold any political position in any jurisdiction?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_od_politicalposition',
                    type: 'select',
                    options: {
                      y: 'Yes',
                      n: 'No'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'Have you been allegedly involved in any tax evasion case in any jurisdiction?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_od_taxevasion',
                    type: 'select',
                    options: {
                      y: 'Yes',
                      n: 'No'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'Have you been involved in any offense regarding laws on anti-money laundering and counter-terrorism and proliferation of weapons of mass destruction financing (AML/CFT)?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_od_lawoffence',
                    type: 'select',
                    options: {
                      y: 'Yes',
                      n: 'No'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label: 'Are you the true beneficiary of this account?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_od_beneficiary',
                    type: 'select',
                    options: {
                      y: 'Yes',
                      n: 'No'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'If are not the true beneficiary of this account please provide the name of beneficiary and telephone number.',
                form: [
                  {
                    label: '',
                    name: 'kyc_lvl1_od_beneficiary_name',
                    type: 'text'
                  }
                ]
              },
              // Financial INFO
              {
                label: 'Financial Information',
                name: 'kyc_lvl1_fi_',
                form: [
                  {
                    label: 'Monthly Income',
                    name: 'kyc_lvl1_fi_monthlyincome',
                    type: 'select',
                    options: {
                      a: '0-20,000 THB',
                      b: '20,001-50,000 THB',
                      c: '50,001-100,000 THB',
                      d: '100,001-200,000 THB',
                      e: '200,001-350,000 THB',
                      f: '350,001-600,000 THB',
                      g: '600,001-850,000 THB',
                      h: '850,001-1,000,000 THB',
                      i: '1,000,001-2,000,000 THB',
                      j: 'More than 2,000,000 THB'
                    },
                    validators: ['required']
                  },
                  {
                    label: 'Net Total Asset',
                    name: 'kyc_lvl1_fi_totalassets',
                    type: 'select',
                    info: '(Total assets - total liabilities)',
                    options: {
                      a: 'Less than 1,000,000 THB',
                      b: '1,000,001-5,000,000 THB',
                      c: '5,000,001-10,000,000 THB',
                      d: '10,000,001-20,000,000 THB',
                      e: '20,000,001-50,000,000 THB',
                      f: '50,000,001-70,000,000 THB',
                      g: '70,000,001-100,000,000 THB',
                      h: 'More than 100,000,000 THB'
                    },
                    validators: ['required']
                  }
                ]
              }
            ]
          },
          {
            prevPageLabel: 'Go Back',
            nextPageLabel: 'Last step',
            sections: [
              //NEXT FORM
              // sustainability test
              {
                label:
                  'Have you ever made a cryptocurrency or token digital based investment before?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_st_cryptoinvestment',
                    type: 'select',
                    options: {
                      st_1_1: 'Never',
                      st_1_2: 'Once',
                      st_1_3: 'A few times',
                      st_1_4: 'Many times'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'Do you feel like you have a clear understandings of the risks involved with a cryptocurrency?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_st_clearunderstanding',
                    type: 'select',
                    options: {
                      st_2_1: 'No Understanding',
                      st_2_2: 'Little Understanding',
                      st_2_3: 'Some Understanding',
                      st_2_4: 'Clear understanding'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'Do you anticipate a cryptocurrency or token digital "crash" or "bubble burst"?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_st_bubbleburst',
                    type: 'select',
                    options: {
                      st_3_1: 'No',
                      st_3_2: 'Not Sure',
                      st_3_3: 'Maybe',
                      st_3_4: 'Yes'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'Are you willing to take on legal risk in pursuing your investment in cryptocurrency or token digital?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_st_legalrisk',
                    type: 'select',
                    options: {
                      st_4_1: 'No',
                      st_4_2: 'Not likely',
                      st_4_3: 'Most likely',
                      st_4_4: 'Yes'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'When I invest money in cryptocurrency or digital token, my primary goal is to?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_st_primarygoal',
                    type: 'select',
                    options: {
                      st_5_1:
                        'To generate secure and constant but low earnings',
                      st_5_2:
                        'To generate constant earnings with some risks of losing some investment principle',
                      st_5_3:
                        'To generate high earnings with high risks of losing some investment principle',
                      st_5_4:
                        'To generate exorbitant earnings with high risks of losing most of investment principle'
                    },
                    validators: ['required']
                  }
                ]
              },
              {
                label:
                  'The degree to which the value of an investment moves up and down is called volatility (risk). More volatile investments generally offer greater growth potential in the long term than less volatile investments, but they may produce greater losses. With how much volatility are you comfortable?',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl1_st_volatilityrisk',
                    type: 'select',
                    options: {
                      st_6_1: '5% or less loss',
                      st_6_2: '5-10% loss',
                      st_6_3: '10-20% loss',
                      st_6_4: '20% or more loss'
                    },
                    validators: ['required']
                  }
                ]
              },

              // UPLOAD ID
              {
                label: 'Upload ID ( Front & Back )',
                form: [
                  {
                    label: 'Upload ID (jpg, jpeg, png)',
                    name: 'scandata',
                    validators: ['required']
                  },
                  {
                    label: 'Upload ID',
                    name: 'backsideimagedata',
                    info: 'National ID only',
                    validators: []
                  },
                  {
                    label: 'ID Type',
                    name: 'DocumentType',
                    validators: ['required']
                  },
                  {
                    label: 'ID Country',
                    name: 'DocumentCountry',
                    validators: ['required']
                  },
                  {
                    label: 'ID State',
                    name: 'DocumentState',
                    validators: []
                  }
                ]
              },
              {
                label: 'Upload Selfie',
                form: [
                  {
                    label: 'Upload Selfie (jpg, jpeg, png)',
                    name: 'faceimages',
                    info: ' '
                  }
                ]
              },
              {
                label:
                  'I hereby declare that the information provided is true and accurate. I shall also immediately inform and update the Company if there is any change to the information provided.',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl2_declration',
                    type: 'select',
                    options: {
                      y: 'Yes',
                      n: 'No'
                    },
                    validators: ['required']
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        level: 1,
        label: 'Level 1',
        button: 'Sign Up',
        description: {
          benefits: [
            'THB 1,000,000 Daily Deposit/Withdrawal Limit',
            'Similar Daily Deposit/Withdrawal Limit for every coin'
          ],
          requirements: [
            'General Information, Education and Experience Background, Other disclosure, Financial Information, Suitability Test, Upload ID and Selfie',
          ],
        },
        verifiedMessage: 'Verified for Level 1',
        underReviewMessage: 'Application under review.',
        steps: [
          {
            nextPageLabel: 'Continue to Proof Of Funds',
            sections: [
              {
                label: 'Upload Address',
                form: [
                  {
                    label: 'Upload Address (jpg, jpeg, png)',
                    name: 'kyc_lvl3_upload_address',
                    type: 'image',
                    validators: ['required']
                  }
                ]
              },
              {
                label: 'Proof of Funds',
                form: [
                  {
                    label: 'Proof of Funds(jpg, jpeg, png)',
                    name: 'kyc_lvl3_upload_proffoffund',
                    type: 'image',
                    validators: ['required']
                  }
                ]
              },
              // waiting for proof of address and funds
              {
                label:
                  'I hereby declare that the information provided is true and accurate. I shall also immediately inform and update the Company if there is any change to the information provided.',
                form: [
                  {
                    label: 'Please Select',
                    name: 'kyc_lvl2_declration',
                    type: 'select',
                    options: {
                      y: 'Yes',
                      n: 'No'
                    },
                    validators: ['required']
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        level: 2,
        label: 'Level 2',
        button: 'Increase to Level 2',
        description: {
          benefits: [
            'Daily Deposit/Withdrawal up to THB10,000,000',
            'Similar Daily Deposit/Withdrawal/Transfer Limit for every coin'
          ],
          requirements: [
            '1- Proof of Funds: Bank statement (3 months) with average movement of at least 2Million baht',
            '2 - Proof of Address: 1) Bank statement with specific address (This should be issued by the bank, showing the bank seal) or 2) Utility Bill (Electricity, telephone, credit card, and internet bill) *** Please make sure that your uploaded proof of address follows the list below: The account holder’s name must be visible. The account holder’s address must be visible. Issue date must not be older than 3 months.',
            'Please contact our Customer Support via Live Chat',
          ]
        },
        verifiedMessage: 'Verified for Level 2',
        underReviewMessage: 'Application under review.',
        steps: [
          {
            nextPageLabel: 'Continue',
            sections: [
              {
                label: ''
              }
            ]
          }
        ]
      },
      {
        level: 3,
        label: 'Level 3',
        button: 'Increase to Level 3',
        description: {
          benefits: [
            'Unlimited Daily Deposit/Withdrawal',
            'Similar Daily Deposit/Withdrawal/Transfer Limit for every coin'
          ],
          requirements: ['Completed KYC Level 2 and contact your Bitazza Customer Relationship']
        },
        verifiedMessage: 'Verified for Level 3',
        underReviewMessage: 'Application under review.',
        sections: []
      }
    ]
  },
  Language: {
    languageNames: {
      af: 'Afrikaans',
      sq: 'Albanian',
      ar: 'Arabic',
      hy: 'Armenian',
      az: 'Azeerbaijani',
      eu: 'Basque',
      be: 'Belarusian',
      bn: 'Bengali',
      bs: 'Bosnian',
      bg: 'Bulgarian',
      ca: 'Catalan',
      zh: 'Chinese',
      co: 'Corsican',
      hr: 'Croatian',
      cs: 'Czech',
      da: 'Danish',
      nl: 'Dutch',
      EN: 'English',
      et: 'Estonian',
      fi: 'Finnish',
      fr: 'French',
      fy: 'Frisian',
      ka: 'Georgian',
      de: 'German',
      el: 'Greek',
      hi: 'Hindi',
      hu: 'Hungarian',
      is: 'Icelandic',
      id: 'Indonesian',
      it: 'Italian',
      ja: 'Japanese',
      jw: 'Javanese',
      ko: 'Korean',
      ku: 'Kurdish',
      ky: 'Kyrgyz',
      lo: 'Lao',
      lv: 'Latvian',
      lt: 'Lithuanian',
      lb: 'Luxembourgish',
      mk: 'Macedonian',
      mg: 'Malagasy',
      ms: 'Malay',
      ml: 'Malayalam',
      mt: 'Maltese',
      mi: 'Maori',
      mn: 'Mongolian',
      my: 'Myanmar',
      ne: 'Nepali',
      no: 'Norwegian',
      ps: 'Pashto',
      fa: 'Persian',
      pl: 'Polish',
      pt: 'Portuguese',
      pa: 'Punjabi',
      ro: 'Romanian',
      ru: 'Russian',
      sm: 'Samoan',
      sr: 'Serbian',
      sk: 'Slovak',
      sl: 'Slovenian',
      so: 'Somali',
      es: 'Spanish',
      su: 'Sundanese',
      sw: 'Swahili',
      sv: 'Swedish',
      tl: 'Tagalog',
      TH: 'Thai',
      tr: 'Turkish',
      uk: 'Ukrainian',
      uz: 'Uzbek',
      vi: 'Vietnamese'
    }
  }
};

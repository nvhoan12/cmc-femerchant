// Language
const getFirstBrowserLanguage = function () {
    let nav = window.navigator
    let browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage']
    let i
    let language
    // support for HTML 5.1 "navigator.languages"
    if (Array.isArray(nav.languages)) {
      for (i = 0; i < nav.languages.length; i++) {
        language = nav.languages[i];
        if (language && language.length) {
          return language.replace(/-[a-zA-Z]+/g, '');
        }
      }
    }
    // support for other well known properties in browsers
    for (i = 0; i < browserLanguagePropertyKeys.length; i++) {
      language = nav[browserLanguagePropertyKeys[i]];
      if (language && language.length) {
        return language.replace(/-[a-zA-Z]+/g, '');
      }
    }
    return null
}

const language_list = [{
	value: 'TH',
	text: 'ไทย',
}, {
	value: 'EN',
	text: 'English',
}]
let current_language 
let language_string

if (localStorage.getItem("language") !== null) {
	const language = localStorage.getItem("language")
	current_language = language_list.find(item => item.value === language) || language_list[0]
} else {
	language_string = getFirstBrowserLanguage().toUpperCase()
	current_language = language_string
		? language_list.find(item => item.value === language_string) || language_list[0]
		: language_list[0]
	localStorage.setItem("language", current_language.value)
}

$('#landing #switch-language').text(current_language.text)
language_list.forEach(language => {
	const language_ele = $(`<a class="dropdown-item language-item" value="${language.value}"></a>`).text(language.text)
	$('#landing #language-menu').append(language_ele)
	language_ele.click(function() {
		language_string = $(this).attr('value')
		current_language = language_list.find(item => item.value === language_string)
		localStorage.setItem("language", current_language.value)
		window.location.reload()
	})
});


// translation
$.getJSON('/local/landing-page/translation/' + current_language.value.toLowerCase() + '.json', function (data) {
	const translate_obj = data
	$('#landing translate').each(function() {
		$(this).replaceWith(function() {
			const key = $(this).attr('data-text')
			return translate_obj[key] || ''
		})
	})
})


$(window).on('load', function() {
	const landing = $("#landing");
	landing.css("display", "block");
	
	//  load Bootstrap js
	if(!/hide/g.test(landing.attr('class'))) {
		console.log("loading Bootstrap js")
		landing.append('<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"><\/script>')
		landing.append('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"><\/script>')
	}
})

if(current_language.value === 'EN') {
	$('#what-is-btz-text-en').css("display", "initial");
	$('#what-is-btz-text-en-static').css("display", "initial");
}

if(current_language.value === 'TH') {
	$('#what-is-btz-text-th').css("display", "initial");
	$('#what-is-btz-text-th-static').css("display", "initial");
}

// =============== kyc-levels ===============
const kyc_levels_reset = function() {
	$('#kyc-levels-1-content').css("display", "none");
	$('#kyc-levels-2-content').css("display", "none");
	$('#kyc-levels-3-content').css("display", "none");
	$('#kyc-levels-1-tab').removeClass('active')
	$('#kyc-levels-2-tab').removeClass('active')
	$('#kyc-levels-3-tab').removeClass('active')
}

const reader_levels_1 = function() {
	kyc_levels_reset()
	$('#kyc-levels-1-content').css("display", "block");
	$('#kyc-levels-1-tab').addClass('active')
}

const reader_levels_2 = function() {
	kyc_levels_reset()
	$('#kyc-levels-2-content').css("display", "block");
	$('#kyc-levels-2-tab').addClass('active')
}

const reader_levels_3 = function() {
	kyc_levels_reset()
	$('#kyc-levels-3-content').css("display", "block");
	$('#kyc-levels-3-tab').addClass('active')
}

$('#kyc-levels-1-tab').click(reader_levels_1)
$('#kyc-levels-2-tab').click(reader_levels_2)
$('#kyc-levels-3-tab').click(reader_levels_3)
